/**
 * (c) 2015 FAO / UN (project: vrmf-psm-ce-model)
 */
package org.fao.fi.vrmf.psm.logbook.test.model;

import org.junit.Test;
import org.fao.fi.vrmf.psm.logbook.model.utils.CoordinatesHelper;
import org.junit.Assert;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 16, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 16, 2015
 */
public class TestCoordinatesHelper {
	@Test
	public void testWrongCoordinates1() {
		Assert.assertFalse(CoordinatesHelper.areCorrect("FOO;BAZ"));
	}
	
	@Test
	public void testWrongCoordinates2() {
		Assert.assertFalse(CoordinatesHelper.areCorrect("88°91'33''N;BAZ"));
	}
	
	@Test
	public void testWrongCoordinates3() {
		Assert.assertFalse(CoordinatesHelper.areCorrect("FOO;123°45'55''E"));
	}
	
	@Test
	public void testWrongCoordinates4() {
		Assert.assertFalse(CoordinatesHelper.areCorrect("81°22'12''A;123°45'55''E"));
	}
	
	@Test
	public void testWrongCoordinates5() {
		Assert.assertFalse(CoordinatesHelper.areCorrect("81°22'12''N;123°45'55''F"));
	}
	
	@Test
	public void testWrongCoordinates6() {
		Assert.assertFalse(CoordinatesHelper.areCorrect("081°22'12''N;123°45'55''W"));
	}
	
	@Test
	public void testWrongCoordinates7() {
		Assert.assertFalse(CoordinatesHelper.areCorrect("81°22'12''N;223°45'55''W"));
	}
	
	@Test
	public void testCorrectCoordinates() {
		Assert.assertTrue(CoordinatesHelper.areCorrect("81°22'12''N;123°45'55''E"));
	}
	
	@Test
	public void testExtractCorrectCoordinates1() {
		String coordinates = "81°22'12''N;123°45'55''E";
		
		double latitude = CoordinatesHelper.extractLatitude(coordinates);
		double longitude = CoordinatesHelper.extractLongitude(coordinates);
		
		Assert.assertEquals(81.36999999999999, latitude, 0.0001);
		Assert.assertEquals(123.76527777777778, longitude, 0.0001);
	}
	
	@Test
	public void testExtractCorrectCoordinates2() {
		String coordinates = "81°22'12''S;123°45'55''E";
		
		double latitude = CoordinatesHelper.extractLatitude(coordinates);
		double longitude = CoordinatesHelper.extractLongitude(coordinates);
		
		Assert.assertEquals(-81.36999999999999, latitude, 0.0001);
		Assert.assertEquals(123.76527777777778, longitude, 0.0001);
	}
	
	@Test
	public void testExtractCorrectCoordinates3() {
		String coordinates = "81°22'12''N;123°45'55''W";
		
		double latitude = CoordinatesHelper.extractLatitude(coordinates);
		double longitude = CoordinatesHelper.extractLongitude(coordinates);
		
		Assert.assertEquals(81.36999999999999, latitude, 0.0001);
		Assert.assertEquals(-123.76527777777778, longitude, 0.0001);
	}
	
	@Test
	public void testExtractCorrectCoordinates4() {
		String coordinates = "81°22'12''S;123°45'55''W";
		
		double latitude = CoordinatesHelper.extractLatitude(coordinates);
		double longitude = CoordinatesHelper.extractLongitude(coordinates);
		
		Assert.assertEquals(-81.36999999999999, latitude, 0.0001);
		Assert.assertEquals(-123.76527777777778, longitude, 0.0001);
	}
	
	@Test
	public void testExtractCorrectCoordinates5() {
		String coordinates = "81°22'12''S;123°45'55''W";
		
		double[] coords = CoordinatesHelper.extractCoordinates(coordinates);
		
		Assert.assertEquals(-81.36999999999999, coords[0], 0.0001);
		Assert.assertEquals(-123.76527777777778, coords[1], 0.0001);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testExtractWrongCoordinates1() {
		CoordinatesHelper.extractLatitude("98°81'31''S;123°45'33''W");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testExtractWrongCoordinates2() {
		CoordinatesHelper.extractLongitude("88°81'31''S;193°45'33''W");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testExtractWrongCoordinates3() {
		CoordinatesHelper.extractLatitude("98°81'31''N;123°45'33''W");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testExtractWrongCoordinates4() {
		CoordinatesHelper.extractLongitude("88°81'31''N;193°45'33''E");
	}
	
	@Test
	public void testQuadrantExtractionLatitudeN() {
		Assert.assertEquals("N", CoordinatesHelper.extractQuadrant("89°35'44''N"));
	}
	
	@Test
	public void testQuadrantExtractionLatitudeS() {
		Assert.assertEquals("S", CoordinatesHelper.extractQuadrant("89°35'44''S"));
	}
	
	@Test
	public void testQuadrantExtractionLongitudeE() {
		Assert.assertEquals("E", CoordinatesHelper.extractQuadrant("119°35'44''E"));
	}
	
	@Test
	public void testQuadrantExtractionLongitudeW() {
		Assert.assertEquals("W", CoordinatesHelper.extractQuadrant("119°35'44''W"));
	}

	@Test
	public void testDegreesExtractionWrongLatitude() {
		Assert.assertEquals(99, CoordinatesHelper.extractDegrees("99°35'44''N"));
	}

	@Test
	public void testDegreesExtractionLatitude() {
		Assert.assertEquals(89, CoordinatesHelper.extractDegrees("89°35'44''N"));
	}

	@Test
	public void testMinutesExtractionLatitude() {
		Assert.assertEquals(35, CoordinatesHelper.extractMinutes("89°35'44''N"));
	}
	
	@Test
	public void testSecondsExtractionLatitude() {
		Assert.assertEquals(44, CoordinatesHelper.extractSeconds("89°35'44''N"));
	}
	
	public void testDegreesExtractionWrongLongitude() {
		Assert.assertEquals(199, CoordinatesHelper.extractDegrees("199°35'44''E"));
	}

	@Test
	public void testDegreesExtractionLongitude() {
		Assert.assertEquals(129, CoordinatesHelper.extractDegrees("129°35'44''E"));
	}
	
	@Test
	public void testMinutesExtractionLongitude() {
		Assert.assertEquals(35, CoordinatesHelper.extractMinutes("129°35'44''E"));
	}
	
	@Test
	public void testSecondsExtractionLongitude() {
		Assert.assertEquals(44, CoordinatesHelper.extractSeconds("129°35'44''E"));
	}
}
