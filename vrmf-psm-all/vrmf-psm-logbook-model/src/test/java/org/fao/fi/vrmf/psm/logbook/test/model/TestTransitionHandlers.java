/**
 * (c) 2015 FAO / UN (project: vrmf-psm-ce-model)
 */
package org.fao.fi.vrmf.psm.logbook.test.model;

import java.util.ArrayList;
import java.util.Date;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.support.DefaultStateTransitionHandler;
import org.fao.fi.vrmf.psm.logbook.model.support.SeinerStateTransitionHandler;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 20, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 20, 2015
 */
public class TestTransitionHandlers {
	static Event event(EventType type) {
		return 
			Event.builder().
			type(type).
			startDate(new Date()).
			coordinatesAtStart("FOO;BAR").
			endDate(new Date()).
			coordinatesAtEnd("BAR;FOO").
		build();
	}
	@Test
	public void testDefaultOK() {
		Logsheet l = new Logsheet();
		l.setEvents(new ArrayList<Event>());
		l.setTransitionHandler(new DefaultStateTransitionHandler());
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.CLEANING));
		l.addEvent(event(EventType.CLEANING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.FISHING));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.FISHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		
		Assert.assertTrue(l.getTransitionHandler().isCompleted());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testDefaultNOK() {
		Logsheet l = new Logsheet();
		l.setEvents(new ArrayList<Event>());
		l.setTransitionHandler(new DefaultStateTransitionHandler());
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.CLEANING));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.FISHING));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.FISHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
	}
	
	@Test(expected=IllegalStateException.class)
	public void testDefaultNOK2() {
		Logsheet l = new Logsheet();
		l.setEvents(new ArrayList<Event>());
		l.setTransitionHandler(new DefaultStateTransitionHandler());
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.CLEANING));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.FISHING));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.FISHING));
		l.addEvent(event(EventType.SEARCHING));
		
		Assert.assertFalse(l.getTransitionHandler().isCompleted());
	}
	
	@Test
	public void testOtherOK() {
		Logsheet l = new Logsheet();
		l.setEvents(new ArrayList<Event>());
		l.setTransitionHandler(new SeinerStateTransitionHandler());
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.CLEANING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SETTING));
		l.addEvent(event(EventType.SETTING));
		l.addEvent(event(EventType.RETRIEVING));
		l.addEvent(event(EventType.RETRIEVING));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		
		Assert.assertTrue(l.getTransitionHandler().isCompleted());
	}
	
	@Test
	public void testOtherOK2() {
		Logsheet l = new Logsheet();
		l.setEvents(new ArrayList<Event>());
		l.setTransitionHandler(new SeinerStateTransitionHandler());
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.CLEANING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SETTING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SETTING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.RETRIEVING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.RETRIEVING));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		
		Assert.assertTrue(l.getTransitionHandler().isCompleted());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testOtherNOK() {
		Logsheet l = new Logsheet();
		l.setEvents(new ArrayList<Event>());
		l.setTransitionHandler(new SeinerStateTransitionHandler());
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.CLEANING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SETTING));
		l.addEvent(event(EventType.FISHING));
		l.addEvent(event(EventType.RETRIEVING));
		l.addEvent(event(EventType.TRANSIT));
	}
	
	@Test
	public void testOtherNOK2() {
		Logsheet l = new Logsheet();
		l.setEvents(new ArrayList<Event>());
		l.setTransitionHandler(new SeinerStateTransitionHandler());
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.BAD_WEATHER));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.CLEANING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.SEARCHING));
		l.addEvent(event(EventType.SETTING));
		l.addEvent(event(EventType.SETTING));
		l.addEvent(event(EventType.RETRIEVING));
		l.addEvent(event(EventType.RETRIEVING));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.TRANSIT));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.TRANSSHIPPING));
		l.addEvent(event(EventType.SEARCHING));
		
		Assert.assertFalse(l.getTransitionHandler().isCompleted());
	}
}
