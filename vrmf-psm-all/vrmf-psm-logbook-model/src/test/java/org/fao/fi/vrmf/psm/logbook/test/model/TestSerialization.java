/**
 * (c) 2015 FAO / UN (project: vrmf-psm-ce-model)
 */
package org.fao.fi.vrmf.psm.logbook.test.model;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;
import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchAndEffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.TransshipmentEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.CanneryUnloadingData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.VesselUnloadingData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DiscardReason;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.PowerUnit;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.VesselType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 14, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 14, 2015
 */
public class TestSerialization {
	private List<Catches> getCatchesMock(int len) {
		List<Catches> catches = new ArrayList<Catches>();
		
		Catches c = null;
		
		for(;len>=0;len--) {
			c = new Catches();
			c.setQuantity(Math.random() * 666);
			c.setQuantityType(len % 2 == 0 ? QuantityType.NUMBER : QuantityType.METRIC_TONS);
			c.setSpecies(Species.builder().id(111 + ( len % 2 ) * 222).scientificName(len % 2 == 0 ? "Foobazzis cuvieri" : "Barfoozzis johnstoni").localName(len % 2 == 0 ? "Foofish" : "Barfish").build());
			
			catches.add(c);
		}
		
		return catches;
	}
	
	private VesselData getVesselDataMock() {
		return VesselData.builder().
			id(69).
			name("Foo of the Bars").
			ircs("FOOBAR").
			registrationNumber("FOORNO").
			registrationCountry(Country.builder().id(666).name("Loucypheria").officialName("The eponymous monarchy of Loucypheria").iso2Code("LF").iso3Code("LCF").build()).
			type(VesselType.LINER).
			fishingCompany("FOO Inc.").
			loa(999.0).
			gt(666.0).
			mainEnginePowerUnit(PowerUnit.KW).
			mainEnginePower(654321.0).
		build();
	}
	
	private VesselData getVesselDataMock2() {
		return VesselData.builder().
			id(96).
			name("Bar of the Foos").
			ircs("FOOBAR").
			registrationNumber("BARNO").
			registrationCountry(Country.builder().id(666).name("Loucypheria").officialName("The eponymous monarchy of Loucypheria").iso2Code("LF").iso3Code("LCF").build()).
			type(VesselType.LINER).
			fishingCompany("BAR Inc.").
			loa(666.0).
			gt(999.0).
			mainEnginePowerUnit(PowerUnit.KW).
			mainEnginePower(123456.0).
		build();
	}
	
	private Logsheet addLogsheetMock(Logbook logbook, VesselData vessel) {
		Logsheet s = logbook.addLogsheet(vessel);
		
		Calendar start = Calendar.getInstance();
		start.set(Calendar.MONTH, Calendar.JANUARY);
		start.set(Calendar.DAY_OF_MONTH, 1);
		start.set(Calendar.HOUR_OF_DAY, 0);
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);
		
		Calendar end = Calendar.getInstance();
		end.setTimeInMillis(start.getTimeInMillis());
		for(int i=0; i<Math.round(Math.random() * 2) + 1; i++) { end.roll(Calendar.HOUR_OF_DAY, true); };
		
		s.setCaptainName("Nemo");
		s.setCrewSize(15);
		s.setDepartureDate(start.getTime());
		s.setDeparturePort(Port.builder().id(1).name("Port Harbour").coordinates("00°00'00''N;000°00'00''E").country(Country.builder().id(666).name("Loucypheria").officialName("The eponymous monarchy of Loucypheria").iso2Code("LF").iso3Code("LCF").build()).build());
		s.setFishOnboardAtStart(Math.random() * 1000);
		s.setFishOnboardQuantityAtStart(QuantityType.METRIC_TONS);
		s.setFishOnboardAfterUnloading(Math.random() * 500);
		s.setFishOnboardQuantityAfterUnloading(QuantityType.KILOGRAMS);
		Event e = Event.builder().type(EventType.TRANSIT).coordinatesAtStart("00°00'00''N;000°00'00''E").startDate(start.getTime()).coordinatesAtEnd("10°00'00''N;100°00'00''E").endDate(end.getTime()).build();
		
		s.addEvent(e);
		
		start = Calendar.getInstance();
		start.setTimeInMillis(end.getTimeInMillis());
		
		end = Calendar.getInstance();
		end.setTimeInMillis(start.getTimeInMillis());
		for(int i=0; i<Math.round(Math.random() * 2) + 1; i++) { end.roll(Calendar.HOUR_OF_DAY, true); };
		
		e = Event.builder().type(EventType.SEARCHING).coordinatesAtStart("10°00'00''N;100°00'00''E").startDate(start.getTime()).coordinatesAtEnd("20°00'00''N;110°00'00''E").endDate(end.getTime()).build();
		
		s.addEvent(e);
		
		start = Calendar.getInstance();
		start.setTimeInMillis(end.getTimeInMillis());
		
		end = Calendar.getInstance();
		end.setTimeInMillis(start.getTimeInMillis());
		for(int i=0; i<Math.round(Math.random() * 2) + 1; i++) { end.roll(Calendar.HOUR_OF_DAY, true); };
		
		CatchAndEffortEvent e2 = new CatchAndEffortEvent();
		e2.setType(EventType.FISHING);
		e2.setCoordinatesAtStart("20°00'00''N;110°00'00''E");
		e2.setStartDate(start.getTime());
		e2.setCoordinatesAtEnd("30°00'00''N;120°00'00''E");
		e2.setEndDate(end.getTime());
		e2.setEfforts(Arrays.asList(new Effort[] {
			Effort.builder().effortType(EffortType.NUMBER_OF_HOOKS).effortValue(Math.random() * 10000).build(),
			Effort.builder().effortType(EffortType.NUMBER_OF_LINES).effortValue(Math.random() * 4000).build()
		}));
		
		e2.setCatches(getCatchesMock(3));
		
		Catches c2 = new Catches();
		c2.setQuantity(Math.random() * 333);
		c2.setQuantityType(QuantityType.METRIC_TONS);
		c2.setSpecies(Species.builder().id(222).scientificName("Barfoozzis latimeri").localName("Barfish").build());
		
		e2.getCatches().add(c2);

		Discards d = new Discards();
		d.setQuantity(Math.random() * 111);
		d.setQuantityType(QuantityType.KILOGRAMS);
		d.setSpecies(Species.builder().id(333).scientificName("Snafuzzis johnstoni").localName("Snafish").build());
		d.setReason(DiscardReason.VESSEL_FULL);
		
		e2.getDiscards().add(d);
		
		s.addEvent(e2);
		
		start = Calendar.getInstance();
		start.setTimeInMillis(end.getTimeInMillis());
		
		end = Calendar.getInstance();
		end.setTimeInMillis(start.getTimeInMillis());
		for(int i=0; i<Math.round(Math.random() * 2) + 1; i++) { end.roll(Calendar.HOUR_OF_DAY, true); };
		
		e = Event.builder().type(EventType.TRANSIT).coordinatesAtStart("21°00'00''N;111°00'00''E").startDate(start.getTime()).coordinatesAtEnd("22°00'00''N;112°00'00''E").endDate(end.getTime()).build();
		
		s.addEvent(e);

		start = Calendar.getInstance();
		start.setTimeInMillis(end.getTimeInMillis());
		
		s.setArrivalDate(start.getTime());
		s.setUnloadingPort(Port.builder().id(2).name("Port Akivuoy").coordinates("23°00'00''N;113°00'00''E").country(Country.builder().id(666).name("Loucypheria").officialName("The eponymous monarchy of Loucypheria").iso2Code("LF").iso3Code("LCF").build()).build());
		
		s.setComments("Lorem ipsum sit dolor amet");
		s.setCanneryUnloads(new ArrayList<CanneryUnloadingData>());
		s.setOtherVesselUnloads(new ArrayList<VesselUnloadingData>());
		
		CanneryUnloadingData u = new CanneryUnloadingData();
		u.setCatchesUnloaded(getCatchesMock(4));
		u.setCanneryName("Can I can this");
		
		s.getCanneryUnloads().add(u);

		VesselUnloadingData o = new VesselUnloadingData();
		o.setCatchesUnloaded(getCatchesMock(6));
		o.setVesselName("Collector's dream");
		o.setVesselRegistrationNumber("SNAFU");
		o.setDestination("Neverland");
		
		s.getOtherVesselUnloads().add(o);
		
		o = new VesselUnloadingData();
		o.setCatchesUnloaded(getCatchesMock(3));
		o.setVesselName("Collector's nightmare");
		o.setVesselRegistrationNumber("FUD");
		o.setDestination("Neverneverland");
		
		s.getOtherVesselUnloads().add(o);
		
		return s.completeLogsheet();
	}
	
	private Logbook getLogbookMock() {
		Logbook l = new Logbook();
		l.setYear(2015);
		l.setVersion("1.0.0");
		
		addLogsheetMock(l, getVesselDataMock());
		addLogsheetMock(l, getVesselDataMock2());
		addLogsheetMock(l, getVesselDataMock());
		
		return l; //return l.deriveEfforts();
	}
	
	@Test
	public void testSerialization() throws Exception {
		Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(getLogbookMock(), System.out);
	}
	
	@Test
	public void testSerialization2() throws Exception {
		Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(getLogbookMock(), System.out);
	}
	
	@Test
	public void testRoundtrip() throws Exception {
		Logbook l = getLogbookMock();
		
		JAXBContext ctx = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, CatchEvent.class, Event.class);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		ctx.createMarshaller().marshal(l, baos);
		
		Logbook l2 = (Logbook)ctx.createUnmarshaller().unmarshal(new StringReader(new String(baos.toByteArray(), "UTF-8")));
		
		Assert.assertEquals(l, l2);
		Assert.assertTrue(l2.getLogsheets().get(0).getEvents().get(2).getClass().getName().equals(CatchAndEffortEvent.class.getName()));
	}
	
	@Test
	public void testJSONSerialization() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
		mapper.getDeserializationConfig().withAnnotationIntrospector(introspector);
		mapper.getSerializationConfig().withAnnotationIntrospector(introspector);
		
		mapper.writerWithDefaultPrettyPrinter().writeValue(System.out, getLogbookMock());
	}
	
	@Test
	public void testJSONRoundtrip() throws Exception {
		Logbook l = getLogbookMock();
		
		ObjectMapper mapper = new ObjectMapper();
		
		AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
		mapper.getDeserializationConfig().withAnnotationIntrospector(introspector);
		mapper.getSerializationConfig().withAnnotationIntrospector(introspector);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		mapper.writeValue(baos, l);
		
		Logbook l2 = mapper.readValue(baos.toByteArray(), Logbook.class);
		
		Assert.assertEquals(l, l2);
		Assert.assertTrue(l2.getLogsheets().get(0).getEvents().get(2).getClass().getName().equals(CatchAndEffortEvent.class.getName()));
	}
}
