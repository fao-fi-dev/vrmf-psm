/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.events.spi;

import java.util.List;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 23, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 23, 2016
 */
public interface CatchesRecord {
	List<Catches> getCatches();
	void setCatches(List<Catches> catches);
	
	List<Discards> getDiscards();
	void setDiscards(List<Discards> discards);
}
