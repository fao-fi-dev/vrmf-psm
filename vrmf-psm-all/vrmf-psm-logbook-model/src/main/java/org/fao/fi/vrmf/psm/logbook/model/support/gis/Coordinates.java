/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.support.gis;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 21, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 21, 2015
 */
@EqualsAndHashCode @AllArgsConstructor
public class Coordinates implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1710593752034769047L;

	@Getter private Latitude latitude;
	@Getter private Longitude longitude;
	
	public Coordinates(String latitude, String longitude) {
		this.latitude = new Latitude(latitude);
		this.longitude = new Longitude(longitude);
	}
}
