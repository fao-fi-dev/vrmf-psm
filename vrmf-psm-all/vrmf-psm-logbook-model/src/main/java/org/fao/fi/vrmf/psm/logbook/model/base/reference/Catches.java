/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.fao.fi.vrmf.psm.logbook.model.base.BasicData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;
import org.fao.fi.vrmf.psm.logbook.model.spi.Checkable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Structured;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity @Inheritance(strategy=InheritanceType.JOINED)
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true) @Builder
public class Catches extends BasicData implements Checkable, Structured<Catches> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7999228273284166986L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute private Integer id;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@Getter @Setter @XmlElement(required=true) private Species species;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private double quantity;
	@Enumerated(EnumType.STRING) @Getter @Setter @XmlElement(required=true) @Column(nullable=false) private QuantityType quantityType;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public Catches cleanup() {
		return isNew() ? null : this; 
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#isCorrectlySet()
	 */
	@JsonIgnore
	public boolean isCorrectlySet() {
		return !CheckResultHelper.hasErrors(checkCorrectness());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
		
		if(species == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The species is not set").cause(this).build());
		if(Double.compare(quantity, 0D) < 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The catch quantity is negative").cause(this).build());
		if(Double.compare(quantity, 0D) == 0) results.add(CheckResult.builder().type(CheckType.WARNING).message("The catch quantity is zero").cause(this).build());
		if(quantityType == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The quantity type is not set").cause(this).build());
		
		return results; 
	}
	
	public CatchesKey key() {
		return new CatchesKey(this);
	}
	
	@EqualsAndHashCode @AllArgsConstructor @NoArgsConstructor
	public class CatchesKey implements Serializable {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 3846221055623291395L;
		
		@Getter @Setter private Species species;
		@Getter @Setter private QuantityType quantityType;
		
		public CatchesKey(Catches catches) {
			this(catches == null ? null : catches.getSpecies(), catches == null ? null : catches.getQuantityType());
		}
	}
}