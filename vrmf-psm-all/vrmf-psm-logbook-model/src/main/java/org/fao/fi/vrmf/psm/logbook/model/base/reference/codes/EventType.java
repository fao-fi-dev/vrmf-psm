/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference.codes;

import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlEnum;

import org.fao.fi.vrmf.psm.logbook.model.spi.CodedEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@XmlEnum
@ToString
public enum EventType implements CodedEntity {
//	IN_PORT("IP", "In port"),
	TRANSIT("TR", "Transit"),
	SEARCHING("SR", "Searching"),
	SETTING("SE", "Setting"),
	RETRIEVING("RE", "Retrieving"),
	FISHING("FI", "Fishing"),
	TRANSSHIPPING("TS", "Transshipping"),
	BAD_WEATHER("BW", "Bad weather"),
	CLEANING("CL", "Cleaning");
	
	@Id @Getter @Setter @XmlAttribute(required=true) private String code;
	@Getter @Setter @XmlAttribute(required=true) private String name;
	
	private EventType(String code, String name) {
		this.code = code;
		this.name = name;
	}
}
