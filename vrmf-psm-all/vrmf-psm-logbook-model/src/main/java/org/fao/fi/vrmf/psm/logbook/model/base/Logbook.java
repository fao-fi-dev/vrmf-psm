/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches.CatchesKey;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.DerivedEffort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DerivedEffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;
import org.fao.fi.vrmf.psm.logbook.model.spi.Checkable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Completable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Structured;
import org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD) 
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true) @Builder
public class Logbook extends BasicData implements Checkable, Completable, Structured<Logbook> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3117914799077308813L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute protected Integer id;

	@Getter @Setter @XmlAttribute(required=true) @Column(nullable=false) private String version;
	@Getter @Setter @XmlAttribute(required=true) @Column(nullable=false) private int year;
	
	@Getter @Setter @XmlAttribute(required=true) @Column(nullable=false) private Date creationDate = new Date();
	
	@Getter @Setter @XmlAttribute(required=true) @Column(nullable=false) private boolean completed;
	@Getter @Setter @XmlAttribute(required=true) private Date completionDate;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true) @OrderBy("id") @Fetch(FetchMode.SUBSELECT)
	@Getter @Setter @XmlElementWrapper(name="logsheets") @XmlElement(name="logsheet") private List<Logsheet> logsheets;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true) @Fetch(FetchMode.SUBSELECT)
	@Getter @Setter @XmlElementWrapper(name="derivedEfforts") @XmlElement(name="derivedEffort") private List<DerivedEffort> derivedEfforts;
	
	@XmlElementWrapper(name="uniqueEfforts") @XmlElement(name="effort")
	@Transient @Getter @Setter private List<Effort> uniqueEfforts;
	
	@XmlElementWrapper(name="uniqueCatches") @XmlElement(name="catches")
	@Transient @Getter @Setter private List<Catches> uniqueCatches;
	
	@XmlElementWrapper(name="uniqueDiscards") @XmlElement(name="discard")
	@Transient @Getter @Setter private List<Discards> uniqueDiscards;
	
	@Transient @JsonIgnore
	public boolean isDirty() {
		boolean isDirty = dirty;
		
		if(logsheets != null)
			for(Logsheet in : logsheets)
				isDirty &= in != null && in.isDirty();
				
		return isDirty;
	}
	
	@Transient @XmlTransient @JsonIgnore
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
		
		if(!dirty) {
			if(logsheets != null)
				for(Logsheet in : logsheets)
					if(in != null) in.setDirty(false);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public Logbook cleanup() {
		if(isNew()) return null;
		
		if(logsheets != null) {
			Logsheet current;
			Iterator<Logsheet> logsheet = logsheets.iterator();
			
			while(logsheet.hasNext()) {
				current = logsheet.next();
				
				if(current != null && current.cleanup() == null)
					logsheet.remove();
			}
		}

		return this;
	}
	
	public boolean canBeCompleted() {
		boolean logsheetsCompleted = logsheets != null && !logsheets.isEmpty();
		
		if(logsheetsCompleted) {
			for(Logsheet in : logsheets)
				logsheetsCompleted &= in.isCompleted();
		}
		
		return logsheetsCompleted && !completed;
	}
	
	public Logbook completeLogbook() {
		if(completed) throw new UnsupportedOperationException("The logbook was already marked as completed");
		if(!isCorrectlySet()) throw new IllegalStateException("The logbook cannot be completed as it is not correctly filled");
		
		boolean logsheetsCompleted = logsheets != null && !logsheets.isEmpty();
		
		if(logsheetsCompleted) {
			for(Logsheet in : logsheets)
				logsheetsCompleted &= in.isCompleted();
		}
		
		completed = logsheetsCompleted;
		
		if(!completed) throw new UnsupportedOperationException("Cannot mark logbook as completed: some of its logsheets are not completed yet");
		
		completionDate = new Date();
		
		return this;
	}
	
	public Logbook deriveEfforts() {
		derivedEfforts = derivedEfforts();
		
		return this;
	}
	
	public List<DerivedEffort> derivedEfforts() {
		List<DerivedEffort> derivedEfforts = new ArrayList<DerivedEffort>();
		
		Map<DerivedEffortType, Double> efforts = new HashMap<DerivedEffortType, Double>();
		
		if(logsheets != null) {
			Set<Integer> vesselInvolved = new HashSet<Integer>();
			
			List<DerivedEffort> logsheetDerivedEfforts;
			
			for(Logsheet in : logsheets) {
				logsheetDerivedEfforts = in.deriveEfforts();
				
				if(logsheetDerivedEfforts != null) {
					DerivedEffortType current;
					for(DerivedEffort of : logsheetDerivedEfforts) {
						if(!efforts.containsKey(current = of.getEffortType())) {
							efforts.put(current, 0D);
						}
						
						if(DerivedEffortType.NUMBER_OF_BOATS.equals(of.getEffortType())) {
							if(!vesselInvolved.contains(in.getVesselData().getId())) {
								vesselInvolved.add(in.getVesselData().getId());
								
								efforts.put(current, efforts.get(current) + of.getEffortValue());
							}
						} else
							efforts.put(current, efforts.get(current) + of.getEffortValue());
					}
				}
			}
		}
		
		for(DerivedEffortType key : efforts.keySet()) 
			derivedEfforts.add(new DerivedEffort(null /*ID*/, key, efforts.get(key)));
		
		return derivedEfforts;
	}
	
	public Logsheet addLogsheet(VesselData vessel) {
		StateTransitionHandler handler = StateTransitionHandler.forType(vessel.getType());
		
		if(logsheets == null) logsheets = new ArrayList<Logsheet>();
		
		Logsheet logsheet = Logsheet.builder().vesselData(vessel).transitionHandler(handler).number(logsheets.size() + 1).events(new ArrayList<Event>()).build();
		
		logsheets.add(logsheet);
		
		return logsheet;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#isCorrectlySet()
	 */
	@Override
	@JsonIgnore public boolean isCorrectlySet() {
		return !CheckResultHelper.hasErrors(checkCorrectness());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
		
		if(version == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The logbook version is null").cause(this).build());
		if(year < 2000) results.add(CheckResult.builder().type(CheckType.ERROR).message("The logbook year is lower than 2000").cause(this).build());
		if(logsheets == null || logsheets.isEmpty()) results.add(CheckResult.builder().type(CheckType.ERROR).message("The logbook does not list any logsheet").cause(this).build());
		
		if(logsheets != null) {
			for(Logsheet in : logsheets)
				if(in != null) {
					int counter = 1;
					for(CheckResult of : in.checkCorrectness()) {
						of.setMessage("Logsheet #" + counter++ + " : " + of.getMessage());
						
						results.add(of);
					}
				}
		}
		
		return results;
	};
	
	public List<Effort> uniqueEfforts() {
		List<Effort> unique = new ArrayList<Effort>();
		
		Map<EffortType, Double> byType = new HashMap<EffortType, Double>();
		
		if(logsheets != null) {
			List<Effort> current = new ArrayList<Effort>();
			
			for(Logsheet in : logsheets) {
				current.addAll(in.uniqueEfforts());
			}
			
			for(Effort in : current) {
				if(!byType.containsKey(in.getEffortType())) {
					byType.put(in.getEffortType(), 0D);
				}
				
				byType.put(in.getEffortType(), byType.get(in.getEffortType()) + in.getEffortValue());
			}
		}

		for(EffortType in : byType.keySet()) {
			unique.add(Effort.builder().effortType(in).effortValue(byType.get(in)).build());
		}

		return unique;
	}
	
	public List<DerivedEffort> uniqueDerivedEfforts() {
		List<DerivedEffort> unique = new ArrayList<DerivedEffort>();
		
		List<DerivedEffort> all = derivedEfforts();
		
		Map<DerivedEffortType, Double> byType = new HashMap<DerivedEffortType, Double>();
		
		for(DerivedEffort in : all) {
			if(!byType.containsKey(in.getEffortType())) {
				byType.put(in.getEffortType(), 0D);
			}
			
			byType.put(in.getEffortType(), byType.get(in.getEffortType()) + in.getEffortValue());
		}
		
		for(DerivedEffortType in : byType.keySet()) {
			unique.add(DerivedEffort.builder().effortType(in).effortValue(byType.get(in)).build());
		}
			
		return unique;
	}
	
	public List<Catches> uniqueCatches() {
		List<Catches> unique = new ArrayList<Catches>();
	
		Map<CatchesKey, Double> bySpeciesAndUnit = new HashMap<CatchesKey, Double>();

		if(logsheets != null) {
			List<Catches> current = new ArrayList<Catches>();
			
			for(Logsheet in : logsheets) {
				current.addAll(in.uniqueCatches());
			}
			
			for(Catches in : current) {
				if(!bySpeciesAndUnit.containsKey(in.key())) {
					bySpeciesAndUnit.put(in.key(), 0D);
				}
				
				bySpeciesAndUnit.put(in.key(), bySpeciesAndUnit.get(in.key()) + in.getQuantity());
			}
		}
		
		for(CatchesKey in : bySpeciesAndUnit.keySet()) {
			unique.add(Catches.builder().species(in.getSpecies()).quantityType(in.getQuantityType()).quantity(bySpeciesAndUnit.get(in)).build());
		}	
			
		return unique;
	}
	
	public List<Discards> uniqueDiscards() {
		List<Discards> unique = new ArrayList<Discards>();
	
		Map<CatchesKey, Double> bySpeciesAndUnit = new HashMap<CatchesKey, Double>();

		if(logsheets != null) {
			List<Discards> current = new ArrayList<Discards>();
			
			for(Logsheet in : logsheets) {
				current.addAll(in.uniqueDiscards());
			}
			
			for(Discards in : current) {
				if(!bySpeciesAndUnit.containsKey(in.key())) {
					bySpeciesAndUnit.put(in.key(), 0D);
				}
				
				bySpeciesAndUnit.put(in.key(), bySpeciesAndUnit.get(in.key()) + in.getQuantity());
			}
		}
		
		Discards current;
		for(CatchesKey in : bySpeciesAndUnit.keySet()) {
			current = new Discards();
			current.setSpecies(in.getSpecies());
			current.setQuantityType(in.getQuantityType());
			current.setQuantity(bySpeciesAndUnit.get(in));

			unique.add(current);
		}
			
		return unique;
	}
	
	public String computeLogbookId() {
		return version + " | " + year + " | " + new SimpleDateFormat("yyyy/MM/dd").format(creationDate);
	}
}
