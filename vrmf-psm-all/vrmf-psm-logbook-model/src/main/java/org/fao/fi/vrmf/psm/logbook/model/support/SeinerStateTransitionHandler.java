/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.support;

import java.util.Arrays;
import java.util.List;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 20, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 20, 2015
 */
public class SeinerStateTransitionHandler extends StateTransitionHandler {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8431310021046729600L;
	
	private int numberOfSettings = 0;
	
	/**
	 * Class constructor
	 *
	 */
	public SeinerStateTransitionHandler() {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler#initialize(java.util.List)
	 */
	@Override
	public StateTransitionHandler initialize(List<Event> events) {
		numberOfSettings = 0;
		
		return super.initialize(events);
	}

	protected void transition(Event next) {
		if(state == null) {
			if(!next.getType().equals(EventType.TRANSIT)) throw new IllegalStateException("First transition must be towards " + EventType.TRANSIT);
			
			state = next;
		} else {
			if(!nextEvents().contains(next.getType())) illegalTransition(state, next);
			
			switch(state.getType()) {
				case SETTING:
					numberOfSettings++;
					break;
				case RETRIEVING:
					numberOfSettings--;
					break;
				default:
					break;
			}
			
			state = next;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler#nextEvents()
	 */
	@Override
	public List<EventType> nextEvents() {
		EventType[] nextEvents = new EventType[0];
		
		if(state == null) {
			nextEvents = new EventType[] { EventType.TRANSIT }; 
		} else {
			switch(state.getType()) {
				case TRANSIT:
					if(numberOfSettings > 0)
						nextEvents = new EventType[] { EventType.TRANSIT, EventType.TRANSSHIPPING, EventType.SEARCHING, EventType.RETRIEVING, EventType.CLEANING, EventType.BAD_WEATHER };
					else
						nextEvents = new EventType[] { EventType.TRANSIT, EventType.TRANSSHIPPING, EventType.SEARCHING, EventType.CLEANING, EventType.BAD_WEATHER };
					break;
				case SEARCHING:
					nextEvents = new EventType[] { EventType.SEARCHING, EventType.TRANSIT, EventType.CLEANING, EventType.BAD_WEATHER, EventType.SETTING }; break;
				case BAD_WEATHER:
					nextEvents = new EventType[] { EventType.BAD_WEATHER, EventType.TRANSIT, EventType.SEARCHING, EventType.CLEANING }; break;
				case CLEANING:
					nextEvents = new EventType[] { EventType.CLEANING, EventType.TRANSIT, EventType.SEARCHING, EventType.BAD_WEATHER }; break;
				case SETTING:
					nextEvents = new EventType[] { EventType.SETTING, EventType.RETRIEVING, EventType.TRANSIT }; break;
				case RETRIEVING:
					nextEvents = new EventType[] { EventType.RETRIEVING, EventType.TRANSSHIPPING, EventType.TRANSIT, EventType.SEARCHING, EventType.CLEANING, EventType.BAD_WEATHER }; break;
//				case FISHING:
//					nextEvents = new EventType[] { EventType.FISHING, EventType.TRANSSHIPPING, EventType.TRANSIT, EventType.SEARCHING, EventType.CLEANING, EventType.BAD_WEATHER }; break;
				case TRANSSHIPPING:
					nextEvents = new EventType[] { EventType.TRANSSHIPPING, EventType.TRANSIT, EventType.SEARCHING }; break;
				default:
					break;
			}
		}
		
		return Arrays.asList(nextEvents);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler#isCompleted()
	 */
	@Override
	public boolean isCompleted() {
		return EventType.TRANSIT.equals(state == null ? null : state.getType()) && numberOfSettings == 0;
	}
}
