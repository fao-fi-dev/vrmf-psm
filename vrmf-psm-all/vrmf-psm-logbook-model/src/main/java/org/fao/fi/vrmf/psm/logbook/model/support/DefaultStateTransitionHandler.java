/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.support;

import java.util.Arrays;
import java.util.List;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 20, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 20, 2015
 */
public class DefaultStateTransitionHandler extends StateTransitionHandler {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6801194575374011289L;

	/**
	 * Class constructor
	 *
	 */
	public DefaultStateTransitionHandler() {
	}
	
	protected void transition(Event next) {
		if(state == null) {
			if(!next.getType().equals(EventType.TRANSIT)) throw new IllegalStateException("First transition must be towards " + EventType.TRANSIT);
			
			state = next;
		} else {
			if(!nextEvents().contains(next.getType())) illegalTransition(state, next);
			
			state = next;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler#isCompleted()
	 */
	@Override
	public boolean isCompleted() {
		return EventType.TRANSIT.equals(state == null ? null :  state.getType());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler#nextEvents()
	 */
	@Override
	public List<EventType> nextEvents() {
		EventType[] nextEvents = new EventType[0];
		
		if(state == null) {
			nextEvents = new EventType[] { EventType.TRANSIT }; 
		} else {
			switch(state.getType()) {
				case TRANSIT:
					nextEvents = new EventType[] { EventType.TRANSIT, EventType.TRANSSHIPPING, EventType.SEARCHING, EventType.CLEANING, EventType.BAD_WEATHER }; break;
				case SEARCHING:
					nextEvents = new EventType[] { EventType.SEARCHING, EventType.TRANSSHIPPING, EventType.TRANSIT, EventType.CLEANING, EventType.BAD_WEATHER, EventType.FISHING }; break;
				case BAD_WEATHER:
					nextEvents = new EventType[] { EventType.BAD_WEATHER, EventType.CLEANING, EventType.TRANSIT, EventType.SEARCHING }; break;
				case CLEANING:
					nextEvents = new EventType[] { EventType.CLEANING, EventType.BAD_WEATHER, EventType.TRANSIT, EventType.SEARCHING }; break;
				case FISHING:
					nextEvents = new EventType[] { EventType.FISHING, EventType.TRANSSHIPPING, EventType.TRANSIT, EventType.SEARCHING, EventType.CLEANING, EventType.BAD_WEATHER }; break;
				case TRANSSHIPPING:
					nextEvents = new EventType[] { EventType.TRANSSHIPPING, EventType.SEARCHING, EventType.TRANSIT }; break;
				default:
					break;
			}
		}
		
		return Arrays.asList(nextEvents);
	}
}
