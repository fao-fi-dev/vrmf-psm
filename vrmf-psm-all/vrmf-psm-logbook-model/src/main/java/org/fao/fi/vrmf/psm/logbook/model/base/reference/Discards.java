/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DiscardReason;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true)
public class Discards extends Catches {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7999228273284166986L;
	
	@Enumerated(EnumType.STRING) @Getter @Setter @XmlElement(required=true) @Column(nullable=false) private DiscardReason reason;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches#isCorrectlySet()
	 */
	@Override
	public boolean isCorrectlySet() {
		return !CheckResultHelper.hasErrors(checkCorrectness());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = super.checkCorrectness();
		
		if(reason == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The discard reason is not set").cause(this).build());
		
		return results;
	}
}
