/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode @Builder
public class Species implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3674064399852060106L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute private Integer id;
	@Getter @Setter @XmlAttribute private String alpha3Code;
	@Getter @Setter @XmlElement private String scientificName;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String localName;
	
	static public Species OTHER = Species.builder().id(0).alpha3Code(null).scientificName(null).localName("Others").build();
	static public Species MIXED = Species.builder().id(-1).alpha3Code(null).scientificName(null).localName("Mixed").build();
}
