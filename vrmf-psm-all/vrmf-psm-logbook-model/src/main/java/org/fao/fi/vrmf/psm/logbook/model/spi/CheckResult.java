/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.spi;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 24, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 24, 2015
 */
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD) 
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode @Builder
public class CheckResult implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7323121287096886157L;
	
	@Getter @Setter @XmlAttribute private CheckType type;
	@Getter @Setter @XmlElement private String message;
	@Getter @Setter @XmlAnyElement private Object cause;
}