/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.events.spi;

import java.util.List;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 23, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 23, 2016
 */
public interface EffortsRecord {
	List<Effort> getEfforts();
	void setEfforts(List<Effort> efforts);
}
