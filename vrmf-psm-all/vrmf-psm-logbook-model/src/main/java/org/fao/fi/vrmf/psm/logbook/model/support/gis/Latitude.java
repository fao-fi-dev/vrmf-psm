/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.support.gis;

import java.io.Serializable;

import org.fao.fi.vrmf.psm.logbook.model.utils.CoordinatesHelper;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 21, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 21, 2015
 */
@EqualsAndHashCode
public class Latitude implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1760294769599047644L;
	
	@Getter private String value;
	
	public Latitude(String value) {
		if(CoordinatesHelper.isLatitudeCorrect(value)) this.value = value;
		else throw new IllegalArgumentException("Wrong latitude value " + value);
	}
	
	public double toDecimalCoordinate() {
		return CoordinatesHelper.latitudeFromString(value);
	}
	
	static public Latitude fromCoordinates(String coordinates) {
		return new Latitude(CoordinatesHelper.latitude(coordinates));
	}
}
