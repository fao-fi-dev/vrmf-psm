/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 26, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 26, 2015
 */
final public class CleaningHelper {
	private CleaningHelper() { }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static public <O> O clean(O toClean) {
		if(toClean == null) return null;
		
		if(toClean instanceof String) {
			String asString = ((String)toClean).trim();
			
			if("".equals(asString))
				return (O)null;
			
			return (O)asString;
		}
		
		if(toClean instanceof Number) {
			Number asNumber = ((Number)toClean);
			
			if(Double.compare(asNumber.doubleValue(), 0D) < 0)
				return null;
			
			return (O)asNumber;
		}
		
		if(toClean instanceof Collection) {
			Collection asCollection = (Collection)toClean;
			
			Iterator collection = asCollection.iterator();
			
			Object element;
			while(collection.hasNext()) {
				element = clean(collection.next());
				
				if(element == null) collection.remove();
			}
				
			return toClean;
		}
		
		Class<?> clazz = toClean.getClass();
		
		List<Field> fields = new ArrayList<Field>();
			
		int modifiers;
		while(clazz != null) {
			for(Field in : clazz.getDeclaredFields()) {
				modifiers = in.getModifiers();
				
				if((modifiers & Modifier.FINAL) != Modifier.FINAL &&
				   (modifiers & Modifier.STATIC) != Modifier.STATIC) {
					in.setAccessible(true);
					fields.add(in);
				}
			}
			
			clazz = clazz.getSuperclass();
		} 
		
		for(Field in : fields) {
			try {
				in.set(toClean, clean(in.get(toClean)));
			} catch(Throwable t) {
				;
			}
		}

		return toClean;
	}
}
