/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.spi;

import java.io.Serializable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 26, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 26, 2015
 */
public interface Cleanable<ENTITY> extends Serializable {
	ENTITY clean();
}
