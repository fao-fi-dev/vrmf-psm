/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.events;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true)
public class TransshipmentEvent extends Event {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5045154363043911491L;
	
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String vesselName;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private Country vesselRegistrationCountry;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String vesselRegistrationNumber;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String destination;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
	@Getter @Setter @XmlElementWrapper(name="catches") @XmlElement(name="catch") private List<Catches> catches = new ArrayList<Catches>();
	
	public TransshipmentEvent(EventType type) {
		super(type);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public TransshipmentEvent cleanup() {
		if(isNew()) return null;
		
		if(catches != null) {
			Catches current;
			Iterator<Catches> catchez = catches.iterator();
			
			while(catchez.hasNext()) {
				current = catchez.next();
				
				if(current != null && current.cleanup() == null)
					catchez.remove();
			}
		}

		return this;
	}
	
	public boolean isDirty() {
		boolean isDirty = dirty;
		
		if(catches != null)
			for(Catches in : catches)
				isDirty &= in != null && in.isDirty();
	
		return isDirty;
	}
	
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
		
		if(!dirty) {
			if(catches != null)
				for(Catches in : catches)
					if(in != null) in.setDirty(false);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.base.Event#isEventCorrectlySet()
	 */
	@Override
	public boolean isCorrectlySet() {
		return 
			super.isCorrectlySet() &&
			vesselName != null && !vesselName.trim().equals("") && 
			vesselRegistrationNumber != null && !vesselRegistrationNumber.trim().equals("") && 
			destination != null && !destination.trim().equals("") &&
			checkCatches();
	}
	
	public boolean checkCatches() {
		if(catches == null || catches.isEmpty()) return true;
		
		for(Catches c : catches)
			if(!c.isCorrectlySet()) return false;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.base.events.Event#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = super.checkCorrectness();
		
		if(vesselName == null || vesselName.trim().equals("")) results.add(CheckResult.builder().type(CheckType.ERROR).message("The receiving vessel name is not set").build());
		if(vesselRegistrationCountry == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The receiving vessel registration country is not set").build());
		if(vesselRegistrationNumber == null || vesselRegistrationNumber.trim().equals("")) results.add(CheckResult.builder().type(CheckType.ERROR).message("The receiving vessel registration number is not set").build());
		if(destination == null || destination.trim().equals("")) results.add(CheckResult.builder().type(CheckType.ERROR).message("The receiving vessel destination is not set").build());
		
		return results;
	}
}