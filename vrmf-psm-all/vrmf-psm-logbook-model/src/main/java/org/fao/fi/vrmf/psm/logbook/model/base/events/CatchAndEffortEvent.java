/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.EffortsRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 23, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 23, 2016
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true)
public class CatchAndEffortEvent extends CatchEvent implements EffortsRecord {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4922733285301556353L;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
	@Getter @Setter @XmlElementWrapper(name="efforts") @XmlElement(name="effort") private List<Effort> efforts;

	public CatchAndEffortEvent(EventType type) {
		super(type);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public CatchAndEffortEvent cleanup() {
		if(isNew()) return null;
		
		super.cleanup();
		
		if(efforts != null) {
			Effort current;
			Iterator<Effort> effort = efforts.iterator();
			
			while(effort.hasNext()) {
				current = effort.next();
				
				if(current != null && current.cleanup() == null)
					effort.remove();
			}
		}

		return this;
	}
	
	public boolean isDirty() {
		boolean isDirty = super.isDirty();
		
		if(efforts != null)
			for(Effort in : efforts)
				isDirty &= in != null && in.isDirty();
				
		return isDirty;
	}
	
	public void setDirty(boolean dirty) {
		super.setDirty(dirty);
		if(!dirty) {
			if(efforts != null)
				for(Effort in : efforts)
					if(in != null) in.setDirty(false);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.base.Event#isEventCorrectlySet()
	 */
	@Override
	public boolean isCorrectlySet() {
		return 
			super.isCorrectlySet() &&
			checkEfforts();
	}
	
	/**
	 * @return
	 */
	public boolean checkEfforts() {
		if(efforts == null || efforts.isEmpty()) return true;
		
		for(Effort e : efforts)
			if(!e.isCorrectlySet()) return false;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.base.events.Event#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = super.checkCorrectness();
		
		if(results == null) results = new ArrayList<CheckResult>();
		
		if(efforts != null) {
			for(Effort e : efforts)
				if(e != null) {
					int counter = 1;
					
					for(CheckResult of : e.checkCorrectness()) {
						of.setMessage("Effort #" + counter++ + " : " + of.getMessage());
						
						results.add(of);
					}
				}
		}
		
		return results;
	}
	
	public List<Effort> uniqueEfforts() {
		List<Effort> unique = new ArrayList<Effort>();
		
		if(efforts != null && !efforts.isEmpty()) {
			Map<EffortType, Double> byType = new HashMap<EffortType, Double>();
			
			for(Effort in : efforts) {
				if(!byType.containsKey(in.getEffortType())) {
					byType.put(in.getEffortType(), 0D);
				}
				
				byType.put(in.getEffortType(), byType.get(in.getEffortType()) + in.getEffortValue());
			}
			
			for(EffortType in : byType.keySet()) {
				unique.add(Effort.builder().effortType(in).effortValue(byType.get(in)).build());
			}
		}
			
		return unique;
	}
}
