/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.support.gis;

import java.io.Serializable;

import org.fao.fi.vrmf.psm.logbook.model.utils.CoordinatesHelper;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 21, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 21, 2015
 */
@EqualsAndHashCode
public class Longitude implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1760294769599047644L;
	
	@Getter private String value;
	
	public Longitude(String value) {
		if(CoordinatesHelper.isLongitudeCorrect(value)) this.value = value;
		else throw new IllegalArgumentException("Wrong longitude value " + value);
	}
	
	public double toDecimalCoordinate() {
		return CoordinatesHelper.longitudeFromString(value);
	}
	
	static public Longitude fromCoordinates(String coordinates) {
		return new Longitude(CoordinatesHelper.longitude(coordinates));
	}
}
