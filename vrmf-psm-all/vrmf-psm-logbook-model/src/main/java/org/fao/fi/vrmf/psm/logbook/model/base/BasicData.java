/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 14, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 14, 2015
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode
abstract public class BasicData {
	@Transient @XmlTransient @JsonIgnore
	@Getter @Setter protected boolean dirty = false;
	
	abstract public Integer getId();
	
	@Transient @XmlTransient @JsonIgnore
	public boolean isNew() {
		return getId() == null;
	}
}
