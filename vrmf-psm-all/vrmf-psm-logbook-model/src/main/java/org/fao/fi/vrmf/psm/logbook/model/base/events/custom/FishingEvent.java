/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.events.custom;

import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchAndEffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 20, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 20, 2015
 */
public class FishingEvent extends CatchAndEffortEvent {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4631742638943398569L;

	/**
	 * Class constructor
	 */
	public FishingEvent() {
		super();
		
		this.setType(EventType.FISHING);
	}
}
