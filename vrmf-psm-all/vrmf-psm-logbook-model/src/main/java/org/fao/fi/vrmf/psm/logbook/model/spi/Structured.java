/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.spi;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 14, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 14, 2015
 */
public interface Structured<SELF extends Structured<SELF>> {
	SELF cleanup();
}
