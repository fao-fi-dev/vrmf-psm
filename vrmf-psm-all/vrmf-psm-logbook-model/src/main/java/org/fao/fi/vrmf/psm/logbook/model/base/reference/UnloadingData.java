/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.fao.fi.vrmf.psm.logbook.model.base.BasicData;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.Checkable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Structured;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity @Inheritance(strategy=InheritanceType.JOINED)
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true) @Builder
public class UnloadingData extends BasicData implements Checkable, Structured<UnloadingData> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2532195854205179662L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute private Integer id;
	
	@Getter @Setter @XmlElement(required=false) @Column(nullable=true) private String comment;
	
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@Getter @Setter @XmlElementWrapper(name="catches") @XmlElement(name="catch") private List<Catches> catchesUnloaded = new ArrayList<Catches>();
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
				
		if(catchesUnloaded != null) {
			for(Catches in : catchesUnloaded)
				if(in != null)
					results.addAll(in.checkCorrectness());
		}
		
		return results;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#isCorrectlySet()
	 */
	@Override @JsonIgnore
	public boolean isCorrectlySet() {
		if(catchesUnloaded != null)
			for(Catches in : catchesUnloaded)
				if(in != null && !in.isCorrectlySet()) return false;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public UnloadingData cleanup() {
		if(isNew()) return null; 

		return this;
	}
}
