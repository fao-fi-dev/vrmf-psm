/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference.codes;

import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlEnum;

import org.fao.fi.vrmf.psm.logbook.model.spi.CodedEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@XmlEnum
@ToString
public enum DerivedEffortType implements CodedEntity {
	NUMBER_OF_BOATS("BO", "Number of boats"),
	NUMBER_OF_TRIPS("TR", "Number of trips"),

	NUMBER_OF_DAYS_AT_SEA("DS", "Number of days at sea"),
	NUMBER_OF_HOURS_AT_SEA("SH", "Number of hours at sea"),
	
	NUMBER_OF_FISHING_DAYS("FD", "Number of fishing days"),
	NUMBER_OF_HOURS_FISHING("FH", "Number of hours fishing"),
	NUMBER_OF_HOURS_FISHING_STANDARDIZED("ST", "Number of hours fishing standardized"),
	
	NUMBER_OF_SEARCHING_DAYS("SD", "Number of searching days"),
	NUMBER_OF_HOURS_SEARCHING("SR", "Number of hours searching"),
	
	LINE_HOURS("LH", "Line-hours"),

	MAN_HOURS("MH", "Man-hours"),
	NET_DAYS("NT", "Net-days"),
	MEN_DAYS("MD", "Men-days");
	
	@Id @Getter @Setter @XmlAttribute(required=true) private String code;
	@Getter @Setter @XmlAttribute(required=true) private String name;
		
	private DerivedEffortType(String code, String name) {
		this.code = code;
		this.name = name;
	}
}