/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.events;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.fao.fi.vrmf.psm.logbook.model.base.BasicData;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.EffortsRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.DerivedEffort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DerivedEffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;
import org.fao.fi.vrmf.psm.logbook.model.spi.Checkable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Completable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Structured;
import org.fao.fi.vrmf.psm.logbook.model.utils.CoordinatesHelper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity @Inheritance(strategy=InheritanceType.JOINED)
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true) @Builder
public class Event extends BasicData implements Checkable, Completable, Structured<Event> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 246926038525755218L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute private Integer id;
	@Getter @Setter @XmlAttribute(required=true) @Enumerated(EnumType.STRING) @Column(nullable=false) private EventType type;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private Date startDate;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String coordinatesAtStart;
	
	@Getter @Setter @XmlElement private Date endDate;
	@Getter @Setter @XmlElement private String coordinatesAtEnd;
	
	@Getter @Setter @XmlElement @Column(length=2048) private String comments;
	
	public Event(EventType type) {
		super();
		this.type = type;
		startDate = new Date();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public Event cleanup() {
		if(isNew()) return null;
		
		return this;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Completable#isCompleted()
	 */
	@JsonIgnore
	public boolean isCompleted() {
		return 
			type != null &&
			startDate != null &&
			coordinatesAtStart != null &&
			endDate != null &&
			coordinatesAtEnd != null;
	}
	
	@JsonIgnore
	public boolean isTransshipmentEvent() { 
		return this instanceof TransshipmentEvent;
	}
	
	@JsonIgnore
	public boolean isEffortEvent() { 
		return this instanceof EffortsRecord;
	}
	
	@JsonIgnore
	public boolean isCatchEvent() { 
		return this instanceof CatchesRecord;
	}
	
	@JsonIgnore
	public boolean isCatchAndEffortEvent() { 
		return isEffortEvent() && isCatchEvent();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#isCorrectlySet()
	 */
	@JsonIgnore
	public boolean isCorrectlySet() {
		return !CheckResultHelper.hasErrors(checkCorrectness());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
		
		if(type == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The event type is not set").cause(this).build());
		if(startDate == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The event start date is not set").cause(this).build());
		if(coordinatesAtStart == null || "".equals(coordinatesAtStart.trim())) results.add(CheckResult.builder().type(CheckType.ERROR).message("The event coordinates at start are not set").cause(this).build());
		
		if(coordinatesAtStart != null && !CoordinatesHelper.areCorrect(coordinatesAtStart)) results.add(CheckResult.builder().type(CheckType.ERROR).message("The event coordinates at start are not correct").cause(this).build());
		
		if(endDate != null && endDate.before(startDate)) results.add(CheckResult.builder().type(CheckType.ERROR).message("The event end date preceeds the event start date").cause(this).build());
		if(endDate != null && ( coordinatesAtEnd == null || "".equals(coordinatesAtEnd.trim()))) results.add(CheckResult.builder().type(CheckType.ERROR).message("The event coordinates at end are not set").cause(this).build());
		if(endDate == null && coordinatesAtEnd != null && !"".equals(coordinatesAtEnd.trim())) results.add(CheckResult.builder().type(CheckType.ERROR).message("The event end date is not set").cause(this).build());

		if(coordinatesAtEnd != null && !CoordinatesHelper.areCorrect(coordinatesAtEnd)) results.add(CheckResult.builder().type(CheckType.ERROR).message("The event coordinates at end are not correct").cause(this).build());
		
		return results; 
	}
	
	public List<DerivedEffort> deriveEfforts() {
		if(!isCorrectlySet()) throw new IllegalArgumentException("Cannot derive efforts from an event not yet correctly set");
		//if(!isCompleted()) throw new IllegalArgumentException("Cannot derive efforts from an event not yet completed");
		
		List<DerivedEffort> derivedEfforts = new ArrayList<DerivedEffort>();
		
		Double days = days(), hours = hours();
		
		days = days == null ? 0 : days;
		hours = hours == null ? 0 : hours;
		
		derivedEfforts.add(DerivedEffort.builder().effortType(DerivedEffortType.NUMBER_OF_DAYS_AT_SEA).effortValue(days).build());
		derivedEfforts.add(DerivedEffort.builder().effortType(DerivedEffortType.NUMBER_OF_HOURS_AT_SEA).effortValue(hours).build());
		
		switch(type) {
			case TRANSIT:
			case BAD_WEATHER:
			case CLEANING:
			case SETTING:
			case RETRIEVING:
			case TRANSSHIPPING:
				break;
			case SEARCHING:
				derivedEfforts.add(DerivedEffort.builder().effortType(DerivedEffortType.NUMBER_OF_SEARCHING_DAYS).effortValue(days()).build());
				break;
			case FISHING:
				derivedEfforts.add(DerivedEffort.builder().effortType(DerivedEffortType.NUMBER_OF_FISHING_DAYS).effortValue(days).build());
				derivedEfforts.add(DerivedEffort.builder().effortType(DerivedEffortType.NUMBER_OF_HOURS_FISHING).effortValue(hours).build());
				derivedEfforts.add(DerivedEffort.builder().effortType(DerivedEffortType.NUMBER_OF_HOURS_FISHING_STANDARDIZED).effortValue(hours).build());
				break;
			default:
				break;
		}
		
		Iterator<DerivedEffort> derived = derivedEfforts.iterator();
		
		DerivedEffort current;
		while(derived.hasNext()) {
			current = derived.next();
			
			if(current == null || Double.compare(current.getEffortValue(), 0) == 0)
				derived.remove();
		}

		return derivedEfforts;
	}
	
	public Double hours() {
		if(endDate == null) return null;
		
		long diff = endDate.getTime() - startDate.getTime();
		double hours = diff * 0.001 / 3600.0;
		
		return hours;
	}
	
	public Double days() {
		if(endDate == null) return null;
		
		return hours() / 24.0;
	}
}
