/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.fao.fi.vrmf.psm.logbook.model.base.BasicData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;
import org.fao.fi.vrmf.psm.logbook.model.spi.Checkable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Structured;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 16, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 16, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true) @Builder
public class Effort extends BasicData implements Checkable, Structured<Effort> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2689165780619860828L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute private Integer id;
	
	@Enumerated(EnumType.STRING) @Getter @Setter @XmlElement(required=true) @Column(nullable=false) private EffortType effortType;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private double effortValue;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public Effort cleanup() {
		return isNew() ? null : this; 
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#isCorrectlySet()
	 */
	@JsonIgnore
	public boolean isCorrectlySet() {
		return !CheckResultHelper.hasErrors(checkCorrectness());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
		
		if(effortType == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The effort type is null").cause(this).build());
		if(Double.compare(effortValue, 0D) < 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The effort value is negative").cause(this).build());
		if(Double.compare(effortValue, 0D) == 0) results.add(CheckResult.builder().type(CheckType.WARNING).message("The effort value is zero").cause(this).build());
		
		return results; 
	}
}
