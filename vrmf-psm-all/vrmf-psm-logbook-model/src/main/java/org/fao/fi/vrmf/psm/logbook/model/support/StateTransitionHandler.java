/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.support;

import java.io.Serializable;
import java.util.List;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.VesselType;

import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 20, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 20, 2015
 */
@Slf4j
abstract public class StateTransitionHandler implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7403900077539907430L;
	
	protected Event state;
	
	/**
	 * Class constructor
	 *
	 */
	public StateTransitionHandler() {
	}
	
	public StateTransitionHandler initialize(List<Event> events) {
		state = null;
		
		return update(events);
	}
	
	abstract protected void transition(Event next);
	
	public StateTransitionHandler update(List<Event> events) {
		if(events != null) {
			for(Event in : events) {
				if(in != null)
					next(in);
			}
		}
		
		return this;
	}
	
	public StateTransitionHandler next(Event event) {
		if(event == null) throw new IllegalArgumentException("Next event cannot be null");
		if(event.getType() == null) throw new IllegalArgumentException("Next event cannot have a null type");
		if(event.getStartDate() == null) throw new IllegalArgumentException("Next event cannot have a null start date");
		
		if(state != null) {
			if(!state.isCompleted()) throw new IllegalArgumentException("Previous event (" + state.getType().getName() + " @ " + state.getStartDate() + ")  is not completed yet");
			
			if(event.getStartDate().before(state.getEndDate())) throw new IllegalArgumentException("Event start date (" + event.getStartDate() + ") cannot preceed previous event end date (" + state.getEndDate() + ")");
		}
		
		log.debug("[ {} @ {} / {} ] -> [ {} @ {} / {} ]", 
					state == null ? null : state.getType().getName(), 
					state == null ? null : state.getStartDate(), 
					state == null ? null : state.getEndDate(), 
					event.getType().getName(), 
					event.getStartDate(),
					event.getEndDate());
		
		transition(event);
		
		return this;
	}
	
	protected void illegalTransition(Event from, Event to) {
		throw new IllegalStateException("Illegal transition from " + from.getType() + " to " + to.getType());
	}
	
	protected boolean isValid(EventType next, EventType... valid) {
		for(EventType in : valid)
			if(in.equals(next))
				return true;
		
		return false;
	}
	
	abstract public boolean isCompleted();
	
	abstract public List<EventType> nextEvents();
	
	static public StateTransitionHandler forType(VesselType type) {
		if(VesselType.SEINER.equals(type)) return new SeinerStateTransitionHandler();
		
		return new DefaultStateTransitionHandler();
	}
}
