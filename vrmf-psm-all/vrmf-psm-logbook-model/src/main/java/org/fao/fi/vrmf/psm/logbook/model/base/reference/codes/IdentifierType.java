/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference.codes;

import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlEnum;

import org.fao.fi.vrmf.psm.logbook.model.spi.CodedEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@XmlEnum
@ToString
public enum IdentifierType implements CodedEntity {
	EU_CFR("EU CFR", "European Union fleet registry identifier"),
	RFB_ID("RFB ID", "RFB ID (specify)"),
	RFMO_ID("RFMO ID", "RFMO ID (specify)"),
	EXT_MARK("EXT MARK", "External marking"),
	TUVI("TUVI", "CLAV TUVI"),
	GR_ID("GR ID", "FAO Global Record ID"),
	HSVAR_ID("HSVAR ID", "HSVAR ID"),
	OTHER("OTH", "Other (specify)");
	
	@Id @Getter @Setter @XmlAttribute(required=true) private String code;
	@Getter @Setter @XmlAttribute(required=true) private String name;
	
	private IdentifierType(String code, String name) {
		this.code = code;
		this.name = name;
	}
}
