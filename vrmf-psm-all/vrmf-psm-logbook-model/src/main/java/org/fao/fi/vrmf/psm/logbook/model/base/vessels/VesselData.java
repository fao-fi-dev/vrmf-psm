/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.vessels;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.fao.fi.vrmf.psm.logbook.model.base.BasicData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Identifier;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.PowerUnit;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.VesselType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;
import org.fao.fi.vrmf.psm.logbook.model.spi.Checkable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Cleanable;
import org.fao.fi.vrmf.psm.logbook.model.utils.CleaningHelper;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true) @Builder
public class VesselData extends BasicData implements Checkable, Cleanable<VesselData> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1389260186024718842L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute private Integer id;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String name;
	
	@Getter @Setter @XmlAttribute(required=true) @Enumerated(EnumType.STRING) @Column(nullable=false) private VesselType type;
	
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String fishingCompany;
	
	@Getter @Setter @XmlElement private String imo;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Getter @Setter @XmlElement private Country registrationCountry;

	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String registrationNumber;

	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String ircs;
	@Getter @Setter @XmlElement private String mmsi;

	@Getter @Setter @XmlElement private String licenseNumber;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true)
	@Fetch(FetchMode.SUBSELECT)
	@Getter @Setter @XmlElementWrapper(name="identifiers") @XmlElement(name="identifier") private List<Identifier> identifiers;
	
	@Getter @Setter @XmlElement @Column(nullable=false) private Double gt = 0D;
	@Getter @Setter @XmlElement(nillable=true) @Column(nullable=true) private Double grt;
	
	@Getter @Setter @XmlElement @Column(nullable=false) private Double loa = 0D;;
	@Getter @Setter @XmlElement(nillable=true) private Double lbp;
	
	@Getter @Setter @XmlElement @Column(nullable=false) private Double mainEnginePower = 0D;;
	@Getter @Setter @XmlElement @Column(nullable=false) private PowerUnit mainEnginePowerUnit;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#isCorrectlySet()
	 */
	@Override @JsonIgnore
	public boolean isCorrectlySet() {
		return !CheckResultHelper.hasErrors(checkCorrectness());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
		
		if(name == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel name is not set").cause(this).build());
		if(type == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel type is not set").cause(this).build());
		if(fishingCompany == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel fishing company is not set").cause(this).build());
		if(registrationCountry == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel registration country is not set").cause(this).build());
		if(registrationNumber == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel registration number is not set").cause(this).build());
		if(ircs == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel IRCS is not set").cause(this).build());
		
		if(gt == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel GT is not set").cause(this).build());
		if(gt != null && Double.compare(gt, 0D) < 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel GT is lower than zero").cause(this).build());
		if(gt != null && Double.compare(gt, 0D) == 0) results.add(CheckResult.builder().type(CheckType.WARNING).message("The vessel GT is zero").cause(this).build());

		if(loa == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel LOA is not set").cause(this).build());
		if(loa != null && Double.compare(loa, 0D) < 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel LOA is lower than zero").cause(this).build());
		if(loa != null && Double.compare(loa, 0D) == 0) results.add(CheckResult.builder().type(CheckType.WARNING).message("The vessel LOA is zero").cause(this).build());
		
		if(mainEnginePower == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel main engine power is not set").cause(this).build());
		if(mainEnginePower != null && Double.compare(mainEnginePower, 0D) < 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel main engine power is lower than zero").cause(this).build());
		if(mainEnginePower != null && Double.compare(mainEnginePower, 0D) == 0) results.add(CheckResult.builder().type(CheckType.WARNING).message("The vessel main engine power is zero").cause(this).build());

		if(mainEnginePowerUnit == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel main engine power unit is not set").cause(this).build());

		if(grt != null && Double.compare(grt, 0D) < 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel GRT is lower than zero").cause(this).build());
		if(grt != null && Double.compare(grt, 0D) == 0) results.add(CheckResult.builder().type(CheckType.WARNING).message("The vessel GRT is zero").cause(this).build());
	
		if(lbp != null && Double.compare(lbp, 0D) < 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel LBP is lower than zero").cause(this).build());
		if(lbp != null && Double.compare(lbp, 0D) == 0) results.add(CheckResult.builder().type(CheckType.WARNING).message("The vessel LBP is zero").cause(this).build());
		
		if(identifiers != null && !identifiers.isEmpty()) {
			for(Identifier in : identifiers)
				if(in != null)
					results.addAll(in.checkCorrectness());
		}
		
		return results; 
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Cleanable#clean()
	 */
	@Override
	public VesselData clean() {
		return CleaningHelper.clean(this);
	}
}
