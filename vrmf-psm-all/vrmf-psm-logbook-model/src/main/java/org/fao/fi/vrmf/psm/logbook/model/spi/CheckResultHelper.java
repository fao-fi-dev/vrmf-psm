/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.spi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 24, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 24, 2015
 */
final public class CheckResultHelper {
	private CheckResultHelper() { super(); }
	
	static public List<CheckResult> filter(List<CheckResult> results, CheckType... ofType) {
		if(results == null) throw new IllegalArgumentException("Check results are null");
		if(ofType == null || ofType.length == 0) throw new IllegalArgumentException("Check result types are null or empty");
		
		Set<CheckType> types = new HashSet<CheckType>(Arrays.asList(ofType));
		
		List<CheckResult> filtered = new ArrayList<CheckResult>();
		
		for(CheckResult in : results) {
			if(in != null && types.contains(in.getType()))
				filtered.add(in);
		}
		
		return filtered;
	}
	
	static public boolean hasAny(List<CheckResult> results, CheckType... ofType) {
		return !filter(results, ofType).isEmpty();
	}
	
	static public boolean hasNotAny(List<CheckResult> results, CheckType... ofType) {
		return !hasAny(results, ofType);
	}
	
	static public List<CheckResult> filterErrors(List<CheckResult> results) {
		return filter(results, CheckType.ERROR);
	}
	
	static public List<CheckResult> filterWarnings(List<CheckResult> results) {
		return filter(results, CheckType.WARNING);
	}

	static public List<CheckResult> filterInfo(List<CheckResult> results) {
		return filter(results, CheckType.INFO);
	}

	static public List<CheckResult> filterErrorsAndWarnings(List<CheckResult> results) {
		return filter(results, CheckType.ERROR, CheckType.WARNING);
	}
	
	static public boolean hasErrors(List<CheckResult> results) {
		return hasAny(results, CheckType.ERROR);
	}
	
	static public boolean hasWarnings(List<CheckResult> results) {
		return hasAny(results, CheckType.WARNING);
	}
	
	static public boolean hasInfo(List<CheckResult> results) {
		return hasAny(results, CheckType.INFO);
	}
	
	static public boolean hasErrorsOrWarnings(List<CheckResult> results) {
		return hasAny(results, CheckType.ERROR, CheckType.WARNING);
	}
}
