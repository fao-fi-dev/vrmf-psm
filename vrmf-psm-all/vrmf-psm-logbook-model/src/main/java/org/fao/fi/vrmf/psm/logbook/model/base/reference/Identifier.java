/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.IdentifierType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;
import org.fao.fi.vrmf.psm.logbook.model.spi.Checkable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Cleanable;
import org.fao.fi.vrmf.psm.logbook.model.utils.CleaningHelper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode @Builder
public class Identifier implements Checkable, Cleanable<Identifier> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2689165780619860828L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute private Integer id;
	
	@Getter @Setter @XmlAttribute(required=true) @Enumerated(EnumType.STRING) @Column(nullable=false) private IdentifierType type;
	@Getter @Setter @XmlAttribute(required=true) @Column(nullable=false) private String identifier;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Cleanable#clean()
	 */
	@Override
	public Identifier clean() {
		return CleaningHelper.clean(this);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#isCorrectlySet()
	 */
	@Override
	public boolean isCorrectlySet() {
		return !CheckResultHelper.hasErrors(checkCorrectness());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
		
		if(type == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The type is not set").cause(this).build());
		if(identifier == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The identifier is not set").cause(this).build());
		
		return results;
	}
}
