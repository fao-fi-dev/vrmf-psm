/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.events;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches.CatchesKey;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.SchoolType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=true)
public class CatchEvent extends Event implements CatchesRecord {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5045154363043911491L;
	
	@Getter @Setter @XmlElement @Enumerated(EnumType.STRING) private SchoolType schoolType;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
	@Getter @Setter @XmlElementWrapper(name="catches") @XmlElement(name="catch") private List<Catches> catches = new ArrayList<Catches>();
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
	@Getter @Setter @XmlElementWrapper(name="discards") @XmlElement(name="discard") private List<Discards> discards = new ArrayList<Discards>();
	
	public CatchEvent(EventType type) {
		super(type);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public CatchEvent cleanup() {
		if(isNew()) return null;
		
		if(catches != null) {
			Catches current;
			Iterator<Catches> catchez = catches.iterator();
			
			while(catchez.hasNext()) {
				current = catchez.next();
				
				if(current != null && current.cleanup() == null)
					catchez.remove();
			}
		}
		
		if(discards != null) {
			Discards current;
			Iterator<Discards> discard = discards.iterator();
			
			while(discard.hasNext()) {
				current = discard.next();
				
				if(current != null && current.cleanup() == null)
					discard.remove();
			}
		}

		return this;
	}
	
	public boolean isDirty() {
		boolean isDirty = dirty;
		
		if(catches != null)
			for(Catches in : catches)
				isDirty &= in != null && in.isDirty();
		
		if(discards != null)
			for(Discards in : discards)
				isDirty &= in != null && in.isDirty();
				
		return isDirty;
	}
	
	public void setDirty(boolean dirty) {
		super.setDirty(dirty);
		
		if(!dirty) {
			if(catches != null)
				for(Catches in : catches)
					if(in != null) in.setDirty(false);
			
			if(discards != null)
				for(Discards in : discards)
					if(in != null) in.setDirty(false);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.base.Event#isEventCorrectlySet()
	 */
	@Override
	public boolean isCorrectlySet() {
		return 
			super.isCorrectlySet() &&
			checkCatches() &&
			checkDiscards();
	}
	
	public boolean checkCatches() {
		if(catches == null || catches.isEmpty()) return true;
		
		for(Catches c : catches)
			if(!c.isCorrectlySet()) return false;
		
		return true;
	}
	
	public boolean checkDiscards() {
		if(discards == null || discards.isEmpty()) return true;
		
		for(Discards d : discards)
			if(!d.isCorrectlySet()) return false;
		
		return true;
	}
	
	public List<Catches> uniqueCatches() {
		List<Catches> unique = new ArrayList<Catches>();
		
		if(catches != null && !catches.isEmpty()) {
			Map<CatchesKey, Double> bySpeciesAndUnit = new HashMap<CatchesKey, Double>();
			
			CatchesKey key;
			for(Catches in : catches) {
				key = in == null ? null : in.key();
				
				if(!bySpeciesAndUnit.containsKey(key)) {
					bySpeciesAndUnit.put(key, 0D);
				}
				
				bySpeciesAndUnit.put(key, bySpeciesAndUnit.get(key) + in.getQuantity());
			}
			
			for(CatchesKey in : bySpeciesAndUnit.keySet()) {
				unique.add(Catches.builder().species(in.getSpecies()).quantityType(in.getQuantityType()).quantity(bySpeciesAndUnit.get(in)).build());
			}
		}
			
		return unique;
	}
	
	public List<Discards> uniqueDiscards() {
		List<Discards> unique = new ArrayList<Discards>();
		
		if(discards != null && !discards.isEmpty()) {
			Map<CatchesKey, Double> bySpeciesAndUnit = new HashMap<CatchesKey, Double>();
			
			CatchesKey key;
			for(Discards in : discards) {
				key = in == null ? null : in.key();
				
				if(!bySpeciesAndUnit.containsKey(key)) {
					bySpeciesAndUnit.put(key, 0D);
				}
				
				bySpeciesAndUnit.put(key, bySpeciesAndUnit.get(key) + in.getQuantity());
			}
			
			Discards current;
			for(CatchesKey in : bySpeciesAndUnit.keySet()) {
				current = new Discards();
				current.setSpecies(in.getSpecies());
				current.setQuantityType(in.getQuantityType());
				current.setQuantity(bySpeciesAndUnit.get(in));

				unique.add(current);
			}
		}
			
		return unique;
	}
}