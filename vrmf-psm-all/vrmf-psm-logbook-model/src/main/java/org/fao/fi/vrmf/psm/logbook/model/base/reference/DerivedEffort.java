/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DerivedEffortType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 16, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 16, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode @Builder
public class DerivedEffort implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2689165780619860828L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute private Integer id;
	
	@Enumerated(EnumType.STRING) @Getter @Setter @XmlElement(required=true) @Column(nullable=false) private DerivedEffortType effortType;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private double effortValue;
}
