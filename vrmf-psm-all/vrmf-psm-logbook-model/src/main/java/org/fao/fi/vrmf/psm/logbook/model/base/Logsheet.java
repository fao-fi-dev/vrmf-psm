/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.EffortsRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.CanneryUnloadingData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches.CatchesKey;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.DerivedEffort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.VesselUnloadingData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DerivedEffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckType;
import org.fao.fi.vrmf.psm.logbook.model.spi.Checkable;
import org.fao.fi.vrmf.psm.logbook.model.spi.Structured;
import org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @ToString(exclude={"transitionHandler"}) @EqualsAndHashCode(callSuper=true, exclude={"transitionHandler"}) @Builder
public class Logsheet extends BasicData implements Checkable, Structured<Logsheet> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5319387595502461811L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter @XmlAttribute protected Integer id;
	
	@Getter @Setter @XmlAttribute @Column(nullable=false) private Integer number;
	
	@Getter @Setter @XmlAttribute(required=true) @Column(nullable=false) private boolean completed;
	@Getter @Setter @XmlAttribute private Date completionDate;
	
	@ManyToOne(fetch = FetchType.EAGER, optional=false)
	@Getter @Setter @XmlElement(required=true) private VesselData vesselData;
	
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private Integer crewSize;
	
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String captainName;
	
	@Getter @Setter @XmlElement private String comments;
	
	@ManyToOne(fetch = FetchType.EAGER, optional=false)
	@Getter @Setter @XmlElement(required=true) private Port departurePort;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private Date departureDate;
	
	@Getter @Setter @XmlElement private double fishOnboardAtStart;
	@Getter @Setter @XmlElement private QuantityType fishOnboardQuantityAtStart;
	
	@OneToOne(fetch = FetchType.EAGER)
	@Getter @Setter @XmlElement private Port unloadingPort;
	@Getter @Setter @XmlElement private Date arrivalDate;
	
	@Getter @Setter @XmlElement private double fishOnboardAfterUnloading;
	@Getter @Setter @XmlElement private @Enumerated(EnumType.STRING) QuantityType fishOnboardQuantityAfterUnloading;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true) @OrderBy("id")
	@Getter @Setter @XmlElementWrapper(name="events", required=true) @XmlElement(name="event") private List<Event> events;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true)
	@Getter @Setter @XmlElementWrapper(name="canneryUnloads") @XmlElement(name="canneryUnload") private List<CanneryUnloadingData> canneryUnloads;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true)
	@Getter @Setter @XmlElementWrapper(name="otherVesselUnloads") @XmlElement(name="otherVesselUnload") private List<VesselUnloadingData> otherVesselUnloads;

	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval = true) 
	@Getter @Setter @XmlElementWrapper(name="derivedEfforts") @XmlElement(name="derivedEffort") private List<DerivedEffort> derivedEfforts;
	
	@Transient @XmlTransient @JsonIgnore
	@Getter @Setter @NonNull private StateTransitionHandler transitionHandler;

	@XmlElementWrapper(name="uniqueEfforts") @XmlElement(name="effort")
	@Transient @Getter @Setter private List<Effort> uniqueEfforts;
	
	@XmlElementWrapper(name="uniqueCatches") @XmlElement(name="catches")
	@Transient @Getter @Setter private List<Catches> uniqueCatches;
	
	@XmlElementWrapper(name="uniqueDiscards") @XmlElement(name="discard")
	@Transient @Getter @Setter private List<Discards> uniqueDiscards;
	
	@Transient @JsonIgnore
	public boolean isDirty() {
		boolean isDirty = dirty;
		
		if(events != null)
			for(Event in : events)
				isDirty &= in != null && in.isDirty();
				
		return isDirty;
	}
	
	@Transient @XmlTransient @JsonIgnore
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
		
		if(!dirty) {
			if(events != null)
				for(Event in : events)
					if(in != null) in.setDirty(false);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Structured#cleanup()
	 */
	@Override
	public Logsheet cleanup() {
		if(isNew()) return null;
		
		if(events != null) {
			Event current;
			Iterator<Event> event = events.iterator();
			
			while(event.hasNext()) {
				current = event.next();
				
				if(current != null && current.cleanup() == null)
					event.remove();
			}
		}

		return this;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#isCorrectlySet()
	 */
	@Override @JsonIgnore
	public boolean isCorrectlySet() {
		return !CheckResultHelper.hasErrors(checkCorrectness());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	@Override
	public List<CheckResult> checkCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
		
		if(departureDate == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The departure date is not set").cause(this).build());
		if(departurePort == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The departure port is not set").cause(this).build());
		if(captainName == null || "".equals(captainName.trim())) results.add(CheckResult.builder().type(CheckType.ERROR).message("The captain name is not set").cause(this).build());
		
		if(crewSize == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The crew size is not set").cause(this).build());
		if(crewSize != null && crewSize < 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The crew size is negative").cause(this).build());
		if(crewSize != null && crewSize == 0) results.add(CheckResult.builder().type(CheckType.ERROR).message("The crew size is zero").cause(this).build());
		
		if(vesselData == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The vessel data are not set").cause(this).build());
		if(vesselData != null) results.addAll(vesselData.checkCorrectness());
		
		if(fishOnboardQuantityAtStart == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The fish onboard quantity type at departure is not set").cause(this).build());
		
		if(completed) {
			if(arrivalDate == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The arrival date is not set").cause(this).build());
			if(unloadingPort == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The arrival port is not set").cause(this).build());
			if(fishOnboardQuantityAfterUnloading == null) results.add(CheckResult.builder().type(CheckType.ERROR).message("The fish onboard quantity type at arrival is not set").cause(this).build());
		}
		
		if(events == null || events.isEmpty()) {
			results.add(CheckResult.builder().type(CheckType.WARNING).message("No events currently defined for this logsheet").cause(this).build());
		} else {
			results.addAll(checkEventsCorrectness());
		}
		
		return results; 
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.model.spi.Checkable#checkCorrectness()
	 */
	public List<CheckResult> checkEventsCorrectness() {
		List<CheckResult> results = new ArrayList<CheckResult>();
		
		for(Event in : events) {
			if(in != null) {
				int counter = 1;
				
				for(CheckResult of : in.checkCorrectness()) {
					of.setMessage("Event #" + counter++ + " : " + of.getMessage());
					
					results.add(of);
				}
			}
		}
		
		return results; 
	}
	
	public Logsheet completeLogsheet() {
		if(completed) throw new UnsupportedOperationException("The logsheet was already marked as completed");
		if(!transitionHandler.isCompleted()) throw new UnsupportedOperationException("The current sequence of events is not complete");

		String message = "";
		
		boolean successful = ( events == null || events.isEmpty() || events.get(events.size() - 1).getType().equals(EventType.TRANSIT) );
		
		if(!successful) {
			message = "the last event must be a 'TRANSIT' event";
		} else {
			successful = checkEvents();
			
			if(!successful) {
				message = "the event sequence is not correct or complete";
			} else {
				successful = isCorrectlySet();
				
				if(!successful) {
					message = "the following errors have been identified for this logsheet: ";
					
					for(CheckResult in : checkCorrectness()) {
						if(CheckType.ERROR.equals(in.getType())) {
							message += "[ " + in.getMessage() + " ] ";
						}
					}
				}
			}
		}
		
		if(!successful) {
			throw new UnsupportedOperationException("Cannot mark logsheet as completed: " + message);
		} 

		completed = true;
		completionDate = new Date();
		
		return this;
	}
	
	public void updateDerivedEfforts() {
		derivedEfforts = deriveEfforts();
	}
	
	public List<DerivedEffort> deriveEfforts() {
		List<DerivedEffort> efforts = new ArrayList<DerivedEffort>();
		
		if(events != null) {
			for(Event in : events) {
				efforts.addAll(in.deriveEfforts());
			}
		}
		
		Map<DerivedEffortType, Double> effortsMap = new HashMap<DerivedEffortType, Double>();
		
		DerivedEffortType currentType = null;
		
		for(DerivedEffort in : efforts) {
			if(!effortsMap.containsKey(currentType = in.getEffortType()))
				effortsMap.put(currentType, 0D);

			effortsMap.put(currentType, effortsMap.get(currentType) + in.getEffortValue());
		}
		
		for(DerivedEffortType key : effortsMap.keySet()) 
			efforts.add(new DerivedEffort(null /*ID*/, key, effortsMap.get(key)));
		
		efforts.add(DerivedEffort.builder().effortType(DerivedEffortType.NUMBER_OF_TRIPS).effortValue(1).build());
		efforts.add(DerivedEffort.builder().effortType(DerivedEffortType.NUMBER_OF_BOATS).effortValue(1).build());
		
		DerivedEffort additional = manHours();
		
		if(additional != null) efforts.add(additional);
		
		additional = menDays();
		
		if(additional != null) efforts.add(additional);
		
		Iterator<DerivedEffort> derived = efforts.iterator();
		
		DerivedEffort current;
		while(derived.hasNext()) {
			current = derived.next();
			
			if(currentType == null || Double.compare(current.getEffortValue(), 0) == 0)
				derived.remove();
		}
		
		return efforts;
	}
	
	private DerivedEffort manHours() {
		if(!completed || events == null || events.isEmpty() || crewSize <= 0) return null;
		
		double hours = (arrivalDate.getTime() - departureDate.getTime()) * 0.001D / 3600D;
		
		return DerivedEffort.builder().effortType(DerivedEffortType.MAN_HOURS).effortValue(hours / crewSize).build();
	}
	
	private DerivedEffort menDays() {
		if(!completed || events == null || events.isEmpty() || crewSize <= 0) return null;
		
		double days = (arrivalDate.getTime() - departureDate.getTime()) * 0.001D / 3600D / 24D;
		
		return DerivedEffort.builder().effortType(DerivedEffortType.MEN_DAYS).effortValue(days / crewSize).build();
	}	
	
	public boolean checkEvents() {
		if(events == null || events.isEmpty()) return true;
		
		for(Event in : events) {
			if(!in.isCorrectlySet() || !in.isCompleted()) {
				if(!in.isCorrectlySet()) System.err.println("Event " + in + " is not correctly set");
				if(!in.isCompleted()) System.err.println("Event " + in + " is not complete");
				
				return false;
			}
		}
		
		//Check that events are properly set in time etc.
		Date current = departureDate;
		
		for(Event in : events) {
			if(in.getStartDate().getTime() < current.getTime()) {
				System.err.println("Event " + in + " start date (" + in.getStartDate() + ") preceeds previous event end date (" + current.getTime() + ")");
				
				return false;
			}
			
			current = in.getEndDate();
		}
		
		if(arrivalDate != null && arrivalDate.getTime() < current.getTime()) {
			System.err.println("Arrival date (" + arrivalDate + ") preceeds last event end date (" + current.getTime() + ")");

			return false;
		}
		
		return true;
	}
	
	public void addEvent(Event event) {
		if(events == null) events = new ArrayList<Event>();
		
		if( events.isEmpty() || events.get(events.size() - 1).isCompleted() ) {
			transitionHandler.next(event);
		
			events.add(event);
		} else throw new UnsupportedOperationException("Previous event is not completed yet");
	}
	
	public List<Effort> uniqueEfforts() {
		List<Effort> unique = new ArrayList<Effort>();
		
		if(events != null) {
			EffortsRecord effortEvent;
			
			Map<EffortType, Double> byType = new HashMap<EffortType, Double>();
			
			for(Event event : events) {
				if(event.isEffortEvent()) {
					effortEvent = (EffortsRecord)event;
					if(effortEvent.getEfforts() != null && !effortEvent.getEfforts().isEmpty()) {
						for(Effort in : effortEvent.getEfforts()) {
							if(!byType.containsKey(in.getEffortType())) {
								byType.put(in.getEffortType(), 0D);
							}
							
							byType.put(in.getEffortType(), byType.get(in.getEffortType()) + in.getEffortValue());
						}
					}
				}
			}
			
			for(EffortType in : byType.keySet()) {
				unique.add(Effort.builder().effortType(in).effortValue(byType.get(in)).build());
			}
		}
			
		return unique;
	}
	
	public List<DerivedEffort> uniqueDerivedEfforts() {
		List<DerivedEffort> unique = new ArrayList<DerivedEffort>();
		
		List<DerivedEffort> all = deriveEfforts();
		
		Map<DerivedEffortType, Double> byType = new HashMap<DerivedEffortType, Double>();
		
		for(DerivedEffort in : all) {
			if(!byType.containsKey(in.getEffortType())) {
				byType.put(in.getEffortType(), 0D);
			}
			
			byType.put(in.getEffortType(), byType.get(in.getEffortType()) + in.getEffortValue());
		}
		
		for(DerivedEffortType in : byType.keySet()) {
			unique.add(DerivedEffort.builder().effortType(in).effortValue(byType.get(in)).build());
		}
			
		return unique;
	}
	
	public List<Catches> uniqueCatches() {
		return uniqueCatches(null);
	}
	
	public List<Catches> uniqueCatches(Date atDate) {
		List<Catches> unique = new ArrayList<Catches>();
		
		if(events != null) {
			CatchesRecord catchEvent;
			
			Map<CatchesKey, Double> bySpeciesAndUnit = new HashMap<CatchesKey, Double>();
			
			for(Event event : events) {
				if(event.isCatchEvent() && ( atDate == null || atDate.getTime() >= event.getEndDate().getTime()) ) {
					catchEvent = (CatchesRecord)event;
					if(catchEvent.getCatches() != null && !catchEvent.getCatches().isEmpty()) {
						for(Catches in : catchEvent.getCatches()) {
							if(!bySpeciesAndUnit.containsKey(in.key())) {
								bySpeciesAndUnit.put(in.key(), 0D);
							}
							
							bySpeciesAndUnit.put(in.key(), bySpeciesAndUnit.get(in.key()) + in.getQuantity());
						}
					}
				}
			}

			for(CatchesKey in : bySpeciesAndUnit.keySet()) {
				unique.add(Catches.builder().species(in.getSpecies()).quantityType(in.getQuantityType()).quantity(bySpeciesAndUnit.get(in)).build());
			}			
		}
			
		return unique;
	}
	
	public List<Discards> uniqueDiscards() {
		List<Discards> unique = new ArrayList<Discards>();
		
		if(events != null) {
			CatchesRecord catchEvent;
			
			Map<CatchesKey, Double> bySpeciesAndUnit = new HashMap<CatchesKey, Double>();
			
			for(Event event : events) {
				if(event.isCatchEvent()) {
					catchEvent = (CatchesRecord)event;
					if(catchEvent.getDiscards() != null && !catchEvent.getDiscards().isEmpty()) {
						for(Discards in : catchEvent.getDiscards()) {
							if(!bySpeciesAndUnit.containsKey(in.key())) {
								bySpeciesAndUnit.put(in.key(), 0D);
							}
							
							bySpeciesAndUnit.put(in.key(), bySpeciesAndUnit.get(in.key()) + in.getQuantity());
						}
					}
				}
			}

			Discards current;
			for(CatchesKey in : bySpeciesAndUnit.keySet()) {
				current = new Discards();
				current.setSpecies(in.getSpecies());
				current.setQuantityType(in.getQuantityType());
				current.setQuantity(bySpeciesAndUnit.get(in));

				unique.add(current);
			}		
		}
			
		return unique;
	}
}