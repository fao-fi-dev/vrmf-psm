/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 16, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 16, 2015
 */
public class CoordinatesHelper {
	final static public String DEGREES_PATTERN = "^([0-9]{2,3}°)";
	final static public String MINUTES_PATTERN = DEGREES_PATTERN + "([0-9]{2}')";
	final static public String SECONDS_PATTERN = MINUTES_PATTERN + "([0-9]{2}'')";
	final static public String QUADRANT_PATTERN = SECONDS_PATTERN + "([NSEW])$";
	
	final static public String LATITUDE_PATTERN  = "^([0-9]{2}°[0-9]{2}'[0-9]{2}''[NS])";
	final static public String LONGITUDE_PATTERN = "([0-1][0-9]{2}°[0-9]{2}'[0-9]{2}''[EW])$";
	final static public String COORDINATE_PATTERN = LATITUDE_PATTERN + "\\;" + LONGITUDE_PATTERN;
	
	final static public Pattern LATITUDE_MATCHER = Pattern.compile(LATITUDE_PATTERN);
	final static public Pattern LONGITUDE_MATCHER = Pattern.compile(LONGITUDE_PATTERN);
	final static public Pattern COORDINATE_MATCHER = Pattern.compile(COORDINATE_PATTERN);
	
	final static public Pattern DEGREES_MATCHER = Pattern.compile(DEGREES_PATTERN + "(.+)");
	final static public Pattern MINUTES_MATCHER = Pattern.compile(MINUTES_PATTERN + "(.+)");
	final static public Pattern SECONDS_MATCHER = Pattern.compile(SECONDS_PATTERN + "(.)$");
	final static public Pattern QUADRANT_MATCHER= Pattern.compile(QUADRANT_PATTERN);
	
	static private boolean isValidLatitude(String latitude) {
		return latitude != null && LATITUDE_MATCHER.matcher(latitude).matches();
	}
	
	static private boolean isValidLongitude(String longitude) {
		return longitude != null && LONGITUDE_MATCHER.matcher(longitude).matches();
	}
	
	@SuppressWarnings("unused")
	static private boolean isValid(String coordinates) {
		return COORDINATE_MATCHER.matcher(coordinates).matches();
	}
	
	static public boolean isLatitudeCorrect(String latitude) {
		return isValidLatitude(latitude) &&
			   extractDegrees(latitude) <= 90 &&
			   extractMinutes(latitude) <= 60 &&
			   extractSeconds(latitude) <= 60;
	}
	
	static public boolean isLongitudeCorrect(String longitude) {
		return isValidLongitude(longitude) &&
			   extractDegrees(longitude) <= 180 &&
			   extractMinutes(longitude) <= 60 &&
			   extractSeconds(longitude) <= 60;
	}
	
	static public boolean areCorrect(String coordinates) {
		if(coordinates == null) return true;
		
		return isLatitudeCorrect(latitude(coordinates)) && isLongitudeCorrect(longitude(coordinates));
	}

	static public String extractQuadrant(String coordinates) {
		if(coordinates == null) throw new IllegalArgumentException("Provided latitude / longitude is NULL");
		
		boolean isLatitude = isValidLatitude(coordinates);
		boolean isLongitude = isValidLongitude(coordinates);
		
		if(!isLatitude && !isLongitude) throw new IllegalArgumentException("Provided coordinate is neither a latitude nor a longitude");
		
		Matcher matcher = QUADRANT_MATCHER.matcher(coordinates);
		
		if(matcher.matches()) {
			return matcher.group(4);
		}
		
		throw new IllegalArgumentException("This should never happen...");
	}

	static public int extractDegrees(String coordinates) {
		if(coordinates == null) throw new IllegalArgumentException("Provided latitude / longitude is NULL");
		
		boolean isLatitude = isValidLatitude(coordinates);
		boolean isLongitude = isValidLongitude(coordinates);
		
		if(!isLatitude && !isLongitude) throw new IllegalArgumentException("Provided coordinate is neither a latitude nor a longitude");
		
		Matcher matcher = DEGREES_MATCHER.matcher(coordinates);
		
		if(matcher.matches()) {
			return Integer.parseInt(matcher.group(1).replaceAll("[^0-9]", ""));
		}
		
		throw new IllegalArgumentException("This should never happen...");
	}
	
	
	static public int extractMinutes(String coordinates) {
		if(coordinates == null) throw new IllegalArgumentException("Provided latitude / longitude is NULL");
		
		boolean isLatitude = isValidLatitude(coordinates);
		boolean isLongitude = isValidLongitude(coordinates);
		
		if(!isLatitude && !isLongitude) throw new IllegalArgumentException("Provided coordinate is neither a latitude nor a longitude");
		
		Matcher matcher = MINUTES_MATCHER.matcher(coordinates);
		
		if(matcher.matches()) {
			return Integer.parseInt(matcher.group(2).replaceAll("[^0-9]", ""));
		}
		
		throw new IllegalArgumentException("This should never happen...");
	}
	
	static public int extractSeconds(String coordinates) {
		if(coordinates == null) throw new IllegalArgumentException("Provided latitude / longitude is NULL");
		
		boolean isLatitude = isValidLatitude(coordinates);
		boolean isLongitude = isValidLongitude(coordinates);
		
		if(!isLatitude && !isLongitude) throw new IllegalArgumentException("Provided coordinate is neither a latitude nor a longitude");
		
		Matcher matcher = SECONDS_MATCHER.matcher(coordinates);
		
		if(matcher.matches()) {
			return Integer.parseInt(matcher.group(3).replaceAll("[^0-9]", ""));
		}
		
		throw new IllegalArgumentException("This should never happen...");
	}
	
	static public String latitude(String coordinates) {
		Matcher matcher = null;
		if((matcher = COORDINATE_MATCHER.matcher(coordinates)).matches()) {
			return matcher.group(1);
		}
		
		return null;
	}
	
	static public double extractLatitude(String coordinates) {
		String latitude = latitude(coordinates);
		
		if(latitude != null) return latitudeFromString(latitude);
		
		throw new IllegalArgumentException("Invalid coordinates " + coordinates);
	}
	
	static public String longitude(String coordinates) {
		Matcher matcher = null;
		if((matcher = COORDINATE_MATCHER.matcher(coordinates)).matches()) {
			return matcher.group(2);
		}
		
		return null;
	}
	
	static public double extractLongitude(String coordinates) {
		String longitude = longitude(coordinates);
		
		if(longitude != null) return longitudeFromString(longitude);
		
		throw new IllegalArgumentException("Invalid coordinates " + coordinates);
	}
	
	static public double[] extractCoordinates(String coordinates) {
		Matcher matcher = null;
		if((matcher = COORDINATE_MATCHER.matcher(coordinates)).matches()) {
			return new double[] { latitudeFromString(matcher.group(1)), longitudeFromString(matcher.group(2)) };
		} 

		throw new IllegalArgumentException("Invalid coordinates " + coordinates);
	}
	
	static public double latitudeFromString(String latitude) {
		double signum = latitude.endsWith("S") ? -1 : 1;
		
		double result = extractDegrees(latitude);
		result += extractMinutes(latitude) / 60.0;
		result += extractSeconds(latitude) / 3600.0;
		
		if(Double.compare(result, 90) > 0) throw new IllegalArgumentException("Latitude out of bounds: " + latitude);
		
		return signum * result;
	}
	
	static public double longitudeFromString(String longitude) {
		double signum = longitude.endsWith("W") ? -1 : 1;
		
		double result = extractDegrees(longitude);
		result += extractMinutes(longitude) / 60.0;
		result += extractSeconds(longitude) / 3600.0;
		
		if(Double.compare(result, 180) > 0) throw new IllegalArgumentException("Longitude out of bounds: " + longitude);
		
		return signum * result;
	}
	
	static public String latitudeFromValue(double latitude) {
		boolean negative = Double.compare(latitude, 0) < 0;
		
		latitude = Math.abs(latitude);
		
		double degrees = Math.floor(latitude);
		double minutes = 60 * ( latitude - degrees );
		double seconds = ( minutes - Math.floor(minutes) ) * 60;
		
		return pad((long)degrees, 2) + "°" + pad((long)minutes, 2) + "'" + pad((long)seconds, 2) + "''" + ( negative ? "S" : "N" );
	}
	
	static public String longitudeFromValue(double longitude) {
		boolean negative = Double.compare(longitude, 0) < 0;
		
		longitude = Math.abs(longitude);
		
		double degrees = Math.floor(longitude);
		double minutes = 60 * ( longitude - degrees );
		double seconds = ( minutes - Math.floor(minutes) ) * 60;
		
		return pad((long)degrees, 3) + "°" + pad((long)minutes, 2) + "'" + pad((long)seconds, 2) + "''" + ( negative ? "W" : "E" );
	}
	
	static private String pad(long value, int length) {
		String padded = String.valueOf(value);
		
		while(padded.length() < length) 
			padded = "0" + padded;
		
		if(padded.length() > length)
			padded = padded.substring(0, length);
		
		return padded;
	}
}
