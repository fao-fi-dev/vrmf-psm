/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-model)
 */
package org.fao.fi.vrmf.psm.logbook.model.base.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@Entity
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@NoArgsConstructor @AllArgsConstructor @ToString @EqualsAndHashCode(callSuper=false)
public class VesselUnloadingData extends UnloadingData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6400735002703153854L;
	
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String vesselName;
	
	@ManyToOne(fetch = FetchType.EAGER, optional=false)
	@Getter @Setter @XmlElement private Country vesselRegistrationCountry;
	@Getter @Setter @XmlElement(required=true) @Column(nullable=false) private String vesselRegistrationNumber;
	
	@Getter @Setter @XmlElement private String destination;
}
