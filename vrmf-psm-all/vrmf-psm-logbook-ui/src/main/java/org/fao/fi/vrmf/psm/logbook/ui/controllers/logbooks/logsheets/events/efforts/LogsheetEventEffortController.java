/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks.logsheets.events.efforts;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.EffortsRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.CatchAndEffortPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.efforts.EffortsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.efforts.model.LogsheetEventEffortTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingListSelectionListener;

import no.tornado.databinding.BindingGroup;
import no.tornado.databinding.BindingStrategy;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 11, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 11, 2015
 */
@Named @Singleton
public class LogsheetEventEffortController extends BaseController {
	@Inject private CatchAndEffortPanel parentView;
	@Inject private EffortsPanel view;
	
	private BindingGroup effortBindings;

	@PostConstruct
	private void initialize() {
		model.registerListener((LogsheetEventListener)this);
		model.registerListener((LogsheetEventEffortListener)this);
		
		view.getEffortsTable().getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				int selectedIndex = view.getEffortsTable().getSelectedRow();
				
				Effort selectedEffort = null;
				
				List<Effort> events = model.getSelectedEvent() != null && model.getSelectedEvent().isEffortEvent() && ((EffortsRecord)model.getSelectedEvent()).getEfforts() != null ? ((EffortsRecord)model.getSelectedEvent()).getEfforts() : null;
				
				if(events != null && selectedIndex >= 0 && selectedIndex <= events.size() - 1) {
					selectedEffort = events.get(view.getEffortsTable().convertRowIndexToModel(selectedIndex));
				}
				
				boolean same = selectedEffort != null && selectedEffort.equals(model.getSelectedEffort());

				if(same || selectedEffort == null) return;
				
				if(application.confirmCancelUpdates(model.isEffortDirty() || model.getEffortStatus() == EditingStatus.ADDING)) {
					if(model.isEffortDirty() && model.getSelectedEffort() != null && model.getSelectedEffort().getId() != null) {
						doRevertEffort(model.getSelectedEffort());
					}
					
					model.select(selectedEffort, model.getEventStatus());
				} else {
					int currentlySelectedEffortIndex = events.indexOf(model.getSelectedEffort());
					
					currentlySelectedEffortIndex = view.getEffortsTable().convertRowIndexToView(currentlySelectedEffortIndex);
					
					view.getEffortsTable().setRowSelectionInterval(currentlySelectedEffortIndex, currentlySelectedEffortIndex);
					
					model.taintEffort();
				}
			}
		});
		
		view.getAddNew().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEffortAction();
			}
		});
		
		view.getUpdateEffort().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				updateEffortAction();
			}
		});
		
		view.getDeleteEffort().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				deleteEffortAction();
			}
		});
		
		view.getCancelEffort().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				cancelEffortAction();
			}
		});
		
		UIUtils.addValueChangeListener(view, new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				if(model.getSelectedEffort() != null) model.taintEffort();
			}
		});
	}
	
	private void applyDummyBinding() {
		bindEffort(new Effort());
		effortBindings.flushModelToUI();
	}
	
	private void bindEffort(Effort selected) {
		unbindEffort();
		
		if(selected != null) {
			effortBindings = new BindingGroup(selected);
			effortBindings.setDefaultBindingStrategy(BindingStrategy.ONFLUSH);

			effortBindings.add(view.getEffortType(), "effortType");
			effortBindings.add(view.getEffortValue(), "effortValue");

			effortBindings.bind();
		}
	}
	
	private void unbindEffort() {
		if(effortBindings != null)
			effortBindings.unbind();
	}
	
	private void doRevertEffort(Effort toRevert) {
		model.revert(toRevert);
	}
	
	private void addEffortAction() {
		model.addLogsheetEventEffort(new Effort());
		
		model.untaintEffort();
	}
	
	private void cancelEffortEditingAction() {
		if(model.getEffortStatus() == EditingStatus.ADDING) {
			if(model.getSelectedEffort() != null && model.getSelectedEffort().getId() == null) ((EffortsRecord)model.getSelectedEvent()).getEfforts().remove(model.getSelectedEffort());
		}
		
		model.select((Effort)null);

		applyDummyBinding();
		
		view.getEffortsTable().clearSelection();
	}
	
	private void updateEffortAction() {
		effortBindings.flushUIToModel();
		
		List<CheckResult> checks = model.getSelectedEffort().checkCorrectness();
		
		StringBuilder messages = new StringBuilder();
		
		if(checks != null && !checks.isEmpty()) {
			for(CheckResult in : checks) {
				messages.
					append("[").append(in.getType().getCode()).append("] : ").
					append(in.getMessage()).
					append("\n");
			}
		}
		
		if(CheckResultHelper.hasErrors(checks)) {
			error("Cannot update selected effort", messages.toString());
			
			log().error("Cannot update selected effort due to the following reasons:");

			for(CheckResult in : checks) {
				log().error("{} >> {}", in.getType().getCode(), in.getMessage());
			}
		} else {
			boolean proceed = !CheckResultHelper.hasWarnings(checks);
			
			if(!proceed) {
				proceed = 
					UIUtils.confirm(
						"Please confirm", 
						"Current effort does contain the following inconsistencies:\n\n" + 
							messages.toString() + "\n" +
						"Are you sure you want to proceed?");
			}
			
			if(proceed) {
				logsheetEventEffortUpdated(model.getSelectedEffort());
				
				model.taintEvent();
				model.untaintEffort();
				
				model.changeLogsheetEventEffortStatus(EditingStatus.NOT_SET);
				
				cancelEffortEditingAction();
			}
		}
	}
	
	private void deleteEffortAction() {
		if(UIUtils.confirm(
			"Please confirm", 
			"Are you sure you want to delete the selected effort?"
		)) {
			List<Effort> logsheetEvents = ((EffortsRecord)model.getSelectedEvent()).getEfforts();
			
			Effort selected = model.getSelectedEffort();
			logsheetEvents.remove(selected);

			logsheetEventUpdated(model.getSelectedEvent());
			
			if(selected.getId() != null) model.taintEvent();

			model.select((Effort)null);
			
			cancelEffortEditingAction();
			
			updateEffortsTable();
		}
	}
	
	private void cancelEffortAction() {
		cancelEffortEditingAction();
	}
	
	private void updateEffortsTable() {
		JTable table = view.getEffortsTable();
		
		((LogsheetEventEffortTableModel)table.getModel()).update(model.getSelectedEvent() == null ? null : ((EffortsRecord)model.getSelectedEvent()).getEfforts());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventSelected(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventSelected(Event selected) {
		if(selected == null || !selected.isEffortEvent()) {
			view.getEffortsTable().getSelectionModel().clearSelection();
		} else {
			boolean isEditing = selected.isCompleted() && model.getEventStatus() == EditingStatus.EDITING;
			
			boolean displayEfforts = selected.isEffortEvent();

			List<Effort> logsheetEvents = ((EffortsRecord)selected).getEfforts();
			
			if(logsheetEvents == null) ((EffortsRecord)selected).setEfforts(logsheetEvents = new ArrayList<Effort>());
			
			((LogsheetEventEffortTableModel)view.getEffortsTable().getModel()).update(logsheetEvents);
			
			view.getAddNew().setEnabled(isEditing && displayEfforts && ( model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING ));
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventEffortStatusChanged(EditingStatus previous, EditingStatus current) {
		boolean enable = current == EditingStatus.ADDING || current == EditingStatus.EDITING || model.isEffortDirty();
		
		boolean enableUpdate = enable;
		boolean enableDelete = enable && current != EditingStatus.ADDING;
		boolean enableCancel = enable;
		
		view.getUpdateEffort().setEnabled(enableUpdate);
		view.getDeleteEffort().setEnabled(enableDelete);
		view.getCancelEffort().setEnabled(enableCancel);
		
		view.getEffortType().setEnabled(enable);
		view.getEffortValue().setEnabled(enable);
		
		view.getAddNew().setEnabled(!enable && current != EditingStatus.VIEWING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortAdded(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventEffortAdded(Effort added) {
		model.select(added, EditingStatus.ADDING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortUpdated(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventEffortUpdated(Effort updated) {
		updateEffortsTable();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortRemoved(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventEffortRemoved(Effort deleted) {
		updateEffortsTable();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortSelected(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventEffortSelected(Effort selected) {
		if(selected != null) {
			bindEffort(selected);
			
			model.untaintEffort();
		}
		
		boolean isEditing = selected != null && ( model.getEffortStatus() == EditingStatus.ADDING || model.getEffortStatus() == EditingStatus.EDITING );
		
		parentView.getCatchAndEffortTabs().setEnabledAt(1, !isEditing && model.getSelectedEvent() != null && model.getSelectedEvent().isCatchEvent());
		parentView.getCatchAndEffortTabs().setEnabledAt(2, !isEditing && model.getSelectedEvent() != null && model.getSelectedEvent().isCatchEvent());
		
		view.getEffortsTable().setEnabled(!isEditing);
	}
}