/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.EffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.TransshipmentEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.EffortsRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.services.data.LogbooksServices;
import org.fao.fi.vrmf.psm.logbook.services.data.LogsheetEventCatchesServices;
import org.fao.fi.vrmf.psm.logbook.services.data.LogsheetEventDiscardsServices;
import org.fao.fi.vrmf.psm.logbook.services.data.LogsheetEventEffortsServices;
import org.fao.fi.vrmf.psm.logbook.services.data.LogsheetEventsServices;
import org.fao.fi.vrmf.psm.logbook.services.data.LogsheetsServices;
import org.fao.fi.vrmf.psm.logbook.services.data.VesselsServices;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.spi.DataModelStatusListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 7, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 7, 2015
 */
@Named @Singleton
@EqualsAndHashCode @NoArgsConstructor @AllArgsConstructor
public class DataModel implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1211678004147810591L;
	
	final public static String MODEL_VERSION = "1.0.0";
	
	@Getter private EditingStatus vesselStatus = EditingStatus.NOT_SET;
	@Getter private EditingStatus logbookStatus = EditingStatus.NOT_SET;
	@Getter private EditingStatus logsheetStatus = EditingStatus.NOT_SET;
	@Getter private EditingStatus eventStatus = EditingStatus.NOT_SET;
	@Getter private EditingStatus effortStatus = EditingStatus.NOT_SET;
	@Getter private EditingStatus catchStatus = EditingStatus.NOT_SET;
	@Getter private EditingStatus discardStatus = EditingStatus.NOT_SET;
	
	@Getter private boolean isVesselDirty = false;
	@Getter private boolean isLogbookDirty = false;
	@Getter private boolean isLogsheetDirty = false;
	@Getter private boolean isEventDirty = false;
	@Getter private boolean isEffortDirty = false;
	@Getter private boolean isCatchDirty = false;
	@Getter private boolean isDiscardDirty = false;
	
	@Getter private VesselData selectedVessel;
	@Getter private Logbook selectedLogbook;
	@Getter private Logsheet selectedLogsheet;
	@Getter private Event selectedEvent;
	@Getter private Effort selectedEffort;
	@Getter private Catches selectedCatch;
	@Getter private Discards selectedDiscard;
	
	@Getter private List<VesselData> vessels;
	@Getter private List<Logbook> logbooks;
	
	@Inject private VesselsServices vesselServices;
	@Inject private LogbooksServices logbookServices;
	@Inject private LogsheetsServices logsheetServices;
	@Inject private LogsheetEventsServices logsheetEventServices;
	@Inject private LogsheetEventEffortsServices logsheetEventEffortServices;
	@Inject private LogsheetEventCatchesServices logsheetEventCatchServices;
	@Inject private LogsheetEventDiscardsServices logsheetEventDiscardServices;

	private List<DataModelStatusListener> statusListeners = new ArrayList<DataModelStatusListener>();
	private List<VesselListener> vesselListeners = new ArrayList<VesselListener>();
	private List<LogbookListener> logbookListeners = new ArrayList<LogbookListener>();
	private List<LogsheetListener> logsheetListeners = new ArrayList<LogsheetListener>();
	private List<LogsheetEventListener> eventListeners = new ArrayList<LogsheetEventListener>();
	private List<LogsheetEventEffortListener> effortListeners = new ArrayList<LogsheetEventEffortListener>();
	private List<LogsheetEventCatchListener> catchListeners = new ArrayList<LogsheetEventCatchListener>();
	private List<LogsheetEventDiscardListener> discardListeners = new ArrayList<LogsheetEventDiscardListener>();
	
	@PostConstruct
	public void initialize() {
		vessels = vesselServices.allVessels();
		logbooks = logbookServices.allLogbooks();
		
		if(vessels == null) vessels = new ArrayList<VesselData>();
		if(logbooks == null) logbooks = new ArrayList<Logbook>();
		
		isVesselDirty = false;
		isLogbookDirty = false;
		isLogsheetDirty = false;
		isEventDirty = false;
		isEffortDirty = false;
		isCatchDirty = false;
		isDiscardDirty = false;
		
		select((VesselData)null, EditingStatus.NOT_SET);
		select((Logbook)null, EditingStatus.NOT_SET);
	}
	
	public boolean isModelTainted() {
		return isVesselDirty || isDataModelTainted();
	}
	
	public boolean isDataModelTainted() {
		return isLogbookDirty || isLogsheetDirty || isEventModelTainted();
	}
	
	public boolean isEventModelTainted() {
		return isEventDirty || isEffortDirty || isCatchDirty || isDiscardDirty;
	}
	
	public void untaintVessel() {
		isVesselDirty = false;
		
		notifyStatusChanged();
	}
	
	public void taintVessel() {
		isVesselDirty = true;
		
		notifyStatusChanged();
	}
	
	public void untaintLogbook() {
		isLogbookDirty = false;
		
		untaintLogsheet();
	}
	
	public void taintLogbook() {
		isLogbookDirty = true;
		
		notifyStatusChanged();
	}
	
	public void untaintLogsheet() {
		isLogsheetDirty = false;
		
		untaintEvent();
	}
	
	public void taintLogsheet() {
		isLogsheetDirty = true;
		
		notifyStatusChanged();
	}
	
	public void untaintEvent() {
		isEventDirty = false;
		
		notifyStatusChanged();
	}
	
	public void taintEvent() {
		isEventDirty = true;
		
		notifyStatusChanged();
	}
	
	public void untaintEffort() {
		isEffortDirty = false;
		
		notifyStatusChanged();
	}
	
	public void taintEffort() {
		isEffortDirty = true;
		
		notifyStatusChanged();
	}
	
	public void untaintCatch() {
		isCatchDirty = false;
		
		notifyStatusChanged();
	}
	
	public void taintCatch() {
		isCatchDirty = true;
		
		notifyStatusChanged();
	}
	
	public void untaintDiscard() {
		isDiscardDirty = false;
		
		notifyStatusChanged();
	}
	
	public void taintDiscard() {
		isDiscardDirty = true;
		
		notifyStatusChanged();
	}
	
	private void notifyStatusChanged() {
		boolean status = isModelTainted();
		
		if(statusListeners != null)
			for(DataModelStatusListener in : statusListeners) 
				in.modelStatusChanged(status);
	}
	
	public List<Logsheet> getCurrentLogsheets() {
		return isLogbookSelected() ? selectedLogbook.getLogsheets() : null;
	}
	
	public void changeVesselStatus(EditingStatus status) {
		EditingStatus previous = vesselStatus;
		
		vesselStatus = status;
		
		for(VesselListener in : vesselListeners)
			in.vesselStatusChanged(previous, vesselStatus);
	}
	
	public VesselData addVessel() {
		VesselData newVessel = new VesselData();
		
		vessels.add(newVessel);
		
		for(VesselListener in : vesselListeners)
			in.vesselAdded(newVessel);
		
		taintLogbook();
		
		return newVessel;
	}
	
	public VesselData updateVessel() {
		if(selectedVessel == null) throw new IllegalArgumentException("No selected vessel");
		
		selectedVessel = vesselServices.update(selectedVessel);
				
		for(VesselListener in : vesselListeners)
			in.vesselUpdated(selectedVessel);
		
		untaintVessel();
		
		return select(selectedVessel);
	}

	public VesselData deleteVessel() {
		if(selectedVessel == null) throw new IllegalArgumentException("No selected vessel");
		
		if(vessels.contains(selectedVessel)) {
			vesselServices.delete(selectedVessel);

			vessels.remove(selectedVessel);
			
			for(VesselListener in : vesselListeners)
				in.vesselRemoved(selectedVessel);
		} else {
			throw new IllegalStateException("The vessel being deleted could not be found in the current list of vessels");
		}
		
		select((VesselData)null, EditingStatus.NOT_SET);
		
		untaintVessel();
		
		return selectedVessel;
	}
	
	public VesselData select(VesselData vessel) {
		return select(vessel, null);
	}
	
	public VesselData select(VesselData vessel, EditingStatus status) {
		if(same(selectedVessel, vessel)) return vessel;
		
		try {
			return selectedVessel = vessel == null ? null : vesselServices.reload(vessel);
		} finally {
			if(vessel == null) changeVesselStatus(EditingStatus.NOT_SET);
			else if(status != null) changeVesselStatus(status);

			for(VesselListener in : vesselListeners)
				in.vesselSelected(selectedVessel);
			
			if(vessel == null || vessel.getId() != null) {
				untaintLogbook();
				untaintVessel();
			}
		}
	}
	
	public void refreshLogbookStatus() { changeLogbookStatus(logbookStatus); }
	
	public void changeLogbookStatus(EditingStatus status) {
		EditingStatus previous = logbookStatus;
		
		logbookStatus = status;
		
		for(LogbookListener in : logbookListeners)
			in.logbookStatusChanged(previous, logbookStatus);
	}
	
	public Logbook addLogbook() {
		Logbook newLogbook = logbookServices.update(Logbook.builder().version(MODEL_VERSION).year(Calendar.getInstance().get(Calendar.YEAR)).creationDate(new Date()).build());
		
		logbooks.add(newLogbook);
		
		for(LogbookListener in : logbookListeners)
			in.logbookAdded(newLogbook);
		
		return newLogbook;
	}
	
	public Logbook updateLogbook() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		
		selectedLogbook = logbookServices.update(selectedLogbook);
				
		for(LogbookListener in : logbookListeners)
			in.logbookUpdated(selectedLogbook);
		
		untaintLogbook();
		
		return select(selectedLogbook);
	}

	public Logbook deleteLogbook() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		
		logbooks.remove(logbookServices.delete(selectedLogbook));
		
		for(LogbookListener in : logbookListeners)
			in.logbookRemoved(selectedLogbook);
		
		select((Logbook)null, EditingStatus.NOT_SET);
		
		untaintLogbook();
		
		return selectedLogbook;
	}
	
	public Logbook select(Logbook logbook) {
		return select(logbook, null);
	}
	
	public Logbook select(Logbook logbook, EditingStatus status) {
		if(same(selectedLogbook, logbook)) return logbook;
		
		try {
			return selectedLogbook = logbook == null ? null : logbookServices.reload(logbook);
		} finally {
			if(logbook == null) changeLogbookStatus(EditingStatus.NOT_SET);
			else if(status != null) changeLogbookStatus(status);

			for(LogbookListener in : logbookListeners)
				in.logbookSelected(selectedLogbook);

			if(logbook == null || !logbook.equals(selectedLogbook))
				select((Logsheet)null);
			
			if(logbook == null || logbook.getId() != null) untaintLogbook();
		}
	}
	
	public void refreshLogsheetStatus() { changeLogsheetStatus(logsheetStatus); }
	
	public void changeLogsheetStatus(EditingStatus status) {
		EditingStatus previous = logsheetStatus;
		
		logsheetStatus = status;
		
		for(LogsheetListener in : logsheetListeners)
			in.logsheetStatusChanged(previous, logsheetStatus);
	}

	public Logsheet addLogsheet() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		
		Logsheet toAdd = new Logsheet();
		
		updateLogbook();
		
		select(toAdd, EditingStatus.ADDING);
		
		for(LogsheetListener in : logsheetListeners)
			in.logsheetAdded(toAdd);
		
		taintLogsheet();
		
		return toAdd;
	}
	
	public Logsheet updateLogsheet() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		
		boolean isNew = selectedLogsheet.getId() == null && !selectedLogbook.getLogsheets().contains(selectedLogsheet);
		
		if(isNew) selectedLogbook.getLogsheets().add(selectedLogsheet);
		
		Logsheet current = ( selectedLogsheet = logsheetServices.update(selectedLogbook, selectedLogsheet) );
				
		for(LogsheetListener in : logsheetListeners) {
			if(isNew) in.logsheetAdded(current);
			else in.logsheetUpdated(current);
		}
		
		updateLogbook();
		
		return select(current);
	}

	public Logsheet deleteLogsheet() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		
		if(selectedLogbook.getLogsheets().remove(selectedLogsheet)) {
			logsheetServices.delete(selectedLogbook, selectedLogsheet);
			
			for(LogsheetListener in : logsheetListeners)
				in.logsheetRemoved(selectedLogsheet);

			updateLogbook();
		}
		
		select((Logsheet)null, EditingStatus.NOT_SET);
		
		untaintLogsheet();
		
		return selectedLogsheet;
	}
	
	public Logsheet select(Logsheet logsheet) {
		return select(logsheet, null, false);
	}

	public Logsheet select(Logsheet logsheet, boolean force) {
		return select(logsheet, null, force);
	}
	
	public Logsheet select(Logsheet logsheet, EditingStatus status) {
		return select(logsheet, status, false);
	}
	
	public Logsheet select(Logsheet logsheet, EditingStatus status, boolean force) {
		if(!force && same(selectedLogsheet, logsheet)) return logsheet;
		
		try {
			return selectedLogsheet = logsheet;
		} finally {
			if(logsheet == null) changeLogsheetStatus(EditingStatus.NOT_SET);
			else if(status != null) changeLogsheetStatus(status);
						
			for(LogsheetListener in : logsheetListeners)
				in.logsheetSelected(selectedLogsheet);
			
			if(logsheet == null || !logsheet.equals(selectedLogsheet))
				select((Event)null);
			
			if(logsheet == null || logsheet.getId() != null) untaintLogsheet();
		}
	}
	
	public void refreshLogsheetEventStatus() { changeLogsheetEventStatus(eventStatus); }
	
	public void changeLogsheetEventStatus(EditingStatus status) {
		EditingStatus previous = eventStatus;
		
		eventStatus = status;
		
		for(LogsheetEventListener in : eventListeners)
			in.logsheetEventStatusChanged(previous, eventStatus);
	}
	
	public Event addLogsheetEvent(Event toAdd) {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(toAdd == null) throw new IllegalArgumentException("No selected event");
				
		selectedLogsheet.getEvents().add(toAdd);
		
		changeLogsheetEventStatus(EditingStatus.ADDING);
		
		for(LogsheetEventListener in : eventListeners)
			in.logsheetEventAdded(toAdd);
		
		taintEvent();
		
		return toAdd;
	}
	
	public Event updateLogsheetEvent() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");

		logsheetServices.update(selectedLogbook, selectedLogsheet);
		
		for(LogsheetEventListener in : eventListeners)
			in.logsheetEventUpdated(selectedEvent);
		
		for(LogsheetListener in : logsheetListeners)
			in.logsheetUpdated(selectedLogsheet);
		
		for(LogbookListener in : logbookListeners)
			in.logbookUpdated(selectedLogbook);
		
		untaintEvent();
		
		return select(selectedEvent);
	}

	public Event deleteLogsheetEvent() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");

		if(selectedLogsheet.getEvents().remove(selectedEvent)) {
			for(LogsheetEventListener in : eventListeners)
				in.logsheetEventRemoved(selectedEvent);

			updateLogsheet();
		}
		
		select((Event)null, EditingStatus.NOT_SET);
		
		untaintEvent();
		
		return selectedEvent;
	}
	
	public Event select(Event event) {
		return select(event, null);
	}
	
	public Event select(Event event, EditingStatus status) {
		if(same(selectedEvent, event)) return event;
		
		try {
			return selectedEvent = event;
		} finally {
			if(event == null) changeLogsheetEventStatus(EditingStatus.NOT_SET);
			else if(status != null) changeLogsheetEventStatus(status);
			
			for(LogsheetEventListener in : eventListeners)
				in.logsheetEventSelected(selectedEvent);
			
			if(event == null || !event.equals(selectedEvent)) {
				select((Effort)null);
				select((Catches)null); 
				select((Discards)null);
			}
			
			if(event == null || event.getId() != null) untaintEvent();
		}
	}
	
	//////////////////////
	
	public void refreshLogsheetEventEffortStatus() { changeLogsheetEventEffortStatus(effortStatus); }
	
	public void changeLogsheetEventEffortStatus(EditingStatus status) {
		EditingStatus previous = effortStatus;
		
		effortStatus = status;
		
		for(LogsheetEventEffortListener in : effortListeners)
			in.logsheetEventEffortStatusChanged(previous, effortStatus);
	}
	
	public Effort addLogsheetEventEffort(Effort toAdd) {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		
		if(!selectedEvent.isEffortEvent()) throw new IllegalArgumentException("Selected event does not expects efforts");
		if(toAdd == null) throw new IllegalArgumentException("No selected effort");
				
		EffortsRecord event = (EffortsRecord)selectedEvent;
		
		if(event.getEfforts() == null) event.setEfforts(new ArrayList<Effort>());
		
		event.getEfforts().add(toAdd);
		
		changeLogsheetEventEffortStatus(EditingStatus.ADDING);
		
		for(LogsheetEventEffortListener in : effortListeners)
			in.logsheetEventEffortAdded(toAdd);
		
		taintEffort();
		
		return toAdd;
	}
	
	public Effort updateLogsheetEventEffort() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		if(!(selectedEvent instanceof EffortEvent)) throw new IllegalArgumentException("Selected event does not expects efforts");
		if(selectedEffort == null) throw new IllegalArgumentException("No selected effort");
		
		logsheetServices.update(selectedLogbook, selectedLogsheet);
		
		for(LogsheetEventEffortListener in : effortListeners)
			in.logsheetEventEffortUpdated(selectedEffort);
		
		for(LogsheetEventListener in : eventListeners)
			in.logsheetEventUpdated(selectedEvent);
		
		for(LogsheetListener in : logsheetListeners)
			in.logsheetUpdated(selectedLogsheet);
		
		for(LogbookListener in : logbookListeners)
			in.logbookUpdated(selectedLogbook);
		
		untaintEffort();
		
		return select(selectedEffort);
	}

	public Effort deleteLogsheetEventEffort() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		if(!(selectedEvent instanceof EffortEvent)) throw new IllegalArgumentException("Selected event does not expects efforts");
		if(selectedEffort == null) throw new IllegalArgumentException("No selected effort");
		
		if(((EffortsRecord)selectedEvent).getEfforts().remove(selectedEffort)) {
			for(LogsheetEventEffortListener in : effortListeners)
				in.logsheetEventEffortRemoved(selectedEffort);

			updateLogsheetEvent();
		}
		
		select((Effort)null, EditingStatus.NOT_SET);
		
		untaintEffort();
		
		return selectedEffort;
	}
	
	public Effort select(Effort effort) {
		return select(effort, null);
	}
	
	public Effort select(Effort effort, EditingStatus status) {
		if(same(selectedEffort, effort)) return effort;
		
		try {
			return selectedEffort = effort;
		} finally {
			if(effort == null) changeLogsheetEventEffortStatus(EditingStatus.NOT_SET);
			else if(status != null) changeLogsheetEventEffortStatus(status);
			
			for(LogsheetEventEffortListener in : effortListeners)
				in.logsheetEventEffortSelected(selectedEffort);
			
			if(effort == null || effort.getId() != null) untaintEffort();
		}
	}
	
	//////////////////////
	
	public void refreshLogsheetEventCatchStatus() { changeLogsheetEventCatchStatus(catchStatus); }
	
	public void changeLogsheetEventCatchStatus(EditingStatus status) {
		EditingStatus previous = catchStatus;
		
		catchStatus = status;
		
		for(LogsheetEventCatchListener in : catchListeners)
			in.logsheetEventCatchStatusChanged(previous, catchStatus);
	}
	
	public Catches addLogsheetEventCatch(Catches toAdd) {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		
		if(!(selectedEvent instanceof CatchEvent)) throw new IllegalArgumentException("Selected event does not expects catches");
		if(toAdd == null) throw new IllegalArgumentException("No selected catch");
				
		CatchesRecord event = ((CatchesRecord)selectedEvent);

		if(event.getCatches() == null) event.setCatches(new ArrayList<Catches>());
		
		event.getCatches().add(toAdd);
		
		changeLogsheetEventCatchStatus(EditingStatus.ADDING);
		
		for(LogsheetEventCatchListener in : catchListeners)
			in.logsheetEventCatchAdded(toAdd);
		
		taintCatch();
		
		return toAdd;
	}
	
	public Catches addLogsheetTransshipmentEventCatch(Catches toAdd) {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		
		if(!(selectedEvent instanceof TransshipmentEvent)) throw new IllegalArgumentException("Selected event does not expects catches");
		if(toAdd == null) throw new IllegalArgumentException("No selected catch");
				
		TransshipmentEvent event = ((TransshipmentEvent)selectedEvent);

		if(event.getCatches() == null) event.setCatches(new ArrayList<Catches>());
		
		event.getCatches().add(toAdd);
		
		changeLogsheetEventCatchStatus(EditingStatus.ADDING);
		
		for(LogsheetEventCatchListener in : catchListeners)
			in.logsheetEventCatchAdded(toAdd);
		
		taintCatch();
		
		return toAdd;
	}
	
	public Catches updateLogsheetEventCatch() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		if(!(selectedEvent instanceof CatchEvent)) throw new IllegalArgumentException("Selected event does not expects catches");
		if(selectedCatch == null) throw new IllegalArgumentException("No selected catch");
		
		logsheetServices.update(selectedLogbook, selectedLogsheet);
		
		for(LogsheetEventCatchListener in : catchListeners)
			in.logsheetEventCatchUpdated(selectedCatch);
		
		for(LogsheetEventListener in : eventListeners)
			in.logsheetEventUpdated(selectedEvent);
		
		for(LogsheetListener in : logsheetListeners)
			in.logsheetUpdated(selectedLogsheet);
		
		for(LogbookListener in : logbookListeners)
			in.logbookUpdated(selectedLogbook);
		
		untaintCatch();
		
		return select(selectedCatch);
	}

	public Catches deleteLogsheetEventCatch() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		if(!(selectedEvent instanceof CatchEvent)) throw new IllegalArgumentException("Selected event does not expects catches");
		if(selectedCatch == null) throw new IllegalArgumentException("No selected catch");
		
		if(((CatchesRecord)selectedEvent).getCatches().remove(selectedCatch)) {
			for(LogsheetEventCatchListener in : catchListeners)
				in.logsheetEventCatchRemoved(selectedCatch);

			updateLogsheetEvent();
		}
		
		select((Catches)null, EditingStatus.NOT_SET);
		
		untaintCatch();
		
		return selectedCatch;
	}
	
	public Catches select(Catches catches) {
		return select(catches, null);
	}
	
	public Catches select(Catches catches, EditingStatus status) {
		if(same(selectedCatch, catches)) return catches;
		
		try {
			return selectedCatch = catches;
		} finally {
			if(catches == null) changeLogsheetEventCatchStatus(EditingStatus.NOT_SET);
			else if(status != null) changeLogsheetEventCatchStatus(status);
			
			for(LogsheetEventCatchListener in : catchListeners)
				in.logsheetEventCatchSelected(selectedCatch);
			
			if(catches == null || catches.getId() != null) untaintCatch();
		}
	}
	
	//////////////////////
	
	public void refreshLogsheetEventDiscardStatus() { changeLogsheetEventDiscardStatus(catchStatus); }
	
	public void changeLogsheetEventDiscardStatus(EditingStatus status) {
		EditingStatus previous = discardStatus;
		
		discardStatus = status;
		
		for(LogsheetEventDiscardListener in : discardListeners)
			in.logsheetEventDiscardStatusChanged(previous, discardStatus);
	}
	
	public Discards addLogsheetEventDiscard(Discards toAdd) {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		
		if(!(selectedEvent instanceof CatchEvent)) throw new IllegalArgumentException("Selected event does not expects discards");
		if(toAdd == null) throw new IllegalArgumentException("No selected discard");
				
		CatchesRecord event = ((CatchesRecord)selectedEvent);

		if(event.getDiscards() == null) event.setDiscards(new ArrayList<Discards>());
		
		event.getDiscards().add(toAdd);
		
		changeLogsheetEventDiscardStatus(EditingStatus.ADDING);
		
		for(LogsheetEventDiscardListener in : discardListeners)
			in.logsheetEventDiscardAdded(toAdd);
		
		taintDiscard();
		
		return toAdd;
	}
	
	public Discards updateLogsheetEventDiscards() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		if(!(selectedEvent instanceof CatchEvent)) throw new IllegalArgumentException("Selected event does not expects discards");
		if(selectedDiscard == null) throw new IllegalArgumentException("No selected discard");
		
		logsheetServices.update(selectedLogbook, selectedLogsheet);
		
		for(LogsheetEventDiscardListener in : discardListeners)
			in.logsheetEventDiscardUpdated(selectedDiscard);
		
		for(LogsheetEventListener in : eventListeners)
			in.logsheetEventUpdated(selectedEvent);
		
		for(LogsheetListener in : logsheetListeners)
			in.logsheetUpdated(selectedLogsheet);
		
		for(LogbookListener in : logbookListeners)
			in.logbookUpdated(selectedLogbook);
		
		untaintDiscard();
		
		return select(selectedDiscard);
	}

	public Discards deleteLogsheetEventDiscard() {
		if(selectedLogbook == null) throw new IllegalArgumentException("No selected logbook");
		if(selectedLogsheet == null) throw new IllegalArgumentException("No selected logsheet");
		if(selectedEvent == null) throw new IllegalArgumentException("No selected event");
		if(!(selectedEvent instanceof CatchEvent)) throw new IllegalArgumentException("Selected event does not expects discards");
		if(selectedDiscard == null) throw new IllegalArgumentException("No selected discard");
		
		if(((CatchesRecord)selectedEvent).getDiscards().remove(selectedDiscard)) {
			for(LogsheetEventDiscardListener in : discardListeners)
				in.logsheetEventDiscardRemoved(selectedDiscard);

			updateLogsheetEvent();
		}
		
		select((Discards)null, EditingStatus.NOT_SET);
		
		untaintDiscard();
		
		return selectedDiscard;
	}
	
	public Discards select(Discards discard) {
		return select(discard, null);
	}
	
	public Discards select(Discards discards, EditingStatus status) {
		if(same(selectedDiscard, discards)) return discards;
		
		try {
			return selectedDiscard = discards;
		} finally {
			if(discards == null) changeLogsheetEventDiscardStatus(EditingStatus.NOT_SET);
			else if(status != null) changeLogsheetEventDiscardStatus(status);
			
			for(LogsheetEventDiscardListener in : discardListeners)
				in.logsheetEventDiscardSelected(selectedDiscard);
			
			if(discards == null || discards.getId() != null) untaintDiscard();
		}
	}
	
	//////////////////////
	
	public VesselData revert(VesselData toRevert) {
		try {
			if(toRevert == null || toRevert.getId() == null) return toRevert;
			
			return vesselServices.reload(toRevert);
		} finally {
			untaintVessel();
		}
	}
	
	public Logbook revert(Logbook toRevert) {
		try {
			if(toRevert == null || toRevert.getId() == null) return toRevert;
			
			return logbookServices.reload(toRevert);
		} finally {
			untaintLogbook();
		}
	}
	
	public Logsheet revert(Logsheet toRevert) {
		try {
			if(toRevert == null || toRevert.getId() == null) return toRevert;
			
			return logsheetServices.reload(toRevert);
		} finally {
			untaintLogsheet();
		}
	}
	
	public Event revert(Event toRevert) {
		try {
			if(toRevert == null || toRevert.getId() == null) return toRevert;
		
			if(toRevert.isEffortEvent()) {
				EffortsRecord withEfforts = (EffortsRecord)toRevert;
				
				if(withEfforts.getEfforts() != null) {
					Iterator<Effort> efforts = withEfforts.getEfforts().iterator();
					Effort current;
					
					while(efforts.hasNext()) {
						current = efforts.next();
						
						if(current == null || current.getId() == null)
							efforts.remove();
					}
				}
			}

			if(toRevert.isCatchEvent()) {
				CatchesRecord withCatches = (CatchesRecord)toRevert;
				
				if(withCatches.getCatches() != null) {
					Iterator<Catches> catches = withCatches.getCatches().iterator();
					Catches current;
					
					while(catches.hasNext()) {
						current = catches.next();
						
						if(current == null || current.getId() == null)
							catches.remove();
					}
				}
				
				if(withCatches.getDiscards() != null) {
					Iterator<Discards> discards = withCatches.getDiscards().iterator();
					Discards current;
					
					while(discards.hasNext()) {
						current = discards.next();
						
						if(current == null || current.getId() == null)
							discards.remove();
					}
				}
			}
			
			return logsheetEventServices.reload(toRevert);
		} finally {
			untaintEvent();
		}
	}
	
	public Effort revert(Effort toRevert) {
		try {
			if(toRevert == null || toRevert.getId() == null) return toRevert;
		
			return logsheetEventEffortServices.reload(toRevert);
		} finally {
			untaintEffort();
		}
	}
	
	public Catches revert(Catches toRevert) {
		try {
			if(toRevert == null || toRevert.getId() == null) return toRevert;
		
			return logsheetEventCatchServices.reload(toRevert);
		} finally {
			untaintCatch();
		}
	}
	
	public Discards revert(Discards toRevert) {
		try {
			if(toRevert == null || toRevert.getId() == null) return toRevert;
		
			return logsheetEventDiscardServices.reload(toRevert);
		} finally {
			untaintDiscard();
		}
	}
	
	public boolean isLogbookSelected() {
		return selectedLogbook != null;
	}
	
	public boolean isLogsheetSelected() {
		return selectedLogsheet != null;
	}
	
	public boolean isEventSelected() {
		return selectedEvent != null;
	}
	
	public boolean isNewLogbook() {
		return isLogbookSelected() && selectedLogbook.getId() == null;
	}
	
	public boolean isNewLogsheet() {
		return isLogsheetSelected() && selectedLogsheet.getId() == null;
	}
	
	public boolean isNewEvent() {
		return isEventSelected() && selectedEvent.getId() == null;
	}
	
	public void registerListener(DataModelStatusListener listener) {
		statusListeners.add(listener);
	}
	
	public void registerListener(VesselListener listener) {
		vesselListeners.add(listener);
	}
	
	public void registerListener(LogbookListener listener) {
		logbookListeners.add(listener);
	}
	
	public void registerListener(LogsheetListener listener) {
		logsheetListeners.add(listener);
	}
	
	public void registerListener(LogsheetEventListener listener) {
		eventListeners.add(listener);
	}
	
	public void registerListener(LogsheetEventEffortListener listener) {
		effortListeners.add(listener);
	}
	
	public void registerListener(LogsheetEventCatchListener listener) {
		catchListeners.add(listener);
	}
	
	public void registerListener(LogsheetEventDiscardListener listener) {
		discardListeners.add(listener);
	}
	
	private boolean same(Object first, Object second) {
		return first == null ? second == null : first.equals(second);
	}
}