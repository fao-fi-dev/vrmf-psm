/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.vessels;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.VesselType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.model.VesselTableModel;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CodedEntityTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CountryTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.DefaultTableHeaderRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.StringTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class VesselsListPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8872498289521272459L;
	
	@Getter private JNoFocusOutlineButton addVessel;
	@Getter private JNoFocusOutlineButton back;

	@Getter private JTable vesselsTable;
	
	public VesselsListPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.rowHeights = new int[] {0, 32};
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);
		
		JPanel vessels = new JPanel();
		vessels.setBorder(new TitledBorder(null, "Vessels", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_vessels = new GridBagConstraints();
		gbc_vessels.weightx = 100.0;
		gbc_vessels.weighty = 100.0;
		gbc_vessels.fill = GridBagConstraints.BOTH;
		gbc_vessels.insets = new Insets(5, 5, 5, 5);
		gbc_vessels.anchor = GridBagConstraints.NORTH;
		gbc_vessels.gridx = 0;
		gbc_vessels.gridy = 0;
		this.add(vessels, gbc_vessels);
		GridBagLayout gbl_vessels = new GridBagLayout();
		gbl_vessels.columnWidths = new int[0];
		gbl_vessels.rowHeights = new int[0];
		gbl_vessels.columnWeights = new double[0];
		gbl_vessels.rowWeights = new double[0];
		vessels.setLayout(gbl_vessels);

		vesselsTable = new JTable(new VesselTableModel(new ArrayList<VesselData>())) {
			/** Field serialVersionUID */
			private static final long serialVersionUID = 5210252037194577047L;
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		vesselsTable.setAutoCreateRowSorter(true);

		vesselsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		vesselsTable.getTableHeader().setReorderingAllowed(false);
		
		vesselsTable.setDefaultRenderer(String.class, new StringTableCellRenderer());
		vesselsTable.setDefaultRenderer(Country.class, new CountryTableCellRenderer());
		vesselsTable.setDefaultRenderer(VesselType.class, new CodedEntityTableCellRenderer());
		
		vesselsTable.getTableHeader().setDefaultRenderer(new DefaultTableHeaderRenderer(vesselsTable));
		
		GridBagConstraints gbc_vesselsTable = new GridBagConstraints();
		gbc_vesselsTable.weighty = 100.0;
		gbc_vesselsTable.weightx = 100.0;
		gbc_vesselsTable.anchor = GridBagConstraints.NORTH;
		gbc_vesselsTable.fill = GridBagConstraints.BOTH;
		gbc_vesselsTable.gridx = 0;
		gbc_vesselsTable.gridy = 0;
		vessels.add(new JScrollPane(vesselsTable), gbc_vesselsTable);
		
		JPanel vesselsActions = new JPanel();
		GridBagConstraints gbc_vesselsActions = new GridBagConstraints();
		gbc_vesselsActions.anchor = GridBagConstraints.SOUTH;
		gbc_vesselsActions.fill = GridBagConstraints.BOTH;
		gbc_vesselsActions.gridx = 0;
		gbc_vesselsActions.gridy = 1;
		this.add(vesselsActions, gbc_vesselsActions);
		GridBagLayout gbl_vesselsActions = new GridBagLayout();
		gbl_vesselsActions.columnWidths = new int[0];
		gbl_vesselsActions.rowHeights = new int[0];
		gbl_vesselsActions.columnWeights = new double[0];
		gbl_vesselsActions.rowWeights = new double[0];
		vesselsActions.setLayout(gbl_vesselsActions);
		
		addVessel = new JNoFocusOutlineButton("Add a new vessel");
		addVessel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_btnAddNew = new GridBagConstraints();
		gbc_btnAddNew.fill = GridBagConstraints.BOTH;
		gbc_btnAddNew.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddNew.gridx = 0;
		gbc_btnAddNew.gridy = 0;
		vesselsActions.add(addVessel, gbc_btnAddNew);
		
		back = new JNoFocusOutlineButton("Go back");
		back.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_btnBack = new GridBagConstraints();
		gbc_btnBack.fill = GridBagConstraints.BOTH;
		gbc_btnBack.gridx = 1;
		gbc_btnBack.gridy = 0;
		vesselsActions.add(back, gbc_btnBack);
	}
}
