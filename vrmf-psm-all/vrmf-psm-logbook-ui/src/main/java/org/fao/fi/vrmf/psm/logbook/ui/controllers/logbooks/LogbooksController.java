/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.services.data.LogbooksServices;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.LogbookReportsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.LogbooksListPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.LogbooksPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.model.LogbookTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.CatchesReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.DerivedEffortsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.DiscardsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.EffortsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingListSelectionListener;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class LogbooksController extends BaseController {
	@Inject private LogbooksPanel view;
	@Inject private LogbooksListPanel logbooksView;
	
	@Inject private LogbooksServices logbookServices;
	
	@PostConstruct
	private void initialize() {
		model.registerListener((LogbookListener)this);
		model.registerListener((LogsheetListener)this);
	
		logbooksView.getLogbooksTable().setModel(new LogbookTableModel(model.getLogbooks()));
		UIUtils.fixColumnWidth(logbooksView.getLogbooksTable(), 0, 48);
		UIUtils.fixColumnWidth(logbooksView.getLogbooksTable(), 1, 48);
		UIUtils.fixColumnWidth(logbooksView.getLogbooksTable(), 2, 64);
		
		logbooksView.getAddNewLogbook().addActionListener(new InterceptingActionListener() {
			@Override
			public void doActionPerformed(ActionEvent e) {
				addNewLogbookAction();
			}
		});

		logbooksView.getDeleteLogbook().addActionListener(new InterceptingActionListener() {
			@Override
			public void doActionPerformed(ActionEvent e) {
				deleteLogbookAction();
			}
		});

		logbooksView.getBack().addActionListener(new InterceptingActionListener() {
			@Override
			public void doActionPerformed(ActionEvent e) {
				backAction();
			}
		});
		
		logbooksView.getCompleteLogbook().addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) {
				completeLogbookAction();
			}
		});
		
		final JTable logbooksListTable = logbooksView.getLogbooksTable();
		
		logbooksListTable.getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				int selectedIndex = logbooksListTable.getSelectedRow();
				
				if(model.getLogbooks() != null && selectedIndex >= 0 && selectedIndex <= model.getLogbooks().size() - 1) {
					Logbook selected = model.getLogbooks().get(logbooksListTable.convertRowIndexToModel(selectedIndex));
					
					selected.setDerivedEfforts(null);
					
					if(selected.getLogsheets() != null)
						for(Logsheet in : selected.getLogsheets()) {
							in.setDerivedEfforts(null);
						}
					
					model.select(selected, selected == null ? EditingStatus.NOT_SET : selected.isCompleted() ? EditingStatus.VIEWING : EditingStatus.EDITING);
				}
			}
		});
		
		view.getLogbookDetails().getLogsheets().getLogsheetsList().getPrintLogbook().addActionListener(new InterceptingActionListener() {
			@Override
			public void doActionPerformed(ActionEvent e) throws Exception {
				printLogbookAction();
			}
		});
	}
	
	private void addNewLogbookAction() {
		model.addLogbook();
	}
	
	private void deleteLogbookAction() {
		if(UIUtils.confirm(
			"Please confirm", 
			"Are you sure you want to delete the selected logbook?"
		)) {
			model.deleteLogbook();
		}
	}
	
	private void backAction() {
		deselectLogbook();

		application.show("main");
	}
	
	private void completeLogbookAction() {
		Logbook selectedLogbook = model.getSelectedLogbook();
		
		if(selectedLogbook != null && !selectedLogbook.isCompleted()) {
			if(UIUtils.confirm(
				"Please confirm", 
				"If you complete this logbook, it will not be possible to add a new logsheet or modify its existing logsheets. Are you sure you want to continue?"
			)) {
				model.getSelectedLogbook().completeLogbook();
				model.updateLogbook();

				log().info("Selected logbook has been marked as completed");
				
				Logbook selected = model.getSelectedLogbook();
				model.select((Logbook)null);
				model.select(selected);
			} else {
				log().warn("Selected logbook was not marked as completed");
			}
		}
	}
	
	private void printLogbookAction() throws Exception {
		application.showGlassPane("Preparing PDF...");
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Logbook selectedLogbook = model.getSelectedLogbook();
				
				try {
					File output = logbookServices.toPdf(logbookServices.export(new File("logbooks"), model.getSelectedLogbook(), null));
					
					if(output != null) {
						Desktop.getDesktop().open(output);
						
						UIUtils.dialog(application.getApplicationFrame(), "Operation performed", "The logbook has been exported as a PDF file to " + output.getAbsolutePath());
					}
				} catch(Throwable t) {
					UIUtils.error(application.getApplicationFrame(), "Unable to produce PDF output", "Unexpected error caught: " + t.getMessage(), t);
				} finally {
					application.hideGlassPane();
					
					model.select(selectedLogbook);
				}
			}
		});
		
		
	}
	
	private void updateLogbooksTable() {
		((LogbookTableModel)logbooksView.getLogbooksTable().getModel()).update(model.getLogbooks());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookSelected(Logbook selected) {
		JTable logbooksTable = logbooksView.getLogbooksTable();
		
		if(selected != null) {
			Logbook selectedLogbook = model.getSelectedLogbook();

			boolean enableCompleteLogbook = !selectedLogbook.isCompleted();
			
			logbooksView.getDeleteLogbook().setEnabled(enableCompleteLogbook);
			
			enableCompleteLogbook &= selectedLogbook.canBeCompleted();
			
			logbooksView.getCompleteLogbook().setEnabled(enableCompleteLogbook);
			
			int selectedIndex = logbooksTable.getSelectedRow();
			
			if(selectedIndex < 0) {
				selectedIndex = logbooksTable.convertRowIndexToView(model.getLogbooks().indexOf(selectedLogbook));
				
				logbooksTable.getSelectionModel().setSelectionInterval(selectedIndex, selectedIndex);
			}
			
			boolean isCompleted = selectedLogbook.isCompleted();
			boolean canBeCompleted = selectedLogbook.canBeCompleted();
			
			if(!isCompleted && selected.getLogsheets() != null && !selected.getLogsheets().isEmpty()) {
				if(!canBeCompleted) {
					warning("Selected logbook cannot be completed as some of its logsheets are not marked as completed yet");
				}
			} else {
				info("Selected logbook is marked as completed, therefore its logsheets cannot be edited");
			}
			
			view.getLogbookDetails().getReports().getTabs().setSelectedIndex(0);
		} else {
			logbooksTable.getSelectionModel().clearSelection();
		}
		
		boolean enablePrint = selected != null && selected.isCompleted();
		
		if(selected != null) 
			view.getLogbookDetails().showReports();
		else
			view.getLogbookDetails().hideReports();
		
		view.getLogbookDetails().getLogsheets().getLogsheetsList().getPrintLogbook().setEnabled(enablePrint);
		
		refreshModels();
	}
	
	private void refreshModels() {
		LogbookReportsPanel reports = view.getLogbookDetails().getReports();
		
		Logbook selected = model.getSelectedLogbook();
		
		if(selected != null) {
			((EffortsReportTableModel)reports.getEffortsPanel().getEffortsTable().getModel()).update(selected.uniqueEfforts());
			((DerivedEffortsReportTableModel)reports.getDerivedEffortsPanel().getEffortsTable().getModel()).update(selected.uniqueDerivedEfforts());
			((CatchesReportTableModel)reports.getCatchesPanel().getCatchesTable().getModel()).update(selected.uniqueCatches());
			((DiscardsReportTableModel)reports.getDiscardsPanel().getDiscardsTable().getModel()).update(selected.uniqueDiscards());
		}
		
		int efforts = selected == null ? 0 : selected.uniqueEfforts().size();
		int derivedEfforts = selected == null ? 0 : selected.uniqueDerivedEfforts().size();
		int catches = selected == null ? 0 : selected.uniqueCatches().size();
		int discards = selected == null ? 0 : selected.uniqueDiscards().size();
		
		reports.getTabs().setEnabledAt(0, efforts > 0);
		reports.getTabs().setEnabledAt(1, derivedEfforts > 0);
		reports.getTabs().setEnabledAt(2, catches > 0);
		reports.getTabs().setEnabledAt(3, discards > 0);
		
		updateTabName(0, efforts == 0 ? null : efforts);
		updateTabName(1, derivedEfforts == 0 ? null : derivedEfforts);
		updateTabName(2, catches == 0 ? null : catches);
		updateTabName(3, discards == 0 ? null : discards);
	}
	
	private void updateTabName(int index, Object replacement) {
		JTabbedPane tabbedPane = view.getLogbookDetails().getReports().getTabs();
		
		String title = tabbedPane.getTitleAt(index).replaceAll("\\d+|\\-", "#").replace("#", replacement == null ? "-" : replacement.toString());
		
		tabbedPane.setTitleAt(index, title);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookAdded(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookAdded(Logbook added) {
		updateLogbooksTable();
		
		model.select(added);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookRemoved(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	public void logbookRemoved(Logbook removed) {
		updateLogbooksTable();
		
		deselectLogbook();
	};
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookUpdated(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookUpdated(Logbook updated) {
		updateLogbooksTable();
	}
	
	private void deselectLogbook() {
		model.select((Logbook)null);
	}
}