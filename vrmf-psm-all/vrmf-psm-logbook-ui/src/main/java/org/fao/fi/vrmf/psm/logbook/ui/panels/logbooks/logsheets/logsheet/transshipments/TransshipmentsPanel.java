/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.transshipments;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.ui.common.renderers.CodedEntityRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.SpeciesRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 10, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 10, 2015
 */
@Named @Singleton
public class TransshipmentsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2788188970919120616L;
	
	@Getter private JPanel recordsList;

	@Getter private JTable recordsTable;
	@Getter private JButton addNew;
	@Getter private JComboBox species;
	@Getter private JSpinner quantity;
	@Getter private JComboBox quantityUnit;
	
	@Getter private JButton updateRecord;
	@Getter private JButton deleteRecord;
	@Getter private JButton cancelRecord;

	/**
	 * Class constructor
	 *
	 */
	public TransshipmentsPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[] {50};
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		JPanel recordsListContainer = new JPanel();
		recordsListContainer.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Transshipped catches data", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_recordsListContainer = new GridBagConstraints();
		gbc_recordsListContainer.weighty = 100.0;
		gbc_recordsListContainer.weightx = 100.0;
		gbc_recordsListContainer.fill = GridBagConstraints.BOTH;
		gbc_recordsListContainer.insets = new Insets(0, 0, 5, 0);
		gbc_recordsListContainer.anchor = GridBagConstraints.NORTH;
		gbc_recordsListContainer.gridx = 0;
		gbc_recordsListContainer.gridy = 0;
		add(recordsListContainer, gbc_recordsListContainer);
		GridBagLayout gbl_recordsListContainer = new GridBagLayout();
		gbl_recordsListContainer.columnWidths = new int[0];
		gbl_recordsListContainer.rowHeights = new int[0];
		gbl_recordsListContainer.columnWeights = new double[0];
		gbl_recordsListContainer.rowWeights = new double[0];
		recordsListContainer.setLayout(gbl_recordsListContainer);
		
		recordsList = new JPanel();
		GridBagConstraints gbc_recordsList = new GridBagConstraints();
		gbc_recordsList.insets = new Insets(0, 0, 5, 0);
		gbc_recordsList.anchor = GridBagConstraints.NORTH;
		gbc_recordsList.weighty = 100.0;
		gbc_recordsList.weightx = 100.0;
		gbc_recordsList.fill = GridBagConstraints.BOTH;
		gbc_recordsList.gridx = 0;
		gbc_recordsList.gridy = 0;
		recordsListContainer.add(recordsList, gbc_recordsList);
		GridBagLayout gbl_recordsList = new GridBagLayout();
		gbl_recordsList.columnWidths = new int[0];
		gbl_recordsList.rowHeights = new int[0];
		gbl_recordsList.columnWeights = new double[0];
		gbl_recordsList.rowWeights = new double[0];
		recordsList.setLayout(gbl_recordsList);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.weighty = 100.0;
		gbc_scrollPane.weightx = 100.0;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		recordsList.add(scrollPane, gbc_scrollPane);
		
		recordsTable = new JTable();
		scrollPane.setViewportView(recordsTable);
		
		addNew = new JButton("Add new");
		addNew.setEnabled(false);
		GridBagConstraints gbc__addNew = new GridBagConstraints();
		gbc__addNew.insets = new Insets(5, 0, 5, 0);
		gbc__addNew.gridx = 0;
		gbc__addNew.gridy = 1;
		recordsListContainer.add(addNew, gbc__addNew);
		
		JPanel recordDataContainer = new JPanel();
		recordDataContainer.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Selected transshipped catches", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_recordDataContainer = new GridBagConstraints();
		gbc_recordDataContainer.weightx = 100.0;
		gbc_recordDataContainer.anchor = GridBagConstraints.NORTH;
		gbc_recordDataContainer.fill = GridBagConstraints.BOTH;
		gbc_recordDataContainer.gridx = 0;
		gbc_recordDataContainer.gridy = 1;
		add(recordDataContainer, gbc_recordDataContainer);
		GridBagLayout gbl_recordDataContainer = new GridBagLayout();
		gbl_recordDataContainer.columnWidths = new int[0];
		gbl_recordDataContainer.rowHeights = new int[0];
		gbl_recordDataContainer.columnWeights = new double[0];
		gbl_recordDataContainer.rowWeights = new double[0];
		recordDataContainer.setLayout(gbl_recordDataContainer);
		
		JPanel recordData = new JPanel();
		GridBagConstraints gbc_recordData = new GridBagConstraints();
		gbc_recordData.weighty = 100.0;
		gbc_recordData.weightx = 100.0;
		gbc_recordData.anchor = GridBagConstraints.NORTH;
		gbc_recordData.insets = new Insets(0, 0, 5, 0);
		gbc_recordData.fill = GridBagConstraints.HORIZONTAL;
		gbc_recordData.gridx = 0;
		gbc_recordData.gridy = 0;
		recordDataContainer.add(recordData, gbc_recordData);
		GridBagLayout gbl_recordData = new GridBagLayout();
		gbl_recordData.columnWidths = new int[0];
		gbl_recordData.rowHeights = new int[0];
		gbl_recordData.columnWeights = new double[0];
		gbl_recordData.rowWeights = new double[0];
		recordData.setLayout(gbl_recordData);
		
		JLabel lblSpecies = new JLabel("Species");
		lblSpecies.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.weightx = 5.0;
		gbc_lblType.fill = GridBagConstraints.BOTH;
		gbc_lblType.anchor = GridBagConstraints.WEST;
		gbc_lblType.insets = new Insets(5, 5, 5, 5);
		gbc_lblType.gridx = 0;
		gbc_lblType.gridy = 0;
		recordData.add(lblSpecies, gbc_lblType);
		
		species = new JComboBox();
		species.setEnabled(false);
		GridBagConstraints gbc__species = new GridBagConstraints();
		gbc__species.gridwidth = 3;
		gbc__species.weightx = 50.0;
		gbc__species.ipady = 5;
		gbc__species.insets = new Insets(5, 5, 5, 5);
		gbc__species.fill = GridBagConstraints.HORIZONTAL;
		gbc__species.gridx = 1;
		gbc__species.gridy = 0;
		recordData.add(species, gbc__species);
		
		species.setRenderer(new SpeciesRenderer());
		
		JLabel lblValue = new JLabel("Quantity");
		lblValue.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblValue = new GridBagConstraints();
		gbc_lblValue.anchor = GridBagConstraints.WEST;
		gbc_lblValue.fill = GridBagConstraints.BOTH;
		gbc_lblValue.insets = new Insets(5, 5, 5, 5);
		gbc_lblValue.gridx = 0;
		gbc_lblValue.gridy = 1;
		recordData.add(lblValue, gbc_lblValue);
		
		quantity = new JSpinner();
		quantity.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(0.1)));
		quantity.setEnabled(false);
		GridBagConstraints gbc__effortValue = new GridBagConstraints();
		gbc__effortValue.weightx = 20.0;
		gbc__effortValue.ipady = 5;
		gbc__effortValue.insets = new Insets(5, 5, 5, 5);
		gbc__effortValue.fill = GridBagConstraints.HORIZONTAL;
		gbc__effortValue.gridx = 1;
		gbc__effortValue.gridy = 1;
		recordData.add(quantity, gbc__effortValue);
		
		JLabel lblNewLabel = new JLabel("Unit");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.weightx = 5.0;
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.insets = new Insets(5, 5, 5, 5);
		gbc_lblNewLabel.gridx = 2;
		gbc_lblNewLabel.gridy = 1;
		recordData.add(lblNewLabel, gbc_lblNewLabel);
		
		quantityUnit = new JComboBox();
		quantityUnit.setEnabled(false);
		GridBagConstraints gbc__unit = new GridBagConstraints();
		gbc__unit.weightx = 30.0;
		gbc__unit.ipady = 5;
		gbc__unit.insets = new Insets(5, 5, 5, 5);
		gbc__unit.fill = GridBagConstraints.HORIZONTAL;
		gbc__unit.gridx = 3;
		gbc__unit.gridy = 1;
		recordData.add(quantityUnit, gbc__unit);
		
		JPanel recordActions = new JPanel();
		GridBagConstraints gbc_recordActions = new GridBagConstraints();
		gbc_recordActions.anchor = GridBagConstraints.SOUTH;
		gbc_recordActions.fill = GridBagConstraints.HORIZONTAL;
		gbc_recordActions.gridx = 0;
		gbc_recordActions.gridy = 1;
		recordDataContainer.add(recordActions, gbc_recordActions);
		GridBagLayout gbl_recordActions = new GridBagLayout();
		gbl_recordActions.columnWidths = new int[0];
		gbl_recordActions.rowHeights = new int[0];
		gbl_recordActions.columnWeights = new double[0];
		gbl_recordActions.rowWeights = new double[0];
		recordActions.setLayout(gbl_recordActions);
		
		updateRecord = new JButton("Update data");
		updateRecord.setEnabled(false);
		GridBagConstraints gbc_updateRecord = new GridBagConstraints();
		gbc_updateRecord.anchor = GridBagConstraints.WEST;
		gbc_updateRecord.fill = GridBagConstraints.HORIZONTAL;
		gbc_updateRecord.insets = new Insets(5, 0, 5, 5);
		gbc_updateRecord.gridx = 0;
		gbc_updateRecord.gridy = 0;
		recordActions.add(updateRecord, gbc_updateRecord);
		
		deleteRecord = new JButton("Delete data");
		deleteRecord.setEnabled(false);
		GridBagConstraints gbc_deleteRecord = new GridBagConstraints();
		gbc_deleteRecord.insets = new Insets(5, 0, 5, 5);
		gbc_deleteRecord.anchor = GridBagConstraints.EAST;
		gbc_deleteRecord.fill = GridBagConstraints.HORIZONTAL;
		gbc_deleteRecord.gridx = 1;
		gbc_deleteRecord.gridy = 0;
		recordActions.add(deleteRecord, gbc_deleteRecord);
		
		cancelRecord = new JButton("Cancel");
		cancelRecord.setEnabled(false);
		GridBagConstraints gbc_cancelRecord = new GridBagConstraints();
		gbc_cancelRecord.insets = new Insets(5, 0, 5, 5);
		gbc_cancelRecord.gridx = 2;
		gbc_cancelRecord.gridy = 0;
		recordActions.add(cancelRecord, gbc_cancelRecord);
		
		quantityUnit.setModel(new DefaultComboBoxModel(QuantityType.values()));
		quantityUnit.setRenderer(new CodedEntityRenderer<QuantityType>());
	}
}
