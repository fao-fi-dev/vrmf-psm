/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.fao.fi.vrmf.psm.logbook.ui.Preferences;
import org.fao.fi.vrmf.psm.ui.common.LogLevel;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JColorTextPane;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;

import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton @Slf4j
public class LogPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5760956769331757505L;
	
	@Inject private Preferences prefs;
	
	private LogLevel level;

	private JColorTextPane logPane;
	private JScrollPane scrollPane;
	
	public LogPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	public @Named @Singleton LogPanel getPanel() {
		return this;
	}
	
	@PostConstruct
	public void initialize() {
		level = prefs.getLogLevel();
		
		setBorder(null);
		setLayout(new GridLayout(1, 1, 0, 0));
		
		JPanel container = new JPanel();
		add(container);
		GridBagLayout gbl_container = new GridBagLayout();
		gbl_container.rowHeights = new int[] {128};
		gbl_container.columnWeights = new double[0];
		gbl_container.rowWeights = new double[0];
		container.setLayout(gbl_container);
		
		this.add(scrollPane = new JScrollPane());
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.weighty = 100.0;
		gbc_scrollPane.weightx = 100.0;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.anchor = GridBagConstraints.NORTHWEST;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		container.add(scrollPane, gbc_scrollPane);
		
		logPane = new JColorTextPane(false);
		logPane.setBackground(Color.BLACK);
		
		logPane.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, new Double(1));
		
		scrollPane.setViewportView(logPane);

		logPane.setFont(new Font("Courier New", Font.PLAIN, 11));
		
		setVisible(prefs.isShowLog());
		
		info("Initialized!");
	}
	
	public void clear() {
		logPane.setText(null);
	}
	
	public String levelAsString() {
		return level == null ? null : level.toString();
	}
	
	public void changeLevel(String requestedLevel) {
		doLog(LogLevel.INFO, "Changing log level from {} to {}", level, requestedLevel);

		level = LogLevel.valueOf(requestedLevel);
	}
	
	public void debug(String message, Object... parameters) {
		log.debug(message, parameters);
		
		doLog(LogLevel.DEBUG, message, parameters);
	}
	
	public void info(String message, Object... parameters) {
		log.info(message, parameters);
		
		doLog(LogLevel.INFO, message, parameters);
	}
	
	public void warn(String message, Object... parameters) {
		log.warn(message, parameters);
		
		doLog(LogLevel.WARN, message, parameters);
	}
	
	public void error(String message, Object... parameters) {
		log.error(message, parameters);
	
		doLog(LogLevel.ERROR, message, parameters);
	}
	
	public void trace(String message, Object... parameters) {
		log.trace(message, parameters);
		
		doLog(LogLevel.TRACE, message, parameters);
	}
	
	private boolean mustLog(LogLevel requested) {
		if(requested == level) return true;
		
		switch(requested) {
			case DEBUG:
				return level == LogLevel.TRACE;
			case INFO:
				return level == LogLevel.TRACE || level == LogLevel.DEBUG;
			case WARN:
				return level == LogLevel.TRACE || level == LogLevel.DEBUG || level == LogLevel.INFO;
			case ERROR:
				return level == LogLevel.TRACE || level == LogLevel.DEBUG || level == LogLevel.INFO || level == LogLevel.WARN;
			default:
				return false;
		}
	}
	
	public void doLog(LogLevel logLevel, String message, Object... parameters) {
		String fullMessage = message;
		
		if(parameters != null) {
			for(Object in : parameters) {
				fullMessage = fullMessage.replaceFirst("\\{\\}", in == null ? "<NULL>" : in.toString());
			}
		}
		
		Color color = null;
		
		switch(logLevel) {
			case TRACE:
				color = Color.CYAN; break;
			case DEBUG:
				color = Color.PINK; break;
			case WARN:
				color = Color.ORANGE; break;
			case ERROR:
				color = Color.RED; break;
			default:
				color = Color.GREEN;
		}
		
		if(mustLog(logLevel)) {
			boolean addNewline = logPane.getDocument().getLength() > 0;
			
			logPane.append(color, (addNewline ? "\n" :  "") + "[ " + Utils.pad(logLevel.toString(), 5) + " ] @ " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + " - " + fullMessage);
		}
	}
}
