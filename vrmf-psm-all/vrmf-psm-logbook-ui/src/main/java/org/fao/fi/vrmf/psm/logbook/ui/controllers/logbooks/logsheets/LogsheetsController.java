/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks.logsheets;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableColumn;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler;
import org.fao.fi.vrmf.psm.logbook.services.data.LogsheetsServices;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.LogbooksPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetDataPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetsActionsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetsListPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetDetailsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.model.LogsheetEventTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.model.LogsheetTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.CatchesReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.DerivedEffortsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.DiscardsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.EffortsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.ui.common.spi.PreferenceListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.EmbeddedTileFactoryInfo;
import org.fao.fi.vrmf.psm.ui.common.support.components.JMapPanel;
import org.fao.fi.vrmf.psm.ui.common.support.components.LazyCachingTileFactoryInfo;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingListSelectionListener;
import org.jxmapviewer.viewer.DefaultTileFactory;

import no.tornado.databinding.BindingGroup;
import no.tornado.databinding.BindingStrategy;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class LogsheetsController extends BaseController implements PreferenceListener {
	@Inject private LogsheetsServices logsheetServices;

	@Inject private LogbooksPanel parentView;
	@Inject private LogsheetsPanel view;
	
	private LogsheetsListPanel logsheetsList;
	private LogsheetDataPanel logsheetData;
	private LogsheetDetailsPanel logsheetDetails;
	private LogsheetsActionsPanel actions;
			
	private BindingGroup logsheetBindings;
	
	@PostConstruct
	private void initialize() {
		prefs.registerListener(this);

		logsheetsList = view.getLogsheetsList();
		logsheetData = view.getLogsheetData();
		logsheetDetails = view.getLogsheetDetails();
		actions = view.getLogsheetsActions();
		
		model.registerListener((LogbookListener)this);
		model.registerListener((LogsheetListener)this);
		
		TableColumn column = logsheetsList.getLogsheetsTable().getColumnModel().getColumn(0);
		column.setMaxWidth(48);
		column.setMinWidth(column.getMaxWidth());
		column.setWidth(column.getMaxWidth());
		column.setResizable(false);
		
		column = logsheetsList.getLogsheetsTable().getColumnModel().getColumn(1);
		column.setMinWidth(160);
		column.setResizable(true);
		
		logsheetsList.getLogsheetsTable().getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				List<Logsheet> logsheets = model.isLogbookSelected() ? model.getSelectedLogbook().getLogsheets() : null;
				
				int selectedIndex = logsheetsList.getLogsheetsTable().getSelectedRow();
				
				if(logsheets != null && selectedIndex >= 0 && selectedIndex <= logsheets.size() - 1) {
					Logsheet selected = logsheets.get(logsheetsList.getLogsheetsTable().convertRowIndexToModel(selectedIndex));
					
					if(!selected.equals(model.getSelectedLogsheet())) {
						model.select(selected, EditingStatus.VIEWING);
					}
				}
			}
		});
		
		logsheetsList.getAddNewLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addLogsheetAction();
			}
		});
		
		actions.getCancel().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				cancelLogsheetSelectionAction();
			}
		});
		
		actions.getViewLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				viewLogsheetAction();
			}
		});
		
		actions.getModifyLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				modifyLogsheetAction();
			}
		});
		
		actions.getPrintLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Exception {
				printLogsheetAction();
			}
		});	
		
		actions.getDeleteLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				deleteLogsheetAction();
			}
		});	
	}
	
	private void addLogsheetAction() {
		model.addLogsheet();
		
		application.show("logsheet");
	}
	
	private void cancelLogsheetSelectionAction() {
		model.select((Logsheet)null);
		model.select(model.getSelectedLogbook());
	}
	
	private void viewLogsheetAction() {
		model.changeLogsheetStatus(EditingStatus.VIEWING);
		
		application.show("logsheet");
	}
	
	private void deleteLogsheetAction() {
		if(UIUtils.confirm(
			"Please confirm", 
			"Are you sure you want to delete the selected logsheet?"
		)) {
			model.deleteLogsheet();
		}
	}
	
	private void modifyLogsheetAction() {
		model.changeLogsheetStatus(EditingStatus.EDITING);
		
		application.show("logsheet");
		
		List<Event> events = model.getSelectedLogsheet().getEvents();
		
		if(events != null && !events.isEmpty()) {
			Event last = events.get(events.size() - 1);
			
			if(!last.isCompleted()) {
				warning("Last event defined for this logsheet is not complete yet: therefore you won't be able to add other events before you explicitly set and ending date and coordinates for it");
			}
		}
	}
	
	private void printLogsheetAction() throws Exception {
		application.showGlassPane("Preparing PDF...");
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Logbook selectedLogbook = model.getSelectedLogbook();
				Logsheet selectedLogsheet = model.getSelectedLogsheet();
				
				try {
					File output = logsheetServices.toPdf(logsheetServices.export(new File("logsheets"), model.getSelectedLogbook(), model.getSelectedLogsheet(), null));
					
					if(output != null) {
						Desktop.getDesktop().open(output);
						
						UIUtils.dialog(application.getApplicationFrame(), "Operation performed", "The logsheet has been exported as a PDF file to " + output.getAbsolutePath());
					}
				} catch(Throwable t) {
					UIUtils.error(application.getApplicationFrame(), "Unable to produce PDF output", "Unexpected error caught: " + t.getMessage(), t);
				} finally {
					application.hideGlassPane();
					
					model.select(selectedLogbook);
					model.select(selectedLogsheet);
				}
			}
		});
		
		
	}
	
	private void bindLogsheet(Logsheet selected) {
		if(logsheetBindings != null) logsheetBindings.unbind();
		
		logsheetBindings = new BindingGroup(selected);
		logsheetBindings.setDefaultBindingStrategy(BindingStrategy.ONFLUSH);

		logsheetBindings.add(logsheetData.getNumber(), "number");
		logsheetBindings.add(logsheetData.getVessel(), "vesselData");
		logsheetBindings.add(logsheetData.getCaptain(), "captainName");
		logsheetBindings.add(logsheetData.getCrew(), "crewSize");
		logsheetBindings.add(logsheetData.getDeparturePort(), "departurePort");
		logsheetBindings.add(logsheetData.getDepartureDate(), "departureDate");
		logsheetBindings.add(logsheetData.getUnloadingPort(), "unloadingPort");
		logsheetBindings.add(logsheetData.getUnloadingDate(), "arrivalDate");
		logsheetBindings.add(logsheetData.getFishOnboardStart(), "fishOnboardAtStart");
		logsheetBindings.add(logsheetData.getFishOnboardStartUnit(), "fishOnboardQuantityAtStart");
		logsheetBindings.add(logsheetData.getFishOnboardEnd(), "fishOnboardAfterUnloading");
		logsheetBindings.add(logsheetData.getFishOnboardEndUnit(), "fishOnboardQuantityAfterUnloading");
		logsheetBindings.add(logsheetData.getComment(), "comments");
		
		if(selected.getDeparturePort() != null) {
			logsheetData.getDepartureCountry().setSelectedItem(selected.getDeparturePort().getCountry());
		} else {
			logsheetData.getDepartureCountry().setSelectedItem(null);
		}

		if(selected.getUnloadingPort() != null) {
			logsheetData.getUnloadingCountry().setSelectedItem(selected.getUnloadingPort().getCountry());
		} else {
			logsheetData.getUnloadingCountry().setSelectedItem(null);
		}

		logsheetBindings.bind();
	}
	
	private void unbindLogsheet() {
		if(logsheetBindings != null)
			logsheetBindings.unbind();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookSelected(Logbook selected) {
		deselectLogsheet();

		((LogsheetTableModel)logsheetsList.getLogsheetsTable().getModel()).update(selected == null ? new ArrayList<Logsheet>() : model.getSelectedLogbook().getLogsheets() );
		
		logsheetsList.getAddNewLogsheet().setEnabled(selected != null && !selected.isCompleted());
	}
		
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetRemoved(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetRemoved(Logsheet removed) {
		deselectLogsheet();
		
		if(removed != null)
			model.select(model.getSelectedLogbook());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetSelected(Logsheet selected) {
		long end, start = System.currentTimeMillis();
		
		try {
			view.getLogsheetsList().setVisible(selected == null);
			view.getLogsheetTabs().setVisible(selected != null);
			
			view.getLogsheetDetails().getTabs().setSelectedIndex(0);
			view.getLogsheetTabs().setSelectedIndex(0);
			
			JTable logsheets = logsheetsList.getLogsheetsTable();
			
			view.getLogsheetMap().updateMap(selected);
			
			if(selected == null) {
				logsheets.getSelectionModel().clearSelection();
			} else {
				Logbook owner = model.getSelectedLogbook();
				
				String logbookNo = owner.getVersion() + " | " + owner.getYear() + " | " + ( new SimpleDateFormat("yyyy/MM/dd").format(owner.getCreationDate()) );
				
				logsheetData.getLogbookNo().setText(logbookNo);
				
				logsheetData.getVessel().setModel(new DefaultComboBoxModel(logsheetServices.allVessels().toArray(new Object[0])));
				
				List<Country> allCountriesWithPorts = commonServices.allCountriesWithValidPorts();
				allCountriesWithPorts.add(0, null);
				
				logsheetData.getDepartureCountry().setModel(new DefaultComboBoxModel(allCountriesWithPorts.toArray(new Country[0])));
				logsheetData.getUnloadingCountry().setModel(new DefaultComboBoxModel(allCountriesWithPorts.toArray(new Country[0])));
				
				if(selected.getDeparturePort() != null)
					logsheetData.getDeparturePort().setModel(new DefaultComboBoxModel(logsheetServices.portsForCountry(selected.getDeparturePort().getCountry()).toArray(new Port[0])));
				
				if(selected.getUnloadingPort() != null)
					logsheetData.getUnloadingPort().setModel(new DefaultComboBoxModel(logsheetServices.portsForCountry(selected.getUnloadingPort().getCountry()).toArray(new Port[0])));
				
				List<Event> events = selected.getEvents();
				
				if(events == null) events = new ArrayList<Event>();
	
				if(selected.getTransitionHandler() == null && selected.getVesselData() != null) {
					selected.setTransitionHandler(StateTransitionHandler.forType(selected.getVesselData().getType()).initialize(events));
				}
				
				refreshModels();
				
				actions.getModifyLogsheet().setEnabled(!selected.isCompleted());
				actions.getDeleteLogsheet().setEnabled(!selected.isCompleted());
				actions.getPrintLogsheet().setEnabled(selected.isCompleted());
				
				view.getLogsheetDetails().getTabs().setSelectedIndex(0);
			}	
			
			if(selected != null) {
				bindLogsheet(selected);
				
				parentView.getLogbookDetails().hideReports();
			} else {
				unbindLogsheet();
				
				parentView.getLogbookDetails().showReports();
			}
			
			model.refreshLogsheetStatus();
			
			logsheetData.disableAll();
		} finally {
			end = System.currentTimeMillis();
			
			log().debug("Selecting the logsheet took {} mSec.", end - start);
			
			if(selected != null && selected.isCompleted())
				info("Selected logsheet is completed and cannot be edited or deleted");
		}
	}

	private void deselectLogsheet() {
		model.select((Logsheet)null);
	}
	
	private void refreshModels() {
		Logsheet selected = model.getSelectedLogsheet();
		
		((LogsheetEventTableModel)logsheetDetails.getEventsPanel().getLogsheetsEventsTable().getModel()).update(selected == null ? null : selected.getEvents());
		((EffortsReportTableModel)logsheetDetails.getEffortsPanel().getEffortsTable().getModel()).update(selected == null ? null : selected.uniqueEfforts());
		((DerivedEffortsReportTableModel)logsheetDetails.getDerivedEffortsPanel().getEffortsTable().getModel()).update(selected == null ? null : selected.uniqueDerivedEfforts());
		((CatchesReportTableModel)logsheetDetails.getCatchesPanel().getCatchesTable().getModel()).update(selected == null ? null : selected.uniqueCatches());
		((DiscardsReportTableModel)logsheetDetails.getDiscardsPanel().getDiscardsTable().getModel()).update(selected == null ? null : selected.uniqueDiscards());
		
		int events = selected == null || selected.getEvents() == null ? 0 : selected.getEvents().size();
		int efforts = selected == null ? 0 : selected.uniqueEfforts().size();
		int derivedEfforts = selected == null ? 0 : selected.uniqueDerivedEfforts().size();
		int catches = selected == null ? 0 : selected.uniqueCatches().size();
		int discards = selected == null ? 0 : selected.uniqueDiscards().size();
		
		logsheetDetails.getTabs().setEnabledAt(0, events > 0);
		logsheetDetails.getTabs().setEnabledAt(1, efforts > 0);
		logsheetDetails.getTabs().setEnabledAt(2, derivedEfforts > 0);
		logsheetDetails.getTabs().setEnabledAt(3, catches > 0);
		logsheetDetails.getTabs().setEnabledAt(4, discards > 0);
		
		updateTabName(0, events == 0 ? null : events);
		updateTabName(1, efforts == 0 ? null : efforts);
		updateTabName(2, derivedEfforts == 0 ? null : derivedEfforts);
		updateTabName(3, catches == 0 ? null : catches);
		updateTabName(4, discards == 0 ? null : discards);
	}
	
	private void updateTabName(int index, Object replacement) {
		JTabbedPane tabbedPane = logsheetDetails.getTabs();
		
		String title = tabbedPane.getTitleAt(index).replaceAll("\\d+|\\-", "#").replace("#", replacement == null ? "-" : replacement.toString());
		
		tabbedPane.setTitleAt(index, title);
	}
	

	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.PreferenceListener#preferencesChanged(java.lang.String, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void preferencesChanged(String property, Object oldValue, Object newValue) {
		if("offline".equals(property)) {
			JMapPanel mapPanel = view.getLogsheetMap();
			
			boolean offline = Boolean.TRUE.equals(newValue);
			
			mapPanel.getMap().setZoom(16);

			if(offline) {
				mapPanel.getMap().setTileFactory(new DefaultTileFactory(new EmbeddedTileFactoryInfo()));
			} else {
//				mapPanel.getMap().setDefaultProvider(DefaultProviders.OpenStreetMaps);
				mapPanel.getMap().setTileFactory(new DefaultTileFactory(new LazyCachingTileFactoryInfo()));
			}
			
			mapPanel.getMap().getMainMap().setRestrictOutsidePanning(true);
			mapPanel.getMap().getMainMap().setHorizontalWrapped(false);
			
			mapPanel.updateMap(model.getSelectedLogsheet());
		}
	}
}