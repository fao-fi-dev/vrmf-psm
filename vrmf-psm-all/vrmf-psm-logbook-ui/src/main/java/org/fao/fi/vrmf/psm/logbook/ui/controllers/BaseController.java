/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.swing.JOptionPane;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.services.data.CommonServices;
import org.fao.fi.vrmf.psm.logbook.ui.ApplicationWindow;
import org.fao.fi.vrmf.psm.logbook.ui.DataModel;
import org.fao.fi.vrmf.psm.logbook.ui.Preferences;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.LogPanel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.slf4j.Logger;

import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Slf4j
abstract public class BaseController implements VesselListener, LogbookListener, LogsheetListener, LogsheetEventListener, LogsheetEventEffortListener, LogsheetEventCatchListener, LogsheetEventDiscardListener {
	@Inject protected ApplicationWindow application;
	@Inject protected Preferences prefs;
	@Inject protected DataModel model;
	
	@Inject protected CommonServices commonServices;
	
	protected Logger _log() { return log; }
	
	public LogPanel log() {
		return application.getLog();
	}
	
	@PostConstruct
	private void initialize() {
		_log().info("Initializing {}", this.getClass().getSimpleName());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void vesselStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("Handling vessel status change ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselAdded(org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData)
	 */
	@Override
	public void vesselAdded(VesselData added) {
		log().trace("Handling vessel added ({}) : {}", this.getClass().getSimpleName(), added);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselRemoved(org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData)
	 */
	@Override
	public void vesselRemoved(VesselData removed) {
		log().trace("Handling vessel removed ({}) : {}", this.getClass().getSimpleName(), removed);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselUpdated(org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData)
	 */
	@Override
	public void vesselUpdated(VesselData updated) {
		log().trace("Handling vessel updated ({}) : {}", this.getClass().getSimpleName(), updated);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselSelected(org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData)
	 */
	@Override
	public void vesselSelected(VesselData selected) {
		log().trace("Handling vessel selected ({}) : {}", this.getClass().getSimpleName(), selected);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("Handling logsheet status change ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetAdded(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetAdded(Logsheet added) {
		log().trace("Handling logsheet added ({}) : {}", this.getClass().getSimpleName(), added);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetRemoved(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetRemoved(Logsheet removed) {
		log().trace("Handling logsheet removed ({}) : {}", this.getClass().getSimpleName(), removed);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetUpdated(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetUpdated(Logsheet updated) {
		log().trace("Handling logsheet updated ({}) : {}", this.getClass().getSimpleName(), updated);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetSelected(Logsheet selected) {
		log().trace("Handling logsheet selected ({}) : {}", this.getClass().getSimpleName(), selected);
		
		application.getScrollingContainer().getVerticalScrollBar().setValue(0);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logbookStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("Handling logbook status change ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookAdded(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookAdded(Logbook added) {
		log().trace("Handling logbook added ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookRemoved(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookRemoved(Logbook removed) {
		log().trace("Handling logbook removed ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookUpdated(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookUpdated(Logbook updated) {
		log().trace("Handling logbook updated ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookSelected(Logbook selected) {
		log().trace("Handling logbook selected ({})", this.getClass().getSimpleName());
		
		application.getScrollingContainer().getVerticalScrollBar().setValue(0);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener#logsheetEventStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("Handling logsheet event status change ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener#logsheetEventAdded(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventAdded(Event added) {
		log().trace("Handling logsheet event added ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener#logsheetEventRemoved(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventRemoved(Event removed) {
		log().trace("Handling logsheet event removed ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener#logsheetEventUpdated(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventUpdated(Event updated) {
		log().trace("Handling logsheet event updated ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener#logsheetEventSelected(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventSelected(Event selected) {
		log().trace("Handling logsheet event selected ({})", this.getClass().getSimpleName());
		
		application.getScrollingContainer().getVerticalScrollBar().setValue(0);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener#logsheetEventDiscardStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventDiscardStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("Handling logsheet event discard status change ({})", this.getClass().getSimpleName());	
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener#logsheetEventDiscardAdded(org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards)
	 */
	@Override
	public void logsheetEventDiscardAdded(Discards added) {
		log().trace("Handling logsheet event discard added ({})", this.getClass().getSimpleName());	
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener#logsheetEventDiscardRemoved(org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards)
	 */
	@Override
	public void logsheetEventDiscardRemoved(Discards removed) {
		log().trace("Handling logsheet event discard removed ({})", this.getClass().getSimpleName());	
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener#logsheetEventDiscardUpdated(org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards)
	 */
	@Override
	public void logsheetEventDiscardUpdated(Discards updated) {
		log().trace("Handling logsheet event discard updated ({})", this.getClass().getSimpleName());	
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener#logsheetEventDiscardSelected(org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards)
	 */
	@Override
	public void logsheetEventDiscardSelected(Discards selected) {
		log().trace("Handling logsheet event discard selected ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener#logsheetEventCatchStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventCatchStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("Handling logsheet event catch status change ({})", this.getClass().getSimpleName());	
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener#logsheetEventCatchAdded(org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches)
	 */
	@Override
	public void logsheetEventCatchAdded(Catches added) {
		log().trace("Handling logsheet event catch added ({})", this.getClass().getSimpleName());	
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener#logsheetEventCatchRemoved(org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches)
	 */
	@Override
	public void logsheetEventCatchRemoved(Catches removed) {
		log().trace("Handling logsheet event catch removed ({})", this.getClass().getSimpleName());	
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener#logsheetEventCatchUpdated(org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches)
	 */
	@Override
	public void logsheetEventCatchUpdated(Catches updated) {
		log().trace("Handling logsheet event catch updated ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener#logsheetEventCatchSelected(org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches)
	 */
	@Override
	public void logsheetEventCatchSelected(Catches selected) {
		log().trace("Handling logsheet event catch selected ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener#logsheetEventEffortStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventEffortStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("Handling logsheet event effort status change ({})", this.getClass().getSimpleName());	
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener#logsheetEventEffortAdded(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventEffortAdded(Effort added) {
		log().trace("Handling logsheet event effort added ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener#logsheetEventEffortRemoved(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventEffortRemoved(Effort removed) {
		log().trace("Handling logsheet event effort removed ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener#logsheetEventEffortUpdated(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventEffortUpdated(Effort updated) {
		log().trace("Handling logsheet event effort updated ({})", this.getClass().getSimpleName());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener#logsheetEventEffortSelected(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventEffortSelected(Effort selected) {
		log().trace("Handling logsheet event effort selected ({})", this.getClass().getSimpleName());
	}
	
	protected boolean confirm(String title, String message) {
		return UIUtils.confirm(application.getApplicationFrame(), title, message);
	}
	
	protected void alert(String message) {
		alert("Important notice", message);
	}
	
	protected void alert(String title, String message) {
		log().warn("{} - {}", title, message);
		
		UIUtils.alert(application.getApplicationFrame(), title, message);
	}
	
	protected void info(String message) {
		info("Information notice", message);
	}
	
	protected void info(String title, String message) {
		log().info("{} - {}", title, message);
		
		if(prefs.isDummyMode()) UIUtils.dialog(application.getApplicationFrame(), JOptionPane.INFORMATION_MESSAGE, title, message);
	}
	
	protected void warning(String message) {
		warning("Warning notice", message);
	}
	
	protected void warning(String title, String message) {
		log().warn("{} - {}", title, message);
		
		if(prefs.isDummyMode()) UIUtils.dialog(application.getApplicationFrame(), JOptionPane.WARNING_MESSAGE, title, message);
	}

	protected void error(String message) {
		error("Error notice", message);
	}
	
	protected void error(String title, String message) {
		log().error("{} - {}", title, message);
		
		UIUtils.dialog(application.getApplicationFrame(), JOptionPane.ERROR_MESSAGE, title, message);
	}
	
	protected void fatal(String message, Throwable t) {
		fatal("Unexpected error caught", message, t);
	}
	
	protected void fatal(String title, String message, Throwable t) {
		log().error("{} - {}", title, message);
		
		UIUtils.error(application.getApplicationFrame(), title, message, t);
	}
}