/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class LogsheetActionsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8872498289521272459L;
	
	@Getter private JNoFocusOutlineButton updateLogsheet;
	@Getter private JNoFocusOutlineButton completeLogsheet;
	@Getter private JNoFocusOutlineButton revertLogsheet;
	@Getter private JNoFocusOutlineButton backToLogsheets;
	
	public LogsheetActionsPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.columnWidths = new int[0];
		gbl_this.rowHeights = new int[0];
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);
	
		updateLogsheet = new JNoFocusOutlineButton("Update logsheet");
		updateLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		updateLogsheet.setEnabled(false);
		GridBagConstraints gbc_btnAddNew = new GridBagConstraints();
		gbc_btnAddNew.anchor = GridBagConstraints.NORTH;
		gbc_btnAddNew.fill = GridBagConstraints.BOTH;
		gbc_btnAddNew.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddNew.gridx = 0;
		gbc_btnAddNew.gridy = 0;
		add(updateLogsheet, gbc_btnAddNew);
		
		revertLogsheet = new JNoFocusOutlineButton("Revert logsheet");
		revertLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		revertLogsheet.setEnabled(true);
		GridBagConstraints gbc_revertLogsheet = new GridBagConstraints();
		gbc_revertLogsheet.insets = new Insets(0, 0, 0, 5);
		gbc_revertLogsheet.gridx = 1;
		gbc_revertLogsheet.gridy = 0;
		add(revertLogsheet, gbc_revertLogsheet);
		
		completeLogsheet = new JNoFocusOutlineButton("Complete logsheet");
		completeLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		completeLogsheet.setEnabled(false);
		GridBagConstraints gbc_completeLogsheet = new GridBagConstraints();
		gbc_completeLogsheet.insets = new Insets(0, 0, 0, 5);
		gbc_completeLogsheet.gridx = 2;
		gbc_completeLogsheet.gridy = 0;
		add(completeLogsheet, gbc_completeLogsheet);
		
		backToLogsheets = new JNoFocusOutlineButton("Go back");
		backToLogsheets.setFont(new Font("Tahoma", Font.PLAIN, 14));
		backToLogsheets.setEnabled(true);
		GridBagConstraints gbc_backToLogsheets = new GridBagConstraints();
		gbc_backToLogsheets.insets = new Insets(0, 0, 0, 5);
		gbc_backToLogsheets.gridx = 3;
		gbc_backToLogsheets.gridy = 0;
		add(backToLogsheets, gbc_backToLogsheets);
	}
	
	public void notifyLogsheetStatusChange(EditingStatus status, Logsheet current) {
		boolean isCompleted = current != null && current.isCompleted();
		boolean canBeCompleted = current != null && 
								!isCompleted && 
								current.checkEvents() &&
							   (current.getTransitionHandler() == null || current.getTransitionHandler().isCompleted());
		
		switch(status) {
			case NOT_SET:
				break;
			case ADDING:
			case COMPLETING:
				revertLogsheet.setEnabled(false);
				updateLogsheet.setEnabled(true);
				completeLogsheet.setEnabled(false);
				break;
			case EDITING:
				revertLogsheet.setEnabled(true);
				updateLogsheet.setEnabled(true);
				completeLogsheet.setEnabled(!isCompleted && canBeCompleted);
				break;
			default:
				revertLogsheet.setEnabled(false);
				updateLogsheet.setEnabled(false);
				completeLogsheet.setEnabled(false);
				break;
		}
		
		backToLogsheets.setEnabled(true);
	}
	
	public void notifyLogsheetEventStatusChange(EditingStatus logsheetStatus, Logsheet logsheet, EditingStatus eventStatus, Event event) {
		switch(eventStatus) {
			case ADDING:
			case EDITING:
				revertLogsheet.setEnabled(false);
				updateLogsheet.setEnabled(false);
				completeLogsheet.setEnabled(false);
				backToLogsheets.setEnabled(false);
				break;
			default:
				notifyLogsheetStatusChange(logsheetStatus, logsheet);
				break;
		}
	}
}
