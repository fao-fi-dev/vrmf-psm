/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.application;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JLabel;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.StatusPanel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.DataModelStatusListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 11, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 11, 2015
 */
@Named @Singleton
public class StatusController extends BaseController implements DataModelStatusListener {
	@PostConstruct
	private void initialize() {
		model.registerListener((DataModelStatusListener)this);
		model.registerListener((VesselListener)this);
		model.registerListener((LogbookListener)this);
		model.registerListener((LogsheetListener)this);
		model.registerListener((LogsheetEventListener)this);
		model.registerListener((LogsheetEventEffortListener)this);
		model.registerListener((LogsheetEventCatchListener)this);
		model.registerListener((LogsheetEventDiscardListener)this);
	}
	
	private StatusPanel status() { 
		return application.getStatus();
	}
	
	private void setStatus(JLabel label, EditingStatus status) {
		label.setText("[ " + status.name() + " ]");
	}
	
	private void setTaintedStatus(JLabel label, boolean tainted) {
		String text = label.getText();
		
		text = text.replaceAll("\\*$", "");
		
		if(tainted) text += "*";
		
		label.setText(text);
	}
	
	@Override
	public void vesselStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("VESSEL STATUS: {} -> {}", previous, current);
		
		setStatus(status().getVesselStatus(), current);
	}
	
	@Override
	public void logbookStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("LOGBOOK STATUS: {} -> {}", previous, current);
		
		setStatus(status().getLogbookStatus(), current);
	}
	
	@Override
	public void logsheetStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("LOGSHEET STATUS: {} -> {}", previous, current);
		
		setStatus(status().getLogsheetStatus(), current);
	}
	
	@Override
	public void logsheetEventStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("LOGSHEET EVENT STATUS: {} -> {}", previous, current);
		
		setStatus(status().getEventStatus(), current);
	}
	
	@Override
	public void logsheetEventEffortStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("LOGSHEET EVENT EFFORT STATUS: {} -> {}", previous, current);
		
		setStatus(status().getEffortStatus(), current);
	}
	
	@Override
	public void logsheetEventCatchStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("LOGSHEET EVENT CATCH STATUS: {} -> {}", previous, current);
		
		setStatus(status().getCatchStatus(), current);
	}
	
	@Override
	public void logsheetEventDiscardStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("LOGSHEET EVENT DISCARD STATUS: {} -> {}", previous, current);
		
		setStatus(status().getDiscardStatus(), current);
	}
	
	@Override
	public void vesselSelected(VesselData selected) {
		log().trace("VESSEL SELECTED: {}", selected == null ? null : selected.hashCode());
	}
	
	@Override
	public void logbookSelected(Logbook selected) {
		log().trace("LOGBOOK SELECTED: {}", selected == null ? null : selected.hashCode());
	}
	
	@Override
	public void logsheetSelected(Logsheet selected) {
		log().trace("LOGSHEET SELECTED: {}", selected == null ? null : selected.hashCode());
	}
	
	@Override
	public void logsheetEventSelected(Event selected) {
		log().trace("LOGSHEET EVENT SELECTED: {}", selected == null ? null : selected.hashCode());
	}
	
	@Override
	public void logsheetEventEffortSelected(Effort selected) {
		log().trace("LOGSHEET EVENT EFFORT SELECTED: {}", selected == null ? null : selected.hashCode());
	}
	
	@Override
	public void logsheetEventCatchSelected(Catches selected) {
		log().trace("LOGSHEET EVENT CATCH SELECTED: {}", selected == null ? null : selected.hashCode());
	}
	
	@Override
	public void logsheetEventDiscardSelected(Discards selected) {
		log().trace("LOGSHEET EVENT DISCARD SELECTED: {}", selected == null ? null : selected.hashCode());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.DataModelStatusListener#statusChanged(boolean)
	 */
	@Override
	public void modelStatusChanged(boolean tainted) {
		log().trace("IS TAINTED: {}" , tainted);
		log().trace("VESSELS ARE TAINTED: {}", model.isVesselDirty());
		log().trace("LOGBOOKS ARE TAINTED: {}", model.isLogbookDirty());
		log().trace("LOGSHEETS ARE TAINTED: {}", model.isLogsheetDirty());
		log().trace("EVENTS ARE TAINTED: {}", model.isEventDirty());
		log().trace("EFFORTS ARE TAINTED: {}", model.isEffortDirty());
		log().trace("CATCHES ARE TAINTED: {}", model.isCatchDirty());
		log().trace("DISCARDS ARE TAINTED: {}", model.isDiscardDirty());
		
		setTaintedStatus(status().getVessel(), model.isVesselDirty());
		setTaintedStatus(status().getLogbook(), model.isLogbookDirty());
		setTaintedStatus(status().getLogsheet(), model.isLogsheetDirty());
		setTaintedStatus(status().getEvent(), model.isEventDirty());
		setTaintedStatus(status().getEffort(), model.isEffortDirty());
		setTaintedStatus(status().getCatches(), model.isCatchDirty());
		setTaintedStatus(status().getDiscards(), model.isDiscardDirty());
	}
}
