/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.efforts;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.efforts.model.LogsheetEventEffortTableModel;
import org.fao.fi.vrmf.psm.ui.common.renderers.CodedEntityRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CodedEntityTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.DefaultTableHeaderRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.StringTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 10, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 10, 2015
 */
@Named @Singleton
public class EffortsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2788188970919120616L;
	@Getter private JPanel effortsList;
	@Getter private JTable effortsTable;
	@Getter private JButton addNew;
	@Getter private JSpinner effortValue;
	@Getter private JComboBox effortType;
	@Getter private JButton updateEffort;
	@Getter private JButton deleteEffort;
	@Getter private JButton cancelEffort;

	/**
	 * Class constructor
	 *
	 */
	public EffortsPanel() {
		if(Utils.IS_LAUNCHING) return;

		initialize();
	}
	
	@PostConstruct
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[] {50};
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		JPanel effortsListContainer = new JPanel();
		effortsListContainer.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Recorded efforts", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_effortsListContainer = new GridBagConstraints();
		gbc_effortsListContainer.weighty = 100.0;
		gbc_effortsListContainer.weightx = 100.0;
		gbc_effortsListContainer.fill = GridBagConstraints.BOTH;
		gbc_effortsListContainer.insets = new Insets(0, 0, 5, 0);
		gbc_effortsListContainer.anchor = GridBagConstraints.NORTHWEST;
		gbc_effortsListContainer.gridx = 0;
		gbc_effortsListContainer.gridy = 0;
		add(effortsListContainer, gbc_effortsListContainer);
		GridBagLayout gbl_effortsListContainer = new GridBagLayout();
		gbl_effortsListContainer.columnWidths = new int[0];
		gbl_effortsListContainer.rowHeights = new int[0];
		gbl_effortsListContainer.columnWeights = new double[0];
		gbl_effortsListContainer.rowWeights = new double[0];
		effortsListContainer.setLayout(gbl_effortsListContainer);
		
		effortsList = new JPanel();
		GridBagConstraints gbc_effortsList = new GridBagConstraints();
		gbc_effortsList.insets = new Insets(0, 0, 5, 0);
		gbc_effortsList.anchor = GridBagConstraints.NORTH;
		gbc_effortsList.weighty = 100.0;
		gbc_effortsList.weightx = 100.0;
		gbc_effortsList.fill = GridBagConstraints.BOTH;
		gbc_effortsList.gridx = 0;
		gbc_effortsList.gridy = 0;
		effortsListContainer.add(effortsList, gbc_effortsList);
		GridBagLayout gbl_effortsList = new GridBagLayout();
		gbl_effortsList.columnWidths = new int[0];
		gbl_effortsList.rowHeights = new int[0];
		gbl_effortsList.columnWeights = new double[0];
		gbl_effortsList.rowWeights = new double[0];
		effortsList.setLayout(gbl_effortsList);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.weighty = 100.0;
		gbc_scrollPane.weightx = 100.0;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		effortsList.add(scrollPane, gbc_scrollPane);
		
		effortsTable = new JTable(new LogsheetEventEffortTableModel(new ArrayList<Effort>()) {
			/** Field serialVersionUID */
			private static final long serialVersionUID = 5210252037194577047L;
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
		
		effortsTable.setAutoCreateRowSorter(true);

		effortsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		effortsTable.setColumnSelectionAllowed(false);
		
		effortsTable.getTableHeader().setReorderingAllowed(false);
		
		effortsTable.setDefaultRenderer(String.class, new StringTableCellRenderer());
		effortsTable.setDefaultRenderer(EffortType.class, new CodedEntityTableCellRenderer());
		
		effortsTable.getTableHeader().setDefaultRenderer(new DefaultTableHeaderRenderer(effortsTable));
		
		TableColumn fixed = null;
		
		for(int i=0; i<=1; i++) {
			fixed = effortsTable.getColumnModel().getColumn(i);
			fixed.setMaxWidth(48);
			fixed.setWidth(fixed.getMaxWidth());
			fixed.setResizable(false);
		}
		
		scrollPane.setViewportView(effortsTable);
		
		addNew = new JButton("Add new");
		addNew.setEnabled(false);
		GridBagConstraints gbc__addNew = new GridBagConstraints();
		gbc__addNew.insets = new Insets(5, 0, 5, 0);
		gbc__addNew.gridx = 0;
		gbc__addNew.gridy = 1;
		effortsListContainer.add(addNew, gbc__addNew);
		
		JPanel effortDataContainer = new JPanel();
		effortDataContainer.setBorder(new TitledBorder(null, "Selected effort", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_effortDataContainer = new GridBagConstraints();
		gbc_effortDataContainer.anchor = GridBagConstraints.NORTH;
		gbc_effortDataContainer.fill = GridBagConstraints.BOTH;
		gbc_effortDataContainer.gridx = 0;
		gbc_effortDataContainer.gridy = 1;
		add(effortDataContainer, gbc_effortDataContainer);
		GridBagLayout gbl_effortDataContainer = new GridBagLayout();
		gbl_effortDataContainer.columnWidths = new int[0];
		gbl_effortDataContainer.rowHeights = new int[0];
		gbl_effortDataContainer.columnWeights = new double[0];
		gbl_effortDataContainer.rowWeights = new double[0];
		effortDataContainer.setLayout(gbl_effortDataContainer);
		
		JPanel effortData = new JPanel();
		GridBagConstraints gbc_effortData = new GridBagConstraints();
		gbc_effortData.weighty = 100.0;
		gbc_effortData.weightx = 100.0;
		gbc_effortData.anchor = GridBagConstraints.NORTH;
		gbc_effortData.insets = new Insets(0, 0, 5, 0);
		gbc_effortData.fill = GridBagConstraints.HORIZONTAL;
		gbc_effortData.gridx = 0;
		gbc_effortData.gridy = 0;
		effortDataContainer.add(effortData, gbc_effortData);
		GridBagLayout gbl_effortData = new GridBagLayout();
		gbl_effortData.columnWidths = new int[0];
		gbl_effortData.rowHeights = new int[0];
		gbl_effortData.columnWeights = new double[0];
		gbl_effortData.rowWeights = new double[0];
		effortData.setLayout(gbl_effortData);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.weightx = 5.0;
		gbc_lblType.fill = GridBagConstraints.BOTH;
		gbc_lblType.anchor = GridBagConstraints.WEST;
		gbc_lblType.insets = new Insets(5, 5, 5, 5);
		gbc_lblType.gridx = 0;
		gbc_lblType.gridy = 0;
		effortData.add(lblType, gbc_lblType);
		
		effortType = new JComboBox();
		effortType.setEnabled(false);
		GridBagConstraints gbc__effortType = new GridBagConstraints();
		gbc__effortType.weightx = 30.0;
		gbc__effortType.ipady = 5;
		gbc__effortType.insets = new Insets(5, 5, 5, 5);
		gbc__effortType.fill = GridBagConstraints.HORIZONTAL;
		gbc__effortType.gridx = 1;
		gbc__effortType.gridy = 0;
		effortData.add(effortType, gbc__effortType);
		
		JLabel lblValue = new JLabel("Value");
		lblValue.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblValue = new GridBagConstraints();
		gbc_lblValue.weightx = 5.0;
		gbc_lblValue.anchor = GridBagConstraints.WEST;
		gbc_lblValue.fill = GridBagConstraints.BOTH;
		gbc_lblValue.insets = new Insets(5, 5, 5, 5);
		gbc_lblValue.gridx = 2;
		gbc_lblValue.gridy = 0;
		effortData.add(lblValue, gbc_lblValue);
		
		effortValue = new JSpinner();
		effortValue.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(0.1)));
		effortValue.setEnabled(false);
		GridBagConstraints gbc__effortValue = new GridBagConstraints();
		gbc__effortValue.weightx = 20.0;
		gbc__effortValue.ipady = 5;
		gbc__effortValue.insets = new Insets(5, 5, 5, 5);
		gbc__effortValue.fill = GridBagConstraints.HORIZONTAL;
		gbc__effortValue.gridx = 3;
		gbc__effortValue.gridy = 0;
		effortData.add(effortValue, gbc__effortValue);
		
		JPanel effortActions = new JPanel();
		GridBagConstraints gbc_effortActions = new GridBagConstraints();
		gbc_effortActions.weightx = 100.0;
		gbc_effortActions.anchor = GridBagConstraints.NORTH;
		gbc_effortActions.fill = GridBagConstraints.HORIZONTAL;
		gbc_effortActions.gridx = 0;
		gbc_effortActions.gridy = 1;
		effortDataContainer.add(effortActions, gbc_effortActions);
		GridBagLayout gbl_effortActions = new GridBagLayout();
		gbl_effortActions.columnWidths = new int[0];
		gbl_effortActions.rowHeights = new int[0];
		gbl_effortActions.columnWeights = new double[0];
		gbl_effortActions.rowWeights = new double[0];
		effortActions.setLayout(gbl_effortActions);
		
		updateEffort = new JButton("Update effort");
		updateEffort.setEnabled(false);
		GridBagConstraints gbc__updateEffort = new GridBagConstraints();
		gbc__updateEffort.anchor = GridBagConstraints.WEST;
		gbc__updateEffort.fill = GridBagConstraints.HORIZONTAL;
		gbc__updateEffort.insets = new Insets(5, 0, 5, 5);
		gbc__updateEffort.gridx = 0;
		gbc__updateEffort.gridy = 0;
		effortActions.add(updateEffort, gbc__updateEffort);
		
		deleteEffort = new JButton("Delete effort");
		deleteEffort.setEnabled(false);
		GridBagConstraints gbc__deleteEffort = new GridBagConstraints();
		gbc__deleteEffort.insets = new Insets(5, 0, 5, 5);
		gbc__deleteEffort.anchor = GridBagConstraints.EAST;
		gbc__deleteEffort.fill = GridBagConstraints.HORIZONTAL;
		gbc__deleteEffort.gridx = 1;
		gbc__deleteEffort.gridy = 0;
		effortActions.add(deleteEffort, gbc__deleteEffort);
		
		cancelEffort = new JButton("Cancel");
		cancelEffort.setEnabled(false);
		GridBagConstraints gbc_cancelEffort = new GridBagConstraints();
		gbc_cancelEffort.insets = new Insets(5, 0, 5, 5);
		gbc_cancelEffort.gridx = 2;
		gbc_cancelEffort.gridy = 0;
		effortActions.add(cancelEffort, gbc_cancelEffort);
		
		//effortType.setModel(new DefaultComboBoxModel(EffortType.values()));
		//The model is set by the owning controller
		effortType.setRenderer(new CodedEntityRenderer<EffortType>());
	}
}
