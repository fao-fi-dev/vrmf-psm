/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.vessels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class VesselsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6349636225216590197L;

	@Getter @Inject private VesselsListPanel list;
	@Getter @Inject private VesselDataPanel data;
	
	private JSplitPane splitPane;
	
	public VesselsPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		list = new VesselsListPanel();
		data = new VesselDataPanel();
			
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		this.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Vessels management", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.columnWidths = new int[0];
		gbl_this.rowHeights = new int[0];
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);
		
		splitPane = new JSplitPane();
		splitPane.setContinuousLayout(true);
		
		GridBagConstraints gbc_splitPane = new GridBagConstraints();
		gbc_splitPane.weightx = 100.0;
		gbc_splitPane.weighty = 100.0;
		gbc_splitPane.fill = GridBagConstraints.BOTH;
		gbc_splitPane.gridx = 0;
		gbc_splitPane.gridy = 0;
		this.add(splitPane, gbc_splitPane);
	
		splitPane.setLeftComponent(list);
		
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		
		splitPane.setRightComponent(data);
		
		splitPane.setDividerLocation(650);
	}
}
