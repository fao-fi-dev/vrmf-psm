/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.spi;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.VesselUnloadingData;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 7, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 7, 2015
 */
public interface LogsheetEventTransshipmentListener {
	void logsheetEventTranshipmentStatusChanged(EditingStatus previous, EditingStatus current);
	
	void logsheetEventTranshipmentAdded(VesselUnloadingData added);
	void logsheetEventTranshipmentRemoved(VesselUnloadingData removed);
	void logsheetEventTranshipmentUpdated(VesselUnloadingData updated);
	void logsheetEventTranshipmentSelected(VesselUnloadingData selected);
	
	void logsheetEventTranshipmentCatchAdded(Catches added);
	void logsheetEventTranshipmentCatchRemoved(Catches removed);
	void logsheetEventTranshipmentCatchUpdated(Catches updated);
	void logsheetEventTranshipmentCatchSelected(Catches selected);
}
