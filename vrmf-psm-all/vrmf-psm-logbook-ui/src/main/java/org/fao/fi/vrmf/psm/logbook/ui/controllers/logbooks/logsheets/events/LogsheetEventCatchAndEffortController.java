/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks.logsheets.events;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.EffortsRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DiscardReason;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.CatchAndEffortPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.CatchesPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.DiscardsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.model.LogsheetEventCatchTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.model.LogsheetEventDiscardTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.efforts.EffortsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.efforts.model.LogsheetEventEffortTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.transshipments.TransshipmentsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CodedEntityTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.SpeciesTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 10, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 10, 2015
 */
@Named @Singleton
public class LogsheetEventCatchAndEffortController extends BaseController {
	@Inject private CatchAndEffortPanel catchAndEffortView;
	@Inject private TransshipmentsPanel transhipmentsView;
	
	@PostConstruct
	private void initialize() {
		EffortsPanel efforts = catchAndEffortView.getEffortsPanel();
		CatchesPanel catches = catchAndEffortView.getCatchesPanel();
		DiscardsPanel discards = catchAndEffortView.getDiscardsPanel();
		
		model.registerListener((LogsheetEventListener)this);
		
		JTable table = UIUtils.initialize(catches.getRecordsTable(), new LogsheetEventCatchTableModel(new ArrayList<Catches>()));
		table.setDefaultRenderer(Species.class, new SpeciesTableCellRenderer());
		table.setDefaultRenderer(QuantityType.class, new CodedEntityTableCellRenderer());
		
		UIUtils.fixColumnWidth(table, 0, 48);
		UIUtils.fixColumnWidth(table, 1, 48);
		UIUtils.fixColumnWidth(table, 2, 48);
		
		table = UIUtils.initialize(discards.getRecordsTable(), new LogsheetEventDiscardTableModel(new ArrayList<Discards>()));
		table.setDefaultRenderer(Species.class, new SpeciesTableCellRenderer());
		table.setDefaultRenderer(QuantityType.class, new CodedEntityTableCellRenderer());
		table.setDefaultRenderer(DiscardReason.class, new CodedEntityTableCellRenderer());
		
		UIUtils.fixColumnWidth(table, 0, 48);
		UIUtils.fixColumnWidth(table, 1, 48);
		UIUtils.fixColumnWidth(table, 2, 48);
		
		table = UIUtils.initialize(efforts.getEffortsTable(), new LogsheetEventEffortTableModel(new ArrayList<Effort>()));
		table.setDefaultRenderer(EffortType.class, new CodedEntityTableCellRenderer());
		
		UIUtils.fixColumnWidth(table, 0, 48);
		UIUtils.fixColumnWidth(table, 1, 48);
		UIUtils.fixColumnWidth(table, 2, 128);
		
		List<Species> allSpecies = commonServices.allSpecies();
		allSpecies.add(0, null);
		
		Species[] asArray = allSpecies.toArray(new Species[allSpecies.size()]);
		
		catches.getSpecies().setModel(new DefaultComboBoxModel(asArray));
		discards.getSpecies().setModel(new DefaultComboBoxModel(asArray));
		
		efforts.getEffortType().setModel(UIUtils.modelForCodedEntity(EffortType.class));
		catches.getQuantityUnit().setModel(UIUtils.modelForCodedEntity(QuantityType.class));
		
		catches.getQuantityUnit().setModel(UIUtils.modelForCodedEntity(QuantityType.class));
		
		discards.getQuantityUnit().setModel(UIUtils.modelForCodedEntity(QuantityType.class));
		discards.getReason().setModel(UIUtils.modelForCodedEntity(DiscardReason.class));
	}
	
	public void logsheetEventSelected(Event selected) {
		boolean isValid = selected != null;
		
		if(!isValid)
			return;
		
		boolean isCatchOrEffort = selected.isCatchEvent() || selected.isEffortEvent();
		boolean isTransshipment = selected.isTransshipmentEvent();
		
		catchAndEffortView.setVisible(isCatchOrEffort);
		
		if(!isCatchOrEffort) return;
		
		boolean displayEfforts = isValid && selected.isEffortEvent();
		boolean displayCatches = isValid && ( selected.isCatchEvent() || isTransshipment );
		boolean displayDiscards = isValid && selected.isCatchEvent();
		
		setCatchAndEffortTabStatus(0, displayEfforts);
		setCatchAndEffortTabStatus(1, displayCatches);
		setCatchAndEffortTabStatus(2, displayDiscards);
		
		updateTabName(0, selected.isEffortEvent() ? Utils.size(((EffortsRecord)selected).getEfforts()) : null);
		updateTabName(1, selected.isCatchEvent() ? Utils.size(((CatchesRecord)selected).getCatches()) : null);
		updateTabName(2, selected.isCatchEvent() ? Utils.size(((CatchesRecord)selected).getDiscards()) : null);
		
		catchAndEffortView.getCatchAndEffortTabs().setSelectedIndex(0);
		
		JTable effortTable = catchAndEffortView.getEffortsPanel().getEffortsTable();
		JTable catchTable = catchAndEffortView.getCatchesPanel().getRecordsTable();
		JTable discardsTable = catchAndEffortView.getDiscardsPanel().getRecordsTable();
		
		LogsheetEventEffortTableModel effortModel = (LogsheetEventEffortTableModel)effortTable.getModel();
		LogsheetEventCatchTableModel catchModel = (LogsheetEventCatchTableModel)catchTable.getModel();
		LogsheetEventDiscardTableModel discardModel = (LogsheetEventDiscardTableModel)discardsTable.getModel();
		
		if(displayEfforts && isValid) {
			effortModel.update(((EffortsRecord)selected).getEfforts());
		} else {
			effortModel.update(new ArrayList<Effort>());
		}
		
		if(displayCatches && isValid) {
			catchModel.update(((CatchesRecord)selected).getCatches());
			discardModel.update(((CatchesRecord)selected).getDiscards());
		} else {
			catchModel.update(new ArrayList<Catches>());
			discardModel.update(new ArrayList<Discards>());
		}
		
		boolean isCompleted = selected.isCompleted();
		
		catchAndEffortView.getEffortsPanel().getAddNew().setEnabled(isCompleted && displayEfforts && ( model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING ));
		catchAndEffortView.getCatchesPanel().getAddNew().setEnabled(isCompleted && displayCatches && ( model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING ));
		catchAndEffortView.getDiscardsPanel().getAddNew().setEnabled(isCompleted && displayCatches && ( model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING ));

		transhipmentsView.getAddNew().setEnabled(isCompleted && displayCatches && ( model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING ) );
		
//		AutoCompleteDecorator.decorate(view.getCatchesPanel().getSpecies(), SpeciesAutocompleter.INSTANCE);
//		AutoCompleteDecorator.decorate(view.getDiscardsPanel().getSpecies(), SpeciesAutocompleter.INSTANCE);
	};
	
	private void setCatchAndEffortTabStatus(int index, boolean enabled) {
		JTabbedPane tabbedPane = catchAndEffortView.getCatchAndEffortTabs();
		
		if(index >= 0) {
			tabbedPane.setEnabledAt(index, enabled);
		}
	}
	
	private void updateTabName(int index, Object replacement) {
		JTabbedPane tabbedPane = catchAndEffortView.getCatchAndEffortTabs();
		
		String title = tabbedPane.getTitleAt(index).replaceAll("\\d+|\\-", "#").replace("#", replacement == null ? "-" : replacement.toString());
		
		tabbedPane.setTitleAt(index, title);
	}
}
