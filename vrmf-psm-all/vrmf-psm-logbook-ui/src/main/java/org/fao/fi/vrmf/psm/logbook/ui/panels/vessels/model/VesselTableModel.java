/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.VesselType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class VesselTableModel extends AbstractTableModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 587756690700916447L;

	@Getter private String[] columnNames = new String[] { 
		"#", "Name", "Reg. country", "Reg. no.", "Type", "IRCS", "IMO"
	};

	@Getter private Object[][] rawData = null;

	/**
	 * Class constructor
	 *
	 * @param rawData
	 */
	public VesselTableModel(List<VesselData> data) {
		super();
		
		rawData = toRawData(data);
	}
	
	private Object[][] toRawData(List<VesselData> data) {
		Object[][] converted;
		
		if(data == null || data.isEmpty())
			converted = new Object[0][];
		else {
			List<Object[]> asList = new ArrayList<Object[]>();
			
			int counter = 0;
			for(VesselData in : data) {
				asList.add(extractValues(counter++, in));
			}
			
			converted = asList.toArray(new Object[0][]);
		}
		
		return converted;
	}
	
	public void update(List<VesselData> data) {
		rawData = toRawData(data);
		
		fireTableDataChanged();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return rawData.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}
	
	public Object getValueAt(int row, int col) {
		return rawData[row][col];
	}

	public Object[] extractValues(int row, VesselData original) {
		if(original == null) return new Object[0];
	
		List<Object> values = new ArrayList<Object>();
		
		for(int col=0; col<getColumnCount(); col++) {
			switch(col) {
				case 0:
					values.add(row + 1);
					break;
				case 1:
					values.add(original.getName());
					break;
				case 2:
					values.add(original.getRegistrationCountry());
					break;
				case 3:
					values.add(original.getRegistrationNumber());
					break;
				case 4:
					values.add(original.getType());
					break;
				case 5:
					values.add(original.getIrcs());
					break;
				case 6:
					values.add(original.getImo());
					break;
				default:
					values.add(null);
					break;
			}
		}
		
		return values.toArray(new Object[values.size()]);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		switch(c) {
			case 0:
				return Integer.class;
			case 2:
				return Country.class;
			case 1:
			case 3:
			case 5:
			case 6:
				return String.class;
			case 4:
				return VesselType.class;
			default:
				return Void.class;
		}
	}
	
	public Object[][] getData() {
		if(rawData == null) return new Object[0][];
		
		Object[][] toReturn = new Object[getRowCount()][getColumnCount()];
		
		for(int row=0; row < getRowCount(); row++) {
			toReturn[row] = new Object[getColumnCount()];
			
			for(int column=0; column < getColumnCount(); column++) {
				toReturn[row][column] = getValueAt(row, column);
			}
		}
		
		return toReturn;
	}
}