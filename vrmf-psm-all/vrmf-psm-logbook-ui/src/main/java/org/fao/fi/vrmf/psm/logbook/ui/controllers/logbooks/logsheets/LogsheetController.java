/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks.logsheets;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.VesselType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler;
import org.fao.fi.vrmf.psm.logbook.services.data.LogsheetsServices;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetDataPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetActionsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetPanel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;

import no.tornado.databinding.BindingGroup;
import no.tornado.databinding.BindingStrategy;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class LogsheetController extends BaseController {
	@Inject private LogsheetPanel view;
	
	private LogsheetDataPanel logsheetData;
	private LogsheetActionsPanel logsheetActions;
	
	@Inject private LogsheetsServices logsheetServices;
			
	private BindingGroup logsheetBindings;
	
	@PostConstruct
	private void initialize() {
		logsheetData = view.getLogsheetData();
		logsheetActions = view.getLogsheetActions();
		
		model.registerListener((LogsheetListener)this);
		model.registerListener((LogsheetEventListener)this);
		
		logsheetData.getVessel().addItemListener(new ItemListener() {
			private VesselData previous;
			
			/* (non-Javadoc)
			 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
			 */
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.DESELECTED) {
					previous = (VesselData)e.getItem();
				} else {
					if(e.getStateChange() == ItemEvent.SELECTED) {
						VesselData vessel = (VesselData)e.getItem();
						VesselType vesselType = vessel.getType();
						
						if(vessel != null) {
							try {
								model.getSelectedLogsheet().setTransitionHandler(StateTransitionHandler.forType(vesselType).initialize(model.getSelectedLogsheet().getEvents()));
							} catch(IllegalStateException ISe) {
								error("Cannot change logsheet vessel", "Selected vessel is of a type (" + vesselType.getCode() + " - " + vesselType.getName() + ") which is not compatible with the current sequence of events for the logsheet");

								logsheetData.getVessel().getModel().setSelectedItem(previous);
							} catch(IllegalArgumentException ISe) {
								_log().error("Unexpected exception caught", ISe);

								error("Problems in loading selected logsheet events", "The logsheet event sequence is not correct: " + ISe.getMessage());
								
								logsheetData.getVessel().getModel().setSelectedItem(previous);
							}
						}
					} 
				}
			}
		});

		logsheetData.getDepartureCountry().addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JComboBox departurePortCombo = logsheetData.getDeparturePort();
				
				if(e.getStateChange() == ItemEvent.DESELECTED) {
					departurePortCombo.setModel(new DefaultComboBoxModel(new Port[0]));
					departurePortCombo.setEnabled(false);
					
					departurePortCombo.setSelectedItem(null);
				} else if(e.getStateChange() == ItemEvent.SELECTED) {
					Country item = (Country)e.getItem();
					
					List<Port> forCountry = logsheetServices.portsForCountry(item);
					
					forCountry.add(0, null);
					
					departurePortCombo.setModel(new DefaultComboBoxModel(forCountry.toArray(new Port[forCountry.size()])));
					departurePortCombo.setEnabled(forCountry.size() > 1);
					
					departurePortCombo.setSelectedItem(null);
				}
			}
		});
		
		logsheetData.getDeparturePort().setEnabled(false);
		
		logsheetData.getUnloadingCountry().addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JComboBox unloadingPortCombo = logsheetData.getUnloadingPort();
				
				if(e.getStateChange() == ItemEvent.DESELECTED) {
					unloadingPortCombo.setModel(new DefaultComboBoxModel(new Port[0]));
					unloadingPortCombo.setEnabled(false);
					
					unloadingPortCombo.setSelectedItem(null);
				} else if(e.getStateChange() == ItemEvent.SELECTED) {
					Country item = (Country)e.getItem();
					
					List<Port> forCountry = logsheetServices.portsForCountry(item);
					
					forCountry.add(0, null);
					
					unloadingPortCombo.setModel(new DefaultComboBoxModel(forCountry.toArray(new Port[forCountry.size()])));
					unloadingPortCombo.setEnabled(forCountry.size() > 1);

					unloadingPortCombo.setSelectedItem(null);
				}
			}
		});
		
		logsheetData.getUnloadingPort().setEnabled(false);
		
		logsheetActions.getUpdateLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				updateLogsheetAction();
			}
		});
		
		logsheetActions.getRevertLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				revertLogsheetAction();
			}
		});
		
		logsheetActions.getBackToLogsheets().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				backToLogsheetsAction();
			}
		});
		
		logsheetActions.getCompleteLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				completeLogsheetAction();
			}
		});
		
		UIUtils.addValueChangeListener(logsheetData, new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				if(model.getSelectedLogsheet() != null) model.taintLogsheet();
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetAdded(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetAdded(Logsheet added) {
		logsheetData.getDeparturePort().setModel(new DefaultComboBoxModel(new Port[0]));
		logsheetData.getUnloadingPort().setModel(new DefaultComboBoxModel(new Port[0]));
		
		logsheetData.getDeparturePort().setEnabled(false);
		logsheetData.getUnloadingPort().setEnabled(false);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetSelected(Logsheet selected) {
		long end, start = System.currentTimeMillis();
		
		try {
			if(selected != null) {
				Logbook owner = model.getSelectedLogbook();
				
				String logbookNo = owner.computeLogbookId();
				
				logsheetData.getLogbookNo().setText(logbookNo);
				
				logsheetData.getVessel().setModel(new DefaultComboBoxModel(logsheetServices.allVessels().toArray(new Object[0])));
				
				List<Country> allCountriesWithPorts = commonServices.allCountriesWithValidPorts();
				allCountriesWithPorts.add(0, null);
				
				logsheetData.getDepartureCountry().setModel(new DefaultComboBoxModel(allCountriesWithPorts.toArray(new Country[0])));
				logsheetData.getUnloadingCountry().setModel(new DefaultComboBoxModel(allCountriesWithPorts.toArray(new Country[0])));
				
				if(selected.getDeparturePort() != null)
					logsheetData.getDeparturePort().setModel(new DefaultComboBoxModel(logsheetServices.portsForCountry(selected.getDeparturePort().getCountry()).toArray(new Port[0])));
				
				if(selected.getUnloadingPort() != null)
					logsheetData.getUnloadingPort().setModel(new DefaultComboBoxModel(logsheetServices.portsForCountry(selected.getUnloadingPort().getCountry()).toArray(new Port[0])));
				
				List<Event> events = selected.getEvents();
				
				if(events == null) events = new ArrayList<Event>();
	
				if(selected.getTransitionHandler() == null && selected.getVesselData() != null) {
					selected.setTransitionHandler(StateTransitionHandler.forType(selected.getVesselData().getType()).initialize(events));
				}
			}	
			
			if(selected != null) {
				bindLogsheet(selected);
			} else {
				unbindLogsheet();
			}
			
			model.refreshLogsheetStatus();
		} finally {
			end = System.currentTimeMillis();
			
			log().debug("Selecting the logsheet took {} mSec.", end - start);
			
			if(selected != null && selected.isCompleted())
				log().warn("Selected logsheet is completed and cannot be edited or deleted");
		}
	}
	
	private void bindLogsheet(Logsheet selected) {
		unbindLogsheet();
		
		if(selected != null) {
			logsheetBindings = new BindingGroup(selected);
			logsheetBindings.setDefaultBindingStrategy(BindingStrategy.ONFLUSH);

			logsheetBindings.add(logsheetData.getNumber(), "number");
			logsheetBindings.add(logsheetData.getVessel(), "vesselData");
			logsheetBindings.add(logsheetData.getCaptain(), "captainName");
			logsheetBindings.add(logsheetData.getCrew(), "crewSize");
			logsheetBindings.add(logsheetData.getDeparturePort(), "departurePort");
			logsheetBindings.add(logsheetData.getDepartureDate(), "departureDate");
			logsheetBindings.add(logsheetData.getUnloadingPort(), "unloadingPort");
			logsheetBindings.add(logsheetData.getUnloadingDate(), "arrivalDate");
			logsheetBindings.add(logsheetData.getFishOnboardStart(), "fishOnboardAtStart");
			logsheetBindings.add(logsheetData.getFishOnboardStartUnit(), "fishOnboardQuantityAtStart");
			logsheetBindings.add(logsheetData.getFishOnboardEnd(), "fishOnboardAfterUnloading");
			logsheetBindings.add(logsheetData.getFishOnboardEndUnit(), "fishOnboardQuantityAfterUnloading");
			logsheetBindings.add(logsheetData.getComment(), "comments");
			
			if(selected.getDeparturePort() != null) {
				logsheetData.getDepartureCountry().setSelectedItem(selected.getDeparturePort().getCountry());
			} else {
				logsheetData.getDepartureCountry().setSelectedItem(null);
			}

			if(selected.getUnloadingPort() != null) {
				logsheetData.getUnloadingCountry().setSelectedItem(selected.getUnloadingPort().getCountry());
			} else {
				logsheetData.getUnloadingCountry().setSelectedItem(null);
			}

			logsheetBindings.bind();
		}
	}
	
	private void unbindLogsheet() {
		if(logsheetBindings != null)
			logsheetBindings.unbind();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetStatusListener#notifyLogsheetStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetStatusChanged(EditingStatus previous, EditingStatus current) {
		logsheetData.notifyLogsheetStatusChange(current);
		logsheetActions.notifyLogsheetStatusChange(current, model.getSelectedLogsheet());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetStatusListener#notifyLogsheetStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventStatusChanged(EditingStatus previous, EditingStatus current) {
		logsheetData.notifyLogsheetEventStatusChange(model.getLogsheetStatus(), model.getSelectedLogsheet(), current, model.getSelectedEvent());
		logsheetActions.notifyLogsheetEventStatusChange(model.getLogsheetStatus(), model.getSelectedLogsheet(), current, model.getSelectedEvent());
	}

	private void updateLogsheetAction() {
		logsheetBindings.flushUIToModel();
		
		if(model.getLogsheetStatus() == EditingStatus.COMPLETING) {
			model.getSelectedLogsheet().setCompleted(true);
			model.getSelectedLogsheet().setCompletionDate(new Date());
		}
		
		List<CheckResult> checks = model.getSelectedLogsheet().checkCorrectness();
		
		if(model.getLogsheetStatus() == EditingStatus.COMPLETING) {
			model.getSelectedLogsheet().setCompleted(false);
			model.getSelectedLogsheet().setCompletionDate(null);
		}
		
		StringBuilder messages = new StringBuilder();
		
		if(checks != null && !checks.isEmpty()) {
			for(CheckResult in : checks) {
				messages.
					append("[").append(in.getType().getCode()).append("] : ").
					append(in.getMessage()).
					append("\n");
			}
		}
		
		if(CheckResultHelper.hasErrors(checks)) {
			if(model.getLogsheetStatus() == EditingStatus.COMPLETING) {
				Logsheet current = model.getSelectedLogsheet();
				
				//Resets the 'completed' status and the completion date in case there's an error
				current.setCompleted(false);
				current.setCompletionDate(null);
			}
			
			alert("Cannot update selected logsheet", messages.toString());
			
			log().error("Cannot update selected logsheet due to the following reasons:");

			for(CheckResult in : checks) {
				log().error("{} >> {}", in.getType().getCode(), in.getMessage());
			}
		} else {
			boolean proceed = !CheckResultHelper.hasWarnings(checks);
			
			if(!proceed) {
				proceed = 
					UIUtils.confirm(
						"Please confirm", 
						"Current logsheet data do contain the following inconsistencies:\n\n" + 
							messages.toString() + "\n" +
						"Are you sure you want to proceed?");
			}
			
			if(proceed) {
				try {
					Logsheet selectedLogsheet = model.getSelectedLogsheet();
				
					checks = selectedLogsheet.checkCorrectness();
					
					messages = new StringBuilder();
					
					if(checks != null && !checks.isEmpty()) {
						for(CheckResult in : checks) {
							messages.
								append("[").append(in.getType().getCode()).append("] : ").
								append(in.getMessage()).
								append("\n");
						}
					}
					
					if(CheckResultHelper.hasErrors(checks)) {
						error("Cannot update selected logsheet", messages.toString());
						
						log().error("Cannot update selected logsheet due to the following reasons:");

						for(CheckResult in : checks) {
							log().error("{} >> {}", in.getType().getCode(), in.getMessage());
						}
					} else {
						if(model.getLogsheetStatus() == EditingStatus.COMPLETING) selectedLogsheet.completeLogsheet();

						if(!selectedLogsheet.isCompleted()) {
							selectedLogsheet.setDerivedEfforts(null);
						}
						
						Logsheet current = model.updateLogsheet();
					
						deselectLogsheet();
						
						Logbook owner = model.getSelectedLogbook();
						
						model.select((Logbook)null);
						model.select(owner);
						
						if(!current.isCompleted()) {
							model.select(current, EditingStatus.EDITING);

							info("Operation successful", "Current logsheet has been successfully updated!");
						} else {
							application.show("logbooks");
							
							info("Operation successful", "Current logsheet has been successfully completed!");
						}
					}
				} catch(Throwable t) {
					log().error("Cannot update selected logsheet: {}", t.getMessage());
					
					error("Cannot update selected logsheet", t.getMessage());
				}
			}
		}
	}
	
	private void revertLogsheetAction() {
		if(model.getSelectedLogsheet() != null) {
			boolean proceed = 
				UIUtils.confirm(
					"Please confirm", 
					"Current logsheet data will be reverted to their original status\n\n" +
					"Are you sure you want to proceed?");

			if(proceed) {
				doRevertLogsheet();

				model.select(model.getSelectedLogsheet(), true);

				model.select((Event)null);
				
				model.refreshLogsheetStatus();
				model.refreshLogsheetEventStatus();
				
				log().info("Logsheet data have been reverted to their original status");
			}
		}
	}
	
	private void doRevertLogsheet() {
		Logsheet selectedLogsheet = model.getSelectedLogsheet();
		
		if(selectedLogsheet != null) {
			if(selectedLogsheet.getId() == null) {
				model.getSelectedLogbook().getLogsheets().remove(selectedLogsheet);
			} else {
				if(selectedLogsheet.getEvents() != null) {
					Iterator<Event> events = selectedLogsheet.getEvents().iterator();
					
					Event current;
					while(events.hasNext()) {
						current = events.next();
						
						if(current.getId() == null) events.remove();
					}
				}
				
				model.revert(selectedLogsheet);
			}
			
			unbindLogsheet();
		}
	}
	
	private void completeLogsheetAction() {
		logsheetBindings.flushUIToModel();
		
		List<CheckResult> checks = model.getSelectedLogsheet().checkCorrectness();
		
		StringBuilder messages = new StringBuilder();
		
		if(checks != null && !checks.isEmpty()) {
			for(CheckResult in : checks) {
				messages.
					append("[").append(in.getType().getCode()).append("] : ").
					append(in.getMessage()).
					append("\n");
			}
		}
		
		if(CheckResultHelper.hasErrors(checks)) {
			error("Cannot complete selected logsheet", messages.toString());
			
			log().error("Cannot complete selected logsheet due to the following reasons:");

			for(CheckResult in : checks) {
				log().error("{} >> {}", in.getType().getCode(), in.getMessage());
			}
		} else {
			info("You can now complete the logsheet by filling all the unloading details and then updating the logsheet itself");
		
			model.changeLogsheetStatus(EditingStatus.COMPLETING);
		}
	}
	
	private void backToLogsheetsAction() {
		if(application.confirmCancelUpdates(model.isDataModelTainted())) {
			List<Event> events = model.getSelectedLogsheet() == null ? null : model.getSelectedLogsheet().getEvents();
			
			if(events != null) {
				Iterator<Event> iterator = events.iterator();
				
				while(iterator.hasNext())
					if(iterator.next().getId() == null) iterator.remove();
			}
			
			doRevertLogsheet();
			
			deselectLogsheet();
			
			Logbook owner = model.getSelectedLogbook();
			
			model.select((Logbook)null);
			model.select(owner);
			
			application.show("logbooks");
		}
	}
	
	private void deselectLogsheet() {
		model.select((Logsheet)null);
	}
}