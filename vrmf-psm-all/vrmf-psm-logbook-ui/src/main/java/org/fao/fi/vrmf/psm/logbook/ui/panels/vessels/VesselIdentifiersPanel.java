/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.vessels;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Identifier;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.IdentifierType;
import org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.model.VesselIdentifiersTableModel;
import org.fao.fi.vrmf.psm.ui.common.renderers.IdentifierTypeRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CodedEntityTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.DefaultTableHeaderRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.StringTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 26, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 26, 2015
 */
@Named @Singleton
public class VesselIdentifiersPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7108947713224323027L;
	
	@Getter private JTable identifiersTable;
	
	@Getter private JNoFocusOutlineButton addIdentifier;
	
	@Getter private JNoFocusOutlineButton updateIdentifier;
	@Getter private JNoFocusOutlineButton deleteIdentifier;
	
	@Getter private JComboBox identifierType;
	@Getter private JTextField identifierValue;
	
	public VesselIdentifiersPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	/**
	 * Create the panel.
	 */
	@PostConstruct
	public void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {500, 0};
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		JPanel identifiersPanel = new JPanel();
		identifiersPanel.setBorder(new TitledBorder(null, "Identifiers", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_identifiersPanel = new GridBagConstraints();
		gbc_identifiersPanel.weightx = 200.0;
		gbc_identifiersPanel.weighty = 100.0;
		gbc_identifiersPanel.anchor = GridBagConstraints.NORTH;
		gbc_identifiersPanel.fill = GridBagConstraints.BOTH;
		gbc_identifiersPanel.gridx = 0;
		gbc_identifiersPanel.gridy = 0;
		add(identifiersPanel, gbc_identifiersPanel);
		GridBagLayout gbl_identifiersPanel = new GridBagLayout();
		gbl_identifiersPanel.rowHeights = new int[] {0, 32};
		gbl_identifiersPanel.columnWeights = new double[0];
		gbl_identifiersPanel.rowWeights = new double[0];
		identifiersPanel.setLayout(gbl_identifiersPanel);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.weightx = 100.0;
		gbc_scrollPane.anchor = GridBagConstraints.NORTH;
		gbc_scrollPane.weighty = 100.0;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(5, 5, 5, 5);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		identifiersPanel.add(scrollPane, gbc_scrollPane);
		
		identifiersTable = new JTable(new VesselIdentifiersTableModel(new ArrayList<Identifier>()) {
			/** Field serialVersionUID */
			private static final long serialVersionUID = 5210252037194577047L;
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
		
		identifiersTable.setAutoCreateRowSorter(true);
		identifiersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		identifiersTable.getTableHeader().setReorderingAllowed(false);
		
		identifiersTable.setDefaultRenderer(String.class, new StringTableCellRenderer(DefaultTableCellRenderer.LEFT));
		identifiersTable.setDefaultRenderer(IdentifierType.class, new CodedEntityTableCellRenderer());
		
		identifiersTable.getTableHeader().setDefaultRenderer(new DefaultTableHeaderRenderer(identifiersTable));
		
		scrollPane.setViewportView(identifiersTable);
		
		JPanel actionsPanel = new JPanel();
		GridBagConstraints gbc_actionsPanel = new GridBagConstraints();
		gbc_actionsPanel.weightx = 100.0;
		gbc_actionsPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_actionsPanel.anchor = GridBagConstraints.SOUTH;
		gbc_actionsPanel.gridx = 0;
		gbc_actionsPanel.gridy = 1;
		identifiersPanel.add(actionsPanel, gbc_actionsPanel);
		GridBagLayout gbl_actionsPanel = new GridBagLayout();
		gbl_actionsPanel.columnWidths = new int[0];
		gbl_actionsPanel.rowHeights = new int[0];
		gbl_actionsPanel.columnWeights = new double[0];
		gbl_actionsPanel.rowWeights = new double[0];
		actionsPanel.setLayout(gbl_actionsPanel);
		
		addIdentifier = new JNoFocusOutlineButton("Add identifier");
		addIdentifier.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.fill = GridBagConstraints.BOTH;
		gbc_btnAdd.insets = new Insets(5, 5, 5, 5);
		gbc_btnAdd.gridx = 0;
		gbc_btnAdd.gridy = 0;
		actionsPanel.add(addIdentifier, gbc_btnAdd);
		
		JPanel identifierPanel = new JPanel();
		identifierPanel.setBorder(new TitledBorder(null, "Selected identifier", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_identifierPanel = new GridBagConstraints();
		gbc_identifierPanel.weightx = 100.0;
		gbc_identifierPanel.weighty = 100.0;
		gbc_identifierPanel.anchor = GridBagConstraints.NORTH;
		gbc_identifierPanel.fill = GridBagConstraints.BOTH;
		gbc_identifierPanel.gridx = 1;
		gbc_identifierPanel.gridy = 0;
		add(identifierPanel, gbc_identifierPanel);
		GridBagLayout gbl_identifierPanel = new GridBagLayout();
		gbl_identifierPanel.columnWidths = new int[0];
		gbl_identifierPanel.rowHeights = new int[] {0, 32};
		gbl_identifierPanel.columnWeights = new double[0];
		gbl_identifierPanel.rowWeights = new double[0];
		identifierPanel.setLayout(gbl_identifierPanel);
		
		add(identifierPanel, gbc_identifierPanel);
		
		JPanel identifierData = new JPanel();
		identifierData.setBorder(null);
		GridBagConstraints gbc_identifierDataPanel = new GridBagConstraints();
		gbc_identifierDataPanel.weightx = 50.0;
		gbc_identifierDataPanel.anchor = GridBagConstraints.NORTH;
		gbc_identifierDataPanel.insets = new Insets(5, 5, 5, 5);
		gbc_identifierDataPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_identifierDataPanel.gridx = 0;
		gbc_identifierDataPanel.gridy = 0;
		identifierPanel.add(identifierData, gbc_identifierDataPanel);
		GridBagLayout gbl_identifierDataPanel = new GridBagLayout();
		gbl_identifierDataPanel.rowHeights = new int[0];
		gbl_identifierDataPanel.columnWeights = new double[0];
		gbl_identifierDataPanel.rowWeights = new double[0];
		identifierData.setLayout(gbl_identifierDataPanel);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.weightx = 10.0;
		gbc_lblType.fill = GridBagConstraints.BOTH;
		gbc_lblType.anchor = GridBagConstraints.NORTH;
		gbc_lblType.insets = new Insets(5, 5, 5, 5);
		gbc_lblType.gridx = 0;
		gbc_lblType.gridy = 0;
		identifierData.add(lblType, gbc_lblType);
		
		JLabel lblIdentifier = new JLabel("Identifier");
		lblIdentifier.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblIdentifier = new GridBagConstraints();
		gbc_lblIdentifier.insets = new Insets(5, 5, 5, 5);
		gbc_lblIdentifier.fill = GridBagConstraints.BOTH;
		gbc_lblIdentifier.anchor = GridBagConstraints.NORTH;
		gbc_lblIdentifier.gridx = 0;
		gbc_lblIdentifier.gridy = 1;
		identifierData.add(lblIdentifier, gbc_lblIdentifier);
		
		identifierType = new JComboBox(IdentifierType.values());
		identifierType.setRenderer(new IdentifierTypeRenderer());
		
		GridBagConstraints gbc_type = new GridBagConstraints();
		gbc_type.ipady = 5;
		gbc_type.weightx = 30.0;
		gbc_type.anchor = GridBagConstraints.WEST;
		gbc_type.insets = new Insets(5, 5, 5, 5);
		gbc_type.fill = GridBagConstraints.BOTH;
		gbc_type.gridx = 1;
		gbc_type.gridy = 0;
		identifierData.add(identifierType, gbc_type);
		
		identifierValue = new JTextField();
		GridBagConstraints gbc_identifier = new GridBagConstraints();
		gbc_identifier.weightx = 30.0;
		gbc_identifier.anchor = GridBagConstraints.WEST;
		gbc_identifier.insets = new Insets(5, 5, 5, 5);
		gbc_identifier.fill = GridBagConstraints.BOTH;
		gbc_identifier.gridx = 1;
		gbc_identifier.gridy = 1;
		identifierData.add(identifierValue, gbc_identifier);
		
		JPanel actionsPanelIdentifier = new JPanel();
		GridBagConstraints gbc_actionsPanelIdentifier = new GridBagConstraints();
		gbc_actionsPanelIdentifier.weightx = 50.0;
		gbc_actionsPanelIdentifier.weighty = 100.0;
		gbc_actionsPanelIdentifier.anchor = GridBagConstraints.NORTH;
		gbc_actionsPanelIdentifier.fill = GridBagConstraints.HORIZONTAL;
		gbc_actionsPanel.anchor = GridBagConstraints.SOUTH;
		gbc_actionsPanelIdentifier.gridx = 0;
		gbc_actionsPanelIdentifier.gridy = 1;
		identifierPanel.add(actionsPanelIdentifier, gbc_actionsPanelIdentifier);
		GridBagLayout gbl_actionsPanelIdentifier = new GridBagLayout();
		gbl_actionsPanelIdentifier.columnWidths = new int[0];
		gbl_actionsPanelIdentifier.rowHeights = new int[0];
		gbl_actionsPanelIdentifier.columnWeights = new double[0];
		gbl_actionsPanelIdentifier.rowWeights = new double[0];
		actionsPanelIdentifier.setLayout(gbl_actionsPanelIdentifier);
		
		updateIdentifier = new JNoFocusOutlineButton("Update identifier");
		updateIdentifier.setFont(new Font("Tahoma", Font.PLAIN, 14));
		updateIdentifier.setEnabled(false);
		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.fill = GridBagConstraints.BOTH;
		gbc_btnUpdate.insets = new Insets(5, 5, 5, 5);
		gbc_btnUpdate.gridx = 0;
		gbc_btnUpdate.gridy = 0;
		actionsPanelIdentifier.add(updateIdentifier, gbc_btnUpdate);
		
		deleteIdentifier = new JNoFocusOutlineButton("Delete identifier");
		deleteIdentifier.setFont(new Font("Tahoma", Font.PLAIN, 14));
		deleteIdentifier.setEnabled(false);
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.insets = new Insets(5, 5, 5, 5);
		gbc_btnDelete.gridx = 1;
		gbc_btnDelete.gridy = 0;
		actionsPanelIdentifier.add(deleteIdentifier, gbc_btnDelete);
	}
	
	public void updateFields(Identifier selected) {
		boolean enabled = selected != null;
		for(JComponent in : new JComponent[] {
			identifierType, identifierValue
		}) {
			in.setEnabled(enabled);
		}
	}
	
	public void updateButtons(Identifier selected) {
		deleteIdentifier.setEnabled(selected != null);
		updateIdentifier.setEnabled(selected != null);
	}
	
	public void updateInputs(Identifier selected) {
		updateFields(selected);
		updateButtons(selected);
	}
}
