/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.spi;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 7, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 7, 2015
 */
public interface LogsheetEventEffortListener {
	void logsheetEventEffortStatusChanged(EditingStatus previous, EditingStatus current);
	
	void logsheetEventEffortAdded(Effort added);
	void logsheetEventEffortRemoved(Effort removed);
	void logsheetEventEffortUpdated(Effort updated);
	void logsheetEventEffortSelected(Effort selected);
}
