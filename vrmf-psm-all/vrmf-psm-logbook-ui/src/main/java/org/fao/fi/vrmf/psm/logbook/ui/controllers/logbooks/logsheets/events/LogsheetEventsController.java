/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks.logsheets.events;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;
import javax.swing.event.ListSelectionEvent;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchAndEffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.EffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.TransshipmentEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetEventsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetActionsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetAddEventActionsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetEventActionsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetEventPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.model.LogsheetEventTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.CatchesReportPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.DerivedEffortsReportPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.DiscardsReportPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.EffortsReportPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.CatchesReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.DerivedEffortsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.DiscardsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.EffortsReportTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventEffortListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.ui.common.renderers.CountryRenderer;
import org.fao.fi.vrmf.psm.ui.common.spi.CoordinatesChangeListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.EmbeddedTileFactoryInfo;
import org.fao.fi.vrmf.psm.ui.common.support.components.JCoordinatesSelectorDialog;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingListSelectionListener;
import org.jxmapviewer.viewer.DefaultTileFactory;

import no.tornado.databinding.BindingGroup;
import no.tornado.databinding.BindingStrategy;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class LogsheetEventsController extends BaseController {
	@Inject private LogsheetPanel logsheetView;
	
	private LogsheetActionsPanel actions;
	private LogsheetEventsPanel eventsList;
	private EffortsReportPanel effortsReport;
	private DerivedEffortsReportPanel derivedEffortsReport;
	private CatchesReportPanel catchesReport;
	private DiscardsReportPanel discardsReport;
	
	private LogsheetAddEventActionsPanel addEventActions;
	private LogsheetEventPanel event;
	private LogsheetEventActionsPanel eventActions;
			
	private BindingGroup eventBindings;
	
	@PostConstruct
	private void initialize() {
		actions = logsheetView.getLogsheetActions();

		eventsList = logsheetView.getLogsheetDetails().getEventsPanel();
		effortsReport = logsheetView.getLogsheetDetails().getEffortsPanel();
		derivedEffortsReport = logsheetView.getLogsheetDetails().getDerivedEffortsPanel();
		catchesReport = logsheetView.getLogsheetDetails().getCatchesPanel();
		discardsReport = logsheetView.getLogsheetDetails().getDiscardsPanel();
		
		event = logsheetView.getLogsheetEvent();
		eventActions = event.getLogsheetEventActions();
		addEventActions = logsheetView.getLogsheetAddEventActions();
		
		List<Country> allCountries = commonServices.allCountries();
		
		event.getVesselRegistrationCountry().setRenderer(new CountryRenderer());
		event.getVesselRegistrationCountry().setModel(new DefaultComboBoxModel(allCountries.toArray(new Country[allCountries.size()])));
		
		model.registerListener((LogsheetListener)this);
		model.registerListener((LogsheetEventListener)this);
		model.registerListener((LogsheetEventEffortListener)this);
		model.registerListener((LogsheetEventCatchListener)this);
		model.registerListener((LogsheetEventDiscardListener)this);
		
		eventsList.getLogsheetsEventsTable().getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				int selectedIndex = eventsList.getLogsheetsEventsTable().getSelectedRow();
				
				Event selected = null;
				
				List<Event> events = model.getSelectedLogsheet() != null && model.getSelectedLogsheet().getEvents() != null ? model.getSelectedLogsheet().getEvents() : null;
				
				if(events != null && selectedIndex >= 0 && selectedIndex <= events.size() - 1) {
					selected = events.get(eventsList.getLogsheetsEventsTable().convertRowIndexToModel(selectedIndex));
				}
				
				boolean same = selected != null && selected.equals(model.getSelectedEvent());

				if(same || selected == null) return;
				
				if(application.confirmCancelUpdates(model.isEventDirty() || model.getEventStatus() == EditingStatus.ADDING)) {
					if(model.isEventDirty()) doRevertEvent(model.getSelectedEvent());
					
					model.select(selected, model.getLogsheetStatus());
				} else {
					int index = events.indexOf(model.getSelectedEvent());
					
					index = eventsList.getLogsheetsEventsTable().convertRowIndexToView(index);
					
					eventsList.getLogsheetsEventsTable().setRowSelectionInterval(index, index);
					
					model.taintEvent();
				}
			}
		});
		
		eventActions.getUpdateEvent().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				updateEventAction(false);
			}
		});
		
		eventActions.getSaveEvent().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				updateEventAction(true);
			}
		});
		
		eventActions.getDeleteEvent().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				deleteEventAction();
			}
		});
		
		eventActions.getCancel().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				cancelEventEditingAction();
			}
		});
		
		addEventActions.getTransit().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEventAction(new Event(EventType.TRANSIT));
			}
		});
		
		addEventActions.getBadWeather().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEventAction(new Event(EventType.BAD_WEATHER));
			}
		});
		
		addEventActions.getCleaning().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEventAction(new Event(EventType.CLEANING));
			}
		});
		
		addEventActions.getTransshipping().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEventAction(new TransshipmentEvent(EventType.TRANSSHIPPING));
			}
		});
		
		addEventActions.getSearching().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEventAction(new Event(EventType.SEARCHING));
			}
		});
		
		addEventActions.getSetting().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEventAction(new EffortEvent(EventType.SETTING));
			}
		});
		
		addEventActions.getRetrieving().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEventAction(new CatchEvent(EventType.RETRIEVING));
			}
		});
		
		addEventActions.getFishing().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addEventAction(new CatchAndEffortEvent(EventType.FISHING));
			}
		});
		
		event.getEditStartCoordinates().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				editStartCoordinatesAction();
			}
		});

		event.getEditEndCoordinates().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				editEndCoordinatesAction();
			}
		});
		
		UIUtils.addValueChangeListener(event, new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				if(model.getSelectedEvent() != null) model.taintEvent();
			}
		});
	}
	
	private void doRevertEvent(Event toRevert) {
		model.revert(toRevert);
	}
	
	private Event lastEvent() {
		Logsheet selectedLogsheet = model.getSelectedLogsheet();
		
		if(selectedLogsheet == null || 
		   selectedLogsheet.getEvents() == null || 
		   selectedLogsheet.getEvents().isEmpty()) return null;
		
		return selectedLogsheet.getEvents().get(selectedLogsheet.getEvents().size() - 1);
	}

	private void addEventAction(Event toAdd) {
		Logsheet logsheet = model.getSelectedLogsheet();
		
		Event last = lastEvent();
		
		if(last != null) {
			if(last.getEndDate() == null) throw new IllegalStateException("Cannot add an event if the last one is missing an ending date");
			if(last.getCoordinatesAtEnd() == null) throw new IllegalStateException("Cannot add an event if the last one is missing the ending coordinates");
			
			if(prefs.isAutoFillNewEvents()) {
				toAdd.setStartDate(last.getEndDate());
				toAdd.setCoordinatesAtStart(last.getCoordinatesAtEnd());
				
				log().info("Start date / coordinates for this event have been set to the ending date / coordinates of previous event");
			}
		} else {
			toAdd.setStartDate(new Date());
			
			if(prefs.isAutoFillNewEvents() && logsheet.getDeparturePort() != null && logsheet.getDeparturePort().getCoordinates() != null) {
				toAdd.setCoordinatesAtStart(logsheet.getDeparturePort().getCoordinates());
				toAdd.setStartDate(logsheet.getDepartureDate());
				
				log().info("Start date / coordinates for this event have been set to the departure date / departure port coordinates");
			}
		}
		
		if(prefs.isAutoFillNewEvents()) {
			toAdd.setEndDate(new Date(toAdd.getStartDate().getTime() + ( 60 * 60 * 1000 )));
			toAdd.setCoordinatesAtEnd(toAdd.getCoordinatesAtStart());
			
			log().info("End date / coordinates for this event have been set to the start date + 1h / start coordinates of the same event");
		}
		
		model.addLogsheetEvent(toAdd);
		
		model.untaintEvent();
	}
	
	private void bindEvent(Event selected) {
		if(selected != null) {
			if(eventBindings != null) unbindEvent();
			
			eventBindings = new BindingGroup(selected);
			
			eventBindings.setDefaultBindingStrategy(BindingStrategy.ONFLUSH);

			eventBindings.add(event.getStartTime(), "startDate");
			eventBindings.add(event.getStartCoordinates(), "coordinatesAtStart");
			eventBindings.add(event.getEndTime(), "endDate");
			eventBindings.add(event.getEndCoordinates(), "coordinatesAtEnd");
			eventBindings.add(event.getComments(), "comments");
			
			if(selected.isTransshipmentEvent()) {
				eventBindings.add(event.getVesselName(), "vesselName");
				eventBindings.add(event.getVesselRegistrationCountry(), "vesselRegistrationCountry");
				eventBindings.add(event.getVesselRegistrationNumber(), "vesselRegistrationNumber");
				eventBindings.add(event.getVesselDestination(), "destination");
			}
			
			eventBindings.bind();
		}
	}
	
	private void unbindEvent() {
		if(eventBindings != null)
			eventBindings.unbind();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetSelected(Logsheet selected) {
		if(selected == null) {
			eventsList.getLogsheetsEventsTable().getSelectionModel().clearSelection();
		} else {
			List<Event> logsheetEvents = selected.getEvents();
			
			if(logsheetEvents == null) selected.setEvents(logsheetEvents = new ArrayList<Event>());
			
			refreshModels();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetStatusListener#notifyLogsheetStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetStatusChanged(EditingStatus previous, EditingStatus current) {
		event.notifyLogsheetStatusChange(current, model.getSelectedLogsheet());
		actions.notifyLogsheetStatusChange(current, model.getSelectedLogsheet());
		addEventActions.notifyLogsheetStatusChange(current, model.getSelectedLogsheet());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventSelected(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventSelected(Event selected) {
		if(selected != null) {
			bindEvent(selected);
			
			event.getType().setText(selected.getType().getCode() + " - " + selected.getType().getName());
		}
		
		event.setVisible(selected != null);
		eventActions.setVisible(selected != null);
		
		event.getPadder().setVisible(!event.getCatchAndEfforts().isVisible());

		eventsList.getLogsheetsEventsTable().setEnabled(selected == null || model.getLogsheetStatus() == EditingStatus.VIEWING || model.getLogsheetStatus() == EditingStatus.NOT_SET);

		effortsReport.getEffortsTable().setEnabled(selected == null);
		derivedEffortsReport.getEffortsTable().setEnabled(selected == null);
		catchesReport.getCatchesTable().setEnabled(selected == null);
		discardsReport.getDiscardsTable().setEnabled(selected == null);
		
		event.updateUI(selected);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventAdded(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventAdded(Event added) {
		model.select(added, EditingStatus.ADDING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventUpdated(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventUpdated(Event updated) {
		refreshModels();
	};

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventStatusListener#notifyLogsheetEventStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	public void logsheetEventStatusChanged(EditingStatus previous, EditingStatus current) {
		event.notifyEventStatusChange(current, model.getSelectedEvent());
		addEventActions.notifyEventStatusChange(model.getLogsheetStatus(), model.getSelectedLogsheet(), current, model.getSelectedEvent());
		eventActions.notifyEventStatusChange(model.getLogsheetStatus(), model.getSelectedLogsheet(), current, model.getSelectedEvent());
		
		event.getEditStartCoordinates().setEnabled(EditingStatus.ADDING == current || EditingStatus.EDITING == current);
		event.getEditEndCoordinates().setEnabled(EditingStatus.ADDING == current || EditingStatus.EDITING == current);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventEffortStatusChanged(EditingStatus previous, EditingStatus current) {
		boolean isModifying = model.getSelectedEffort() != null && ( current == EditingStatus.ADDING || current == EditingStatus.EDITING );
		
		if(isModifying) {
			logsheetEventStatusChanged(EditingStatus.VIEWING, EditingStatus.VIEWING);
		} else {
			model.refreshLogsheetEventStatus();
		};
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventCatchStatusChanged(EditingStatus previous, EditingStatus current) {
		boolean isModifying = model.getSelectedCatch() != null && ( current == EditingStatus.ADDING || current == EditingStatus.EDITING );
		
		if(isModifying) {
			logsheetEventStatusChanged(EditingStatus.VIEWING, EditingStatus.VIEWING);
		} else {
			model.refreshLogsheetEventStatus();
		};
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventDiscardStatusChanged(EditingStatus previous, EditingStatus current) {
		boolean isModifying = model.getSelectedDiscard() != null && ( current == EditingStatus.ADDING || current == EditingStatus.EDITING );
		
		if(isModifying) {
			logsheetEventStatusChanged(EditingStatus.VIEWING, EditingStatus.VIEWING);
		} else {
			model.refreshLogsheetEventStatus();
		};
	}
	
	private void updateEventAction(boolean close) {
		String startCoordinates = event.getStartCoordinates().getText();
		String endCoordinates = event.getEndCoordinates().getText();
		
		eventBindings.flushUIToModel();
		
		model.getSelectedEvent().setCoordinatesAtStart(startCoordinates);
		model.getSelectedEvent().setCoordinatesAtEnd(endCoordinates);
		
		List<CheckResult> checks = model.getSelectedEvent().checkCorrectness();
		
		StringBuilder messages = new StringBuilder();
		
		if(checks != null && !checks.isEmpty()) {
			for(CheckResult in : checks) {
				messages.
					append("[").append(in.getType().getCode()).append("] : ").
					append(in.getMessage()).
					append("\n");
			}
		}
		
		if(CheckResultHelper.hasErrors(checks)) {
			error("Cannot update selected event", messages.toString());
			
			log().error("Cannot update selected event due to the following reasons:");

			for(CheckResult in : checks) {
				log().error("{} >> {}", in.getType().getCode(), in.getMessage());
			}
		} else {
			boolean proceed = !CheckResultHelper.hasWarnings(checks);
			
			if(!proceed) {
				proceed = 
					UIUtils.confirm(
						"Please confirm", 
						"Current event does contain the following inconsistencies:\n\n" + 
							messages.toString() + "\n" +
						"Are you sure you want to proceed?");
			}
			
			if(proceed) {
				Logsheet selectedLogsheet = model.getSelectedLogsheet();
				
				selectedLogsheet.getTransitionHandler().initialize(selectedLogsheet.getEvents());

				Event selected = model.getSelectedEvent();

				logsheetEventUpdated(selected);
				
				model.select((Event)null);
				
				model.taintLogsheet();
				model.untaintEvent();
				
				cancelEventEditingAction();
				
				info("The event has been correctly updated");
				
				if(!close) model.select(selected, EditingStatus.EDITING);
			}
		}
	}
	
	private void deleteEventAction() {
		if(UIUtils.confirm(
			"Please confirm", 
			"Are you sure you want to delete the selected event?"
		)) {
			List<Event> logsheetEvents = model.getSelectedLogsheet().getEvents();
			
			Event selected = model.getSelectedEvent();
			logsheetEvents.remove(selected);
			
			model.getSelectedLogsheet().getTransitionHandler().initialize(model.getSelectedLogsheet().getEvents());

			logsheetEventUpdated(selected);

			model.select((Event)null);
			
			if(selected.getId() != null) model.taintLogsheet();
			
			cancelEventEditingAction();
		}
	}
	
	private void cancelEventEditingAction() {
		if(model.getEventStatus() == EditingStatus.ADDING) {
			if(model.getSelectedEvent() != null && model.getSelectedEvent().getId() == null) model.getSelectedLogsheet().getEvents().remove(model.getSelectedEvent());
		}
		
		bindEvent(new Event());
		
		model.select((Event)null);
		
		eventsList.getLogsheetsEventsTable().clearSelection();
	}
	
	private void refreshModels() {
		Logsheet selected = model.getSelectedLogsheet();
		
		if(selected != null) {
			((LogsheetEventTableModel)eventsList.getLogsheetsEventsTable().getModel()).update(model.getSelectedLogsheet().getEvents());
			((EffortsReportTableModel)effortsReport.getEffortsTable().getModel()).update(model.getSelectedLogsheet().uniqueEfforts());
			((DerivedEffortsReportTableModel)derivedEffortsReport.getEffortsTable().getModel()).update(model.getSelectedLogsheet().uniqueDerivedEfforts());
			((CatchesReportTableModel)catchesReport.getCatchesTable().getModel()).update(model.getSelectedLogsheet().uniqueCatches());
			((DiscardsReportTableModel)discardsReport.getDiscardsTable().getModel()).update(model.getSelectedLogsheet().uniqueDiscards());
		}
		
		int events = selected == null || selected.getEvents() == null ? 0 : selected.getEvents().size();
		int efforts = selected == null ? 0 : selected.uniqueEfforts().size();
		int derivedEfforts = selected == null ? 0 : selected.uniqueDerivedEfforts().size();
		int catches = selected == null ? 0 : selected.uniqueCatches().size();
		int discards = selected == null ? 0 : selected.uniqueDiscards().size();
		
		logsheetView.getLogsheetDetails().getTabs().setEnabledAt(0, events > 0);
		logsheetView.getLogsheetDetails().getTabs().setEnabledAt(1, efforts > 0);
		logsheetView.getLogsheetDetails().getTabs().setEnabledAt(2, derivedEfforts > 0);
		logsheetView.getLogsheetDetails().getTabs().setEnabledAt(3, catches > 0);
		logsheetView.getLogsheetDetails().getTabs().setEnabledAt(4, discards > 0);
		
		updateTabName(0, events == 0 ? null : events);
		updateTabName(1, efforts == 0 ? null : efforts);
		updateTabName(2, derivedEfforts == 0 ? null : derivedEfforts);
		updateTabName(3, catches == 0 ? null : catches);
		updateTabName(4, discards == 0 ? null : discards);
	}
	
	private void updateTabName(int index, Object replacement) {
		JTabbedPane tabbedPane = logsheetView.getLogsheetDetails().getTabs();
		
		String title = tabbedPane.getTitleAt(index).replaceAll("\\d+|\\-", "#").replace("#", replacement == null ? "-" : replacement.toString());
		
		tabbedPane.setTitleAt(index, title);
	}
	
	private void editStartCoordinatesAction() {
		CoordinatesChangeListener listener = new CoordinatesChangeListener() {
			@Override
			public void coordinatesSelected(String coordinates) {
				event.getStartCoordinates().setText(coordinates);
			}
			
			@Override
			public void coordinatesCanceled() {
			}
		};
		
		new JCoordinatesSelectorDialog(
			application.getApplicationFrame(), 
			prefs.isOffline() ? new DefaultTileFactory(new EmbeddedTileFactoryInfo()) : null, 
			event.getStartCoordinates().getText(), 
			listener
		);
	}
	
	private void editEndCoordinatesAction() {
		CoordinatesChangeListener listener = new CoordinatesChangeListener() {
			@Override
			public void coordinatesSelected(String coordinates) {
				event.getEndCoordinates().setText(coordinates);
			}
			
			@Override
			public void coordinatesCanceled() {
			}
		};
		
		new JCoordinatesSelectorDialog(
			application.getApplicationFrame(), 
			prefs.isOffline() ? new DefaultTileFactory(new EmbeddedTileFactoryInfo()) : null, 
			event.getEndCoordinates().getText(), 
			listener
		);
	}
}