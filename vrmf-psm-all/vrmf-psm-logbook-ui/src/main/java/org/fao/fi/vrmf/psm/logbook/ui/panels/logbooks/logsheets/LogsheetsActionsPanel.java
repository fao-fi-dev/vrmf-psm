/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;

import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class LogsheetsActionsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8872498289521272459L;
	
	@Getter private JNoFocusOutlineButton viewLogsheet;
	@Getter private JNoFocusOutlineButton modifyLogsheet;
	@Getter private JNoFocusOutlineButton deleteLogsheet;
	@Getter private JNoFocusOutlineButton printLogsheet;
	@Getter private JNoFocusOutlineButton cancel;
	
	public LogsheetsActionsPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.columnWidths = new int[0];
		gbl_this.rowHeights = new int[0];
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);
		
		viewLogsheet = new JNoFocusOutlineButton("View logsheet");
		viewLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_viewLogsheet = new GridBagConstraints();
		gbc_viewLogsheet.fill = GridBagConstraints.BOTH;
		gbc_viewLogsheet.anchor = GridBagConstraints.NORTH;
		gbc_viewLogsheet.insets = new Insets(0, 0, 0, 5);
		gbc_viewLogsheet.gridx = 0;
		gbc_viewLogsheet.gridy = 0;
		add(viewLogsheet, gbc_viewLogsheet);
	
		modifyLogsheet = new JNoFocusOutlineButton("Edit logsheet");
		modifyLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		modifyLogsheet.setEnabled(false);
		GridBagConstraints gbc_btnAddNew = new GridBagConstraints();
		gbc_btnAddNew.anchor = GridBagConstraints.NORTH;
		gbc_btnAddNew.fill = GridBagConstraints.BOTH;
		gbc_btnAddNew.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddNew.gridx = 1;
		gbc_btnAddNew.gridy = 0;
		add(modifyLogsheet, gbc_btnAddNew);
		
		deleteLogsheet = new JNoFocusOutlineButton("Delete logsheet");
		deleteLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		deleteLogsheet.setEnabled(false);
		GridBagConstraints gbc_deleteLogsheet = new GridBagConstraints();
		gbc_deleteLogsheet.insets = new Insets(0, 0, 0, 5);
		gbc_deleteLogsheet.gridx = 2;
		gbc_deleteLogsheet.gridy = 0;
		add(deleteLogsheet, gbc_deleteLogsheet);
		
		printLogsheet = new JNoFocusOutlineButton("Print logsheet");
		printLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		printLogsheet.setEnabled(false);
		GridBagConstraints gbc_printLogsheet = new GridBagConstraints();
		gbc_printLogsheet.insets = new Insets(0, 0, 0, 5);
		gbc_printLogsheet.gridx = 3;
		gbc_printLogsheet.gridy = 0;
		add(printLogsheet, gbc_printLogsheet);
		
		cancel = new JNoFocusOutlineButton("Cancel");
		cancel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cancel.setEnabled(true);
		GridBagConstraints gbc_cancel = new GridBagConstraints();
		gbc_cancel.insets = new Insets(0, 0, 0, 5);
		gbc_cancel.gridx = 4;
		gbc_cancel.gridy = 0;
		add(cancel, gbc_cancel);
	}
}
