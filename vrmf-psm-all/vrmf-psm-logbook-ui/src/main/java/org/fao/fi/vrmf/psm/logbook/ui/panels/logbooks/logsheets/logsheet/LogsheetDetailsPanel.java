/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetEventsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.CatchesReportPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.DerivedEffortsReportPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.DiscardsReportPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.EffortsReportPanel;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;
import javax.swing.border.TitledBorder;
import java.awt.Insets;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 10, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 10, 2015
 */
@Named
public class LogsheetDetailsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2396944113608681889L;
	
	@Inject @Getter private LogsheetEventsPanel eventsPanel;
	@Inject @Getter private EffortsReportPanel effortsPanel;
	@Inject @Getter private DerivedEffortsReportPanel derivedEffortsPanel;
	@Inject @Getter private CatchesReportPanel catchesPanel;
	@Inject @Getter private DiscardsReportPanel discardsPanel;
	
	@Getter private JTabbedPane tabs;
	
	@Getter private JPanel eventsContainer;
	@Getter private JPanel derivedEffortsReportContainer;
	@Getter private JPanel effortsReportContainer;
	@Getter private JPanel catchesReportContainer;
	@Getter private JPanel discardReportsContainer;
	
	/**
	 * Class constructor
	 */
	public LogsheetDetailsPanel() {
		setBorder(new TitledBorder(null, "Selected logsheet reports", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		if(Utils.IS_LAUNCHING) return;
	
		eventsPanel = new LogsheetEventsPanel();
		effortsPanel = new EffortsReportPanel();
		derivedEffortsPanel = new DerivedEffortsReportPanel();
		catchesPanel = new CatchesReportPanel();
		discardsPanel = new DiscardsReportPanel();
		
		initialize();
	}
	
	@PostConstruct
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		tabs = new JTabbedPane(JTabbedPane.TOP);
		tabs.setEnabled(true);
		GridBagConstraints gbc_catchAndEffortTabs = new GridBagConstraints();
		gbc_catchAndEffortTabs.insets = new Insets(5, 5, 5, 5);
		gbc_catchAndEffortTabs.weighty = 100.0;
		gbc_catchAndEffortTabs.weightx = 100.0;
		gbc_catchAndEffortTabs.fill = GridBagConstraints.BOTH;
		gbc_catchAndEffortTabs.gridx = 0;
		gbc_catchAndEffortTabs.gridy = 0;
		add(tabs, gbc_catchAndEffortTabs);
		
		eventsContainer = new JPanel();
		tabs.addTab("Logsheet events (#)", null, eventsContainer, "Events for this logsheet");
		GridBagLayout gbl__eventsContainer = new GridBagLayout();
		gbl__eventsContainer.columnWidths = new int[0];
		gbl__eventsContainer.rowHeights = new int[0];
		gbl__eventsContainer.columnWeights = new double[0];
		gbl__eventsContainer.rowWeights = new double[0];
		eventsContainer.setLayout(gbl__eventsContainer);
		
		GridBagConstraints gbc_eventsPanel = new GridBagConstraints();
		gbc_eventsPanel.weighty = 100.0;
		gbc_eventsPanel.weightx = 100.0;
		gbc_eventsPanel.fill = GridBagConstraints.BOTH;
		gbc_eventsPanel.gridx = 0;
		gbc_eventsPanel.gridy = 0;
		eventsContainer.add(eventsPanel, gbc_eventsPanel);
		
		effortsReportContainer = new JPanel();
		tabs.addTab("Logsheet efforts (#)", null, effortsReportContainer, "Effort reports for this logsheet");
		GridBagLayout gbl__effortsContainer = new GridBagLayout();
		gbl__effortsContainer.columnWidths = new int[0];
		gbl__effortsContainer.rowHeights = new int[0];
		gbl__effortsContainer.columnWeights = new double[0];
		gbl__effortsContainer.rowWeights = new double[0];
		effortsReportContainer.setLayout(gbl__effortsContainer);
		
		GridBagConstraints gbc_effortsPanel = new GridBagConstraints();
		gbc_effortsPanel.weighty = 100.0;
		gbc_effortsPanel.weightx = 100.0;
		gbc_effortsPanel.fill = GridBagConstraints.BOTH;
		gbc_effortsPanel.gridx = 0;
		gbc_effortsPanel.gridy = 0;
		effortsReportContainer.add(effortsPanel, gbc_effortsPanel);
		
		derivedEffortsReportContainer = new JPanel();
		tabs.addTab("Derived logsheet efforts (#)", null, derivedEffortsReportContainer, "Derived effort reports for this logsheet");
		GridBagLayout gbl__derivedEffortsContainer = new GridBagLayout();
		gbl__derivedEffortsContainer.columnWidths = new int[0];
		gbl__derivedEffortsContainer.rowHeights = new int[0];
		gbl__derivedEffortsContainer.columnWeights = new double[0];
		gbl__derivedEffortsContainer.rowWeights = new double[0];
		derivedEffortsReportContainer.setLayout(gbl__derivedEffortsContainer);
		
		GridBagConstraints gbc_derivedEffortsPanel = new GridBagConstraints();
		gbc_derivedEffortsPanel.weighty = 100.0;
		gbc_derivedEffortsPanel.weightx = 100.0;
		gbc_derivedEffortsPanel.fill = GridBagConstraints.BOTH;
		gbc_derivedEffortsPanel.gridx = 0;
		gbc_derivedEffortsPanel.gridy = 0;
		derivedEffortsReportContainer.add(derivedEffortsPanel, gbc_derivedEffortsPanel);
		
		catchesReportContainer = new JPanel();
		tabs.addTab("Logsheet catches (#)", null, catchesReportContainer, "Catch reports for this logsheet");
		GridBagLayout gbl__catchesContainer = new GridBagLayout();
		gbl__catchesContainer.columnWidths = new int[0];
		gbl__catchesContainer.rowHeights = new int[0];
		gbl__catchesContainer.columnWeights = new double[0];
		gbl__catchesContainer.rowWeights = new double[0];
		catchesReportContainer.setLayout(gbl__catchesContainer);
		
		GridBagConstraints gbc_catchesPanel = new GridBagConstraints();
		gbc_catchesPanel.weighty = 100.0;
		gbc_catchesPanel.weightx = 100.0;
		gbc_catchesPanel.fill = GridBagConstraints.BOTH;
		gbc_catchesPanel.gridx = 0;
		gbc_catchesPanel.gridy = 0;
		catchesReportContainer.add(catchesPanel, gbc_catchesPanel);
		
		discardReportsContainer = new JPanel();
		tabs.addTab("Logsheet discards (#)", null, discardReportsContainer, "Discard reports for this logsheet");
		GridBagLayout gbl__discardsContainer = new GridBagLayout();
		gbl__discardsContainer.columnWidths = new int[0];
		gbl__discardsContainer.rowHeights = new int[0];
		gbl__discardsContainer.columnWeights = new double[0];
		gbl__discardsContainer.rowWeights = new double[0];
		discardReportsContainer.setLayout(gbl__discardsContainer);
		
		GridBagConstraints gbc_discardsPanel = new GridBagConstraints();
		gbc_discardsPanel.weighty = 100.0;
		gbc_discardsPanel.weightx = 100.0;
		gbc_discardsPanel.fill = GridBagConstraints.BOTH;
		gbc_discardsPanel.gridx = 0;
		gbc_discardsPanel.gridy = 0;
		discardReportsContainer.add(discardsPanel, gbc_discardsPanel);
	}
}
