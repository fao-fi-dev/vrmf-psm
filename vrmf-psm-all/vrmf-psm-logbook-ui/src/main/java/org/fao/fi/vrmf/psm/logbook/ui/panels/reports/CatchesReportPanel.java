/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.reports;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model.CatchesReportTableModel;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CodedEntityTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.SpeciesTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 15, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 15, 2015
 */
@Named
public class CatchesReportPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5732902513150740917L;
	
	@Getter private JTable catchesTable;

	/**
	 * Create the panel.
	 */
	public CatchesReportPanel() {
		
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}

	@PostConstruct
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		add(scrollPane, gbc_scrollPane);
		
		catchesTable = UIUtils.initialize(new JTable());
		
		scrollPane.setViewportView(catchesTable);
		
		catchesTable.setModel(new CatchesReportTableModel(new ArrayList<Catches>()));
		UIUtils.fixColumnWidth(catchesTable, 0, 48);
		UIUtils.fixColumnWidth(catchesTable, 1, 48);
		UIUtils.fixColumnWidth(catchesTable, 4, 128);
		
		catchesTable.setDefaultRenderer(Species.class, new SpeciesTableCellRenderer());
		catchesTable.setDefaultRenderer(QuantityType.class, new CodedEntityTableCellRenderer());
	}
}
