/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetDataPanel;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 2, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 2, 2015
 */
@Named @Singleton
public class LogsheetPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7402015025649151005L;
	
	@Getter @Setter @Inject private LogsheetDataPanel logsheetData;
	@Getter @Setter @Inject private LogsheetDetailsPanel logsheetDetails;
	@Getter @Setter @Inject private LogsheetActionsPanel logsheetActions;
	@Getter @Setter @Inject private LogsheetAddEventActionsPanel logsheetAddEventActions;
	@Getter @Setter @Inject private LogsheetEventPanel logsheetEvent;
	
	/**
	 * Class constructor
	 */
	public LogsheetPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		logsheetData = new LogsheetDataPanel();
		logsheetDetails = new LogsheetDetailsPanel();
		logsheetActions = new LogsheetActionsPanel();
		logsheetAddEventActions = new LogsheetAddEventActionsPanel();
		logsheetEvent = new LogsheetEventPanel();
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		GridBagConstraints gbc_logsheetsData = new GridBagConstraints();
		gbc_logsheetsData.weightx = 50.0;
		gbc_logsheetsData.insets = new Insets(0, 0, 5, 0);
		gbc_logsheetsData.fill = GridBagConstraints.BOTH;
		gbc_logsheetsData.gridx = 0;
		gbc_logsheetsData.gridy = 0;
		add(logsheetData, gbc_logsheetsData);
		
		GridBagConstraints gbc_logsheetDetails = new GridBagConstraints();
		gbc_logsheetDetails.weightx = 50.0;
		gbc_logsheetDetails.weighty = 100.0;
		gbc_logsheetDetails.insets = new Insets(0, 0, 5, 0);
		gbc_logsheetDetails.fill = GridBagConstraints.BOTH;
		gbc_logsheetDetails.gridx = 0;
		gbc_logsheetDetails.gridy = 1;
		add(logsheetDetails, gbc_logsheetDetails);
		
		GridBagConstraints gbc_logsheetActions = new GridBagConstraints();
		gbc_logsheetActions.weightx = 50.0;
		gbc_logsheetActions.insets = new Insets(5, 5, 5, 5);
		gbc_logsheetActions.anchor = GridBagConstraints.SOUTH;
		gbc_logsheetActions.fill = GridBagConstraints.HORIZONTAL;
		gbc_logsheetActions.gridx = 0;
		gbc_logsheetActions.gridy = 3;
		add(logsheetActions, gbc_logsheetActions);
		
		JPanel eventManagement = new JPanel();
		eventManagement.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Event management", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_selectedEvent = new GridBagConstraints();
		gbc_selectedEvent.weightx = 100.0;
		gbc_selectedEvent.insets = new Insets(5, 0, 0, 0);
		gbc_selectedEvent.weighty = 100.0;
		gbc_selectedEvent.anchor = GridBagConstraints.NORTH;
		gbc_selectedEvent.gridheight = 4;
		gbc_selectedEvent.fill = GridBagConstraints.BOTH;
		gbc_selectedEvent.gridx = 1;
		gbc_selectedEvent.gridy = 0;
		add(eventManagement, gbc_selectedEvent);
		GridBagLayout gbl_selectedEvent = new GridBagLayout();
		gbl_selectedEvent.columnWeights = new double[0];
		gbl_selectedEvent.rowWeights = new double[0];
		eventManagement.setLayout(gbl_selectedEvent);

		GridBagConstraints gbc_logsheetAddEventActions = new GridBagConstraints();
		gbc_logsheetAddEventActions.weightx = 100.0;
		gbc_logsheetAddEventActions.fill = GridBagConstraints.BOTH;
		gbc_logsheetAddEventActions.anchor = GridBagConstraints.NORTH;
		gbc_logsheetAddEventActions.gridx = 0;
		gbc_logsheetAddEventActions.gridy = 0;
		eventManagement.add(logsheetAddEventActions, gbc_logsheetAddEventActions);

		JPanel currentEventContainer = new JPanel();
		currentEventContainer.setBorder(new TitledBorder(null, "Current event", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_currentEventContainer = new GridBagConstraints();
		gbc_currentEventContainer.weighty = 100.0;
		gbc_currentEventContainer.anchor = GridBagConstraints.NORTH;
		gbc_currentEventContainer.weightx = 100.0;
		gbc_currentEventContainer.fill = GridBagConstraints.BOTH;
		gbc_currentEventContainer.gridx = 0;
		gbc_currentEventContainer.gridy = 1;
		eventManagement.add(currentEventContainer, gbc_currentEventContainer);
		
		GridBagLayout gbl_currentEventContainer = new GridBagLayout();
		gbl_currentEventContainer.columnWeights = new double[0];
		gbl_currentEventContainer.rowWeights = new double[0];
		currentEventContainer.setLayout(gbl_currentEventContainer);
		
		GridBagConstraints gbc_logsheetEvent = new GridBagConstraints();
		gbc_logsheetEvent.weighty = 100.0;
		gbc_logsheetEvent.weightx = 100.0;
		gbc_logsheetEvent.fill = GridBagConstraints.BOTH;
		gbc_logsheetEvent.anchor = GridBagConstraints.NORTH;
		gbc_logsheetEvent.gridx = 0;
		gbc_logsheetEvent.gridy = 0;
		currentEventContainer.add(logsheetEvent, gbc_logsheetEvent);
		
		logsheetEvent.setVisible(false);
	}
//	
//	public void notifyStatusChange(EditingStatus status, Logsheet current) {
//		logsheetData.notifyStatusChange(status);
//		logsheetEvents.notifyStatusChange(status);
//		logsheetActions.notifyStatusChange(status, current);
//		logsheetAddEventActions.notifyLogsheetStatusChange(status, current);
//		logsheetEvent.notifyLogsheetStatusChange(status, current);
//	}
}
