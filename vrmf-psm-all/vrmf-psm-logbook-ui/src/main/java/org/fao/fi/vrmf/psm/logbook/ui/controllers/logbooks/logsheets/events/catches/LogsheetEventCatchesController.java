/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks.logsheets.events.catches;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.CatchAndEffortPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.CatchesPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.model.LogsheetEventCatchTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingListSelectionListener;

import no.tornado.databinding.BindingGroup;
import no.tornado.databinding.BindingStrategy;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 11, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 11, 2015
 */
@Named @Singleton
public class LogsheetEventCatchesController extends BaseController {
	@Inject private CatchAndEffortPanel parentView;
	@Inject private CatchesPanel view;
	
	private BindingGroup catchBindings;

	@PostConstruct
	private void initialize() {
		model.registerListener((LogsheetEventListener)this);
		model.registerListener((LogsheetEventCatchListener)this);
		
		view.getRecordsTable().getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				int selectedIndex = view.getRecordsTable().getSelectedRow();
				
				Catches selectedCatches = null;
				
				List<Catches> catches = model.getSelectedEvent() != null && model.getSelectedEvent().isCatchEvent() && ((CatchesRecord)model.getSelectedEvent()).getCatches() != null ? ((CatchesRecord)model.getSelectedEvent()).getCatches() : null;
				
				if(catches != null && selectedIndex >= 0 && selectedIndex <= catches.size() - 1) {
					selectedCatches = catches.get(view.getRecordsTable().convertRowIndexToModel(selectedIndex));
				}
				
				boolean same = selectedCatches != null && selectedCatches.equals(model.getSelectedCatch());

				if(same || selectedCatches == null) return;
				
				if(application.confirmCancelUpdates(model.isCatchDirty() || model.getCatchStatus() == EditingStatus.ADDING)) {
					if(model.isCatchDirty() && model.getSelectedCatch() != null && model.getSelectedCatch().getId() != null) {
						doRevertCatch(model.getSelectedCatch());
					}
					
					model.select(selectedCatches, model.getEventStatus());
				} else {
					int currentlySelectedCatchIndex = catches.indexOf(model.getSelectedCatch());
					
					currentlySelectedCatchIndex = view.getRecordsTable().convertRowIndexToView(currentlySelectedCatchIndex);
					
					view.getRecordsTable().setRowSelectionInterval(currentlySelectedCatchIndex, currentlySelectedCatchIndex);
					
					model.taintCatch();
				}
			}
		});
		
		view.getAddNew().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addCatchAction();
			}
		});
		
		view.getUpdateRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				updateRecordAction();
			}
		});
		
		view.getDeleteRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				deleteRecordAction();
			}
		});
		
		view.getCancelRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				cancelCatchAction();
			}
		});
		
		UIUtils.addValueChangeListener(view, new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				if(model.getSelectedCatch() != null) model.taintCatch();
			}
		});
	}
	
	private void applyDummyBinding() {
		bindCatch(new Catches());
		catchBindings.flushModelToUI();
	}
	
	private void bindCatch(Catches selected) {
		unbindCatch();
		
		if(selected != null) {
			catchBindings = new BindingGroup(selected);
			catchBindings.setDefaultBindingStrategy(BindingStrategy.ONFLUSH);

			catchBindings.add(view.getSpecies(), "species");
			catchBindings.add(view.getQuantity(), "quantity");
			catchBindings.add(view.getQuantityUnit(), "quantityType");

			catchBindings.bind();
		}
	}
	
	private void unbindCatch() {
		if(catchBindings != null)
			catchBindings.unbind();
	}
	
	private void doRevertCatch(Catches toRevert) {
		model.revert(toRevert);
	}
	
	private void addCatchAction() {
		model.addLogsheetEventCatch(new Catches());
		
		model.untaintCatch();
	}
	
	private void cancelCatchEditingAction() {
		if(model.getCatchStatus() == EditingStatus.ADDING) {
			if(model.getSelectedCatch() != null && model.getSelectedCatch().getId() == null) ((CatchesRecord)model.getSelectedEvent()).getCatches().remove(model.getSelectedCatch());
		}
		
		model.select((Catches)null);

		applyDummyBinding();
		
		view.getRecordsTable().clearSelection();
	}
	
	private void updateRecordAction() {
		catchBindings.flushUIToModel();
		
		List<CheckResult> checks = model.getSelectedCatch().checkCorrectness();
		
		StringBuilder messages = new StringBuilder();
		
		if(checks != null && !checks.isEmpty()) {
			for(CheckResult in : checks) {
				messages.
					append("[").append(in.getType().getCode()).append("] : ").
					append(in.getMessage()).
					append("\n");
			}
		}
		
		if(CheckResultHelper.hasErrors(checks)) {
			error("Cannot update selected catch", messages.toString());
			
			log().error("Cannot update selected catch due to the following reasons:");

			for(CheckResult in : checks) {
				log().error("{} >> {}", in.getType().getCode(), in.getMessage());
			}
		} else {
			boolean proceed = !CheckResultHelper.hasWarnings(checks);
			
			if(!proceed) {
				proceed = 
					UIUtils.confirm(
						"Please confirm", 
						"Current catch does contain the following inconsistencies:\n\n" + 
							messages.toString() + "\n" +
						"Are you sure you want to proceed?");
			}
			
			if(proceed) {
				logsheetEventCatchUpdated(model.getSelectedCatch());
				
				model.taintEvent();
				model.untaintCatch();
				
				model.changeLogsheetEventCatchStatus(EditingStatus.NOT_SET);
				
				cancelCatchEditingAction();
			}
		}
	}
	
	private void deleteRecordAction() {
		if(UIUtils.confirm(
			"Please confirm", 
			"Are you sure you want to delete the selected catch?"
		)) {
			List<Catches> eventCatches = ((CatchesRecord)model.getSelectedEvent()).getCatches();
			
			Catches selected = model.getSelectedCatch();
			eventCatches.remove(selected);

			logsheetEventUpdated(model.getSelectedEvent());
			
			if(selected.getId() != null) model.taintEvent();

			model.select((Catches)null);
			
			cancelCatchEditingAction();
			
			updateCatchesTable();
		}
	}
	
	private void cancelCatchAction() {
		cancelCatchEditingAction();
	}
	
	private void updateCatchesTable() {
		JTable table = view.getRecordsTable();
		
		((LogsheetEventCatchTableModel)table.getModel()).update(model.getSelectedEvent() == null ? null : ((CatchesRecord)model.getSelectedEvent()).getCatches());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventSelected(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventSelected(Event selected) {
		if(selected == null || !selected.isCatchEvent()) {
			view.getRecordsTable().getSelectionModel().clearSelection();
		} else {
			boolean isEditing = selected.isCompleted() && model.getEventStatus() == EditingStatus.EDITING;
			boolean displayCatches = selected.isCatchEvent();

			List<Catches> eventCatches = ((CatchesRecord)selected).getCatches();
			
			if(eventCatches == null) ((CatchesRecord)selected).setCatches(eventCatches = new ArrayList<Catches>());
			
			((LogsheetEventCatchTableModel)view.getRecordsTable().getModel()).update(eventCatches);
			
			view.getAddNew().setEnabled(isEditing && displayCatches && ( model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING ));
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventCatchStatusChanged(EditingStatus previous, EditingStatus current) {
		boolean enable = current == EditingStatus.ADDING || current == EditingStatus.EDITING || model.isCatchDirty();
		
		boolean enableUpdate = enable;
		boolean enableDelete = enable && current != EditingStatus.ADDING;
		boolean enableCancel = enable;
		
		view.getUpdateRecord().setEnabled(enableUpdate);
		view.getDeleteRecord().setEnabled(enableDelete);
		view.getCancelRecord().setEnabled(enableCancel);
		
		view.getSpecies().setEnabled(enable);
		view.getQuantity().setEnabled(enable);
		view.getQuantityUnit().setEnabled(enable);
		
		view.getAddNew().setEnabled(!enable && current != EditingStatus.VIEWING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortAdded(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventCatchAdded(Catches added) {
		model.select(added, EditingStatus.ADDING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortUpdated(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventCatchUpdated(Catches updated) {
		updateCatchesTable();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortRemoved(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventCatchRemoved(Catches deleted) {
		updateCatchesTable();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortSelected(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventCatchSelected(Catches selected) {
		if(selected != null) {
			bindCatch(selected);
			
			model.untaintCatch();
		}
		
		boolean isEditing = selected != null && ( model.getCatchStatus() == EditingStatus.ADDING || model.getCatchStatus() == EditingStatus.EDITING );
		
		if(model.getSelectedEvent() != null) {
			parentView.getCatchAndEffortTabs().setEnabledAt(0, !isEditing && model.getSelectedEvent().isCatchEvent());
			parentView.getCatchAndEffortTabs().setEnabledAt(2, !isEditing && model.getSelectedEvent().isCatchEvent());
		}
		
		view.getRecordsTable().setEnabled(!isEditing);
	}
}