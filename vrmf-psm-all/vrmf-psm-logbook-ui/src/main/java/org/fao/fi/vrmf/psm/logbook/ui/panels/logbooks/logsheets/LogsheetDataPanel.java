/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.ui.common.renderers.CodedEntityRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.CountryRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.PortRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.VesselDataRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JDateTimePicker;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named
public class LogsheetDataPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6905745646906504149L;
	
	@Getter private JNoFocusOutlineButton updateLogsheet;
	@Getter private JNoFocusOutlineButton deleteLogsheet;
	
	@Getter private JTextField logbookNo;

	@Getter private JTextField number;
	@Getter private JComboBox vessel;
	@Getter private JTextArea captain;
	@Getter private JSpinner crew;

	@Getter private JDateTimePicker departureDate;
	@Getter private JNoFocusOutlineButton departureDateNow;
	@Getter private JComboBox departureCountry;
	@Getter private JComboBox departurePort;
	
	@Getter private JDateTimePicker unloadingDate;
	@Getter private JNoFocusOutlineButton unloadingDateNow;
	@Getter private JComboBox unloadingCountry;
	@Getter private JComboBox unloadingPort;
	
	@Getter private JSpinner fishOnboardStart;
	@Getter private JSpinner fishOnboardEnd;
	
	@Getter private JTextArea comment;
	@Getter private JComboBox fishOnboardStartUnit;
	@Getter private JComboBox fishOnboardEndUnit;

	public LogsheetDataPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	private void initialize() {
		this.setBorder(null);
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);
		
		JPanel selectedLogsheetData = new JPanel();
		selectedLogsheetData.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Selected logsheet data", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_selectedLogsheetData = new GridBagConstraints();
		gbc_selectedLogsheetData.weightx = 100.0;
		gbc_selectedLogsheetData.weighty = 100.0;
		gbc_selectedLogsheetData.anchor = GridBagConstraints.NORTH;
		gbc_selectedLogsheetData.insets = new Insets(5, 5, 5, 5);
		gbc_selectedLogsheetData.fill = GridBagConstraints.HORIZONTAL;
		gbc_selectedLogsheetData.gridx = 0;
		gbc_selectedLogsheetData.gridy = 0;
		this.add(selectedLogsheetData, gbc_selectedLogsheetData);
		GridBagLayout gbl_selectedLogsheetData = new GridBagLayout();
		gbl_selectedLogsheetData.columnWidths = new int[0];
		gbl_selectedLogsheetData.columnWeights = new double[0];
		gbl_selectedLogsheetData.rowWeights = new double[0];
		selectedLogsheetData.setLayout(gbl_selectedLogsheetData);
		
		JLabel lblNumber = new JLabel("Number");
		lblNumber.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNumber = new GridBagConstraints();
		gbc_lblNumber.weightx = 3.0;
		gbc_lblNumber.anchor = GridBagConstraints.WEST;
		gbc_lblNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNumber.insets = new Insets(5, 5, 5, 5);
		gbc_lblNumber.gridx = 0;
		gbc_lblNumber.gridy = 0;
		selectedLogsheetData.add(lblNumber, gbc_lblNumber);
		
		number = new JTextField("");
		number.setEditable(false);
		GridBagConstraints gbc__number = new GridBagConstraints();
		gbc__number.weightx = 5.0;
		gbc__number.fill = GridBagConstraints.HORIZONTAL;
		gbc__number.anchor = GridBagConstraints.WEST;
		gbc__number.insets = new Insets(5, 5, 5, 5);
		gbc__number.gridx = 1;
		gbc__number.gridy = 0;
		selectedLogsheetData.add(number, gbc__number);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.weightx = 5.0;
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 2;
		gbc_horizontalStrut.gridy = 0;
		selectedLogsheetData.add(horizontalStrut, gbc_horizontalStrut);
		
		JLabel lblLogbook = new JLabel("Logbook");
		lblLogbook.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblLogbook = new GridBagConstraints();
		gbc_lblLogbook.anchor = GridBagConstraints.WEST;
		gbc_lblLogbook.insets = new Insets(5, 5, 5, 5);
		gbc_lblLogbook.gridx = 3;
		gbc_lblLogbook.gridy = 0;
		selectedLogsheetData.add(lblLogbook, gbc_lblLogbook);
		
		logbookNo = new JTextField("LOGBOOK_NO");
		logbookNo.setEditable(false);
		GridBagConstraints gbc__logbookNo = new GridBagConstraints();
		gbc__logbookNo.gridwidth = 2;
		gbc__logbookNo.fill = GridBagConstraints.BOTH;
		gbc__logbookNo.insets = new Insets(5, 5, 5, 5);
		gbc__logbookNo.gridx = 4;
		gbc__logbookNo.gridy = 0;
		selectedLogsheetData.add(logbookNo, gbc__logbookNo);
		
		JLabel lblDepartureDate = new JLabel("Departure date");
		lblDepartureDate.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblDepartureDate = new GridBagConstraints();
		gbc_lblDepartureDate.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblDepartureDate.anchor = GridBagConstraints.WEST;
		gbc_lblDepartureDate.insets = new Insets(5, 5, 5, 5);
		gbc_lblDepartureDate.gridx = 0;
		gbc_lblDepartureDate.gridy = 3;
		selectedLogsheetData.add(lblDepartureDate, gbc_lblDepartureDate);
		
		departureDate = new JDateTimePicker();
		departureDate.setFormats(new String[] {"yyyy/MM/dd HH:mm:ss"});
		departureDate.setTimeFormat(new SimpleDateFormat("HH:mm:ss"));
		
		GridBagConstraints gbc__departureDate = new GridBagConstraints();
		gbc__departureDate.weightx = 5.0;
		gbc__departureDate.fill = GridBagConstraints.HORIZONTAL;
		gbc__departureDate.anchor = GridBagConstraints.WEST;
		gbc__departureDate.insets = new Insets(5, 5, 5, 5);
		gbc__departureDate.gridx = 1;
		gbc__departureDate.gridy = 3;
		selectedLogsheetData.add(departureDate, gbc__departureDate);
		
		departureDateNow = new JNoFocusOutlineButton("Now");
		GridBagConstraints gbc_departureDateNow = new GridBagConstraints();
		gbc_departureDateNow.fill = GridBagConstraints.BOTH;
		gbc_departureDateNow.insets = new Insets(5, 5, 5, 5);
		gbc_departureDateNow.gridx = 2;
		gbc_departureDateNow.gridy = 3;
		selectedLogsheetData.add(departureDateNow, gbc_departureDateNow);
		
		departureDateNow.addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				departureDate.setDate(new Date());
			}
		});
		
		JLabel lblCompletionDate = new JLabel("Unloading date");
		lblCompletionDate.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblCompletionDate = new GridBagConstraints();
		gbc_lblCompletionDate.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCompletionDate.anchor = GridBagConstraints.WEST;
		gbc_lblCompletionDate.weightx = 3.0;
		gbc_lblCompletionDate.insets = new Insets(5, 5, 5, 5);
		gbc_lblCompletionDate.gridx = 3;
		gbc_lblCompletionDate.gridy = 3;
		selectedLogsheetData.add(lblCompletionDate, gbc_lblCompletionDate);
		
		unloadingDate = new JDateTimePicker();
		unloadingDate.setFormats(new String[] {"yyyy/MM/dd HH:mm:ss"});
		unloadingDate.setTimeFormat(new SimpleDateFormat("HH:mm:ss"));
		
		GridBagConstraints gbc_unloadingDate = new GridBagConstraints();
		gbc_unloadingDate.weightx = 5.0;
		gbc_unloadingDate.fill = GridBagConstraints.HORIZONTAL;
		gbc_unloadingDate.anchor = GridBagConstraints.WEST;
		gbc_unloadingDate.insets = new Insets(5, 5, 5, 5);
		gbc_unloadingDate.gridx = 4;
		gbc_unloadingDate.gridy = 3;
		selectedLogsheetData.add(unloadingDate, gbc_unloadingDate);
		
		unloadingDateNow = new JNoFocusOutlineButton("Now");
		GridBagConstraints gbc_unloadingDateNow = new GridBagConstraints();
		gbc_unloadingDateNow.fill = GridBagConstraints.BOTH;
		gbc_unloadingDateNow.insets = new Insets(5, 5, 5, 5);
		gbc_unloadingDateNow.gridx = 5;
		gbc_unloadingDateNow.gridy = 3;
		selectedLogsheetData.add(unloadingDateNow, gbc_unloadingDateNow);
		
		unloadingDateNow.addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				unloadingDate.setDate(new Date());
			}
		});
		
		JLabel lblVessel = new JLabel("Vessel");
		lblVessel.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblVessel = new GridBagConstraints();
		gbc_lblVessel.weightx = 3.0;
		gbc_lblVessel.anchor = GridBagConstraints.WEST;
		gbc_lblVessel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblVessel.insets = new Insets(5, 5, 5, 5);
		gbc_lblVessel.gridx = 0;
		gbc_lblVessel.gridy = 1;
		selectedLogsheetData.add(lblVessel, gbc_lblVessel);
		
		vessel = new JComboBox();
		vessel.setRenderer(new VesselDataRenderer());
		vessel.setEditable(false);
		GridBagConstraints gbc__vessel = new GridBagConstraints();
		gbc__vessel.ipady = 5;
		gbc__vessel.weightx = 10.0;
		gbc__vessel.gridwidth = 2;
		gbc__vessel.fill = GridBagConstraints.HORIZONTAL;
		gbc__vessel.anchor = GridBagConstraints.WEST;
		gbc__vessel.insets = new Insets(5, 5, 5, 5);
		gbc__vessel.gridx = 1;
		gbc__vessel.gridy = 1;
		selectedLogsheetData.add(vessel, gbc__vessel);
		
		JLabel lblCaptain = new JLabel("Captain");
		lblCaptain.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblCaptain = new GridBagConstraints();
		gbc_lblCaptain.weightx = 3.0;
		gbc_lblCaptain.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCaptain.anchor = GridBagConstraints.WEST;
		gbc_lblCaptain.insets = new Insets(5, 5, 5, 5);
		gbc_lblCaptain.gridx = 3;
		gbc_lblCaptain.gridy = 1;
		selectedLogsheetData.add(lblCaptain, gbc_lblCaptain);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.weightx = 10.0;
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridheight = 2;
		gbc_scrollPane_1.gridwidth = 2;
		gbc_scrollPane_1.insets = new Insets(5, 5, 5, 5);
		gbc_scrollPane_1.gridx = 4;
		gbc_scrollPane_1.gridy = 1;
		selectedLogsheetData.add(scrollPane_1, gbc_scrollPane_1);
		
		captain = new JTextArea("");
		captain.setFont(new Font("Monospaced", Font.PLAIN, 11));
		scrollPane_1.setViewportView(captain);
		
		JLabel lblCrew = new JLabel("Crew");
		lblCrew.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblCrew = new GridBagConstraints();
		gbc_lblCrew.weightx = 3.0;
		gbc_lblCrew.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCrew.anchor = GridBagConstraints.WEST;
		gbc_lblCrew.insets = new Insets(5, 5, 5, 5);
		gbc_lblCrew.gridx = 0;
		gbc_lblCrew.gridy = 2;
		selectedLogsheetData.add(lblCrew, gbc_lblCrew);
		
		crew = new JSpinner();
		crew.setModel(new SpinnerNumberModel(new Integer(1), new Integer(0), null, new Integer(1)));
		GridBagConstraints gbc__crew = new GridBagConstraints();
		gbc__crew.fill = GridBagConstraints.HORIZONTAL;
		gbc__crew.anchor = GridBagConstraints.WEST;
		gbc__crew.insets = new Insets(5, 5, 5, 5);
		gbc__crew.gridx = 1;
		gbc__crew.gridy = 2;
		selectedLogsheetData.add(crew, gbc__crew);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.gridwidth = 2;
		gbc_horizontalStrut_2.weightx = 10.0;
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 2;
		gbc_horizontalStrut_2.gridy = 3;
		selectedLogsheetData.add(horizontalStrut_2, gbc_horizontalStrut_2);
				
		JLabel lblDepartureCountry = new JLabel("Departure country");
		lblDepartureCountry.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblDepartureCountry = new GridBagConstraints();
		gbc_lblDepartureCountry.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblDepartureCountry.anchor = GridBagConstraints.WEST;
		gbc_lblDepartureCountry.insets = new Insets(5, 5, 5, 5);
		gbc_lblDepartureCountry.gridx = 0;
		gbc_lblDepartureCountry.gridy = 4;
		selectedLogsheetData.add(lblDepartureCountry, gbc_lblDepartureCountry);
		
		departureCountry = new JComboBox();
		departureCountry.setRenderer(new CountryRenderer());
		GridBagConstraints gbc__departureCountry = new GridBagConstraints();
		gbc__departureCountry.ipady = 5;
		gbc__departureCountry.weightx = 10.0;
		gbc__departureCountry.gridwidth = 2;
		gbc__departureCountry.fill = GridBagConstraints.HORIZONTAL;
		gbc__departureCountry.anchor = GridBagConstraints.WEST;
		gbc__departureCountry.insets = new Insets(5, 5, 5, 5);
		gbc__departureCountry.gridx = 1;
		gbc__departureCountry.gridy = 4;
		selectedLogsheetData.add(departureCountry, gbc__departureCountry);
		
		JLabel lblUnloadingCountry = new JLabel("Unloading country");
		lblUnloadingCountry.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblUnloadingCountry = new GridBagConstraints();
		gbc_lblUnloadingCountry.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblUnloadingCountry.anchor = GridBagConstraints.WEST;
		gbc_lblUnloadingCountry.insets = new Insets(5, 5, 5, 5);
		gbc_lblUnloadingCountry.gridx = 3;
		gbc_lblUnloadingCountry.gridy = 4;
		selectedLogsheetData.add(lblUnloadingCountry, gbc_lblUnloadingCountry);
		
		unloadingCountry = new JComboBox();
		unloadingCountry.setRenderer(new CountryRenderer());
		GridBagConstraints gbc__unloadingCountry = new GridBagConstraints();
		gbc__unloadingCountry.ipady = 5;
		gbc__unloadingCountry.weightx = 10.0;
		gbc__unloadingCountry.gridwidth = 2;
		gbc__unloadingCountry.fill = GridBagConstraints.HORIZONTAL;
		gbc__unloadingCountry.anchor = GridBagConstraints.WEST;
		gbc__unloadingCountry.insets = new Insets(5, 5, 5, 5);
		gbc__unloadingCountry.gridx = 4;
		gbc__unloadingCountry.gridy = 4;
		selectedLogsheetData.add(unloadingCountry, gbc__unloadingCountry);
		
		fishOnboardStartUnit = new JComboBox(QuantityType.values());
		fishOnboardStartUnit.setRenderer(new CodedEntityRenderer<QuantityType>());
		GridBagConstraints gbc__fishOnboardStartUnit = new GridBagConstraints();
		gbc__fishOnboardStartUnit.ipady = 5;
		gbc__fishOnboardStartUnit.anchor = GridBagConstraints.WEST;
		gbc__fishOnboardStartUnit.fill = GridBagConstraints.BOTH;
		gbc__fishOnboardStartUnit.weightx = 2.0;
		gbc__fishOnboardStartUnit.insets = new Insets(5, 5, 5, 5);
		gbc__fishOnboardStartUnit.gridx = 2;
		gbc__fishOnboardStartUnit.gridy = 6;
		selectedLogsheetData.add(fishOnboardStartUnit, gbc__fishOnboardStartUnit);
		
		fishOnboardEndUnit = new JComboBox(QuantityType.values());
		fishOnboardEndUnit.setRenderer(new CodedEntityRenderer<QuantityType>());
		GridBagConstraints gbc__fishOnboardEndUnit = new GridBagConstraints();
		gbc__fishOnboardEndUnit.ipady = 5;
		gbc__fishOnboardEndUnit.anchor = GridBagConstraints.WEST;
		gbc__fishOnboardEndUnit.fill = GridBagConstraints.HORIZONTAL;
		gbc__fishOnboardEndUnit.weightx = 2.0;
		gbc__fishOnboardEndUnit.insets = new Insets(5, 5, 5, 5);
		gbc__fishOnboardEndUnit.gridx = 5;
		gbc__fishOnboardEndUnit.gridy = 6;
		selectedLogsheetData.add(fishOnboardEndUnit, gbc__fishOnboardEndUnit);
		
		JLabel lblComment = new JLabel("Comments");
		GridBagConstraints gbc_lblComment = new GridBagConstraints();
		gbc_lblComment.weightx = 3.0;
		gbc_lblComment.weighty = 10.0;
		gbc_lblComment.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblComment.anchor = GridBagConstraints.WEST;
		gbc_lblComment.insets = new Insets(5, 5, 5, 5);
		gbc_lblComment.gridx = 0;
		gbc_lblComment.gridy = 7;
		selectedLogsheetData.add(lblComment, gbc_lblComment);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.weightx = 25.0;
		gbc_scrollPane.insets = new Insets(5, 5, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridheight = 2;
		gbc_scrollPane.gridwidth = 5;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 7;
		selectedLogsheetData.add(scrollPane, gbc_scrollPane);
		
		comment = new JTextArea("");
		comment.setFont(new Font("Monospaced", Font.PLAIN, 11));
		scrollPane.setViewportView(comment);
		
		JLabel lblDeparturePort = new JLabel("Departure port");
		lblDeparturePort.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblDeparturePort = new GridBagConstraints();
		gbc_lblDeparturePort.weightx = 3.0;
		gbc_lblDeparturePort.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblDeparturePort.anchor = GridBagConstraints.WEST;
		gbc_lblDeparturePort.insets = new Insets(5, 5, 5, 5);
		gbc_lblDeparturePort.gridx = 0;
		gbc_lblDeparturePort.gridy = 5;
		selectedLogsheetData.add(lblDeparturePort, gbc_lblDeparturePort);
		
		departurePort = new JComboBox();
		departurePort.setRenderer(new PortRenderer());
		GridBagConstraints gbc__departurePort = new GridBagConstraints();
		gbc__departurePort.ipady = 5;
		gbc__departurePort.weightx = 10.0;
		gbc__departurePort.gridwidth = 2;
		gbc__departurePort.fill = GridBagConstraints.HORIZONTAL;
		gbc__departurePort.anchor = GridBagConstraints.WEST;
		gbc__departurePort.insets = new Insets(5, 5, 5, 5);
		gbc__departurePort.gridx = 1;
		gbc__departurePort.gridy = 5;
		selectedLogsheetData.add(departurePort, gbc__departurePort);
		
		JLabel lblUnloadingPort = new JLabel("Unloading port");
		lblUnloadingPort.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblUnloadingPort = new GridBagConstraints();
		gbc_lblUnloadingPort.weightx = 3.0;
		gbc_lblUnloadingPort.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblUnloadingPort.anchor = GridBagConstraints.WEST;
		gbc_lblUnloadingPort.insets = new Insets(5, 5, 5, 5);
		gbc_lblUnloadingPort.gridx = 3;
		gbc_lblUnloadingPort.gridy = 5;
		selectedLogsheetData.add(lblUnloadingPort, gbc_lblUnloadingPort);
		
		unloadingPort = new JComboBox();
		unloadingPort.setRenderer(new PortRenderer());
		
		GridBagConstraints gbc__unoadingPort = new GridBagConstraints();
		gbc__unoadingPort.ipady = 5;
		gbc__unoadingPort.weightx = 10.0;
		gbc__unoadingPort.gridwidth = 2;
		gbc__unoadingPort.fill = GridBagConstraints.HORIZONTAL;
		gbc__unoadingPort.anchor = GridBagConstraints.WEST;
		gbc__unoadingPort.insets = new Insets(5, 5, 5, 5);
		gbc__unoadingPort.gridx = 4;
		gbc__unoadingPort.gridy = 5;
		selectedLogsheetData.add(unloadingPort, gbc__unoadingPort);
		
		JLabel lblFishOnBoard = new JLabel("Fish on board (start)");
		lblFishOnBoard.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblFishOnBoard = new GridBagConstraints();
		gbc_lblFishOnBoard.weightx = 3.0;
		gbc_lblFishOnBoard.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblFishOnBoard.anchor = GridBagConstraints.WEST;
		gbc_lblFishOnBoard.insets = new Insets(5, 5, 5, 5);
		gbc_lblFishOnBoard.gridx = 0;
		gbc_lblFishOnBoard.gridy = 6;
		selectedLogsheetData.add(lblFishOnBoard, gbc_lblFishOnBoard);
		
		fishOnboardStart = new JSpinner();
		fishOnboardStart.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		GridBagConstraints gbc__fishOnboardStart = new GridBagConstraints();
		gbc__fishOnboardStart.ipadx = 5;
		gbc__fishOnboardStart.weightx = 8.0;
		gbc__fishOnboardStart.fill = GridBagConstraints.HORIZONTAL;
		gbc__fishOnboardStart.anchor = GridBagConstraints.WEST;
		gbc__fishOnboardStart.insets = new Insets(5, 5, 5, 5);
		gbc__fishOnboardStart.gridx = 1;
		gbc__fishOnboardStart.gridy = 6;
		selectedLogsheetData.add(fishOnboardStart, gbc__fishOnboardStart);
		
		JLabel lblFishOnBoard_1 = new JLabel("Fish on board (end)");
		lblFishOnBoard_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblFishOnBoard_1 = new GridBagConstraints();
		gbc_lblFishOnBoard_1.weightx = 3.0;
		gbc_lblFishOnBoard_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblFishOnBoard_1.anchor = GridBagConstraints.WEST;
		gbc_lblFishOnBoard_1.insets = new Insets(5, 5, 5, 5);
		gbc_lblFishOnBoard_1.gridx = 3;
		gbc_lblFishOnBoard_1.gridy = 6;
		selectedLogsheetData.add(lblFishOnBoard_1, gbc_lblFishOnBoard_1);
		
		fishOnboardEnd = new JSpinner();
		fishOnboardEnd.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		GridBagConstraints gbc__fishOnboardEnd = new GridBagConstraints();
		gbc__fishOnboardEnd.weightx = 8.0;
		gbc__fishOnboardEnd.insets = new Insets(5, 5, 5, 5);
		gbc__fishOnboardEnd.anchor = GridBagConstraints.WEST;
		gbc__fishOnboardEnd.fill = GridBagConstraints.HORIZONTAL;
		gbc__fishOnboardEnd.gridx = 4;
		gbc__fishOnboardEnd.gridy = 6;
		selectedLogsheetData.add(fishOnboardEnd, gbc__fishOnboardEnd);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(5, 5, 0, 5);
		gbc_verticalStrut.gridx = 0;
		gbc_verticalStrut.gridy = 8;
		selectedLogsheetData.add(verticalStrut, gbc_verticalStrut);
	}

	public void disableAll() {
		setAllComponentsStatus(false);
	}
	
	private void enableAll() {
		setAllComponentsStatus(true);
	}
	
	private void setAllComponentsStatus(boolean enabled) {
		for(JComponent in : new JComponent[] {
			getNumber(),
			getCaptain(),
			getVessel(),
			getCrew(),
			getDepartureDate(),
			departureDateNow,
			getDepartureCountry(),
			getDeparturePort(),
			getUnloadingDate(),
			unloadingDateNow,
			getUnloadingCountry(),
			getUnloadingPort(),
			getFishOnboardStart(),
			getFishOnboardStartUnit(),
			getFishOnboardEnd(),
			getFishOnboardEndUnit(),
			getComment()
		}) {
			in.setEnabled(enabled);
		};

	}
	
	public void notifyLogsheetStatusChange(EditingStatus status) {
		disableAll();
		
		switch(status) {
			case NOT_SET:
			case VIEWING:
				break;
			case ADDING:
			case EDITING:
				enableAll();
				unloadingDate.setEnabled(false);
				unloadingDateNow.setEnabled(false);
				unloadingCountry.setEnabled(false);
				unloadingPort.setEnabled(false);
				fishOnboardEnd.setEnabled(false);
				fishOnboardEndUnit.setEnabled(false);
				break;
			case COMPLETING:
				unloadingDate.setEnabled(true);
				unloadingDateNow.setEnabled(true);
				unloadingCountry.setEnabled(true);
				unloadingPort.setEnabled(true);
				fishOnboardEnd.setEnabled(true);
				fishOnboardEndUnit.setEnabled(true);
				comment.setEnabled(true);
				break;
		}
	}
	
	public void notifyLogsheetEventStatusChange(EditingStatus logsheetStatus, Logsheet logsheet, EditingStatus eventStatus, Event event) {
		disableAll();
		
		switch(eventStatus) {
			case ADDING:
			case EDITING:
				disableAll();
				break;
			default:
				notifyLogsheetStatusChange(logsheetStatus);
				break;
		}
	}
}