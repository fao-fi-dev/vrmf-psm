/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.vrmf.psm.ui.common.LogLevel;
import org.fao.fi.vrmf.psm.ui.common.spi.PreferenceListener;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 11, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 11, 2015
 */
@Named @Singleton
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @EqualsAndHashCode @ToString
public class Preferences implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1524814101649746754L;
	
	@Transient @XmlTransient
	private List<PreferenceListener> listeners = new ArrayList<PreferenceListener>();
	
	@Getter @XmlAttribute(required=true) private boolean offline = true;
	@Getter @XmlAttribute(required=true) private boolean dummyMode = false;
	@Getter @XmlAttribute(required=true) private boolean showLog = false;
	@Getter @XmlAttribute(required=true) private LogLevel logLevel = LogLevel.INFO;
	
	@Getter @Setter @XmlElement(required=true, nillable=false) private boolean autoFillNewEvents = true;
	
	public Preferences registerListener(PreferenceListener listener) {
		if(listener == null) throw new IllegalArgumentException("Cannot register a NULL listener");
		
		listeners.add(listener);
		
		return this;
	}
	
	private void notify(String property, Object oldValue, Object newValue) {
		for(PreferenceListener in : listeners)
			in.preferencesChanged(property, oldValue, newValue);
	}
	
	final public void setOffline(boolean offline) {
		boolean old = this.offline;
		
		this.offline = offline;
		
		notify("offline", old, offline);
	}
	
	final public void setDummyMode(boolean dummyMode) {
		boolean old = this.dummyMode;
		
		this.dummyMode = dummyMode;
		
		notify("dummyMode", old, dummyMode);
	}
	
	final public void setShowLog(boolean showLog) {
		boolean old = this.showLog;
		
		this.showLog = showLog;
		
		notify("showLog", old, showLog);
	}
}
