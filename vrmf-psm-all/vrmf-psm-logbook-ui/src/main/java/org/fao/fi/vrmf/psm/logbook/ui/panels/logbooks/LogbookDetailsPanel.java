/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;

import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.LogsheetsPanel;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;
import java.awt.Insets;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 15, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 15, 2015
 */
@Named @Singleton
public class LogbookDetailsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5081503477197564779L;
	
	@Getter @Inject private LogbookReportsPanel reports;
	@Getter @Inject private LogsheetsPanel logsheets;

	/**
	 * Class constructor
	 *
	 */
	public LogbookDetailsPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		reports = new LogbookReportsPanel();
		logsheets = new LogsheetsPanel();
		
		initialize();
	}
	
	@PostConstruct
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		GridBagConstraints gbc_logsheets = new GridBagConstraints();
		gbc_logsheets.weighty = 100.0;
		gbc_logsheets.weightx = 100.0;
		gbc_logsheets.fill = GridBagConstraints.BOTH;
		gbc_logsheets.anchor = GridBagConstraints.NORTH;
		gbc_logsheets.gridx = 0;
		gbc_logsheets.gridy = 0;
		add(logsheets, gbc_logsheets);

		GridBagConstraints gbc_reports = new GridBagConstraints();
		gbc_reports.insets = new Insets(5, 5, 5, 5);
		gbc_reports.weighty = 100.0;
		gbc_reports.weightx = 100.0;
		gbc_reports.fill = GridBagConstraints.BOTH;
		gbc_reports.anchor = GridBagConstraints.SOUTH;
		gbc_reports.gridx = 0;
		gbc_reports.gridy = 1;
		add(reports, gbc_reports);
	}
	
	public void hideReports() {
		reports.setVisible(false);
	}
	
	public void showReports() {
		reports.setVisible(true);
	}
}
