/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class LogsheetTableModel extends AbstractTableModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 587756690700916447L;

	@Getter private String[] columnNames = new String[] { 
		"#", "Vessel", "Reg. no.","Departure date", "Departure port", "Arrival date", "Unloading port", "Crew size", "Captain", "No. events", "Completed", "Completion date"
	};

	@Getter private List<Logsheet> rawData = null;

	/**
	 * Class constructor
	 *
	 * @param rawData
	 */
	public LogsheetTableModel(List<Logsheet> data) {
		super();
		
		rawData = data == null ? new ArrayList<Logsheet>() : data;
	}
	
	public void update(List<Logsheet> data) {
		rawData = data == null ? new ArrayList<Logsheet>() : data;
		
		fireTableDataChanged();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return rawData.size();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		Logsheet record = rawData == null || rawData.size() < row + 1 ? null : rawData.get(row);
	
		if(record == null) return null;
		
		switch(col) {
			case 0:
				return record.getNumber();
			case 1:
				return record.getVesselData();
			case 2:
				return record.getVesselData().getRegistrationNumber();
			case 3:
				return record.getDepartureDate();
			case 4:
				return record.getDeparturePort();
			case 5:
				return record.getArrivalDate();
			case 6:
				return record.getUnloadingPort();
			case 7:
				return record.getCrewSize();
			case 8:
				return record.getCaptainName();
			case 9:
				return record.getEvents() == null ? 0 : record.getEvents().size();
			case 10:
				return record.isCompleted();
			case 11:
				return record.getCompletionDate();
			default:
				return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		switch(c) {
			case 0:
			case 7:
			case 9:
				return Integer.class;
			case 3:
			case 5:
			case 11:
				return Date.class;
			case 4:
			case 6:
				return Port.class;
			case 1:
				return VesselData.class;
			case 2:
			case 8:
				return String.class;
			case 10:
				return Boolean.class;
			default:
				return Void.class;
		}
	}
	
	public Object[][] getData() {
		if(rawData == null) return new Object[0][];
		
		Object[][] toReturn = new Object[getRowCount()][getColumnCount()];
		
		for(int row=0; row < getRowCount(); row++) {
			toReturn[row] = new Object[getColumnCount()];
			
			for(int column=0; column < getColumnCount(); column++) {
				toReturn[row][column] = getValueAt(row, column);
			}
		}
		
		return toReturn;
	}
}