/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.spi;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 9, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 9, 2015
 */
public interface DataModelStatusListener {
	void modelStatusChanged(boolean tainted);
}
