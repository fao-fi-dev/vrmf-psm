/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class LogbookTableModel extends AbstractTableModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 587756690700916447L;

	@Getter private String[] columnNames = new String[] { 
		"#", "Year", "Version", "Creation date", "No. logsheets", "Completed", "Completion date"
	};

	@Getter private List<Logbook> rawData = null;

	/**
	 * Class constructor
	 *
	 * @param rawData
	 */
	public LogbookTableModel(List<Logbook> data) {
		super();
		
		rawData = data == null ? new ArrayList<Logbook>() : data;
	}
	
	public void update() {
		fireTableDataChanged();
	}
	
	public void update(List<Logbook> data) {
		rawData = data == null ? new ArrayList<Logbook>() : data;
		
		update();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return rawData.size();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		Logbook record = rawData == null || rawData.size() < row + 1 ? null : rawData.get(row);
	
		if(record == null) return null;
		
		switch(col) {
			case 0:
				return row + 1;
			case 1:
				return record.getYear();
			case 2:
				return record.getVersion();
			case 3:
				return record.getCreationDate();
			case 4:
				return record.getLogsheets() == null ? 0 : record.getLogsheets().size();
			case 5:
				return record.isCompleted();
			case 6:
				return record.getCompletionDate();
			default:
				return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		switch(c) {
			case 0:
			case 1:
			case 4:
				return Integer.class;
			case 2:
				return String.class;
			case 3:
			case 6:
				return Date.class;
			case 5:
				return Boolean.class;
			default:
				return Void.class;
		}
	}
	
	public Object[][] getData() {
		if(rawData == null) return new Object[0][];
		
		Object[][] toReturn = new Object[getRowCount()][getColumnCount()];
		
		for(int row=0; row < getRowCount(); row++) {
			toReturn[row] = new Object[getColumnCount()];
			
			for(int column=0; column < getColumnCount(); column++) {
				toReturn[row][column] = getValueAt(row, column);
			}
		}
		
		return toReturn;
	}
}