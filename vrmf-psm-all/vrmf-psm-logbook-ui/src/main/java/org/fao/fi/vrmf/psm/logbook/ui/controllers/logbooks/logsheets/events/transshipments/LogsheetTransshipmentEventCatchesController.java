/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks.logsheets.events.transshipments;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.TransshipmentEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetEventPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.transshipments.TransshipmentsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.transshipments.model.LogsheetEventTransshipmentTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventCatchListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CodedEntityTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.SpeciesTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingListSelectionListener;

import no.tornado.databinding.BindingGroup;
import no.tornado.databinding.BindingStrategy;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 11, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 11, 2015
 */
@Named @Singleton
public class LogsheetTransshipmentEventCatchesController extends BaseController {
	@Inject private LogsheetEventPanel parentView;
	@Inject private TransshipmentsPanel view;
	
	private BindingGroup catchBindings;

	@PostConstruct
	private void initialize() {
		model.registerListener((LogsheetEventListener)this);
		model.registerListener((LogsheetEventCatchListener)this);
		
		JTable table = UIUtils.initialize(view.getRecordsTable(), new LogsheetEventTransshipmentTableModel(new ArrayList<Catches>()));
		table.setDefaultRenderer(Species.class, new SpeciesTableCellRenderer());
		table.setDefaultRenderer(QuantityType.class, new CodedEntityTableCellRenderer());
		
		UIUtils.fixColumnWidth(table, 0, 48);
		UIUtils.fixColumnWidth(table, 1, 48);
		UIUtils.fixColumnWidth(table, 2, 48);
		
		view.getRecordsTable().getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				int selectedIndex = view.getRecordsTable().getSelectedRow();
				
				Catches selectedCatches = null;
				
				List<Catches> catches = model.getSelectedEvent() != null && model.getSelectedEvent().isTransshipmentEvent() && ((TransshipmentEvent)model.getSelectedEvent()).getCatches() != null ? ((TransshipmentEvent)model.getSelectedEvent()).getCatches() : null;
				
				if(catches != null && selectedIndex >= 0 && selectedIndex <= catches.size() - 1) {
					selectedCatches = catches.get(view.getRecordsTable().convertRowIndexToModel(selectedIndex));
				}
				
				boolean same = selectedCatches != null && selectedCatches.equals(model.getSelectedCatch());

				if(same || selectedCatches == null) return;
				
				if(application.confirmCancelUpdates(model.isCatchDirty() || model.getCatchStatus() == EditingStatus.ADDING)) {
					if(model.isCatchDirty() && model.getSelectedCatch() != null && model.getSelectedCatch().getId() != null) {
						doRevertCatch(model.getSelectedCatch());
					}
					
					model.select(selectedCatches, model.getEventStatus());
				} else {
					int currentlySelectedCatchIndex = catches.indexOf(model.getSelectedCatch());
					
					currentlySelectedCatchIndex = view.getRecordsTable().convertRowIndexToView(currentlySelectedCatchIndex);
					
					view.getRecordsTable().setRowSelectionInterval(currentlySelectedCatchIndex, currentlySelectedCatchIndex);
					
					model.taintCatch();
				}
			}
		});
		
		view.getAddNew().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addTranshipmentDataAction();
			}
		});
		
		view.getUpdateRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				updateTranshipmentDataAction();
			}
		});
		
		view.getDeleteRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				deleteTranshipmentDataAction();
			}
		});
		
		view.getCancelRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				cancelTranshipmentDataAction();
			}
		});
		
		UIUtils.addValueChangeListener(view, new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				if(model.getSelectedCatch() != null) model.taintCatch();
			}
		});
	}
	
	private void applyDummyBinding() {
		bindCatch(new Catches());
		catchBindings.flushModelToUI();
	}
	
	private void bindCatch(Catches selected) {
		unbindCatch();
		
		if(selected != null) {
			catchBindings = new BindingGroup(selected);
			catchBindings.setDefaultBindingStrategy(BindingStrategy.ONFLUSH);

			catchBindings.add(view.getSpecies(), "species");
			catchBindings.add(view.getQuantity(), "quantity");
			catchBindings.add(view.getQuantityUnit(), "quantityType");

			catchBindings.bind();
		}
	}
	
	private void unbindCatch() {
		if(catchBindings != null)
			catchBindings.unbind();
	}
	
	private void doRevertCatch(Catches toRevert) {
		model.revert(toRevert);
	}
	
	private void addTranshipmentDataAction() {
		model.addLogsheetTransshipmentEventCatch(new Catches());
		
		model.untaintCatch();
	}
	
	private void cancelCatchEditingAction() {
		if(model.getCatchStatus() == EditingStatus.ADDING) {
			if(model.getSelectedCatch() != null && model.getSelectedCatch().getId() == null) ((TransshipmentEvent)model.getSelectedEvent()).getCatches().remove(model.getSelectedCatch());
		}
		
		model.select((Catches)null);

		applyDummyBinding();
		
		view.getRecordsTable().clearSelection();
	}
	
	private void updateTranshipmentDataAction() {
		catchBindings.flushUIToModel();
		
		List<CheckResult> checks = model.getSelectedCatch().checkCorrectness();
		
		StringBuilder messages = new StringBuilder();
		
		if(checks != null && !checks.isEmpty()) {
			for(CheckResult in : checks) {
				messages.
					append("[").append(in.getType().getCode()).append("] : ").
					append(in.getMessage()).
					append("\n");
			}
		}
		
		if(CheckResultHelper.hasErrors(checks)) {
			error("Cannot update selected transhipment data", messages.toString());
			
			log().error("Cannot update selected transhipment data due to the following reasons:");

			for(CheckResult in : checks) {
				log().error("{} >> {}", in.getType().getCode(), in.getMessage());
			}
		} else {
			boolean proceed = !CheckResultHelper.hasWarnings(checks);
			
			if(!proceed) {
				proceed = 
					UIUtils.confirm(
						"Please confirm", 
						"Current transhipment data does contain the following inconsistencies:\n\n" + 
							messages.toString() + "\n" +
						"Are you sure you want to proceed?");
			}
			
			if(proceed) {
				logsheetEventCatchUpdated(model.getSelectedCatch());
				
				model.taintEvent();
				model.untaintCatch();
				
				model.changeLogsheetEventCatchStatus(EditingStatus.NOT_SET);
				
				cancelCatchEditingAction();
			}
		}
	}
	
	private void deleteTranshipmentDataAction() {
		if(UIUtils.confirm(
			"Please confirm", 
			"Are you sure you want to delete the selected transhipment data?"
		)) {
			List<Catches> eventCatches = ((TransshipmentEvent)model.getSelectedEvent()).getCatches();
			
			Catches selected = model.getSelectedCatch();
			eventCatches.remove(selected);

			logsheetEventUpdated(model.getSelectedEvent());
			
			if(selected.getId() != null) model.taintEvent();

			model.select((Catches)null);
			
			cancelCatchEditingAction();
			
			updateCatchesTable();
		}
	}
	
	private void cancelTranshipmentDataAction() {
		cancelCatchEditingAction();
	}
	
	private void updateCatchesTable() {
		JTable table = view.getRecordsTable();
		
		((LogsheetEventTransshipmentTableModel)table.getModel()).update(model.getSelectedEvent() == null ? null : ((TransshipmentEvent)model.getSelectedEvent()).getCatches());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventSelected(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventSelected(Event selected) {
		boolean isValid = selected != null;
		
		if(!isValid) {
			view.getRecordsTable().getSelectionModel().clearSelection();
			
			return;
		}
		
		boolean isTransshipment = selected.isTransshipmentEvent();
		
		view.setVisible(isTransshipment);
		
		if(!isTransshipment) return;
		
		boolean isEditing = selected.isCompleted() && model.getEventStatus() == EditingStatus.EDITING;

		List<Catches> eventCatches = ((TransshipmentEvent)selected).getCatches();
		
		if(eventCatches == null) ((TransshipmentEvent)selected).setCatches(eventCatches = new ArrayList<Catches>());
		
		((LogsheetEventTransshipmentTableModel)view.getRecordsTable().getModel()).update(eventCatches);
		
		view.getAddNew().setEnabled(isEditing && ( model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING ));
		
		boolean enableInputs = model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING;
		
		for(Component in : new Component[] {
			parentView.getVesselName(),
			parentView.getVesselRegistrationCountry(),
			parentView.getVesselRegistrationNumber(),
			parentView.getVesselDestination()
		}) {
			in.setEnabled(enableInputs);
		}
		
//		List<Catches> previous = model.getSelectedLogsheet().uniqueCatches(model.getSelectedEvent().getStartDate());
//
//		List<Species> uniqueSpecies = new ArrayList<Species>();
//		
//		if(previous == null || previous.isEmpty()) {
//			warning("Cannot add a transshipment event", "No catches has been recorded so far, therefore there's nothing to transship");
//		} else {
//			uniqueSpecies.add(null);
//
//			Species current;
//			for(Catches in : previous) {
//				current = in.getSpecies();
//				
//				if(!uniqueSpecies.contains(current))
//					uniqueSpecies.add(current);
//			}
//		}
		
		List<Species> uniqueSpecies = commonServices.allSpecies();
		
		view.getSpecies().setModel(new DefaultComboBoxModel(uniqueSpecies.toArray(new Species[uniqueSpecies.size()])));
		view.getSpecies().setSelectedItem(null);
		view.getSpecies().setEnabled(false);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventCatchStatusChanged(EditingStatus previous, EditingStatus current) {
		boolean enable = current == EditingStatus.ADDING || current == EditingStatus.EDITING || model.isCatchDirty();
		
		boolean enableUpdate = enable;
		boolean enableDelete = enable && current != EditingStatus.ADDING;
		boolean enableCancel = enable;
		
		view.getUpdateRecord().setEnabled(enableUpdate);
		view.getDeleteRecord().setEnabled(enableDelete);
		view.getCancelRecord().setEnabled(enableCancel);
		
		view.getSpecies().setEnabled(enable);
		view.getQuantity().setEnabled(enable);
		view.getQuantityUnit().setEnabled(enable);
		
		view.getAddNew().setEnabled(!enable && current != EditingStatus.VIEWING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortAdded(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventCatchAdded(Catches added) {
		model.select(added, EditingStatus.ADDING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortUpdated(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventCatchUpdated(Catches updated) {
		updateCatchesTable();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortRemoved(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventCatchRemoved(Catches deleted) {
		updateCatchesTable();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortSelected(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventCatchSelected(Catches selected) {
		if(selected != null) {
			bindCatch(selected);
			
			model.untaintCatch();
		}
		
		boolean isEditing = selected != null && ( model.getCatchStatus() == EditingStatus.ADDING || model.getCatchStatus() == EditingStatus.EDITING );
	
		view.getRecordsTable().setEnabled(!isEditing);
	}
}