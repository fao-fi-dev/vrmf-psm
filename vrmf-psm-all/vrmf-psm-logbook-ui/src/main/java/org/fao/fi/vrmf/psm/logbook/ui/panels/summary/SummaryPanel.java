/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.summary;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 7, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 7, 2016
 */
@Named @Singleton
public class SummaryPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7800549105653209089L;
	@Getter @Setter private JLabel logbooks;
	@Getter @Setter private JLabel completedLogbooks;
	@Getter @Setter private JLabel logsheets;
	@Getter @Setter private JLabel completedLogsheets;
	@Getter @Setter private JProgressBar logbookCompletion;
	@Getter @Setter private JProgressBar logsheetCompletion;

	/**
	 * Create the panel.
	 */
	public SummaryPanel() {
		setBorder(new TitledBorder(null, "Summary", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		JPanel logbooksPanel = new JPanel();
		logbooksPanel.setBorder(new TitledBorder(null, "Logbooks", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_logbooksPanel = new GridBagConstraints();
		gbc_logbooksPanel.weightx = 100.0;
		gbc_logbooksPanel.insets = new Insets(0, 0, 5, 0);
		gbc_logbooksPanel.fill = GridBagConstraints.BOTH;
		gbc_logbooksPanel.gridx = 0;
		gbc_logbooksPanel.gridy = 0;
		add(logbooksPanel, gbc_logbooksPanel);
		GridBagLayout gbl_logbooksPanel = new GridBagLayout();
		gbl_logbooksPanel.rowHeights = new int[0];
		gbl_logbooksPanel.columnWidths = new int[0];
		gbl_logbooksPanel.columnWeights = new double[0];
		gbl_logbooksPanel.rowWeights = new double[0];
		logbooksPanel.setLayout(gbl_logbooksPanel);
		
		JLabel lblTotalLogbooks = new JLabel("Total:");
		lblTotalLogbooks.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lblTotalLogbooks = new GridBagConstraints();
		gbc_lblTotalLogbooks.anchor = GridBagConstraints.WEST;
		gbc_lblTotalLogbooks.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblTotalLogbooks.insets = new Insets(0, 10, 5, 10);
		gbc_lblTotalLogbooks.gridx = 0;
		gbc_lblTotalLogbooks.gridy = 0;
		logbooksPanel.add(lblTotalLogbooks, gbc_lblTotalLogbooks);
		
		logbooks = new JLabel("0");
		logbooks.setFont(new Font("Tahoma", Font.PLAIN, 35));
		GridBagConstraints gbc__logbooks = new GridBagConstraints();
		gbc__logbooks.anchor = GridBagConstraints.WEST;
		gbc__logbooks.fill = GridBagConstraints.HORIZONTAL;
		gbc__logbooks.insets = new Insets(0, 0, 5, 5);
		gbc__logbooks.gridx = 1;
		gbc__logbooks.gridy = 0;
		logbooksPanel.add(logbooks, gbc__logbooks);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.weightx = 100.0;
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut.gridx = 2;
		gbc_horizontalStrut.gridy = 0;
		logbooksPanel.add(horizontalStrut, gbc_horizontalStrut);
		
		JLabel lblCompletedLogbooks = new JLabel("Completed:");
		lblCompletedLogbooks.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lblCompletedLogbooks = new GridBagConstraints();
		gbc_lblCompletedLogbooks.anchor = GridBagConstraints.WEST;
		gbc_lblCompletedLogbooks.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCompletedLogbooks.insets = new Insets(0, 10, 5, 10);
		gbc_lblCompletedLogbooks.gridx = 0;
		gbc_lblCompletedLogbooks.gridy = 1;
		logbooksPanel.add(lblCompletedLogbooks, gbc_lblCompletedLogbooks);
		
		completedLogbooks = new JLabel("0");
		completedLogbooks.setFont(new Font("Tahoma", Font.PLAIN, 35));
		GridBagConstraints gbc__completedLogbooks = new GridBagConstraints();
		gbc__completedLogbooks.anchor = GridBagConstraints.WEST;
		gbc__completedLogbooks.insets = new Insets(0, 0, 5, 5);
		gbc__completedLogbooks.fill = GridBagConstraints.HORIZONTAL;
		gbc__completedLogbooks.gridx = 1;
		gbc__completedLogbooks.gridy = 1;
		logbooksPanel.add(completedLogbooks, gbc__completedLogbooks);
		
		JLabel lblLogbooksCompletionLevel = new JLabel("Completion level:");
		lblLogbooksCompletionLevel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lblLogbooksCompletionLevel = new GridBagConstraints();
		gbc_lblLogbooksCompletionLevel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblLogbooksCompletionLevel.anchor = GridBagConstraints.WEST;
		gbc_lblLogbooksCompletionLevel.insets = new Insets(0, 10, 0, 10);
		gbc_lblLogbooksCompletionLevel.gridx = 0;
		gbc_lblLogbooksCompletionLevel.gridy = 2;
		logbooksPanel.add(lblLogbooksCompletionLevel, gbc_lblLogbooksCompletionLevel);
		
		logbookCompletion = new JProgressBar();
		GridBagConstraints gbc__logbookCompletion = new GridBagConstraints();
		gbc__logbookCompletion.anchor = GridBagConstraints.WEST;
		gbc__logbookCompletion.fill = GridBagConstraints.HORIZONTAL;
		gbc__logbookCompletion.insets = new Insets(10, 0, 5, 5);
		gbc__logbookCompletion.gridx = 1;
		gbc__logbookCompletion.gridy = 2;
		logbooksPanel.add(logbookCompletion, gbc__logbookCompletion);
		
		JPanel logsheetsPanel = new JPanel();
		logsheetsPanel.setBorder(new TitledBorder(null, "Logsheets", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_logsheetsPanel = new GridBagConstraints();
		gbc_logsheetsPanel.insets = new Insets(0, 0, 5, 0);
		gbc_logsheetsPanel.weightx = 100.0;
		gbc_logsheetsPanel.fill = GridBagConstraints.BOTH;
		gbc_logsheetsPanel.gridx = 0;
		gbc_logsheetsPanel.gridy = 1;
		add(logsheetsPanel, gbc_logsheetsPanel);
		GridBagLayout gbl_logsheetsPanel = new GridBagLayout();
		gbl_logsheetsPanel.columnWidths = new int[0];
		gbl_logsheetsPanel.rowHeights = new int[0];
		gbl_logsheetsPanel.columnWeights = new double[]{0.0, 0.0, 0.0};
		gbl_logsheetsPanel.rowWeights = new double[]{0.0, 0.0, 0.0};
		logsheetsPanel.setLayout(gbl_logsheetsPanel);
		
		JLabel lblTotalLogsheets = new JLabel("Total:");
		lblTotalLogsheets.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lblTotalLogsheets = new GridBagConstraints();
		gbc_lblTotalLogsheets.anchor = GridBagConstraints.WEST;
		gbc_lblTotalLogsheets.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblTotalLogsheets.insets = new Insets(0, 10, 5, 10);
		gbc_lblTotalLogsheets.gridx = 0;
		gbc_lblTotalLogsheets.gridy = 0;
		logsheetsPanel.add(lblTotalLogsheets, gbc_lblTotalLogsheets);
		
		logsheets = new JLabel("0");
		logsheets.setFont(new Font("Tahoma", Font.PLAIN, 35));
		GridBagConstraints gbc__logsheets = new GridBagConstraints();
		gbc__logsheets.anchor = GridBagConstraints.WEST;
		gbc__logsheets.fill = GridBagConstraints.HORIZONTAL;
		gbc__logsheets.insets = new Insets(0, 0, 5, 5);
		gbc__logsheets.gridx = 1;
		gbc__logsheets.gridy = 0;
		logsheetsPanel.add(logsheets, gbc__logsheets);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.weightx = 100.0;
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_1.gridx = 2;
		gbc_horizontalStrut_1.gridy = 0;
		logsheetsPanel.add(horizontalStrut_1, gbc_horizontalStrut_1);
		
		JLabel lblCompletedLogshets = new JLabel("Completed:");
		lblCompletedLogshets.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lblCompletedLogshets = new GridBagConstraints();
		gbc_lblCompletedLogshets.anchor = GridBagConstraints.WEST;
		gbc_lblCompletedLogshets.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCompletedLogshets.insets = new Insets(0, 10, 5, 10);
		gbc_lblCompletedLogshets.gridx = 0;
		gbc_lblCompletedLogshets.gridy = 1;
		logsheetsPanel.add(lblCompletedLogshets, gbc_lblCompletedLogshets);
		
		completedLogsheets = new JLabel("0");
		completedLogsheets.setFont(new Font("Tahoma", Font.PLAIN, 35));
		GridBagConstraints gbc__completedLogsheets = new GridBagConstraints();
		gbc__completedLogsheets.insets = new Insets(0, 0, 5, 5);
		gbc__completedLogsheets.anchor = GridBagConstraints.WEST;
		gbc__completedLogsheets.fill = GridBagConstraints.HORIZONTAL;
		gbc__completedLogsheets.gridx = 1;
		gbc__completedLogsheets.gridy = 1;
		logsheetsPanel.add(completedLogsheets, gbc__completedLogsheets);
		
		JLabel lblLogsheetsCompletionLevel = new JLabel("Completion level:");
		lblLogsheetsCompletionLevel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lblLogsheetsCompletionLevel = new GridBagConstraints();
		gbc_lblLogsheetsCompletionLevel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblLogsheetsCompletionLevel.anchor = GridBagConstraints.WEST;
		gbc_lblLogsheetsCompletionLevel.insets = new Insets(0, 10, 0, 10);
		gbc_lblLogsheetsCompletionLevel.gridx = 0;
		gbc_lblLogsheetsCompletionLevel.gridy = 2;
		logsheetsPanel.add(lblLogsheetsCompletionLevel, gbc_lblLogsheetsCompletionLevel);
		
		logsheetCompletion = new JProgressBar();
		GridBagConstraints gbc__logsheetCompletion = new GridBagConstraints();
		gbc__logsheetCompletion.anchor = GridBagConstraints.WEST;
		gbc__logsheetCompletion.fill = GridBagConstraints.HORIZONTAL;
		gbc__logsheetCompletion.insets = new Insets(10, 0, 0, 5);
		gbc__logsheetCompletion.gridx = 1;
		gbc__logsheetCompletion.gridy = 2;
		logsheetsPanel.add(logsheetCompletion, gbc__logsheetCompletion);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.weighty = 100.0;
		gbc_verticalStrut.gridx = 0;
		gbc_verticalStrut.gridy = 2;
		add(verticalStrut, gbc_verticalStrut);
		
		logbookCompletion.setStringPainted(true);
		logbookCompletion.addChangeListener(new ChangeListener() {
			@Override 
			public void stateChanged(ChangeEvent event) {
				logbookCompletion.setString(logbookCompletion.getValue() + " %");
			}
		});
		
		logsheetCompletion.setStringPainted(true);
		logsheetCompletion.addChangeListener(new ChangeListener() {
			@Override 
			public void stateChanged(ChangeEvent event) {
				logsheetCompletion.setString(logsheetCompletion.getValue() + " %");
			}
		});
	}
}
