/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.other;


import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableColumn;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Identifier;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.IdentifierType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.services.data.VesselsServices;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.VesselDataPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.VesselIdentifiersPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.VesselsListPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.model.VesselIdentifiersTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.model.VesselTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener;
import org.fao.fi.vrmf.psm.ui.common.spi.PreferenceListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingAction;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingListSelectionListener;

import lombok.AccessLevel;
import lombok.Getter;
import no.tornado.databinding.BindingGroup;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class VesselsController extends BaseController implements PreferenceListener {
	@Inject private VesselsServices vesselsServices;
	
	@Inject private VesselsListPanel list;
	@Inject private VesselDataPanel data;
	@Inject private VesselIdentifiersPanel identifiers;

	@Getter(AccessLevel.PRIVATE) private Identifier selectedIdentifier;

	private BindingGroup vesselMainBindings;
	private BindingGroup vesselOtherBindings;
	private BindingGroup vesselIdentifierBindings;
	
	@PostConstruct
	private void initialize() {
		initializeVessels();
		initializeVesselIdentifiers();
	}
	
	private void initializeVessels() {
		prefs.registerListener(this);
		model.registerListener((VesselListener)this);
		list.getVesselsTable().setModel(new VesselTableModel(model.getVessels()));
		
		TableColumn fixed = list.getVesselsTable().getColumnModel().getColumn(0);
		fixed.setMaxWidth(48);
		fixed.setWidth(fixed.getMaxWidth());
		fixed.setResizable(false);
		
		list.getVesselsTable().getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				int selectedIndex = list.getVesselsTable().getSelectedRow();
				
				VesselData selected = null;
				
				List<VesselData> events = model.getVessels();
				
				if(events != null && selectedIndex >= 0 && selectedIndex <= events.size() - 1) {
					selected = events.get(list.getVesselsTable().convertRowIndexToModel(selectedIndex));
				}
				
				boolean same = selected != null && selected.equals(model.getSelectedVessel());

				if(same || selected == null) return;
				
				if(application.confirmCancelUpdates(model.isVesselDirty())) {
					doRevertVessel();
				} else {
					int index = events.indexOf(model.getSelectedEvent());
					
					index = list.getVesselsTable().convertRowIndexToView(index);
					
					list.getVesselsTable().setRowSelectionInterval(index, index);
					
					model.taintVessel();
				}
				
				model.select(selected, selected == null ? EditingStatus.NOT_SET : selected.getId() == null ? EditingStatus.ADDING : EditingStatus.EDITING);
			}
		});
		
		list.getAddVessel().addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) {
				addVesselAction();
			}
		});
		
		Action deleteVessel = new InterceptingAction("Delete") {
			/** Field serialVersionUID */
			private static final long serialVersionUID = 8393094023934842112L;

			@Override
			protected void doActionPerformed(ActionEvent e) {
				deleteVesselAction();
			}
		};
		
		deleteVessel.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control D"));
		
		data.getDeleteVessel().setActionCommand("DeleteVessel");
		data.getDeleteVessel().getActionMap().put("DeleteVessel", deleteVessel);
		data.getDeleteVessel().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put((KeyStroke)deleteVessel.getValue(Action.ACCELERATOR_KEY), "DeleteVessel");
		
//		data.getDeleteVessel().addActionListener(new ActionListener() {
//			@Override
//			protected void doActionPerformed(ActionEvent e) {
//				deleteVesselAction();
//			}
//		});
		
		data.getUpdateVessel().addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) {
				updateVesselAction();
			}
		});	
		
		data.getRevertVessel().addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) {
				revertVesselAction();
			}
		});	
		
		
		data.getIdentifyVessel().addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) {
				identifyVesselAction();
			}
		});
		
		list.getBack().addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) {
				backAction();
			}
		});


		UIUtils.addValueChangeListener(data, new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				if(model.getSelectedVessel() != null) model.taintVessel();
			}
		});
		
		data.getRegCountry().setModel(new DefaultComboBoxModel(commonServices.allCountries().toArray(new Object[0])));
//		AutoCompleteDecorator.decorate(data.getRegCountry(), CountryAutocompleter.INSTANCE);
		
		data.setContentVisibility(false);
	}
	
	private void initializeVesselIdentifiers() {
		identifiers.getIdentifiersTable().setModel(new VesselIdentifiersTableModel(new ArrayList<Identifier>()));
		
		TableColumn fixed = null;
		
		for(int i=0; i<=1; i++) {
			fixed = identifiers.getIdentifiersTable().getColumnModel().getColumn(i);
			
			fixed.setMaxWidth(48);
			fixed.setWidth(fixed.getMaxWidth());
			fixed.setResizable(false);
		}
		
		identifiers.getIdentifiersTable().getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				int selectedIndex = identifiers.getIdentifiersTable().getSelectedRow();
				
				List<Identifier> rawData = model.getSelectedVessel() == null ? null : model.getSelectedVessel().getIdentifiers();
				
				if(rawData != null && selectedIndex >= 0 && selectedIndex <= rawData.size() - 1) {
					Identifier selected = rawData.get(identifiers.getIdentifiersTable().convertRowIndexToModel(selectedIndex)); 
					
					if(!selected.equals(selectedIdentifier)) selectIdentifier(selected);
				}
			}
		});
		
		identifiers.getAddIdentifier().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addIdentifierAction();
			}
		});
		
		identifiers.getUpdateIdentifier().addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) {
				vesselIdentifierBindings.flushUIToModel();
				updateIdentifierAction();
			}
		});
		
		identifiers.getDeleteIdentifier().addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) {
				deleteIdentifierAction();
			}
		});
	}
	
	private void doSelect(VesselData selected) {
		if(selected != null) {
			bindVessel(selected);
			
			selectIdentifier(null);
		} else {
			unbindVessel();
			unbindIdentifier();
		}
		
		updateIdentifiersList();
		
		model.untaintVessel();
	}

	private void bindVessel(VesselData selected) {
		if(selected != null) {
			unbindVessel();
			
			vesselMainBindings = new BindingGroup(selected);

			vesselMainBindings.add(data.getVesselName(), "name");
			vesselMainBindings.add(data.getType(), "type");
			vesselMainBindings.add(data.getIRCS(), "ircs");
			vesselMainBindings.add(data.getMMSI(), "mmsi");
			vesselMainBindings.add(data.getIMO(), "imo");
			vesselMainBindings.add(data.getRegNo(), "registrationNumber");
			vesselMainBindings.add(data.getRegCountry(), "registrationCountry");
			vesselMainBindings.add(data.getFishingCompany(), "fishingCompany");
			
			vesselMainBindings.bind();
			
			vesselOtherBindings = new BindingGroup(selected);
			
			vesselOtherBindings.add(data.getLOA(), "loa");
			vesselOtherBindings.add(data.getGT(), "gt");
			vesselOtherBindings.add(data.getLBP(), "lbp");
			vesselOtherBindings.add(data.getGRT(), "grt");
			vesselOtherBindings.add(data.getMainEnginePower(), "mainEnginePower");
			vesselOtherBindings.add(data.getMainEnginePowerUnit(), "mainEnginePowerUnit");
			
			vesselOtherBindings.bind();
		}
	}
	
	private void unbindVessel() {
		if(vesselMainBindings != null)
			vesselMainBindings.unbind();
		
		if(vesselOtherBindings != null)
			vesselOtherBindings.unbind();
	}
	
	private void bindIdentifier(Identifier selected) {
		if(selected != null) {
			vesselIdentifierBindings = new BindingGroup(selected);

			vesselIdentifierBindings.add(identifiers.getIdentifierType(), "type");
			vesselIdentifierBindings.add(identifiers.getIdentifierValue(), "identifier");
			
			vesselIdentifierBindings.bind();
		}
	}
	
	private void unbindIdentifier() {
		if(vesselIdentifierBindings != null)
			vesselIdentifierBindings.unbind();
	}

	private void updateVesselList() {
		JTable table = list.getVesselsTable();
		
		((VesselTableModel)table.getModel()).update(model.getVessels());
		
		table.clearSelection();

		deselectVessel();
	}
	
	private void updateIdentifiersList() {
		JTable table = identifiers.getIdentifiersTable();
		
		((VesselIdentifiersTableModel)table.getModel()).update(model.getSelectedVessel() == null || model.getSelectedVessel().getIdentifiers() == null ? new ArrayList<Identifier>() : model.getSelectedVessel().getIdentifiers());
		
		table.clearSelection();
	}
	
	private void backAction() {
		if(application.confirmCancelUpdates(model.isLogbookDirty() || model.isVesselDirty())) {
			model.initialize();

			application.show("main");
		}
	}
	
	private void addVesselAction() {
		if(application.confirmCancelUpdates(model.isVesselDirty())) {
			deselectVessel();
			
			model.addVessel();
		}
	}
	
	private void updateVesselAction() {
		if(model.getSelectedVessel() != null) {
			vesselMainBindings.flushUIToModel();
			vesselOtherBindings.flushUIToModel();

			List<CheckResult> checks = model.getSelectedVessel().clean().checkCorrectness();
			
			StringBuilder messages = new StringBuilder();
			
			if(checks != null && !checks.isEmpty()) {
				for(CheckResult in : checks) {
					messages.
						append("[").append(in.getType().getCode()).append("] : ").
						append(in.getMessage()).
						append("\n");
				}
			}
			
			if(CheckResultHelper.hasErrors(checks)) {
				error("Cannot update selected vessel", messages.toString());
				
				log().error("Cannot update selected vessel due to the following reasons:");

				for(CheckResult in : checks) {
					log().error("{} >> {}", in.getType().getCode(), in.getMessage());
				}
			} else {
				boolean proceed = !CheckResultHelper.hasWarnings(checks);
				
				if(!proceed) {
					proceed = 
						UIUtils.confirm(
							"Please confirm", 
							"Current vessel data do contain the following inconsistencies:\n\n" + 
								messages.toString() + "\n" +
							"Are you sure you want to proceed?");
				}
				
				if(proceed) {
					try {
						model.updateVessel();
					} catch(Throwable t) {
						fatal("Unable to update selected vessel", t.getMessage(), t);
					}
				}
			}
		}
	}
	
	private void revertVesselAction() {
		if(model.getSelectedVessel() != null) {
			boolean proceed = 
					UIUtils.confirm(
						"Please confirm", 
						"Current vessel data will be reverted to their original status\n\n" +
						"Are you sure you want to proceed?");

			if(proceed) {
				doRevertVessel();

				doSelect(model.getSelectedVessel());
				selectIdentifier(null);

				log().info("Vessel data have been reverted to their original status");
			}
		}
	}
	
	private void doRevertVessel() {
		VesselData selectedVessel = model.getSelectedVessel();
		
		if(selectedVessel != null) {
			if(selectedVessel.getId() == null) {
				model.getVessels().remove(selectedVessel);
			} else {
				model.revert(selectedVessel);
			}
			
			selectedIdentifier = null;
			
			unbindVessel();
			unbindIdentifier();
		}
	}
	
	private void identifyVesselAction() {
		if(model.getSelectedVessel() != null) {
			String name, IMO, ircs, regNo;
			Country registrationCountry;
			
			name = Utils.trim(data.getVesselName().getText());
			IMO = Utils.trim(data.getIMO().getText());
			ircs = Utils.trim(data.getIRCS().getText());
			regNo = Utils.trim(data.getRegNo().getText());
			
			registrationCountry = (Country)data.getRegCountry().getSelectedItem();
			
			boolean proceed = registrationCountry != null && (IMO != null || name != null || ircs != null || regNo != null);
			
			if(!proceed) {
				UIUtils.alert(application.getApplicationFrame(), "In order to identify a vessel it must have a registration country and at least one data set among IMO, name, IRCS and registration number");
			} else {
				UIUtils.alert(application.getApplicationFrame(), "Not implemented yet");
			}
		}
	}
	
	private void deleteVesselAction() {
		if(UIUtils.confirm(
			"Please confirm", 
			"Are you sure you want to delete the selected vessel?"
		)) {
			try {
				model.deleteVessel();
			} catch(Throwable t) {
				fatal("Unable to delete selected vessel", t.getMessage(), t);
			}
		}
	}
	
	private void deselectVessel() {
		model.select((VesselData)null);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselAdded(org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData)
	 */
	@Override
	public void vesselAdded(VesselData added) {
		model.select(added);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselRemoved(org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData)
	 */
	@Override
	public void vesselRemoved(VesselData removed) {
		 updateVesselList();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselUpdated(org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData)
	 */
	@Override
	public void vesselUpdated(VesselData updated) {
		updateVesselList();
		
		model.select(updated);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselSelected(org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData)
	 */
	@Override
	public void vesselSelected(VesselData selected) {
		data.setContentVisibility(selected != null);

		doSelect(selected);
		
		if(selected == null) {
			 list.getVesselsTable().clearSelection();
		} else {
			log().trace("Selected vessel: {} {}", selected.hashCode(), selected.getName());

			boolean canBeDeleted = vesselsServices.canBeDeleted(selected);
			
			if(!canBeDeleted)
				log().warn("Selected vessel ({} [{} - {}] {}) cannot be deleted as it is referenced by at least one logsheet / logbook", 
							selected.getName(),
							selected.getType().getCode(),
							selected.getIrcs(),
							selected.getRegistrationNumber());
			
			data.updateInputs(selected, canBeDeleted);
			identifiers.updateInputs(null);
		 }
	}
	

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.VesselListener#vesselStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void vesselStatusChanged(EditingStatus previous, EditingStatus current) {
		 // TODO Auto-generated method stub
	}
	
	private void selectIdentifier(Identifier selected) {
		if(selected != null) {
			bindIdentifier(selected);
		} else {
			bindIdentifier(new Identifier());
			unbindIdentifier();
		}
		
		identifiers.updateInputs(selectedIdentifier = selected);
	}
	
	private void addIdentifierAction() {
		selectIdentifier(new Identifier());
	}
	
	private void updateIdentifierAction() {
		if(selectedIdentifier != null) {
			List<CheckResult> checks = selectedIdentifier.clean().checkCorrectness();
			
			StringBuilder messages = new StringBuilder();
			
			if(checks != null && !checks.isEmpty()) {
				for(CheckResult in : checks) {
					messages.
						append("[").append(in.getType().getCode()).append("] : ").
						append(in.getMessage()).
						append("\n");
				}
			}
			
			if(CheckResultHelper.hasErrors(checks)) {
				error("Cannot update selected identifier", messages.toString());
				
				log().error("Cannot update selected identifier due to the following reasons:");

				for(CheckResult in : checks) {
					log().error("{} >> {}", in.getType().getCode(), in.getMessage());
				}
			} else {
				VesselData selectedVessel = model.getSelectedVessel();
				
				boolean proceed = !CheckResultHelper.hasWarnings(checks);
				
				if(!proceed) {
					proceed = 
						UIUtils.confirm(
							"Please confirm", 
							"Current vessel identifier does contain the following inconsistencies:\n\n" + 
								messages.toString() + "\n" +
							"Are you sure you want to proceed?");
				}
				
				if(proceed) {
					model.taintVessel();
					
					if(selectedIdentifier.getId() == null) {
						if(selectedVessel.getIdentifiers() == null) selectedVessel.setIdentifiers(new ArrayList<Identifier>());
						
						selectedVessel.getIdentifiers().add(selectedIdentifier);
					}
				
					((VesselIdentifiersTableModel)data.getIdentifiers().getIdentifiersTable().getModel()).update(selectedVessel.getIdentifiers());
				
					deselectIdentifier();
				}
			}
		}		
	}
	
	private void deleteIdentifierAction() {
		if(selectedIdentifier.getId() == null || UIUtils.confirm("Please confirm", "Are you sure you want to delete the selected vessel identifier?")) {
			VesselData selectedVessel = model.getSelectedVessel();
			
			String identifier = selectedIdentifier.getIdentifier();
			IdentifierType type = selectedIdentifier.getType();
			
			selectedVessel.getIdentifiers().remove(selectedIdentifier);
			
			((VesselIdentifiersTableModel)data.getIdentifiers().getIdentifiersTable().getModel()).update(selectedVessel.getIdentifiers());
			
			if(selectedIdentifier.getId() != null) model.taintVessel();
			
			if(selectedIdentifier.isCorrectlySet()) log().info("The identifier '{}' of type {} ({}) has been successfully removed from vessel {} / {}", identifier, type.getCode(), type.getName(), selectedVessel.getName(), selectedVessel.getIrcs());
			
			deselectIdentifier();
		}
	}
	
	private void deselectIdentifier() {
		selectIdentifier(null);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.ui.common.spi.PreferenceListener#preferencesChanged(java.lang.String, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void preferencesChanged(String property, Object oldValue, Object newValue) {
		if("offline".equals(property)) {
			boolean enableIdentification = !Boolean.TRUE.equals(newValue);
			
			data.getIdentifyVessel().setEnabled(enableIdentification);
		}
	}
}