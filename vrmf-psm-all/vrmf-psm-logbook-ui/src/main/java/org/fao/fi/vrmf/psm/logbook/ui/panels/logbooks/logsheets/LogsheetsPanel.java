/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetDetailsPanel;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JMapPanel;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 30, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 30, 2015
 */
@Named @Singleton 
public class LogsheetsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5410339222808726456L;
	@Getter @Setter @Inject private LogsheetsListPanel logsheetsList;
	@Getter @Setter @Inject private LogsheetDataPanel logsheetData;
	@Getter @Setter @Inject private LogsheetDetailsPanel logsheetDetails;
	@Getter @Setter @Inject private LogsheetsActionsPanel logsheetsActions;
	
	
	@Getter private JTabbedPane logsheetTabs;
	@Getter private JMapPanel logsheetMap;
	
	/**
	 * Class constructor
	 *
	 */
	public LogsheetsPanel() {
		logsheetMap = new JMapPanel();

		if(Utils.IS_LAUNCHING) return;
		
		logsheetsList = new LogsheetsListPanel();
		logsheetData = new LogsheetDataPanel();
		logsheetDetails = new LogsheetDetailsPanel();
		logsheetsActions = new LogsheetsActionsPanel();
		
		initialize();
	}
	
	/**
	 * Create the panel.
	 */
	@PostConstruct
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		GridBagConstraints gbc_logsheetsList = new GridBagConstraints();
		gbc_logsheetsList.weighty = 50.0;
		gbc_logsheetsList.anchor = GridBagConstraints.NORTH;
		gbc_logsheetsList.weightx = 100.0;
		gbc_logsheetsList.fill = GridBagConstraints.BOTH;
		gbc_logsheetsList.gridx = 0;
		gbc_logsheetsList.gridy = 0;
		add(logsheetsList, gbc_logsheetsList);
		
		logsheetTabs = new JTabbedPane();
		
		GridBagConstraints gbc_tabs = new GridBagConstraints();
		gbc_tabs.weighty = 50.0;
		gbc_tabs.anchor = GridBagConstraints.SOUTH;
		gbc_tabs.weightx = 100.0;
		gbc_tabs.fill = GridBagConstraints.BOTH;
		gbc_tabs.gridx = 0;
		gbc_tabs.gridy = 1;
		add(logsheetTabs, gbc_tabs);
		
		JPanel data = new JPanel();
		GridBagLayout dataGridBagLayout = new GridBagLayout();
		dataGridBagLayout.columnWidths = new int[0];
		dataGridBagLayout.columnWeights = new double[0];
		dataGridBagLayout.rowWeights = new double[0];
		data.setLayout(dataGridBagLayout);
		
		GridBagConstraints gbc_logsheetData = new GridBagConstraints();
		gbc_logsheetData.weightx = 100.0;
		gbc_logsheetData.anchor = GridBagConstraints.NORTH;
		gbc_logsheetData.fill = GridBagConstraints.BOTH;
		gbc_logsheetData.gridx = 0;
		gbc_logsheetData.gridy = 1;
		data.add(logsheetData, gbc_logsheetData);
//		logsheetData.setVisible(false);
		
		GridBagConstraints gbc_logsheetEvents = new GridBagConstraints();
		gbc_logsheetEvents.weighty = 50.0;
		gbc_logsheetEvents.anchor = GridBagConstraints.NORTH;
		gbc_logsheetEvents.weightx = 100.0;
		gbc_logsheetEvents.fill = GridBagConstraints.BOTH;
		gbc_logsheetEvents.gridx = 0;
		gbc_logsheetEvents.gridy = 2;
		data.add(logsheetDetails, gbc_logsheetEvents);
//		logsheetDetails.setVisible(false);
		
		GridBagConstraints gbc_logsheetsActions = new GridBagConstraints();
		gbc_logsheetsActions.anchor = GridBagConstraints.SOUTH;
		gbc_logsheetsActions.weightx = 100.0;
		gbc_logsheetsActions.fill = GridBagConstraints.BOTH;
		gbc_logsheetsActions.gridx = 0;
		gbc_logsheetsActions.gridy = 3;
		data.add(logsheetsActions, gbc_logsheetsActions);
//		logsheetsActions.setVisible(false);s
		
		GridBagConstraints gbc_data = new GridBagConstraints();
		gbc_data.weighty = 100.0;
		gbc_data.anchor = GridBagConstraints.NORTH;
		gbc_data.weightx = 100.0;
		gbc_data.fill = GridBagConstraints.BOTH;
		gbc_data.gridx = 0;
		gbc_data.gridy = 0;
		logsheetTabs.add("Logsheet data", data);
		
		logsheetTabs.add("Trip events map", logsheetMap);
		
		logsheetTabs.setVisible(false);
		
//		JTable disabled = logsheetDetails.getEventsPanel().getLogsheetsEventsTable();
//		disabled.setEnabled(false);
	}
}
