/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.support;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 2, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 2, 2015
 */
public enum EditingStatus {
	NOT_SET,
	VIEWING,
	ADDING,
	EDITING,
	COMPLETING
}
