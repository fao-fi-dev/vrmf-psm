/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class LogsheetEventCatchTableModel extends AbstractTableModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 587756690700916447L;

	@Getter private String[] columnNames = new String[] { 
		"New", "#", "3AC", "Species", "Scientific name", "Quantity", "Unit"
	};

	@Getter private List<Catches> rawData = null;

	/**
	 * Class constructor
	 *
	 * @param rawData
	 */
	public LogsheetEventCatchTableModel(List<Catches> data) {
		super();
		
		rawData = data == null ? new ArrayList<Catches>() : data;
	}
	
	public void update(List<Catches> data) {
		rawData = data == null ? new ArrayList<Catches>() : data;
		
		fireTableDataChanged();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return rawData.size();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		Catches record = rawData == null || rawData.size() < row + 1 ? null : rawData.get(row);
	
		if(record == null) return null;
		
		switch(col) {
			case 0:
				return record.getId() == null;
			case 1:
				return row + 1;
			case 2:
				return record.getSpecies().getAlpha3Code();
			case 3:
				return record.getSpecies().getLocalName();
			case 4:
				return record.getSpecies().getScientificName();
			case 5:
				return record.getQuantity();
			case 6:
				return record.getQuantityType();
			default:
				return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		switch(c) {
			case 0:
				return Boolean.class;
			case 1:
				return Integer.class;
			case 2:
			case 3:
			case 4:
				return String.class;
			case 5:
				return Double.class;
			case 6:
				return QuantityType.class;
			default:
				return Void.class;
		}
	}
	
	public Object[][] getData() {
		if(rawData == null) return new Object[0][];
		
		Object[][] toReturn = new Object[getRowCount()][getColumnCount()];
		
		for(int row=0; row < getRowCount(); row++) {
			toReturn[row] = new Object[getColumnCount()];
			
			for(int column=0; column < getColumnCount(); column++) {
				toReturn[row][column] = getValueAt(row, column);
			}
		}
		
		return toReturn;
	}
}