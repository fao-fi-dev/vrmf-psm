/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.CatchesPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.DiscardsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.efforts.EffortsPanel;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 10, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 10, 2015
 */
@Named @Singleton
public class CatchAndEffortPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2396944113608681889L;
	
	@Inject @Getter private EffortsPanel effortsPanel;
	@Inject @Getter private CatchesPanel catchesPanel;
	@Inject @Getter private DiscardsPanel discardsPanel;
	
	@Getter private JTabbedPane catchAndEffortTabs;
	
	@Getter private JPanel effortsContainer;
	@Getter private JPanel catchesContainer;
	@Getter private JPanel discardsContainer;
	
	/**
	 * Class constructor
	 */
	public CatchAndEffortPanel() {
		if(Utils.IS_LAUNCHING) return;
	
		effortsPanel = new EffortsPanel();
		effortsPanel.getAddNew().setEnabled(false);

		catchesPanel = new CatchesPanel();
		discardsPanel = new DiscardsPanel();
		
		initialize();
	}
	
	@PostConstruct
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		catchAndEffortTabs = new JTabbedPane(JTabbedPane.TOP);
		catchAndEffortTabs.setEnabled(true);
		GridBagConstraints gbc_catchAndEffortTabs = new GridBagConstraints();
		gbc_catchAndEffortTabs.weighty = 100.0;
		gbc_catchAndEffortTabs.weightx = 100.0;
		gbc_catchAndEffortTabs.fill = GridBagConstraints.BOTH;
		gbc_catchAndEffortTabs.gridx = 0;
		gbc_catchAndEffortTabs.gridy = 0;
		add(catchAndEffortTabs, gbc_catchAndEffortTabs);
		
		effortsContainer = new JPanel();
		catchAndEffortTabs.addTab("Efforts (#)", null, effortsContainer, "Effort records for this event");
		GridBagLayout gbl__effortsContainer = new GridBagLayout();
		gbl__effortsContainer.columnWidths = new int[0];
		gbl__effortsContainer.rowHeights = new int[0];
		gbl__effortsContainer.columnWeights = new double[0];
		gbl__effortsContainer.rowWeights = new double[0];
		effortsContainer.setLayout(gbl__effortsContainer);
		
		GridBagConstraints gbc_effortsPanel = new GridBagConstraints();
		gbc_effortsPanel.weighty = 100.0;
		gbc_effortsPanel.weightx = 100.0;
		gbc_effortsPanel.fill = GridBagConstraints.BOTH;
		gbc_effortsPanel.gridx = 0;
		gbc_effortsPanel.gridy = 0;
		effortsContainer.add(effortsPanel, gbc_effortsPanel);
		
		catchesContainer = new JPanel();
		catchAndEffortTabs.addTab("Catches (#)", null, catchesContainer, "Catch records for this event");
		GridBagLayout gbl__catchesContainer = new GridBagLayout();
		gbl__catchesContainer.columnWidths = new int[0];
		gbl__catchesContainer.rowHeights = new int[0];
		gbl__catchesContainer.columnWeights = new double[0];
		gbl__catchesContainer.rowWeights = new double[0];
		catchesContainer.setLayout(gbl__catchesContainer);
		
		GridBagConstraints gbc_catchesPanel = new GridBagConstraints();
		gbc_catchesPanel.weighty = 100.0;
		gbc_catchesPanel.weightx = 100.0;
		gbc_catchesPanel.fill = GridBagConstraints.BOTH;
		gbc_catchesPanel.gridx = 0;
		gbc_catchesPanel.gridy = 0;
		catchesContainer.add(catchesPanel, gbc_catchesPanel);
		
		discardsContainer = new JPanel();
		catchAndEffortTabs.addTab("Discards (#)", null, discardsContainer, "Discard records for this event");
		GridBagLayout gbl__discardsContainer = new GridBagLayout();
		gbl__discardsContainer.columnWidths = new int[0];
		gbl__discardsContainer.rowHeights = new int[0];
		gbl__discardsContainer.columnWeights = new double[0];
		gbl__discardsContainer.rowWeights = new double[0];
		discardsContainer.setLayout(gbl__discardsContainer);
		
		GridBagConstraints gbc_discardsPanel = new GridBagConstraints();
		gbc_discardsPanel.weighty = 100.0;
		gbc_discardsPanel.weightx = 100.0;
		gbc_discardsPanel.fill = GridBagConstraints.BOTH;
		gbc_discardsPanel.gridx = 0;
		gbc_discardsPanel.gridy = 0;
		discardsContainer.add(discardsPanel, gbc_discardsPanel);
	}
}
