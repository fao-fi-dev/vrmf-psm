/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.reports.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.DerivedEffort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DerivedEffortType;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class DerivedEffortsReportTableModel extends AbstractTableModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 587756690700916447L;

	@Getter private String[] columnNames = new String[] { 
		"#", "Value", "Type"
	};

	@Getter private List<DerivedEffort> rawData = null;

	/**
	 * Class constructor
	 *
	 * @param rawData
	 */
	public DerivedEffortsReportTableModel(List<DerivedEffort> data) {
		super();
		
		rawData = data == null ? new ArrayList<DerivedEffort>() : data;
	}
	
	public void update(List<DerivedEffort> data) {
		rawData = data == null ? new ArrayList<DerivedEffort>() : data;
		
		fireTableDataChanged();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return rawData.size();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		DerivedEffort record = rawData == null || rawData.size() < row + 1 ? null : rawData.get(row);
	
		if(record == null) return null;
		
		switch(col) {
			case 0:
				return row + 1;
			case 1:
				return record.getEffortValue();
			case 2:
				return record.getEffortType();
			default:
				return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		switch(c) {
			case 0:
				return Integer.class;
			case 1:
				return Double.class;
			case 2:
				return DerivedEffortType.class;
			default:
				return Void.class;
		}
	}
	
	public Object[][] getData() {
		if(rawData == null) return new Object[0][];
		
		Object[][] toReturn = new Object[getRowCount()][getColumnCount()];
		
		for(int row=0; row < getRowCount(); row++) {
			toReturn[row] = new Object[getColumnCount()];
			
			for(int column=0; column < getColumnCount(); column++) {
				toReturn[row][column] = getValueAt(row, column);
			}
		}
		
		return toReturn;
	}
}