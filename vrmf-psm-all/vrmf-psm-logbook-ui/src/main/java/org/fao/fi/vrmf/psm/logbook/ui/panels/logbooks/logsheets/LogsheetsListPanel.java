/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.model.LogsheetTableModel;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.DateTimeCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.DefaultTableHeaderRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.PortTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.StringTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.VesselDataTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class LogsheetsListPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8872498289521272459L;
	
	@Getter private JNoFocusOutlineButton addNewLogsheet;
	@Getter private JNoFocusOutlineButton printLogbook;
	
	@Getter private JTable logsheetsTable;
	
	public LogsheetsListPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.rowHeights = new int[] {0, 32};
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);
		
		JPanel logsheets = new JPanel();
		logsheets.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Logsheets for selected logbook", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_logsheets = new GridBagConstraints();
		gbc_logsheets.fill = GridBagConstraints.BOTH;
		gbc_logsheets.weighty = 100.0;
		gbc_logsheets.anchor = GridBagConstraints.NORTH;
		gbc_logsheets.weightx = 100.0;
		gbc_logsheets.insets = new Insets(5, 5, 5, 5);
		gbc_logsheets.gridx = 0;
		gbc_logsheets.gridy = 0;
		this.add(logsheets, gbc_logsheets);
		GridBagLayout gbl_logsheets = new GridBagLayout();
		gbl_logsheets.columnWidths = new int[0];
		gbl_logsheets.columnWeights = new double[0];
		gbl_logsheets.rowWeights = new double[0];
		logsheets.setLayout(gbl_logsheets);

		logsheetsTable = new JTable(new LogsheetTableModel(new ArrayList<Logsheet>()) {
			/** Field serialVersionUID */
			private static final long serialVersionUID = 5210252037194577047L;
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
		
		logsheetsTable.setAutoCreateRowSorter(true);

		logsheetsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		logsheetsTable.getTableHeader().setReorderingAllowed(false);
		
		logsheetsTable.setDefaultRenderer(String.class, new StringTableCellRenderer());
		logsheetsTable.setDefaultRenderer(VesselData.class, new VesselDataTableCellRenderer());
		logsheetsTable.setDefaultRenderer(Port.class, new PortTableCellRenderer());
		logsheetsTable.setDefaultRenderer(Date.class, new DateTimeCellRenderer());
		
		logsheetsTable.getTableHeader().setDefaultRenderer(new DefaultTableHeaderRenderer(logsheetsTable));
		
		TableColumn column = logsheetsTable.getColumnModel().getColumn(0);
		column.setMaxWidth(48);
		column.setMinWidth(column.getMaxWidth());
		column.setWidth(column.getMaxWidth());
		column.setResizable(false);
		
		column = logsheetsTable.getColumnModel().getColumn(1);
		column.setMinWidth(160);
		column.setWidth(column.getMinWidth());
		column.setResizable(true);
		
		GridBagConstraints gbc_logsheetsTable = new GridBagConstraints();
		gbc_logsheetsTable.fill = GridBagConstraints.BOTH;
		gbc_logsheetsTable.weighty = 100.0;
		gbc_logsheetsTable.weightx = 100.0;
		gbc_logsheetsTable.gridx = 0;
		gbc_logsheetsTable.gridy = 0;
		logsheets.add(new JScrollPane(logsheetsTable), gbc_logsheetsTable);
		
		JPanel logsheetsActions = new JPanel();
		GridBagConstraints gbc_logsheetsActions = new GridBagConstraints();
		gbc_logsheetsActions.anchor = GridBagConstraints.SOUTH;
		gbc_logsheetsActions.fill = GridBagConstraints.BOTH;
		gbc_logsheetsActions.gridx = 0;
		gbc_logsheetsActions.gridy = 1;
		this.add(logsheetsActions, gbc_logsheetsActions);
		GridBagLayout gbl_logsheetsActions = new GridBagLayout();
		gbl_logsheetsActions.columnWidths = new int[0];
		gbl_logsheetsActions.rowHeights = new int[0];
		gbl_logsheetsActions.columnWeights = new double[0];
		gbl_logsheetsActions.rowWeights = new double[0];
		logsheetsActions.setLayout(gbl_logsheetsActions);
		
		addNewLogsheet = new JNoFocusOutlineButton("Add a new logsheet");
		addNewLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		addNewLogsheet.setEnabled(false);
		
		GridBagConstraints gbc_btnAddNew = new GridBagConstraints();
		gbc_btnAddNew.anchor = GridBagConstraints.WEST;
		gbc_btnAddNew.fill = GridBagConstraints.BOTH;
		gbc_btnAddNew.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddNew.gridx = 0;
		gbc_btnAddNew.gridy = 0;
		logsheetsActions.add(addNewLogsheet, gbc_btnAddNew);
		
		printLogbook = new JNoFocusOutlineButton("Print logbook");
		printLogbook.setFont(new Font("Tahoma", Font.PLAIN, 14));
		printLogbook.setEnabled(false);
		
		GridBagConstraints gbc_btnPrintLogbook = new GridBagConstraints();
		gbc_btnPrintLogbook.anchor = GridBagConstraints.WEST;
		gbc_btnPrintLogbook.fill = GridBagConstraints.BOTH;
		gbc_btnPrintLogbook.insets = new Insets(0, 0, 0, 5);
		gbc_btnPrintLogbook.gridx = 1;
		gbc_btnPrintLogbook.gridy = 0;
		logsheetsActions.add(printLogbook, gbc_btnPrintLogbook);
	}
}
