/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.support.StateTransitionHandler;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 3, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 3, 2015
 */
@Named @Singleton
public class LogsheetAddEventActionsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2803197355460461872L;
	
	@Getter private JNoFocusOutlineButton transit;
	@Getter private JNoFocusOutlineButton badWeather;
	@Getter private JNoFocusOutlineButton cleaning;
	@Getter private JNoFocusOutlineButton transshipping;
	@Getter private JNoFocusOutlineButton searching;
	@Getter private JNoFocusOutlineButton fishing;
	@Getter private JNoFocusOutlineButton setting;
	@Getter private JNoFocusOutlineButton retrieving;
	
	/**
	 * Class constructor
	 *
	 */
	public LogsheetAddEventActionsPanel() {
		setBorder(new TitledBorder(null, "Add a new event", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		
		setLayout(gridBagLayout);
		
		JPanel container = new JPanel();
		container.setBorder(null);
		
		GridBagConstraints gbc__container = new GridBagConstraints();
		gbc__container.weightx = 100.0;
		gbc__container.weighty = 100.0;
		gbc__container.anchor = GridBagConstraints.NORTH;
		gbc__container.fill = GridBagConstraints.BOTH;
		gbc__container.insets = new Insets(0, 0, 0, 0);
		gbc__container.gridx = 0;
		gbc__container.gridy = 0;
		add(container, gbc__container);
		container.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
		
		transit = new JNoFocusOutlineButton(EventType.TRANSIT.getCode());
		transit.setToolTipText(EventType.TRANSIT.getName());
		transit.setEnabled(false);
		container.add(transit);
		
		badWeather = new JNoFocusOutlineButton(EventType.BAD_WEATHER.getCode());
		badWeather.setToolTipText(EventType.BAD_WEATHER.getName());
		badWeather.setEnabled(false);
		container.add(badWeather);
		
		cleaning = new JNoFocusOutlineButton(EventType.CLEANING.getCode());
		cleaning.setToolTipText(EventType.CLEANING.getName());
		cleaning.setEnabled(false);
		container.add(cleaning);
		
		transshipping = new JNoFocusOutlineButton(EventType.TRANSSHIPPING.getCode());
		transshipping.setToolTipText(EventType.TRANSSHIPPING.getName());
		transshipping.setEnabled(false);
		container.add(transshipping);
		
		searching = new JNoFocusOutlineButton(EventType.SEARCHING.getCode());
		searching.setToolTipText(EventType.SEARCHING.getName());
		searching.setEnabled(false);
		container.add(searching);
		
		setting = new JNoFocusOutlineButton(EventType.SETTING.getCode());
		setting.setToolTipText(EventType.SETTING.getName());
		setting.setEnabled(false);
		container.add(setting);
		
		retrieving = new JNoFocusOutlineButton(EventType.RETRIEVING.getCode());
		retrieving.setToolTipText(EventType.RETRIEVING.getName());
		retrieving.setEnabled(false);
		container.add(retrieving);
		
		fishing = new JNoFocusOutlineButton(EventType.FISHING.getCode());
		fishing.setToolTipText(EventType.FISHING.getName());
		fishing.setEnabled(false);
		container.add(fishing);
		
		for(JNoFocusOutlineButton in : new JNoFocusOutlineButton[] {
			transit,
			badWeather,
			cleaning,
			transshipping,
			searching,
			setting,
			retrieving,
			fishing
		}) {
			in.setMinimumSize(new Dimension(48, 24));
			in.setPreferredSize(in.getMinimumSize());
			in.setMaximumSize(in.getMinimumSize());
		}
	}
	
	public void notifyLogsheetStatusChange(EditingStatus status, Logsheet selected) {
		if(selected == null) return;
		
		StateTransitionHandler handler = selected.getTransitionHandler();
		
		List<EventType> nextEvents = handler == null ? new ArrayList<EventType>() : handler.nextEvents();
		
		Event currentEvent = selected.getEvents() == null || selected.getEvents().isEmpty() ? null : selected.getEvents().get(selected.getEvents().size() - 1);
		
		boolean enable = !EditingStatus.COMPLETING.equals(status) && !EditingStatus.VIEWING.equals(status) && 
						( currentEvent == null || ( currentEvent.isCompleted() && currentEvent.isCorrectlySet() ) );
		
		transit.setEnabled(enable && nextEvents.contains(EventType.TRANSIT));
		badWeather.setEnabled(enable && nextEvents.contains(EventType.BAD_WEATHER));
		cleaning.setEnabled(enable && nextEvents.contains(EventType.CLEANING));
		searching.setEnabled(enable && nextEvents.contains(EventType.SEARCHING));
		transshipping.setEnabled(enable && nextEvents.contains(EventType.TRANSSHIPPING));
		setting.setEnabled(enable && nextEvents.contains(EventType.SETTING));
		retrieving.setEnabled(enable && nextEvents.contains(EventType.RETRIEVING));
		fishing.setEnabled(enable && nextEvents.contains(EventType.FISHING));
	}
	
	public void notifyEventStatusChange(EditingStatus logsheetStatus, Logsheet logsheet, EditingStatus eventStatus, Event event) {
		if(EditingStatus.ADDING == eventStatus || EditingStatus.EDITING == eventStatus) {
			for(JNoFocusOutlineButton in : new JNoFocusOutlineButton[] { transit, badWeather, cleaning, searching, transshipping, setting, retrieving, fishing })
				in.setEnabled(false);
		} else {
			notifyLogsheetStatusChange(logsheetStatus, logsheet);
		}
	}
}
