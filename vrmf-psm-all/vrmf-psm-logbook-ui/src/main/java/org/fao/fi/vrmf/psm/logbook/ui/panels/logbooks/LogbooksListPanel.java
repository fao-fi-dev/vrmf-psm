/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.model.LogbookTableModel;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class LogbooksListPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8872498289521272459L;
	
	@Getter private JNoFocusOutlineButton addNewLogbook;
	@Getter private JNoFocusOutlineButton completeLogbook;
	@Getter private JNoFocusOutlineButton back;
	@Getter private JNoFocusOutlineButton deleteLogbook;
	
	@Getter private JTable logbooksTable;
	
	public LogbooksListPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.rowHeights = new int[] {0, 32};
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);
		
		JPanel logbooks = new JPanel();
		logbooks.setBorder(new TitledBorder(null, "Logbooks", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_logbooks = new GridBagConstraints();
		gbc_logbooks.weightx = 100.0;
		gbc_logbooks.weighty = 100.0;
		gbc_logbooks.fill = GridBagConstraints.BOTH;
		gbc_logbooks.insets = new Insets(5, 5, 5, 5);
		gbc_logbooks.anchor = GridBagConstraints.NORTH;
		gbc_logbooks.gridx = 0;
		gbc_logbooks.gridy = 0;
		this.add(logbooks, gbc_logbooks);
		GridBagLayout gbl_logbooks = new GridBagLayout();
		gbl_logbooks.columnWidths = new int[0];
		gbl_logbooks.rowHeights = new int[0];
		gbl_logbooks.columnWeights = new double[0];
		gbl_logbooks.rowWeights = new double[0];
		logbooks.setLayout(gbl_logbooks);
		
		logbooksTable = UIUtils.initialize(new JTable(new LogbookTableModel(new ArrayList<Logbook>())));
		
		GridBagConstraints gbc_logbooksTable = new GridBagConstraints();
		gbc_logbooksTable.weighty = 100.0;
		gbc_logbooksTable.weightx = 100.0;
		gbc_logbooksTable.anchor = GridBagConstraints.NORTH;
		gbc_logbooksTable.fill = GridBagConstraints.BOTH;
		gbc_logbooksTable.gridx = 0;
		gbc_logbooksTable.gridy = 0;
		logbooks.add(new JScrollPane(logbooksTable), gbc_logbooksTable);
		
		JPanel logbooksActions = new JPanel();
		GridBagConstraints gbc_logbooksActions = new GridBagConstraints();
		gbc_logbooksActions.anchor = GridBagConstraints.SOUTH;
		gbc_logbooksActions.fill = GridBagConstraints.BOTH;
		gbc_logbooksActions.gridx = 0;
		gbc_logbooksActions.gridy = 1;
		this.add(logbooksActions, gbc_logbooksActions);
		GridBagLayout gbl_logbooksActions = new GridBagLayout();
		gbl_logbooksActions.columnWidths = new int[0];
		gbl_logbooksActions.rowHeights = new int[0];
		gbl_logbooksActions.columnWeights = new double[0];
		gbl_logbooksActions.rowWeights = new double[0];
		logbooksActions.setLayout(gbl_logbooksActions);
		
		addNewLogbook = new JNoFocusOutlineButton("Add new");
		addNewLogbook.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_btnAddNew = new GridBagConstraints();
		gbc_btnAddNew.fill = GridBagConstraints.BOTH;
		gbc_btnAddNew.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddNew.gridx = 0;
		gbc_btnAddNew.gridy = 0;
		logbooksActions.add(addNewLogbook, gbc_btnAddNew);
		
		completeLogbook = new JNoFocusOutlineButton("Complete");
		completeLogbook.setFont(new Font("Tahoma", Font.PLAIN, 14));
		completeLogbook.setEnabled(false);
		GridBagConstraints gbc_completeLogbook = new GridBagConstraints();
		gbc_completeLogbook.insets = new Insets(0, 0, 0, 5);
		gbc_completeLogbook.gridx = 1;
		gbc_completeLogbook.gridy = 0;
		logbooksActions.add(completeLogbook, gbc_completeLogbook);
	
		deleteLogbook = new JNoFocusOutlineButton("Delete");
		deleteLogbook.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_deleteLogbook = new GridBagConstraints();
		gbc_deleteLogbook.insets = new Insets(0, 0, 0, 5);
		gbc_deleteLogbook.gridx = 2;
		gbc_deleteLogbook.gridy = 0;
		logbooksActions.add(deleteLogbook, gbc_deleteLogbook);
		
		back = new JNoFocusOutlineButton("Go back");
		back.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_btnBack = new GridBagConstraints();
		gbc_btnBack.fill = GridBagConstraints.BOTH;
		gbc_btnBack.gridx = 34;
		gbc_btnBack.gridy = 0;
		logbooksActions.add(back, gbc_btnBack);
		
		completeLogbook.setEnabled(false);
		deleteLogbook.setEnabled(false);
	}
}