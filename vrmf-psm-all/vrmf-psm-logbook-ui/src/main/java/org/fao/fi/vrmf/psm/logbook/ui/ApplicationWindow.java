/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.services.data.LogbooksServices;
import org.fao.fi.vrmf.psm.logbook.services.data.LogsheetsServices;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.ActionsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.LogPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.StatusPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.LogbooksPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.LogsheetPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.summary.SummaryPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.vessels.VesselsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.DataModelStatusListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.ui.common.panels.GlassPane;
import org.fao.fi.vrmf.psm.ui.common.spi.PreferenceListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.binding.JCoordinatesInputUIBridge;
import org.fao.fi.vrmf.psm.ui.common.support.components.binding.JDateTimePickerUIBridge;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import no.tornado.databinding.uibridge.UIBridgeRegistry;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Singleton @Named("app")
public class ApplicationWindow extends WindowAdapter implements LogbookListener, LogsheetListener, DataModelStatusListener, PreferenceListener { 
	static final private Logger LOG = LoggerFactory.getLogger(ApplicationWindow.class);

	@Inject private Preferences prefs;
	@Inject private DataModel model;
	
	@Inject private LogbooksServices logbookServices;
	@Inject private LogsheetsServices logsheetServices;
	
	@Getter private JFrame applicationFrame;
	@Getter private JScrollPane scrollingContainer;
	
	@Getter @Inject private GlassPane glassPane;
	@Getter @Inject private LogPanel log;
	@Getter @Inject private StatusPanel status;
	
	@Getter private JPanel mainPanel;

	private CardLayout layout;
	
	@Inject private SummaryPanel summary;
	@Inject private ActionsPanel actions;
	@Inject private VesselsPanel vessels;
	@Inject private LogbooksPanel logbooks;
	@Inject private LogsheetPanel logsheet;
	
	@Getter private JMenu appMenu;
	
	@Getter private JMenu dataMenu;
	@Getter private JMenuItem exportLogbook;
	@Getter private JMenuItem exportLogsheet;
	@Getter private JMenuItem exportLogbookAsXML;
	@Getter private JMenuItem exportLogbookAsHTML;
	@Getter private JMenuItem exportLogbookAsPDF;
	@Getter private JMenuItem exportLogbookAsFLUX;
	@Getter private JMenuItem exportLogsheetAsXML;
	@Getter private JMenuItem exportLogsheetAsHTML;
	@Getter private JMenuItem exportLogsheetAsPDF;
	@Getter private JMenuItem exportLogsheetAsFLUX;
	
	@Getter private JMenu windowMenu;
	
	private GridBagLayout mainLayout;
	
	
	/* (non-Javadoc)
	 * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosing(WindowEvent arg0) {
		int dialogButton = JOptionPane.YES_NO_OPTION;
		dialogButton = JOptionPane.showConfirmDialog(this.applicationFrame, "Are you sure you want to close the application?", "Please confirm", dialogButton);

		if(dialogButton == JOptionPane.YES_OPTION) {
			super.windowClosing(arg0);
			
			System.exit(0);
		}
	}

	public Thread getShutdownHook() {
		final ApplicationWindow $this = this;

		return new Thread() {
			@Override
			public void run() {
				try {
					ApplicationWindow.LOG.warn("The application is shutting down...");
				} catch (Throwable t) {
					UIUtils.error($this.applicationFrame, "Unexpected error", "Unable to complete application shutdown: " + Utils.getErrorMessage(t), t);
				}
			}
		};
	};

	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	@PostConstruct
	private void initialize() throws Exception {
		if(!Utils.IS_LAUNCHING) {
			log = new LogPanel();
			status = new StatusPanel();
		}
		
		//See: http://databinding.tornado.no/
		UIBridgeRegistry.addBridge(JDateTimePickerUIBridge.INSTANCE);
		UIBridgeRegistry.addBridge(JCoordinatesInputUIBridge.INSTANCE);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		int width = (int)(Math.round(screenSize.getWidth()));
		int height = (int)(Math.round(screenSize.getHeight()) - 32);

		Dimension preferred = new Dimension(width, height);
		Dimension minimum = new Dimension(1366, 768);
		
		width = (int)preferred.getWidth();
		height = (int)preferred.getHeight();
		
		applicationFrame = new JFrame();
		applicationFrame.getContentPane().setBackground(UIManager.getColor("Panel.background"));

		BufferedImage logo = ImageIO.read(Thread.currentThread().getContextClassLoader().getResourceAsStream("media/icons/FAO_FI_logo.png"));
		applicationFrame.setIconImage(logo);

		applicationFrame.setResizable(true);
		applicationFrame.setMinimumSize(minimum);
		applicationFrame.setPreferredSize(preferred);
		applicationFrame.setMaximumSize(preferred);
		applicationFrame.setSize(preferred);
		
		applicationFrame.setLocationRelativeTo(null);
		
		applicationFrame.setLocation((int)(screenSize.getWidth() - width) / 2, 0);

		applicationFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		applicationFrame.addWindowListener(this);
		
		mainLayout = new GridBagLayout();
		mainLayout.rowHeights = new int[] { 0, 128 };
		mainLayout.columnWeights = new double[0];
		mainLayout.rowWeights = new double[0];
		
		applicationFrame.getContentPane().setLayout(mainLayout);
		
		GlassPane glassPane = getGlassPane();
		
		applicationFrame.setGlassPane(glassPane);
		glassPane.setOpaque(false);
		
		JPanel main = new JPanel();

		GridBagLayout gbl_main = new GridBagLayout();
		gbl_main.columnWidths = new int[0];
		gbl_main.rowHeights = new int[0];
		gbl_main.columnWeights = new double[0];
		gbl_main.rowWeights = new double[0];
		main.setLayout(gbl_main);
		
		GridBagLayout gbl_actions = (GridBagLayout) actions.getLayout();
		gbl_actions.rowWeights = new double[0];
		gbl_actions.rowHeights = new int[0];
		gbl_actions.columnWeights = new double[0];
		gbl_actions.columnWidths = new int[0];
		GridBagConstraints gbc_actions = new GridBagConstraints();
		gbc_actions.weighty = 100.0;
		gbc_actions.insets = new Insets(0, 5, 5, 0);
		gbc_actions.fill = GridBagConstraints.BOTH;
		gbc_actions.gridx = 0;
		gbc_actions.gridy = 0;
		main.add(actions, gbc_actions);

		GridBagConstraints gbc_summary = new GridBagConstraints();
		gbc_summary.insets = new Insets(0, 0, 5, 5);
		gbc_summary.weighty = 100.0;
		gbc_summary.weightx = 100.0;
		gbc_summary.fill = GridBagConstraints.BOTH;
		gbc_summary.gridx = 1;
		gbc_summary.gridy = 0;
		main.add(summary, gbc_summary);

		mainPanel = new JPanel();

		scrollingContainer = new JScrollPane(mainPanel);
		scrollingContainer.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollingContainer.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		scrollingContainer.setMinimumSize(new Dimension(1360 - 32, 1024 - 32));
		scrollingContainer.setPreferredSize(new Dimension(1360 - 32, 1024 - 32));
		scrollingContainer.setMaximumSize(new Dimension(width - 32, height - 32));
		
		mainPanel.setMinimumSize(scrollingContainer.getMinimumSize());
		mainPanel.setPreferredSize(scrollingContainer.getPreferredSize());
		mainPanel.setMaximumSize(scrollingContainer.getMaximumSize());
		
		GridBagConstraints gbc_mainPanel = new GridBagConstraints();
		gbc_mainPanel.anchor = GridBagConstraints.NORTH;
		gbc_mainPanel.weighty = 90.0;
		gbc_mainPanel.weightx = 100.0;
		gbc_mainPanel.fill = GridBagConstraints.BOTH;
		gbc_mainPanel.gridx = 0;
		gbc_mainPanel.gridy = 0;
		
		layout = new CardLayout(0, 0);
		
		mainPanel.setLayout(layout);
	
		applicationFrame.getContentPane().add(scrollingContainer, gbc_mainPanel);
		
		mainPanel.add(main, "main");
		mainPanel.add(vessels, "vessels");
		mainPanel.add(logbooks, "logbooks");
		mainPanel.add(logsheet, "logsheet");
		
		GridBagConstraints gbc_logPanel = new GridBagConstraints();
		gbc_logPanel.anchor = GridBagConstraints.SOUTH;
		gbc_logPanel.weightx = 100.0;
		gbc_logPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_logPanel.gridx = 0;
		gbc_logPanel.gridy = 1;
		applicationFrame.getContentPane().add(log, gbc_logPanel);
		
		GridBagConstraints gbc_statusPanel = new GridBagConstraints();
		gbc_statusPanel.anchor = GridBagConstraints.SOUTH;
		gbc_statusPanel.weightx = 100.0;
		gbc_statusPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_statusPanel.gridx = 0;
		gbc_statusPanel.gridy = 2;
		applicationFrame.getContentPane().add(status, gbc_statusPanel);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		applicationFrame.setJMenuBar(menuBar);
		
		appMenu = new JMenu("Application");
		menuBar.add(appMenu);
		
		JMenu preferencesMenu = new JMenu("Preferences");
		appMenu.add(preferencesMenu);
		
		JRadioButtonMenuItem toggleOfflineMode = new JRadioButtonMenuItem("Offline mode");
		preferencesMenu.add(toggleOfflineMode);
		toggleOfflineMode.setAction(new ToggleOfflineModeAction());
		toggleOfflineMode.setSelected(prefs.isOffline());
		
		JRadioButtonMenuItem toggleDummyMode = new JRadioButtonMenuItem("Dummy mode");
		preferencesMenu.add(toggleDummyMode);
		toggleDummyMode.setAction(new ToggleDummyModeAction());
		toggleDummyMode.setSelected(prefs.isDummyMode());
		
		JRadioButtonMenuItem toggleAutofillNewEvents = new JRadioButtonMenuItem("Autofill new events");
		preferencesMenu.add(toggleAutofillNewEvents);
		toggleAutofillNewEvents.setAction(new ToggleAutofillNewEventsAction());
		toggleAutofillNewEvents.setSelected(prefs.isAutoFillNewEvents());
		
		dataMenu = new JMenu("Data");
		menuBar.add(dataMenu);
		
		JMenu mnLogbook = new JMenu("Current logbook");
		dataMenu.add(mnLogbook);
		
		exportLogbook = new JMenuItem("Export logbook");
		mnLogbook.add(exportLogbook);
		exportLogbook.setAction(new ExportLogbookAction());
		exportLogbook.setEnabled(false);
		
		exportLogbookAsXML = new JMenuItem("Export logbook as XML");
		exportLogbookAsXML.setEnabled(false);
		exportLogbookAsXML.setAction(new ExportLogbookAsXMLAction());
		mnLogbook.add(exportLogbookAsXML);
		
		exportLogbookAsHTML = new JMenuItem("Export logbook as HTML");
		exportLogbookAsHTML.setEnabled(false);
		exportLogbookAsHTML.setAction(new ExportLogbookAsHTMLAction());
		mnLogbook.add(exportLogbookAsHTML);
		
		exportLogbookAsPDF = new JMenuItem("Export logbook as PDF");
		exportLogbookAsPDF.setEnabled(false);
		exportLogbookAsPDF.setAction(new ExportLogbookAsPDFAction());
		mnLogbook.add(exportLogbookAsPDF);
		
		exportLogbookAsFLUX = new JMenuItem("Export logbook as FLUX");
		exportLogbookAsFLUX.setEnabled(false);
		mnLogbook.add(exportLogbookAsFLUX);
		
		JMenu mnLogsheet = new JMenu("Current logsheet");
		dataMenu.add(mnLogsheet);
		
		exportLogsheet = new JMenuItem("Export logsheet");
		mnLogsheet.add(exportLogsheet);
		exportLogsheet.setAction(new ExportLogsheetAction());
		exportLogsheet.setEnabled(false);
		
		exportLogsheetAsXML = new JMenuItem("Export logsheet as XML");
		exportLogsheetAsXML.setEnabled(false);
		exportLogsheetAsXML.setAction(new ExportLogsheetAsXMLAction());
		mnLogsheet.add(exportLogsheetAsXML);
		
		exportLogsheetAsHTML = new JMenuItem("Export logsheet as HTML");
		exportLogsheetAsHTML.setEnabled(false);
		exportLogsheetAsHTML.setAction(new ExportLogsheetAsHTMLAction());
		mnLogsheet.add(exportLogsheetAsHTML);
		
		exportLogsheetAsPDF = new JMenuItem("Export logsheet as PDF");
		exportLogsheetAsPDF.setEnabled(false);
		exportLogsheetAsPDF.setAction(new ExportLogsheetAsPDFAction());
		mnLogsheet.add(exportLogsheetAsPDF);
		
		exportLogsheetAsFLUX = new JMenuItem("Export logsheet as FLUX");
		exportLogsheetAsFLUX.setEnabled(false);
		mnLogsheet.add(exportLogsheetAsFLUX);
		
//		JMenuItem importLogbook = new JMenuItem("Import logbook");
//		logbooksMenu.add(importLogbook);
//		
//		importLogbook.setAction(new ImportLogbookAction());
		
		windowMenu = new JMenu("Window");
		menuBar.add(windowMenu);
			
		JMenu logMenu = new JMenu("Log");
		windowMenu.add(logMenu);
		
		JRadioButtonMenuItem showLog = new JRadioButtonMenuItem("Show");
		logMenu.add(showLog);
		showLog.setAction(new ToggleLogAction());
		showLog.setSelected(prefs.isShowLog());
		
		JMenuItem clearLog = new JMenuItem("Clear");
		logMenu.add(clearLog);
		clearLog.setAction(new ClearLogAction());
		
		JMenu logLevel = new JMenu("Level");
		logMenu.add(logLevel);
		
		ButtonGroup levelChoices = new ButtonGroup();
		
		for(String level : new String[] { "TRACE", "DEBUG", "INFO", "WARNING", "ERROR" }) {
			JRadioButtonMenuItem forLevel = new JRadioButtonMenuItem(level);
			forLevel.setAction(new ChangeLogLevelAction(forLevel.getText()));
			forLevel.setSelected(level.equals(log.levelAsString()));
			levelChoices.add(forLevel);
			logLevel.add(forLevel);
		}
		
		JMenuItem menuItemAbout = new JMenuItem("About");
		windowMenu.add(menuItemAbout);
		
		JMenuItem quit = new JMenuItem("Quit");
		quit.setAction(new QuitAction());
		windowMenu.add(quit);
		
		updateLayout();
		
		model.registerListener((DataModelStatusListener)this); 
		model.registerListener((LogbookListener)this);
		model.registerListener((LogsheetListener)this);
		prefs.registerListener(this);
		
		updateSummary();
		updateTitle();
	}

	public void show(String panel) {
		layout.show(mainPanel, panel);
		scrollingContainer.getVerticalScrollBar().setValue(0);
	}
	
	private class ToggleOfflineModeAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 9130293025565216426L;
		
		public ToggleOfflineModeAction() {
			putValue(NAME, "Offline mode");
			putValue(SHORT_DESCRIPTION, "Toggle offline mode");
		}
		
		protected void doActionPerformed(ActionEvent e) {
			prefs.setOffline(!prefs.isOffline());

			log.info("Offline mode has been {}", prefs.isOffline() ? "enabled" : "disabled");
		}
	}
	
	private class ToggleDummyModeAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 9130293025565216426L;
		
		public ToggleDummyModeAction() {
			putValue(NAME, "Dummy mode");
			putValue(SHORT_DESCRIPTION, "Toggle dummy mode");
		}
		
		protected void doActionPerformed(ActionEvent e) {
			prefs.setDummyMode(!prefs.isDummyMode());

			log.info("Dummy mode has been {}", prefs.isDummyMode() ? "enabled" : "disabled");
		}
	}
	
	private class ToggleAutofillNewEventsAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 9130293025565216426L;
		
		public ToggleAutofillNewEventsAction() {
			putValue(NAME, "Autofill new events");
			putValue(SHORT_DESCRIPTION, "Toggle autofill when adding a new event");
		}
		
		protected void doActionPerformed(ActionEvent e) {
			prefs.setAutoFillNewEvents(!prefs.isAutoFillNewEvents());

			log.info("Autofill for new event has been {}", prefs.isDummyMode() ? "enabled" : "disabled");
		}
	}
	
	private class ToggleLogAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 9130293025565216426L;
		
		public ToggleLogAction() {
			putValue(NAME, "Show");
			putValue(SHORT_DESCRIPTION, "Toggle log visibility");
		}
		
		protected void doActionPerformed(ActionEvent e) {
			prefs.setShowLog(!prefs.isShowLog());

			log.setVisible(prefs.isShowLog());
			status.setVisible(prefs.isShowLog());
			
			updateLayout();
		}
	}
	
	private void updateLayout() {
		if(log.isVisible())
			mainLayout.rowHeights = new int[] {0, 128};
		else
			mainLayout.rowHeights = new int[0];
	}
	
	private void doExportLogbook(String type) throws Exception {
		Logbook selectedLogbook = model.getSelectedLogbook();
		
		File directory = UIUtils.selectDirectory(mainPanel, "Select the destination directory", "./logbooks", null, "The logbook has not been exported");
		
		if(directory != null) {
			boolean proceed = !logbookServices.exportedLogbookExists(directory, selectedLogbook, type);

			if(!proceed) proceed = UIUtils.confirm("Please confirm", "Another export for the same logbook has been found in the target folder and it will be overwritten. Are you sure you want to proceed?");
			
			if(proceed) {
				File exported = logbookServices.export(directory, selectedLogbook, type);
				
				model.select(selectedLogbook);
				
				UIUtils.dialog(applicationFrame, "Process completed", "The selected logbook has been exported to " + exported.getAbsolutePath());
			} else 
				UIUtils.dialog(applicationFrame, "Operation canceled", "The selected logbook has not been exported");
		}
	}
	
	private class ExportLogbookAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ExportLogbookAction() {
			putValue(NAME, "Export logbook");
			putValue(SHORT_DESCRIPTION, "Export currently selected logbook");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			doExportLogbook(null);
		}
	}
	
	private class ExportLogbookAsXMLAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ExportLogbookAsXMLAction() {
			putValue(NAME, "Export logbook as XML");
			putValue(SHORT_DESCRIPTION, "Export currently selected logbook as XML");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			doExportLogbook("xml");
		}
	}
	
	private class ExportLogbookAsHTMLAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ExportLogbookAsHTMLAction() {
			putValue(NAME, "Export logbook as HTML");
			putValue(SHORT_DESCRIPTION, "Export currently selected logbook as HTML");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			doExportLogbook("html");
		}
	}
	
	private class ExportLogbookAsPDFAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ExportLogbookAsPDFAction() {
			putValue(NAME, "Export logbook as PDF");
			putValue(SHORT_DESCRIPTION, "Export currently selected logbook as PDF");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			doExportLogbook("pdf");
		}
	}
	
	private void doExportLogsheet(String type) throws Exception {
		Logbook selectedLogbook = model.getSelectedLogbook();
		Logsheet selectedLogsheet = model.getSelectedLogsheet();
		
		File directory = UIUtils.selectDirectory(mainPanel, "Select the destination directory", "./logsheets", null, "The logsheet has not been exported");
		
		if(directory != null) {
			boolean proceed = !logsheetServices.exportedLogsheetExists(directory, selectedLogbook, selectedLogsheet, type);

			if(!proceed) proceed = UIUtils.confirm("Please confirm", "Another export for the same logsheet has been found in the target folder and it will be overwritten. Are you sure you want to proceed?");
			
			if(proceed) {
				File exported = logsheetServices.export(directory, selectedLogbook, selectedLogsheet, type);
				
				model.select(selectedLogbook);
				model.select(selectedLogsheet);
				
				UIUtils.dialog(applicationFrame, "Process completed", "The selected logsheet has been exported to " + exported.getAbsolutePath());
			} else 
				UIUtils.dialog(applicationFrame, "Operation canceled", "The selected logsheet has not been exported");
		}
	}
	
	private class ExportLogsheetAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ExportLogsheetAction() {
			putValue(NAME, "Export logsheet");
			putValue(SHORT_DESCRIPTION, "Export currently selected logsheet");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			doExportLogsheet(null);
		}
	}
	
	private class ExportLogsheetAsXMLAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ExportLogsheetAsXMLAction() {
			putValue(NAME, "Export logsheet as XML");
			putValue(SHORT_DESCRIPTION, "Export currently selected logsheet");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			doExportLogsheet("xml");
		}
	}
	
	private class ExportLogsheetAsHTMLAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ExportLogsheetAsHTMLAction() {
			putValue(NAME, "Export logsheet as HTML");
			putValue(SHORT_DESCRIPTION, "Export currently selected logsheet");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			doExportLogsheet("html");
		}
	}
	
	private class ExportLogsheetAsPDFAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ExportLogsheetAsPDFAction() {
			putValue(NAME, "Export logsheet as PDF");
			putValue(SHORT_DESCRIPTION, "Export currently selected logsheet");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			doExportLogsheet("pdf");
		}
	}
	
	@SuppressWarnings("unused")
	private class ImportLogbookAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ImportLogbookAction() {
			putValue(NAME, "Import logbook");
			putValue(SHORT_DESCRIPTION, "Import an existing logbook");
		}
		
		protected void doActionPerformed(ActionEvent e) throws Throwable {
			FileFilter filter = new FileFilter() {
				@Override
				public boolean accept(File f) {
					return f != null && f.isFile() && f.getName().endsWith(".logbook");
				}
				
				@Override
				public String getDescription() {
					return "Logbook";
				}
			};
			
			File selected = UIUtils.selectFile(mainPanel, "Select an exported logbook file", "logbooks", filter, "The operation has been canceled");
			
			if(selected != null) {
				logbookServices.importLogbook(selected);
				
				UIUtils.dialog(applicationFrame, "Operation completed", "The selected logbook has been successfully imported");
			}
		}
	}
	
	private class ClearLogAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -5595672488013817725L;

		public ClearLogAction() {
			putValue(NAME, "Clear");
			putValue(SHORT_DESCRIPTION, "Clear the log content");
		}
		
		protected void doActionPerformed(ActionEvent e) {
			log.clear();
			log.info("The log has been cleared");
		}
	}
	
	private class ChangeLogLevelAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 3401283923135018273L;
		
		private String level;
		
		public ChangeLogLevelAction(String level) {
			this.level = level;
			
			putValue(NAME, level);
			putValue(SHORT_DESCRIPTION, "Change log level to " + level);
		}
		
		protected void doActionPerformed(ActionEvent e) {
			log.changeLevel(level);
		}
	}
	private class QuitAction extends InterceptingAction {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -2368441393329093910L;
		
		public QuitAction() {
			putValue(NAME, "Quit");
			putValue(SHORT_DESCRIPTION, "Exit from the application");
		}
		protected void doActionPerformed(ActionEvent e) {
			applicationFrame.dispatchEvent(new WindowEvent(applicationFrame, WindowEvent.WINDOW_CLOSING));
		}
	}
	
	public boolean confirmCancelUpdates(boolean tainted) {
		return confirmCancelUpdates(tainted, "Some updates have not been committed", "If you confirm, pending updates will be lost. Are you sure you want to continue?");
	}
	
	public boolean confirmCancelUpdates(boolean tainted, String title, String message) {
		return !tainted || UIUtils.confirm(title, message);
	}
	
	private void updateTitle() {
		applicationFrame.setTitle("FAO logbook manager [ UI version: 1.0.0 - model version: " + DataModel.MODEL_VERSION + " ] " + 
								 " [ " + ( prefs.isOffline() ? "OFFLINE" : "ONLINE" ) + " ] " +
								 ( model.isDataModelTainted() ? "*" : "" ));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.DataModelStatusListener#modelStatusChanged(boolean)
	 */
	@Override
	public void modelStatusChanged(boolean tainted) {
		updateTitle();
//		if(tainted) {
//			if(!applicationFrame.getTitle().endsWith("*")) applicationFrame.setTitle(applicationFrame.getTitle() + "*");
//		} else {
//			applicationFrame.setTitle(applicationFrame.getTitle().replace("*", ""));
//		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.PreferenceListener#preferencesChanged(java.lang.String, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void preferencesChanged(String property, Object oldValue, Object newValue) {
		updateTitle();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookSelected(Logbook selected) {
		updateApplicationMenus();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookAdded(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookAdded(Logbook added) {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookRemoved(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookRemoved(Logbook removed) {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookUpdated(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookUpdated(Logbook updated) {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener#logbookStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logbookStatusChanged(EditingStatus previous, EditingStatus current) {
		updateApplicationMenus();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetAdded(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetAdded(Logsheet added) {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetRemoved(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetRemoved(Logsheet removed) {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetUpdated(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetUpdated(Logsheet updated) {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetStatusChanged(EditingStatus previous, EditingStatus current) {
		updateApplicationMenus();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener#logsheetSelected(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetSelected(Logsheet selected) {
		updateApplicationMenus();
	}
	
	public void updateSummary() {
		int logbooks = 0, completedLogbooks = 0, logsheets = 0, completedLogsheets = 0;

		if(model.getLogbooks() != null) {
			summary.getLogbooks().setText(String.valueOf(logbooks = model.getLogbooks().size()));
			
			if(model.getLogbooks() != null) {
				for(Logbook in : model.getLogbooks()) {
					if(in.isCompleted()) completedLogbooks++;
					
					if(in.getLogsheets() != null) {
						for(Logsheet of : in.getLogsheets()) {
							logsheets++;
							
							if(of.isCompleted()) completedLogsheets++;
						}
					}
				}
			}
		}

		summary.getCompletedLogbooks().setText(String.valueOf(completedLogbooks));
		summary.getLogsheets().setText(String.valueOf(logsheets));
		summary.getCompletedLogsheets().setText(String.valueOf(completedLogsheets));
		
		if(logbooks > 0) summary.getLogbookCompletion().setValue((int)Math.round(completedLogbooks * 100.0 / logbooks));
		if(logsheets > 0) summary.getLogsheetCompletion().setValue((int)Math.round(completedLogsheets * 100.0 / logsheets));
	}
	
	private void updateApplicationMenus() {
		Logsheet selectedLogsheet = model.getSelectedLogsheet();
		Logbook selectedLogbook = model.getSelectedLogbook();
		
		exportLogbook.setEnabled(selectedLogbook != null && selectedLogbook.isCompleted());
		exportLogbookAsXML.setEnabled(selectedLogbook != null && selectedLogbook.isCompleted());
		exportLogbookAsHTML.setEnabled(selectedLogbook != null && selectedLogbook.isCompleted());
		exportLogbookAsPDF.setEnabled(selectedLogbook != null && selectedLogbook.isCompleted());
		exportLogsheet.setEnabled(selectedLogsheet != null && selectedLogsheet.isCompleted());
		exportLogsheetAsXML.setEnabled(selectedLogsheet != null && selectedLogsheet.isCompleted());
		exportLogsheetAsHTML.setEnabled(selectedLogsheet != null && selectedLogsheet.isCompleted());
		exportLogsheetAsPDF.setEnabled(selectedLogsheet != null && selectedLogsheet.isCompleted());
	}
	
	public void showGlassPane(String message) {
		GlassPane glassPane = getGlassPane();
		glassPane.getText().setText(message);
		glassPane.setVisible(true);
	}
	
	public void hideGlassPane() {
		getGlassPane().setVisible(false);
	}
}
