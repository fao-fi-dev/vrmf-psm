/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels;

import java.awt.FlowLayout;
import java.awt.Font;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.fao.fi.vrmf.psm.logbook.ui.Preferences;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 11, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 11, 2015
 */
@Named @Singleton
public class StatusPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2626256388716021342L;
	
	@Inject private Preferences prefs;

	@Getter private JLabel vesselStatus;
	@Getter private JLabel logbookStatus;
	@Getter private JLabel logsheetStatus;
	@Getter private JLabel eventStatus;
	@Getter private JLabel effortStatus;
	@Getter private JLabel catchStatus;
	@Getter private JLabel discardStatus;
	@Getter private JLabel vessel;
	@Getter private JLabel logbook;
	@Getter private JLabel logsheet;
	@Getter private JLabel event;
	@Getter private JLabel effort;
	@Getter private JLabel catches;
	@Getter private JLabel discards;

	/**
	 * Class constructor
	 *
	 */
	public StatusPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	private void initialize() {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		JLabel lblStatus = new JLabel("Data status >> ");
		lblStatus.setFont(new Font("Tahoma", Font.BOLD, 11));
		add(lblStatus);
		
		vessel = new JLabel("Vessel:");
		add(vessel);
		
		vesselStatus = new JLabel("[ " + EditingStatus.NOT_SET.name() + " ]");
		vesselStatus.setFont(new Font("Tahoma", Font.PLAIN, 11));
		add(vesselStatus);
		
		logbook = new JLabel("Logbook:");
		add(logbook);
		
		logbookStatus = new JLabel("[ " + EditingStatus.NOT_SET.name() + " ]");
		logbookStatus.setFont(new Font("Tahoma", Font.PLAIN, 11));
		add(logbookStatus);
		
		logsheet = new JLabel("Logsheet:");
		add(logsheet);
		
		logsheetStatus = new JLabel("[ " + EditingStatus.NOT_SET.name() + " ]");
		logsheetStatus.setFont(new Font("Tahoma", Font.PLAIN, 11));
		add(logsheetStatus);
		
		event = new JLabel("Event:");
		add(event);
		
		eventStatus = new JLabel("[ " + EditingStatus.NOT_SET.name() + " ]");
		eventStatus.setFont(new Font("Tahoma", Font.PLAIN, 11));
		add(eventStatus);
		
		effort = new JLabel("Effort:");
		add(effort);
		
		effortStatus = new JLabel("[ " + EditingStatus.NOT_SET.name() + " ]");
		effortStatus.setFont(new Font("Tahoma", Font.PLAIN, 11));
		add(effortStatus);
		
		catches = new JLabel("Catch:");
		add(catches);
		
		catchStatus = new JLabel("[ " + EditingStatus.NOT_SET.name() + " ]");
		catchStatus.setFont(new Font("Tahoma", Font.PLAIN, 11));
		add(catchStatus);
		
		discards = new JLabel("Discard:");
		add(discards);
		
		discardStatus = new JLabel("[ " + EditingStatus.NOT_SET.name() + " ]");
		discardStatus.setFont(new Font("Tahoma", Font.PLAIN, 11));
		add(discardStatus);
		
		setVisible(prefs.isShowLog());
	}
}
