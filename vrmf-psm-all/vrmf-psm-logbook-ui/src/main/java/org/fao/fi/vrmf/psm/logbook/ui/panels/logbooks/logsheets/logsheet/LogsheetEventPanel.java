/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.transshipments.TransshipmentsPanel;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JCoordinatesInput;
import org.fao.fi.vrmf.psm.ui.common.support.components.JDateTimePicker;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;

import lombok.Getter;
import javax.swing.JComboBox;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 4, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 4, 2015
 */
@Named @Singleton
public class LogsheetEventPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7126211449086206806L;
	
	@Getter private JLabel type;
	@Getter private JDateTimePicker startTime;
	@Getter private JButton cleanStartDate;
	@Getter private JNoFocusOutlineButton startTimeNow;
	@Getter private JTextField startCoordinates;
	@Getter private JButton cleanStartCoordinates;
	@Getter private JNoFocusOutlineButton editStartCoordinates;
	@Getter private JDateTimePicker endTime;
	@Getter private JButton cleanEndDate;
	@Getter private JNoFocusOutlineButton endTimeNow;
	@Getter private JTextField endCoordinates;
	@Getter private JButton cleanEndCoordinates;
	@Getter private JNoFocusOutlineButton editEndCoordinates;
	@Getter private JTextField comments;
	@Getter private JTextField vesselName;
	@Getter private JComboBox vesselRegistrationCountry;
	@Getter private JTextField vesselRegistrationNumber;
	@Getter private JTextField vesselDestination;
	
	@Getter private JLabel lblReceivingVessel;
	@Getter private JLabel lblVesselName;
	@Getter private JLabel lblVesselNrn;
	@Getter private JLabel lblVesselDestination;
	@Getter private JLabel lblVesselRegCountry;
	
	private Component padder;
	
	@Getter @Inject private LogsheetEventActionsPanel logsheetEventActions;
	@Getter @Inject private CatchAndEffortPanel catchAndEfforts;
	@Getter @Inject private TransshipmentsPanel transshipments;

	
	/**
	 * Class constructor
	 *
	 */
	public LogsheetEventPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		logsheetEventActions = new LogsheetEventActionsPanel();
		catchAndEfforts = new CatchAndEffortPanel();
		transshipments = new TransshipmentsPanel();
		
		initialize();
	}

	@PostConstruct
	public void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		GridBagLayout gbl_container = new GridBagLayout();
		gbl_container.columnWidths = new int[0];
		gbl_container.rowHeights = new int[0];
		gbl_container.columnWeights = new double[0];
		gbl_container.rowWeights = new double[0];

		JPanel container = new JPanel();
		container.setLayout(gbl_container);
		
		GridBagConstraints gbc_container = new GridBagConstraints();
		gbc_container.insets = new Insets(0, 0, 5, 0);
		gbc_container.weightx = 100;
		gbc_container.anchor = GridBagConstraints.NORTH;
		gbc_container.fill = GridBagConstraints.HORIZONTAL;
		gbc_container.gridx = 0;
		gbc_container.gridy = 0;
		add(container, gbc_container);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.weightx = 2.0;
		gbc_lblType.insets = new Insets(5, 5, 5, 5);
		gbc_lblType.anchor = GridBagConstraints.WEST;
		gbc_lblType.fill = GridBagConstraints.BOTH;
		gbc_lblType.gridx = 0;
		gbc_lblType.gridy = 0;
		container.add(lblType, gbc_lblType);
		
		type = new JLabel("EVENT_TYPE");
		type.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		GridBagConstraints gbc__type = new GridBagConstraints();
		gbc__type.weightx = 10.0;
		gbc__type.insets = new Insets(5, 5, 5, 5);
		gbc__type.fill = GridBagConstraints.BOTH;
		gbc__type.anchor = GridBagConstraints.WEST;
		gbc__type.gridx = 1;
		gbc__type.gridy = 0;
		container.add(type, gbc__type);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut.weightx = 2.0;
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut.gridx = 3;
		gbc_horizontalStrut.gridy = 0;
		container.add(horizontalStrut, gbc_horizontalStrut);
		
		JLabel lblStartDate = new JLabel("Start date");
		lblStartDate.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblStartDate = new GridBagConstraints();
		gbc_lblStartDate.weightx = 2.0;
		gbc_lblStartDate.anchor = GridBagConstraints.WEST;
		gbc_lblStartDate.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblStartDate.insets = new Insets(5, 5, 5, 5);
		gbc_lblStartDate.gridx = 0;
		gbc_lblStartDate.gridy = 1;
		container.add(lblStartDate, gbc_lblStartDate);
		
		startTime = new JDateTimePicker();
		startTime.setFormats(new String[] {"yyyy/MM/dd HH:mm:ss"});
		startTime.setTimeFormat(new SimpleDateFormat("HH:mm:ss"));

		GridBagConstraints gbc__startTime = new GridBagConstraints();
		gbc__startTime.ipady = 5;
		gbc__startTime.anchor = GridBagConstraints.WEST;
		gbc__startTime.fill = GridBagConstraints.HORIZONTAL;
		gbc__startTime.insets = new Insets(5, 5, 5, 5);
		gbc__startTime.gridx = 1;
		gbc__startTime.gridy = 1;
		container.add(startTime, gbc__startTime);
		
		cleanStartDate = new JButton("X");
		cleanStartDate.setEnabled(false);
		GridBagConstraints gbc_cleanStartDate = new GridBagConstraints();
		gbc_cleanStartDate.insets = new Insets(5, 0, 5, 5);
		gbc_cleanStartDate.gridx = 2;
		gbc_cleanStartDate.gridy = 1;
		container.add(cleanStartDate, gbc_cleanStartDate);
		
		startTimeNow = new JNoFocusOutlineButton("Now");
		GridBagConstraints gbc_startTimeNow = new GridBagConstraints();
		gbc_startTimeNow.fill = GridBagConstraints.HORIZONTAL;
		gbc_startTimeNow.anchor = GridBagConstraints.WEST;
		gbc_startTimeNow.weightx = 1.0;
		gbc_startTimeNow.insets = new Insets(5, 5, 5, 0);
		gbc_startTimeNow.gridx = 3;
		gbc_startTimeNow.gridy = 1;
		container.add(startTimeNow, gbc_startTimeNow);
		
		JLabel lblStartCoordinates = new JLabel("Coordinates");
		lblStartCoordinates.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblStartCoordinates = new GridBagConstraints();
		gbc_lblStartCoordinates.weightx = 2.0;
		gbc_lblStartCoordinates.fill = GridBagConstraints.BOTH;
		gbc_lblStartCoordinates.insets = new Insets(5, 5, 5, 5);
		gbc_lblStartCoordinates.anchor = GridBagConstraints.WEST;
		gbc_lblStartCoordinates.gridx = 0;
		gbc_lblStartCoordinates.gridy = 2;
		container.add(lblStartCoordinates, gbc_lblStartCoordinates);
		
		startCoordinates = new JCoordinatesInput();
		startCoordinates.setColumns(20);
		GridBagConstraints gbc_startCoordinates = new GridBagConstraints();
		gbc_startCoordinates.ipady = 5;
		gbc_startCoordinates.anchor = GridBagConstraints.WEST;
		gbc_startCoordinates.insets = new Insets(5, 5, 5, 5);
		gbc_startCoordinates.fill = GridBagConstraints.BOTH;
		gbc_startCoordinates.gridx = 1;
		gbc_startCoordinates.gridy = 2;
		container.add(startCoordinates, gbc_startCoordinates);
		
		cleanStartCoordinates = new JButton("X");
		cleanStartCoordinates.setEnabled(false);
		GridBagConstraints gbc_cleanStartCoordinates = new GridBagConstraints();
		gbc_cleanStartCoordinates.insets = new Insets(5, 0, 5, 5);
		gbc_cleanStartCoordinates.gridx = 2;
		gbc_cleanStartCoordinates.gridy = 2;
		container.add(cleanStartCoordinates, gbc_cleanStartCoordinates);
		
		JLabel lblEndDate = new JLabel("End date");
		lblEndDate.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblEndDate = new GridBagConstraints();
		gbc_lblEndDate.weightx = 2.0;
		gbc_lblEndDate.anchor = GridBagConstraints.WEST;
		gbc_lblEndDate.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEndDate.insets = new Insets(5, 5, 5, 5);
		gbc_lblEndDate.gridx = 0;
		gbc_lblEndDate.gridy = 3;
		container.add(lblEndDate, gbc_lblEndDate);
		
		endTime = new JDateTimePicker();
		endTime.setFormats(new String[] {"yyyy/MM/dd HH:mm:ss"});
		endTime.setTimeFormat(new SimpleDateFormat("HH:mm:ss"));

		GridBagConstraints gbc__endTime = new GridBagConstraints();
		gbc__endTime.ipady = 5;
		gbc__endTime.anchor = GridBagConstraints.WEST;
		gbc__endTime.fill = GridBagConstraints.BOTH;
		gbc__endTime.insets = new Insets(5, 5, 5, 5);
		gbc__endTime.gridx = 1;
		gbc__endTime.gridy = 3;
		container.add(endTime, gbc__endTime);
		
		cleanEndDate = new JButton("X");
		cleanEndDate.setEnabled(false);
		GridBagConstraints gbc_cleanEndDate = new GridBagConstraints();
		gbc_cleanEndDate.insets = new Insets(5, 0, 5, 5);
		gbc_cleanEndDate.gridx = 2;
		gbc_cleanEndDate.gridy = 3;
		container.add(cleanEndDate, gbc_cleanEndDate);
		
		endTimeNow = new JNoFocusOutlineButton("Now");
		GridBagConstraints gbc_endTimeNow = new GridBagConstraints();
		gbc_endTimeNow.fill = GridBagConstraints.HORIZONTAL;
		gbc_endTimeNow.anchor = GridBagConstraints.WEST;
		gbc_endTimeNow.weightx = 1.0;
		gbc_endTimeNow.insets = new Insets(5, 5, 5, 0);
		gbc_endTimeNow.gridx = 3;
		gbc_endTimeNow.gridy = 3;
		container.add(endTimeNow, gbc_endTimeNow);
		
		JLabel lblEndCoordinates = new JLabel("Coordinates");
		lblEndCoordinates.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblEndCoordinates = new GridBagConstraints();
		gbc_lblEndCoordinates.weightx = 2.0;
		gbc_lblEndCoordinates.fill = GridBagConstraints.BOTH;
		gbc_lblEndCoordinates.anchor = GridBagConstraints.WEST;
		gbc_lblEndCoordinates.insets = new Insets(5, 5, 5, 5);
		gbc_lblEndCoordinates.gridx = 0;
		gbc_lblEndCoordinates.gridy = 4;
		container.add(lblEndCoordinates, gbc_lblEndCoordinates);
		
		endCoordinates = new JCoordinatesInput();
		endCoordinates.setColumns(20);
		GridBagConstraints gbc_endCoordinates = new GridBagConstraints();
		gbc_endCoordinates.ipady = 5;
		gbc_endCoordinates.anchor = GridBagConstraints.WEST;
		gbc_endCoordinates.insets = new Insets(5, 5, 5, 5);
		gbc_endCoordinates.fill = GridBagConstraints.BOTH;
		gbc_endCoordinates.gridx = 1;
		gbc_endCoordinates.gridy = 4;
		container.add(endCoordinates, gbc_endCoordinates);
		
		editStartCoordinates = new JNoFocusOutlineButton("Edit");
		editStartCoordinates.setEnabled(false);
		GridBagConstraints gbc__showStartCoordinates = new GridBagConstraints();
		gbc__showStartCoordinates.fill = GridBagConstraints.HORIZONTAL;
		gbc__showStartCoordinates.weightx = 1.0;
		gbc__showStartCoordinates.anchor = GridBagConstraints.WEST;
		gbc__showStartCoordinates.insets = new Insets(5, 5, 5, 0);
		gbc__showStartCoordinates.gridx = 3;
		gbc__showStartCoordinates.gridy = 2;
		container.add(editStartCoordinates, gbc__showStartCoordinates);
		
		cleanEndCoordinates = new JButton("X");
		cleanEndCoordinates.setEnabled(false);
		GridBagConstraints gbc_cleanEndCoordinates = new GridBagConstraints();
		gbc_cleanEndCoordinates.insets = new Insets(5, 0, 5, 5);
		gbc_cleanEndCoordinates.gridx = 2;
		gbc_cleanEndCoordinates.gridy = 4;
		container.add(cleanEndCoordinates, gbc_cleanEndCoordinates);
		
		editEndCoordinates = new JNoFocusOutlineButton("Edit");
		editEndCoordinates.setEnabled(false);
		GridBagConstraints gbc__showEndCoordinates = new GridBagConstraints();
		gbc__showEndCoordinates.fill = GridBagConstraints.HORIZONTAL;
		gbc__showEndCoordinates.weightx = 1.0;
		gbc__showEndCoordinates.anchor = GridBagConstraints.WEST;
		gbc__showEndCoordinates.insets = new Insets(5, 5, 5, 0);
		gbc__showEndCoordinates.gridx = 3;
		gbc__showEndCoordinates.gridy = 4;
		container.add(editEndCoordinates, gbc__showEndCoordinates);
		
		JLabel commentsLabel = new JLabel("Comments");
		GridBagConstraints gbc_commentsLabel = new GridBagConstraints();
		gbc_commentsLabel.weightx = 2.0;
		gbc_commentsLabel.anchor = GridBagConstraints.WEST;
		gbc_commentsLabel.fill = GridBagConstraints.BOTH;
		gbc_commentsLabel.insets = new Insets(5, 5, 5, 5);
		gbc_commentsLabel.gridx = 0;
		gbc_commentsLabel.gridy = 5;
		container.add(commentsLabel, gbc_commentsLabel);
		
		comments = new JTextField();
		comments.setEnabled(false);
		GridBagConstraints gbc__comments = new GridBagConstraints();
		gbc__comments.ipady = 5;
		gbc__comments.anchor = GridBagConstraints.WEST;
		gbc__comments.fill = GridBagConstraints.BOTH;
		gbc__comments.insets = new Insets(5, 5, 5, 5);
		gbc__comments.gridx = 1;
		gbc__comments.gridy = 5;
		container.add(comments, gbc__comments);
		
		Component horizontalStrut2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut2 = new GridBagConstraints();
		gbc_horizontalStrut2.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut2.gridwidth = 2;
		gbc_horizontalStrut2.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut2.gridx = 2;
		gbc_horizontalStrut2.gridy = 5;
		container.add(horizontalStrut2, gbc_horizontalStrut2);
		
		lblReceivingVessel = new JLabel("Receiving vessel");
		lblReceivingVessel.setFont(new Font("Tahoma", Font.BOLD, 13));
		GridBagConstraints gbc_lblReceivingVessel = new GridBagConstraints();
		gbc_lblReceivingVessel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblReceivingVessel.anchor = GridBagConstraints.WEST;
		gbc_lblReceivingVessel.insets = new Insets(5, 5, 5, 5);
		gbc_lblReceivingVessel.gridx = 0;
		gbc_lblReceivingVessel.gridy = 6;
		container.add(lblReceivingVessel, gbc_lblReceivingVessel);
		
		lblVesselName = new JLabel("Name");
		lblVesselName.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc__lblVesselName = new GridBagConstraints();
		gbc__lblVesselName.fill = GridBagConstraints.BOTH;
		gbc__lblVesselName.anchor = GridBagConstraints.WEST;
		gbc__lblVesselName.insets = new Insets(5, 5, 5, 5);
		gbc__lblVesselName.gridx = 0;
		gbc__lblVesselName.gridy = 7;
		container.add(lblVesselName, gbc__lblVesselName);
		
		vesselName = new JTextField("");
		GridBagConstraints gbc_vesselName = new GridBagConstraints();
		gbc_vesselName.ipady = 5;
		gbc_vesselName.fill = GridBagConstraints.HORIZONTAL;
		gbc_vesselName.insets = new Insets(5, 5, 5, 5);
		gbc_vesselName.gridx = 1;
		gbc_vesselName.gridy = 7;
		container.add(vesselName, gbc_vesselName);
		
		lblVesselRegCountry = new JLabel("Reg. country");
		lblVesselRegCountry.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.fill = GridBagConstraints.HORIZONTAL;
		gbc_label.insets = new Insets(5, 5, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 8;
		container.add(lblVesselRegCountry, gbc_label);
		
		vesselRegistrationCountry = new JComboBox();
		GridBagConstraints gbc_vesselRegistrationCountry = new GridBagConstraints();
		gbc_vesselRegistrationCountry.ipady = 5;
		gbc_vesselRegistrationCountry.fill = GridBagConstraints.HORIZONTAL;
		gbc_vesselRegistrationCountry.insets = new Insets(5, 5, 5, 5);
		gbc_vesselRegistrationCountry.gridx = 1;
		gbc_vesselRegistrationCountry.gridy = 8;
		container.add(vesselRegistrationCountry, gbc_vesselRegistrationCountry);
		
		lblVesselNrn = new JLabel("Reg. no.");
		lblVesselNrn.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc__lblVesselIRCS = new GridBagConstraints();
		gbc__lblVesselIRCS.fill = GridBagConstraints.BOTH;
		gbc__lblVesselIRCS.anchor = GridBagConstraints.WEST;
		gbc__lblVesselIRCS.insets = new Insets(5, 5, 5, 5);
		gbc__lblVesselIRCS.gridx = 0;
		gbc__lblVesselIRCS.gridy = 9;
		container.add(lblVesselNrn, gbc__lblVesselIRCS);
		
		vesselRegistrationNumber = new JTextField();
		GridBagConstraints gbc_vesselIRCS = new GridBagConstraints();
		gbc_vesselIRCS.ipady = 5;
		gbc_vesselIRCS.fill = GridBagConstraints.HORIZONTAL;
		gbc_vesselIRCS.insets = new Insets(5, 5, 5, 5);
		gbc_vesselIRCS.gridx = 1;
		gbc_vesselIRCS.gridy = 9;
		container.add(vesselRegistrationNumber, gbc_vesselIRCS);
		
		lblVesselDestination = new JLabel("Destination");
		lblVesselDestination.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc__lblVesselDestination = new GridBagConstraints();
		gbc__lblVesselDestination.fill = GridBagConstraints.BOTH;
		gbc__lblVesselDestination.anchor = GridBagConstraints.WEST;
		gbc__lblVesselDestination.insets = new Insets(5, 5, 5, 5);
		gbc__lblVesselDestination.gridx = 0;
		gbc__lblVesselDestination.gridy = 10;
		container.add(lblVesselDestination, gbc__lblVesselDestination);
		
		vesselDestination = new JTextField();
		GridBagConstraints gbc_vesselDestination = new GridBagConstraints();
		gbc_vesselDestination.ipady = 5;
		gbc_vesselDestination.fill = GridBagConstraints.HORIZONTAL;
		gbc_vesselDestination.insets = new Insets(5, 5, 5, 5);
		gbc_vesselDestination.gridx = 1;
		gbc_vesselDestination.gridy = 10;
		container.add(vesselDestination, gbc_vesselDestination);
		
		GridBagConstraints gbc_eventActions = new GridBagConstraints();
		gbc_eventActions.insets = new Insets(0, 0, 5, 0);
		gbc_eventActions.weightx = 100.0;
		gbc_eventActions.anchor = GridBagConstraints.NORTH;
		gbc_eventActions.fill = GridBagConstraints.HORIZONTAL;
		gbc_eventActions.gridx = 0;
		gbc_eventActions.gridy = 1;
		gbc_eventActions.gridwidth = 1;
		add(logsheetEventActions, gbc_eventActions);
		
		GridBagConstraints gbc_catchAndEfforts = new GridBagConstraints();
		gbc_catchAndEfforts.weighty = 100.0;
		gbc_catchAndEfforts.weightx = 100.0;
		gbc_catchAndEfforts.anchor = GridBagConstraints.SOUTH;
		gbc_catchAndEfforts.fill = GridBagConstraints.BOTH;
		gbc_catchAndEfforts.insets = new Insets(10, 0, 5, 0);
		gbc_catchAndEfforts.gridx = 0;
		gbc_catchAndEfforts.gridy = 2;
		gbc_catchAndEfforts.gridwidth = 1;
		add(catchAndEfforts, gbc_catchAndEfforts);
		
		GridBagConstraints gbc_transshipments = new GridBagConstraints();
		gbc_transshipments.weighty = 100.0;
		gbc_transshipments.weightx = 100.0;
		gbc_transshipments.anchor = GridBagConstraints.SOUTH;
		gbc_transshipments.fill = GridBagConstraints.BOTH;
		gbc_transshipments.insets = new Insets(10, 0, 5, 0);
		gbc_transshipments.gridx = 0;
		gbc_transshipments.gridy = 2;
		gbc_transshipments.gridwidth = 1;
		add(transshipments, gbc_transshipments);
		
		padder = Box.createVerticalStrut(20);
		GridBagConstraints gbc_padder = new GridBagConstraints();
		gbc_padder.weighty = 100.0;
		gbc_padder.fill = GridBagConstraints.VERTICAL;
		gbc_padder.gridx = 0;
		gbc_padder.gridy = 3;
		add(padder, gbc_padder);
		
		padder.setVisible(false);
		
		startTimeNow.addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				startTime.setDate(new Date());
			}
		});
		
		endTimeNow.addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				endTime.setDate(new Date());
			}
		});
	}
	
	public void notifyLogsheetStatusChange(EditingStatus status, Logsheet current) {
		boolean readOnly = status == EditingStatus.NOT_SET || status == EditingStatus.VIEWING;
		
		for(Component in : new Component[] {
			startTime, startTimeNow, startCoordinates, endTime, endTimeNow, endCoordinates, comments
		}) in.setEnabled(!readOnly);
	}
	
	public void notifyEventStatusChange(EditingStatus status, Event current) {
		notifyLogsheetStatusChange(status, null);
	}
	
	public void updateUI(Event event) {
		if(event == null) return;
		
		boolean isTranshipmentEvent = event.isTransshipmentEvent();
		
		for(Component in : new Component[] {
			lblReceivingVessel,
			lblVesselName,
			lblVesselRegCountry,
			lblVesselNrn,
			lblVesselDestination,
			vesselName,
			vesselRegistrationCountry,
			vesselRegistrationNumber,
			vesselDestination
		}) {
			in.setVisible(isTranshipmentEvent);
		}
	}
	
	public Component getPadder() {
		return padder;
	}
}
