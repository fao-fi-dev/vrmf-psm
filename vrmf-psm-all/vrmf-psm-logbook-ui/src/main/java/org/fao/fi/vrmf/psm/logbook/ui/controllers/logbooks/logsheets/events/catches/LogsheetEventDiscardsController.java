/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.logbooks.logsheets.events.catches;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResult;
import org.fao.fi.vrmf.psm.logbook.model.spi.CheckResultHelper;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.CatchAndEffortPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.DiscardsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet.catches.model.LogsheetEventDiscardTableModel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventDiscardListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetEventListener;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingListSelectionListener;

import no.tornado.databinding.BindingGroup;
import no.tornado.databinding.BindingStrategy;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 11, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 11, 2015
 */
@Named @Singleton
public class LogsheetEventDiscardsController extends BaseController {
	@Inject private CatchAndEffortPanel parentView;
	@Inject private DiscardsPanel view;
	
	private BindingGroup discardBindings;

	@PostConstruct
	private void initialize() {
		model.registerListener((LogsheetEventListener)this);
		model.registerListener((LogsheetEventDiscardListener)this);
		
		view.getRecordsTable().getSelectionModel().addListSelectionListener(new InterceptingListSelectionListener() {
			protected void doValueChanged(ListSelectionEvent event) {
				int selectedIndex = view.getRecordsTable().getSelectedRow();
				
				Discards selectedDiscards = null;
				
				List<Discards> discards = model.getSelectedEvent() != null && model.getSelectedEvent().isCatchEvent() && ((CatchesRecord)model.getSelectedEvent()).getDiscards() != null ? ((CatchesRecord)model.getSelectedEvent()).getDiscards() : null;
				
				if(discards != null && selectedIndex >= 0 && selectedIndex <= discards.size() - 1) {
					selectedDiscards = discards.get(view.getRecordsTable().convertRowIndexToModel(selectedIndex));
				}
				
				boolean same = selectedDiscards != null && selectedDiscards.equals(model.getSelectedDiscard());

				if(same || selectedDiscards == null) return;
				
				if(application.confirmCancelUpdates(model.isDiscardDirty() || model.getDiscardStatus() == EditingStatus.ADDING)) {
					if(model.isDiscardDirty() && model.getSelectedDiscard() != null && model.getSelectedDiscard().getId() != null) {
						doRevertDiscard(model.getSelectedDiscard());
					}
					
					model.select(selectedDiscards, model.getEventStatus());
				} else {
					int currentlySelectedDiscardIndex = discards.indexOf(model.getSelectedDiscard());
					
					currentlySelectedDiscardIndex = view.getRecordsTable().convertRowIndexToView(currentlySelectedDiscardIndex);
					
					view.getRecordsTable().setRowSelectionInterval(currentlySelectedDiscardIndex, currentlySelectedDiscardIndex);
					
					model.taintDiscard();
				}
			}
		});
		
		view.getAddNew().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				addDiscardAction();
			}
		});
		
		view.getUpdateRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				updateRecordAction();
			}
		});
		
		view.getDeleteRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				deleteRecordAction();
			}
		});
		
		view.getCancelRecord().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				cancelDiscardAction();
			}
		});
		
		UIUtils.addValueChangeListener(view, new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				if(model.getSelectedDiscard() != null) model.taintDiscard();
			}
		});
	}
	
	private void applyDummyBinding() {
		bindDiscard(new Discards());
		discardBindings.flushModelToUI();
	}
	
	private void bindDiscard(Discards selected) {
		unbindDiscard();
		
		if(selected != null) {
			discardBindings = new BindingGroup(selected);
			discardBindings.setDefaultBindingStrategy(BindingStrategy.ONFLUSH);

			discardBindings.add(view.getSpecies(), "species");
			discardBindings.add(view.getQuantity(), "quantity");
			discardBindings.add(view.getQuantityUnit(), "quantityType");
			discardBindings.add(view.getReason(), "reason");
			
			discardBindings.bind();
		}
	}
	
	private void unbindDiscard() {
		if(discardBindings != null)
			discardBindings.unbind();
	}
	
	private void doRevertDiscard(Discards toRevert) {
		model.revert(toRevert);
	}
	
	private void addDiscardAction() {
		model.addLogsheetEventDiscard(new Discards());
		
		model.untaintDiscard();
	}
	
	private void cancelDiscardEditingAction() {
		if(model.getDiscardStatus() == EditingStatus.ADDING) {
			if(model.getSelectedDiscard() != null && model.getSelectedDiscard().getId() == null) ((CatchesRecord)model.getSelectedEvent()).getDiscards().remove(model.getSelectedDiscard());
		}
		
		model.select((Discards)null);

		applyDummyBinding();
		
		view.getRecordsTable().clearSelection();
	}
	
	private void updateRecordAction() {
		discardBindings.flushUIToModel();
		
		List<CheckResult> checks = model.getSelectedDiscard().checkCorrectness();
		
		StringBuilder messages = new StringBuilder();
		
		if(checks != null && !checks.isEmpty()) {
			for(CheckResult in : checks) {
				messages.
					append("[").append(in.getType().getCode()).append("] : ").
					append(in.getMessage()).
					append("\n");
			}
		}
		
		if(CheckResultHelper.hasErrors(checks)) {
			error("Cannot update selected discard", messages.toString());
			
			log().error("Cannot update selected discard due to the following reasons:");

			for(CheckResult in : checks) {
				log().error("{} >> {}", in.getType().getCode(), in.getMessage());
			}
		} else {
			boolean proceed = !CheckResultHelper.hasWarnings(checks);
			
			if(!proceed) {
				proceed = 
					UIUtils.confirm(
						"Please confirm", 
						"Current discard does contain the following inconsistencies:\n\n" + 
							messages.toString() + "\n" +
						"Are you sure you want to proceed?");
			}
			
			if(proceed) {
				logsheetEventDiscardUpdated(model.getSelectedDiscard());
				
				model.taintEvent();
				model.untaintDiscard();
				
				model.changeLogsheetEventDiscardStatus(EditingStatus.NOT_SET);
				
				cancelDiscardEditingAction();
			}
		}
	}
	
	private void deleteRecordAction() {
		if(UIUtils.confirm(
			"Please confirm", 
			"Are you sure you want to delete the selected discard?"
		)) {
			List<Discards> eventDiscards = ((CatchesRecord)model.getSelectedEvent()).getDiscards();
			
			Discards selected = model.getSelectedDiscard();
			eventDiscards.remove(selected);

			logsheetEventUpdated(model.getSelectedEvent());
			
			if(selected.getId() != null) model.taintEvent();

			model.select((Discards)null);
			
			cancelDiscardEditingAction();
			
			updateDiscardsTable();
		}
	}
	
	private void cancelDiscardAction() {
		cancelDiscardEditingAction();
	}
	
	private void updateDiscardsTable() {
		JTable table = view.getRecordsTable();
		
		((LogsheetEventDiscardTableModel)table.getModel()).update(model.getSelectedEvent() == null ? null : ((CatchesRecord)model.getSelectedEvent()).getDiscards());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventSelected(org.fao.fi.vrmf.psm.logbook.model.base.events.Event)
	 */
	@Override
	public void logsheetEventSelected(Event selected) {
		if(selected == null || !selected.isCatchEvent()) {
			view.getRecordsTable().getSelectionModel().clearSelection();
		} else {
			boolean isEditing = selected.isCompleted() && model.getEventStatus() == EditingStatus.EDITING;
			boolean displayDiscards = selected.isCatchEvent();

			List<Discards> eventDiscards = ((CatchesRecord)selected).getDiscards();
			
			if(eventDiscards == null) ((CatchesRecord)selected).setDiscards(eventDiscards = new ArrayList<Discards>());
			
			((LogsheetEventDiscardTableModel)view.getRecordsTable().getModel()).update(eventDiscards);
			
			view.getAddNew().setEnabled(isEditing && displayDiscards && ( model.getEventStatus() == EditingStatus.ADDING || model.getEventStatus() == EditingStatus.EDITING ));
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortStatusChanged(org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus, org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus)
	 */
	@Override
	public void logsheetEventDiscardStatusChanged(EditingStatus previous, EditingStatus current) {
		boolean enable = current == EditingStatus.ADDING || current == EditingStatus.EDITING || model.isDiscardDirty();
		
		boolean enableUpdate = enable;
		boolean enableDelete = enable && current != EditingStatus.ADDING;
		boolean enableCancel = enable;
		
		view.getUpdateRecord().setEnabled(enableUpdate);
		view.getDeleteRecord().setEnabled(enableDelete);
		view.getCancelRecord().setEnabled(enableCancel);
		
		view.getSpecies().setEnabled(enable);
		view.getQuantity().setEnabled(enable);
		view.getQuantityUnit().setEnabled(enable);
		view.getReason().setEnabled(enable);
		
		view.getAddNew().setEnabled(!enable && current != EditingStatus.VIEWING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortAdded(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventDiscardAdded(Discards added) {
		model.select(added, EditingStatus.ADDING);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortUpdated(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventDiscardUpdated(Discards updated) {
		updateDiscardsTable();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortRemoved(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventDiscardRemoved(Discards deleted) {
		updateDiscardsTable();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetEventEffortSelected(org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort)
	 */
	@Override
	public void logsheetEventDiscardSelected(Discards selected) {
		if(selected != null) {
			bindDiscard(selected);
			
			model.untaintDiscard();
		}
		
		boolean isEditing = selected != null && ( model.getDiscardStatus() == EditingStatus.ADDING || model.getDiscardStatus() == EditingStatus.EDITING );
		
		parentView.getCatchAndEffortTabs().setEnabledAt(0, !isEditing && model.getSelectedEvent() != null && model.getSelectedEvent().isCatchEvent());
		parentView.getCatchAndEffortTabs().setEnabledAt(1, !isEditing && model.getSelectedEvent() != null && model.getSelectedEvent().isCatchEvent());
	
		view.getRecordsTable().setEnabled(!isEditing);
	}
}