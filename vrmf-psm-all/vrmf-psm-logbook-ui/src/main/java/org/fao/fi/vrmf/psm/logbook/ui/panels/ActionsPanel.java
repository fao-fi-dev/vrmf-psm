/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class ActionsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8345093652881047572L;
	
	@Getter private JNoFocusOutlineButton manageLogbooks;
	@Getter private JNoFocusOutlineButton manageVessels;
	@Getter private JNoFocusOutlineButton lastLogbook;
	@Getter private JNoFocusOutlineButton lastLogsheet;
	@Getter private JNoFocusOutlineButton dataSynchronization;

	public ActionsPanel() {
		if(Utils.IS_LAUNCHING) return;
			
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		setBorder(new TitledBorder(null, "Available actions", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		
		this.setLayout(gridBagLayout);
		
		JPanel actions = new JPanel();
		
		GridBagConstraints gbc_actions = new GridBagConstraints();
		gbc_actions.anchor = GridBagConstraints.NORTH;
		gbc_actions.weighty = 100.0;
		gbc_actions.fill = GridBagConstraints.HORIZONTAL;
		gbc_actions.insets = new Insets(5, 5, 5, 5);
		gbc_actions.weightx = 100.0;
		gbc_actions.gridx = 0;
		gbc_actions.gridy = 0;
		
		GridBagLayout gbl_actions = new GridBagLayout();
		gbl_actions.rowHeights = new int[0];
		gbl_actions.columnWeights = new double[0];
		gbl_actions.rowWeights = new double[0];
		
		actions.setLayout(gbl_actions);
	
		this.add(actions, gbc_actions);
		
		manageLogbooks = new JNoFocusOutlineButton("Manage logbooks");
		manageLogbooks.setFont(new Font("Tahoma", Font.PLAIN, 20));
		manageLogbooks.setToolTipText("Manage logbooks (create / delete / update / view)");
		GridBagConstraints gbc_btnLogbooks = new GridBagConstraints();
		gbc_btnLogbooks.insets = new Insets(0, 0, 5, 0);
		gbc_btnLogbooks.fill = GridBagConstraints.BOTH;
		gbc_btnLogbooks.gridx = 0;
		gbc_btnLogbooks.gridy = 1;
		actions.add(manageLogbooks, gbc_btnLogbooks);
		
		manageVessels = new JNoFocusOutlineButton("Manage vessels");
		manageVessels.setFont(new Font("Tahoma", Font.PLAIN, 20));
		manageVessels.setToolTipText("Manage vessels (create / delete / update / view)");
		GridBagConstraints gbc_btnVessels = new GridBagConstraints();
		gbc_btnVessels.insets = new Insets(5, 0, 5, 0);
		gbc_btnVessels.fill = GridBagConstraints.BOTH;
		gbc_btnVessels.gridx = 0;
		gbc_btnVessels.gridy = 0;
		actions.add(manageVessels, gbc_btnVessels);
		
		lastLogbook = new JNoFocusOutlineButton("Manage last logbook");
		lastLogbook.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lastLogbook.setToolTipText("Manage the last, non-completed logbook");
		GridBagConstraints gbc_btnLastlogbook = new GridBagConstraints();
		gbc_btnLastlogbook.insets = new Insets(0, 0, 5, 0);
		gbc_btnLastlogbook.fill = GridBagConstraints.BOTH;
		gbc_btnLastlogbook.gridx = 0;
		gbc_btnLastlogbook.gridy = 2;
		actions.add(lastLogbook, gbc_btnLastlogbook);
		
		lastLogsheet = new JNoFocusOutlineButton("Edit last logsheet");
		lastLogsheet.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lastLogsheet.setToolTipText("Edit the last, non-completed logsheet");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 3;
		actions.add(lastLogsheet, gbc_btnNewButton);

		dataSynchronization = new JNoFocusOutlineButton("Synchronize reference data");
		dataSynchronization.setFont(new Font("Tahoma", Font.PLAIN, 20));
		dataSynchronization.setToolTipText("Synchronize reference data to their latest version");
		GridBagConstraints gbc_btnSynchronizeData = new GridBagConstraints();
		gbc_btnSynchronizeData.fill = GridBagConstraints.BOTH;
		gbc_btnSynchronizeData.insets = new Insets(0, 0, 5, 0);
		gbc_btnSynchronizeData.gridx = 0;
		gbc_btnSynchronizeData.gridy = 4;
		actions.add(dataSynchronization, gbc_btnSynchronizeData);
		
		final JPanel $this = this;
		
		dataSynchronization.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UIUtils.alert($this, "Not implemented yet");
			}
		});
		
		for(JNoFocusOutlineButton in : new JNoFocusOutlineButton[] {
			manageLogbooks, manageVessels, lastLogbook, lastLogsheet, dataSynchronization
		}) in.setFocusPainted(false);
	}
}
