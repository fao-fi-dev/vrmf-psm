/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.controllers.application;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.ActionsPanel;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogbookListener;
import org.fao.fi.vrmf.psm.logbook.ui.spi.LogsheetListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class NavigationController extends BaseController {
	@Inject private ActionsPanel actions;
	
	@PostConstruct
	private void initialize() {
		model.registerListener((LogbookListener)this);
		model.registerListener((LogsheetListener)this);
		
		actions.getManageLogbooks().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				model.select((Logbook)null);
				
				application.show("logbooks");
			}
		});
		
		actions.getManageVessels().addActionListener(new InterceptingActionListener() {
			@Override
			public void doActionPerformed(ActionEvent e) {
				model.select((VesselData)null);
				
				application.show("vessels");
			}
		});
		
		actions.getLastLogbook().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				lastLogbookAction();
			}
		});
		
		actions.getLastLogsheet().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) {
				lastLogsheetAction();
			}
		});
		
		updateStatus();
	}
	
	private boolean isLastLogbookActionEnabled() {
		return lastNonCompletedLogbook() != null;
	}
	
	private boolean isLastLogsheetActionEnabled() {
		return lastNonCompletedLogsheet() != null;
	}
	
	@Override
	public void logbookStatusChanged(EditingStatus previous, EditingStatus current) {
		log().trace("LOGBOOK STATUS: {} -> {}", previous, current);
	}
	
	private void lastLogbookAction() {
		model.select(lastNonCompletedLogbook());
		
		application.show("logbooks");
	}
	
	private void lastLogsheetAction() {
		Logsheet last = lastNonCompletedLogsheet();
		
		model.select(logbookForLogsheet(last));
		model.select(last, EditingStatus.EDITING);
		
		application.show("logsheet");
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logbookAdded(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookAdded(Logbook added) {
		updateStatus();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logbookRemoved(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookRemoved(Logbook removed) {
		updateStatus();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logbookUpdated(org.fao.fi.vrmf.psm.logbook.model.base.Logbook)
	 */
	@Override
	public void logbookUpdated(Logbook updated) {
		updateStatus();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetAdded(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetAdded(Logsheet added) {
		updateStatus();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetRemoved(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetRemoved(Logsheet removed) {
		updateStatus();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.controllers.BaseController#logsheetUpdated(org.fao.fi.vrmf.psm.logbook.model.base.Logsheet)
	 */
	@Override
	public void logsheetUpdated(Logsheet updated) {
		updateStatus();
	}
	
	private void updateStatus() {
		updateSummaryStatus();
		updateButtonStatus();
	}
	
	private void updateSummaryStatus() {
		application.updateSummary();
	}
	
	private void updateButtonStatus() {
		actions.getLastLogbook().setEnabled(isLastLogbookActionEnabled());
		actions.getLastLogsheet().setEnabled(isLastLogsheetActionEnabled());
	}
	
	private List<Logbook> nonCompletedLogbooks() {
		List<Logbook> nonCompleted = new ArrayList<Logbook>();
		
		if(model.getLogbooks() != null) {
			for(Logbook in : model.getLogbooks()) {
				if(!in.isCompleted())
					nonCompleted.add(in);
			}
		}
		
		Collections.sort(nonCompleted, new Comparator<Logbook>() { 
			@Override
			public int compare(Logbook o1, Logbook o2) {
				return o2.getCreationDate().compareTo(o1.getCreationDate());
			}
		});
		
		return nonCompleted;
	}
	
	private Logbook lastNonCompletedLogbook() {
		List<Logbook> nonCompleted = nonCompletedLogbooks();
		
		return nonCompleted.isEmpty() ? null : nonCompleted.get(0);
	}
	
	private List<Logsheet> nonCompletedLogsheets() {
		List<Logsheet> nonCompleted = new ArrayList<Logsheet>();
		
		for(Logbook owner : nonCompletedLogbooks()) {
			if(owner.getLogsheets() != null) {
				for(Logsheet in : owner.getLogsheets()) {
					if(!in.isCompleted())
						nonCompleted.add(in);
				}
			}
		}
		
		Collections.sort(nonCompleted, new Comparator<Logsheet>() { 
			@Override
			public int compare(Logsheet o1, Logsheet o2) {
				return o2.getId().compareTo(o1.getId());
			}
		});
		
		return nonCompleted;
	}
	
	private Logsheet lastNonCompletedLogsheet() {
		List<Logsheet> nonCompleted = nonCompletedLogsheets();
		
		return nonCompleted.isEmpty() ? null : nonCompleted.get(0);
	}
	
	private Logbook logbookForLogsheet(Logsheet selected) {
		if(model.getLogbooks() != null) {
			for(Logbook in : model.getLogbooks()) {
				if(in.getLogsheets() != null && in.getLogsheets().contains(selected))
					return in;
			}
		}
		
		return null;
	}
}