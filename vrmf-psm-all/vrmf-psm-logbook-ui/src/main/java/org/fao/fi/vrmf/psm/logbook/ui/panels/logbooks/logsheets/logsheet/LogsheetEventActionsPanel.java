/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.logsheet;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 4, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 4, 2015
 */
@Named @Singleton
public class LogsheetEventActionsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5155113194441012377L;
	
	@Getter private JNoFocusOutlineButton updateEvent;
	@Getter private JNoFocusOutlineButton saveEvent;
	@Getter private JNoFocusOutlineButton deleteEvent;
	@Getter private JNoFocusOutlineButton cancel;

	/**
	 * Class constructor
	 *
	 */
	public LogsheetEventActionsPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		saveEvent = new JNoFocusOutlineButton("Save event");
		saveEvent.setEnabled(false);
		saveEvent.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc__saveEvent = new GridBagConstraints();
		gbc__saveEvent.fill = GridBagConstraints.HORIZONTAL;
		gbc__saveEvent.insets = new Insets(5, 0, 5, 5);
		gbc__saveEvent.gridx = 0;
		gbc__saveEvent.gridy = 0;
		add(saveEvent, gbc__saveEvent);
		
		updateEvent = new JNoFocusOutlineButton("Update event");
		updateEvent.setEnabled(false);
		updateEvent.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc__updateEvent = new GridBagConstraints();
		gbc__updateEvent.fill = GridBagConstraints.HORIZONTAL;
		gbc__updateEvent.insets = new Insets(5, 0, 5, 5);
		gbc__updateEvent.gridx = 1;
		gbc__updateEvent.gridy = 0;
		add(updateEvent, gbc__updateEvent);
		
		deleteEvent = new JNoFocusOutlineButton("Delete event");
		deleteEvent.setEnabled(false);
		deleteEvent.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc__deleteEvent = new GridBagConstraints();
		gbc__deleteEvent.insets = new Insets(5, 0, 5, 5);
		gbc__deleteEvent.fill = GridBagConstraints.HORIZONTAL;
		gbc__deleteEvent.gridx = 2;
		gbc__deleteEvent.gridy = 0;
		add(deleteEvent, gbc__deleteEvent);
		
		cancel = new JNoFocusOutlineButton("Cancel");
		cancel.setEnabled(false);
		cancel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc__cancel = new GridBagConstraints();
		gbc__cancel.insets = new Insets(5, 0, 5, 0);
		gbc__cancel.fill = GridBagConstraints.BOTH;
		gbc__cancel.gridx = 3;
		gbc__cancel.gridy = 0;
		add(cancel, gbc__cancel);
	}
	
	public void notifyEventStatusChange(EditingStatus logsheetStatus, Logsheet logsheet, EditingStatus eventStatus, Event event) {
		boolean isLastEvent = 
				logsheet != null && 
				logsheet.getEvents() != null && 
			   !logsheet.getEvents().isEmpty() && 
				event != null && 
				logsheet.getEvents().indexOf(event) == logsheet.getEvents().size() - 1;
		
		updateEvent.setEnabled(eventStatus == EditingStatus.ADDING || eventStatus == EditingStatus.EDITING);
		saveEvent.setEnabled(eventStatus == EditingStatus.ADDING || eventStatus == EditingStatus.EDITING);
		deleteEvent.setEnabled(eventStatus == EditingStatus.EDITING && isLastEvent);
		cancel.setEnabled(eventStatus == EditingStatus.ADDING || eventStatus == EditingStatus.EDITING);
	}
}
