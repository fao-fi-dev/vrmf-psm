/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.EffortsRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.support.gis.Latitude;
import org.fao.fi.vrmf.psm.logbook.model.support.gis.Longitude;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class LogsheetEventTableModel extends AbstractTableModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 587756690700916447L;

	@Getter private String[] columnNames = new String[] { 
		"New", "#", "Type", "Start", "Lat.", "Lon.", "End", "Lat.", "Lon.", "Duration (hrs)", "Efforts", "Catches", "Discards"
	};

	@Getter private List<Event> rawData = null;

	/**
	 * Class constructor
	 *
	 * @param rawData
	 */
	public LogsheetEventTableModel(List<Event> data) {
		super();
		
		rawData = data == null ? new ArrayList<Event>() : data;
	}
	
	public void update(List<Event> data) {
		rawData = data == null ? new ArrayList<Event>() : data;
		
		fireTableDataChanged();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return rawData.size();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		Event record = rawData == null || rawData.size() < row + 1 ? null : rawData.get(row);
	
		if(record == null) return null;
		
		switch(col) {
			case 0:
				return record.getId() == null;
			case 1:
				return row + 1;
			case 2:
				return record.getType();
			case 3:
				return record.getStartDate();
			case 4:
				return record.getCoordinatesAtStart() == null ? null : Latitude.fromCoordinates(record.getCoordinatesAtStart());
			case 5:
				return record.getCoordinatesAtStart() == null ? null : Longitude.fromCoordinates(record.getCoordinatesAtStart());
			case 6:
				return record.getEndDate();
			case 7:
				return record.getCoordinatesAtEnd() == null ? null : Latitude.fromCoordinates(record.getCoordinatesAtEnd());
			case 8:
				return record.getCoordinatesAtEnd() == null ? null : Longitude.fromCoordinates(record.getCoordinatesAtEnd());
			case 9:
				return record.hours();
			case 10: 
				return record.isEffortEvent() && ((EffortsRecord)record).getEfforts() != null ? ((EffortsRecord)record).getEfforts().size() : null;
			case 11: 
				return record.isCatchEvent() && ((CatchesRecord)record).getCatches() != null ? ((CatchesRecord)record).getCatches().size() : null;
			case 12: 
				return record.isCatchEvent() && ((CatchesRecord)record).getDiscards() != null ? ((CatchesRecord)record).getDiscards().size() : null;
			default:
				return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		switch(c) {
			case 0:
				return Boolean.class;
			case 1:
				return Integer.class;
			case 2:
				return EventType.class;
			case 3:
			case 6:
				return Date.class;
			case 4:
			case 7:
				return Latitude.class;
			case 5:
			case 8:
				return Longitude.class;
			case 9: 
				return Double.class;
			case 10:
			case 11:
			case 12:
				return Integer.class;
			default:
				return Void.class;
		}
	}
	
	public Object[][] getData() {
		if(rawData == null) return new Object[0][];
		
		Object[][] toReturn = new Object[getRowCount()][getColumnCount()];
		
		for(int row=0; row < getRowCount(); row++) {
			toReturn[row] = new Object[getColumnCount()];
			
			for(int column=0; column < getColumnCount(); column++) {
				toReturn[row][column] = getValueAt(row, column);
			}
		}
		
		return toReturn;
	}
}