/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.vessels;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.PowerUnit;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.VesselType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.ui.common.renderers.CountryRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.PowerUnitRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.VesselTypeRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named @Singleton
public class VesselDataPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6905745646906504149L;
	
	@Getter private JPanel selectedVesselData;
	@Getter private JPanel selectedVesselActions;
	
	@Getter @Inject private VesselIdentifiersPanel identifiers;
	
	@Getter private JTextField vesselName;
	@Getter private JComboBox type;
	@Getter private JTextField IRCS;
	@Getter private JTextField MMSI;
	@Getter private JTextField regNo;
	@Getter private JComboBox regCountry;
	@Getter private JTextField IMO;
	@Getter private JTextField fishingCompany;
	@Getter private JSpinner LOA;
	@Getter private JSpinner GT;
	@Getter private JSpinner LBP;
	@Getter private JSpinner GRT;
	@Getter private JSpinner mainEnginePower;
	@Getter private JComboBox mainEnginePowerUnit;
	
	@Getter private JNoFocusOutlineButton updateVessel;
	@Getter private JNoFocusOutlineButton deleteVessel;
	@Getter private JNoFocusOutlineButton revertVessel;
	@Getter private JNoFocusOutlineButton identifyVessel;

	public VesselDataPanel() {
		selectedVesselData = new JPanel();
		selectedVesselActions = new JPanel();
		
		if(Utils.IS_LAUNCHING) return;

		identifiers = new VesselIdentifiersPanel();
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		this.setBorder(null);
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.rowHeights = new int[] {0, 32};
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);

		selectedVesselData.setBorder(new TitledBorder(null, "Selected vessel data", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_selectedVesselData = new GridBagConstraints();
		gbc_selectedVesselData.weightx = 100.0;
		gbc_selectedVesselData.weighty = 100.0;
		gbc_selectedVesselData.insets = new Insets(5, 5, 5, 5);
		gbc_selectedVesselData.fill = GridBagConstraints.BOTH;
		gbc_selectedVesselData.gridx = 0;
		gbc_selectedVesselData.gridy = 0;
		this.add(selectedVesselData, gbc_selectedVesselData);
		GridBagLayout gbl_selectedVesselData = new GridBagLayout();
		gbl_selectedVesselData.columnWidths = new int[0];
		gbl_selectedVesselData.columnWeights = new double[0];
		gbl_selectedVesselData.rowWeights = new double[0];
		selectedVesselData.setLayout(gbl_selectedVesselData);
		
		JPanel mainData = new JPanel();
		mainData.setBorder(new TitledBorder(null, "Main data", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_mainData = new GridBagConstraints();
		gbc_mainData.anchor = GridBagConstraints.NORTH;
		gbc_mainData.weightx = 100.0;
		gbc_mainData.insets = new Insets(0, 0, 5, 0);
		gbc_mainData.fill = GridBagConstraints.HORIZONTAL;
		gbc_mainData.gridx = 0;
		gbc_mainData.gridy = 0;
		selectedVesselData.add(mainData, gbc_mainData);
		GridBagLayout gbl_mainData = new GridBagLayout();
		gbl_mainData.columnWidths = new int[0];
		gbl_mainData.rowHeights = new int[0];
		gbl_mainData.columnWeights = new double[0];
		gbl_mainData.rowWeights = new double[0];
		mainData.setLayout(gbl_mainData);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.weightx = 5.0;
		gbc_lblName.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblName.anchor = GridBagConstraints.WEST;
		gbc_lblName.insets = new Insets(5, 5, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		mainData.add(lblName, gbc_lblName);
		
		vesselName = new JTextField();
		GridBagConstraints gbc_name = new GridBagConstraints();
		gbc_name.weightx = 100.0;
		gbc_name.gridwidth = 4;
		gbc_name.insets = new Insets(5, 5, 5, 5);
		gbc_name.fill = GridBagConstraints.HORIZONTAL;
		gbc_name.anchor = GridBagConstraints.WEST;
		gbc_name.gridx = 1;
		gbc_name.gridy = 0;
		mainData.add(vesselName, gbc_name);
				
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.weightx = 5.0;
		gbc_lblType.anchor = GridBagConstraints.WEST;
		gbc_lblType.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblType.insets = new Insets(5, 5, 5, 5);
		gbc_lblType.gridx = 0;
		gbc_lblType.gridy = 1;
		mainData.add(lblType, gbc_lblType);
		
		type = new JComboBox(VesselType.values());
		type.setRenderer(new VesselTypeRenderer());
		
		GridBagConstraints gbc__type = new GridBagConstraints();
		gbc__type.ipady = 5;
		gbc__type.weightx = 5.0;
		gbc__type.anchor = GridBagConstraints.WEST;
		gbc__type.insets = new Insets(5, 5, 5, 5);
		gbc__type.fill = GridBagConstraints.HORIZONTAL;
		gbc__type.gridx = 1;
		gbc__type.gridy = 1;
		mainData.add(type, gbc__type);
		
		JLabel lblRegNo = new JLabel("Reg. no.");
		lblRegNo.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblRegNo = new GridBagConstraints();
		gbc_lblRegNo.weightx = 5.0;
		gbc_lblRegNo.anchor = GridBagConstraints.WEST;
		gbc_lblRegNo.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblRegNo.insets = new Insets(5, 5, 5, 5);
		gbc_lblRegNo.gridx = 0;
		gbc_lblRegNo.gridy = 2;
		mainData.add(lblRegNo, gbc_lblRegNo);
		
		regNo = new JTextField();
		GridBagConstraints gbc_regNo = new GridBagConstraints();
		gbc_regNo.weightx = 10.0;
		gbc_regNo.anchor = GridBagConstraints.WEST;
		gbc_regNo.insets = new Insets(5, 5, 5, 5);
		gbc_regNo.fill = GridBagConstraints.HORIZONTAL;
		gbc_regNo.gridx = 1;
		gbc_regNo.gridy = 2;
		mainData.add(regNo, gbc_regNo);
		regNo.setColumns(15);
		
		JLabel lblRegCountry = new JLabel("Reg. country");
		lblRegCountry.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblRegCountry = new GridBagConstraints();
		gbc_lblRegCountry.weightx = 5.0;
		gbc_lblRegCountry.anchor = GridBagConstraints.WEST;
		gbc_lblRegCountry.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblRegCountry.insets = new Insets(5, 5, 5, 5);
		gbc_lblRegCountry.gridx = 0;
		gbc_lblRegCountry.gridy = 3;
		mainData.add(lblRegCountry, gbc_lblRegCountry);
		
		regCountry = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.ipady = 5;
		gbc_comboBox.weightx = 10.0;
		gbc_comboBox.anchor = GridBagConstraints.WEST;
		gbc_comboBox.insets = new Insets(5, 5, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 3;
		mainData.add(regCountry, gbc_comboBox);
		
		regCountry.setRenderer(new CountryRenderer());
		
		JLabel lblIRCS = new JLabel("IRCS");
		lblIRCS.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblIRCS = new GridBagConstraints();
		gbc_lblIRCS.weightx = 2.0;
		gbc_lblIRCS.anchor = GridBagConstraints.WEST;
		gbc_lblIRCS.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblIRCS.insets = new Insets(5, 5, 5, 5);
		gbc_lblIRCS.gridx = 3;
		gbc_lblIRCS.gridy = 1;
		mainData.add(lblIRCS, gbc_lblIRCS);
		
		IRCS = new JTextField();
		GridBagConstraints gbc_IRCS = new GridBagConstraints();
		gbc_IRCS.fill = GridBagConstraints.HORIZONTAL;
		gbc_IRCS.weightx = 10.0;
		gbc_IRCS.anchor = GridBagConstraints.WEST;
		gbc_IRCS.insets = new Insets(5, 5, 5, 5);
		gbc_IRCS.gridx = 4;
		gbc_IRCS.gridy = 1;
		mainData.add(IRCS, gbc_IRCS);
		
		JLabel lblImo = new JLabel("IMO");
		GridBagConstraints gbc_lblImo = new GridBagConstraints();
		gbc_lblImo.weightx = 2.0;
		gbc_lblImo.anchor = GridBagConstraints.WEST;
		gbc_lblImo.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblImo.insets = new Insets(5, 5, 5, 5);
		gbc_lblImo.gridx = 3;
		gbc_lblImo.gridy = 2;
		mainData.add(lblImo, gbc_lblImo);

		IMO = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.weightx = 10.0;
		gbc_textField.anchor = GridBagConstraints.WEST;
		gbc_textField.insets = new Insets(5, 5, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 4;
		gbc_textField.gridy = 2;
		mainData.add(IMO, gbc_textField);
		
		JLabel lblMmsi = new JLabel("MMSI");
		GridBagConstraints gbc_lblMmsi = new GridBagConstraints();
		gbc_lblMmsi.weightx = 2.0;
		gbc_lblMmsi.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblMmsi.anchor = GridBagConstraints.WEST;
		gbc_lblMmsi.insets = new Insets(5, 5, 5, 5);
		gbc_lblMmsi.gridx = 3;
		gbc_lblMmsi.gridy = 3;
		mainData.add(lblMmsi, gbc_lblMmsi);
		
		MMSI = new JTextField();
		GridBagConstraints gbc_MMSI = new GridBagConstraints();
		gbc_MMSI.fill = GridBagConstraints.HORIZONTAL;
		gbc_MMSI.weightx = 10.0;
		gbc_MMSI.anchor = GridBagConstraints.WEST;
		gbc_MMSI.insets = new Insets(5, 5, 5, 5);
		gbc_MMSI.gridx = 4;
		gbc_MMSI.gridy = 3;
		mainData.add(MMSI, gbc_MMSI);
		
		JLabel lblFishingCompany = new JLabel("Fishing co.");
		lblFishingCompany.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblFishingCompany = new GridBagConstraints();
		gbc_lblFishingCompany.weightx = 5.0;
		gbc_lblFishingCompany.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblFishingCompany.anchor = GridBagConstraints.WEST;
		gbc_lblFishingCompany.insets = new Insets(5, 5, 5, 5);
		gbc_lblFishingCompany.gridx = 0;
		gbc_lblFishingCompany.gridy = 4;
		mainData.add(lblFishingCompany, gbc_lblFishingCompany);
		
		fishingCompany = new JTextField();
		GridBagConstraints gbc_fishingCompany = new GridBagConstraints();
		gbc_fishingCompany.weightx = 100.0;
		gbc_fishingCompany.gridwidth = 4;
		gbc_fishingCompany.anchor = GridBagConstraints.WEST;
		gbc_fishingCompany.insets = new Insets(5, 5, 5, 5);
		gbc_fishingCompany.fill = GridBagConstraints.HORIZONTAL;
		gbc_fishingCompany.gridx = 1;
		gbc_fishingCompany.gridy = 4;
		mainData.add(fishingCompany, gbc_fishingCompany);
		
		JPanel otherData = new JPanel();
		otherData.setBorder(new TitledBorder(null, "Other data", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_otherData = new GridBagConstraints();
		gbc_otherData.weightx = 100.0;
		gbc_otherData.anchor = GridBagConstraints.NORTH;
		gbc_otherData.insets = new Insets(0, 0, 5, 0);
		gbc_otherData.fill = GridBagConstraints.HORIZONTAL;
		gbc_otherData.gridx = 0;
		gbc_otherData.gridy = 1;
		selectedVesselData.add(otherData, gbc_otherData);
		GridBagLayout gbl_otherData = new GridBagLayout();
		gbl_otherData.columnWidths = new int[0];
		gbl_otherData.rowHeights = new int[0];
		gbl_otherData.columnWeights = new double[0];
		gbl_otherData.rowWeights = new double[0];
		otherData.setLayout(gbl_otherData);
		
		JLabel lblLoa = new JLabel("LOA (m)");
		lblLoa.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblLoa = new GridBagConstraints();
		gbc_lblLoa.weightx = 10.0;
		gbc_lblLoa.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblLoa.anchor = GridBagConstraints.WEST;
		gbc_lblLoa.insets = new Insets(5, 5, 5, 5);
		gbc_lblLoa.gridx = 0;
		gbc_lblLoa.gridy = 0;
		otherData.add(lblLoa, gbc_lblLoa);
		
		LOA = new JSpinner();
		LOA.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		GridBagConstraints gbc_loa = new GridBagConstraints();
		gbc_loa.fill = GridBagConstraints.HORIZONTAL;
		gbc_loa.weightx = 100.0;
		gbc_loa.anchor = GridBagConstraints.WEST;
		gbc_loa.insets = new Insets(5, 5, 5, 5);
		gbc_loa.gridx = 1;
		gbc_loa.gridy = 0;
		otherData.add(LOA, gbc_loa);
		
		JLabel lblGt = new JLabel("GT (t)");
		lblGt.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblGt = new GridBagConstraints();
		gbc_lblGt.weightx = 5.0;
		gbc_lblGt.insets = new Insets(5, 5, 5, 5);
		gbc_lblGt.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblGt.anchor = GridBagConstraints.WEST;
		gbc_lblGt.gridx = 3;
		gbc_lblGt.gridy = 0;
		otherData.add(lblGt, gbc_lblGt);
		
		GT = new JSpinner();
		GT.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		GridBagConstraints gbc_gt = new GridBagConstraints();
		gbc_gt.weightx = 100.0;
		gbc_gt.anchor = GridBagConstraints.WEST;
		gbc_gt.insets = new Insets(5, 5, 5, 5);
		gbc_gt.fill = GridBagConstraints.HORIZONTAL;
		gbc_gt.gridx = 4;
		gbc_gt.gridy = 0;
		otherData.add(GT, gbc_gt);
		
		JLabel lblLbp = new JLabel("LBP (m)");
		GridBagConstraints gbc_lblLbp = new GridBagConstraints();
		gbc_lblLbp.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblLbp.anchor = GridBagConstraints.WEST;
		gbc_lblLbp.weightx = 5.0;
		gbc_lblLbp.insets = new Insets(5, 5, 5, 5);
		gbc_lblLbp.gridx = 0;
		gbc_lblLbp.gridy = 1;
		otherData.add(lblLbp, gbc_lblLbp);
		
		LBP = new JSpinner();
		LBP.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinner.anchor = GridBagConstraints.WEST;
		gbc_spinner.insets = new Insets(5, 5, 5, 5);
		gbc_spinner.gridx = 1;
		gbc_spinner.gridy = 1;
		otherData.add(LBP, gbc_spinner);
		
		JLabel lblGrt = new JLabel("GRT (t)");
		GridBagConstraints gbc_lblGrt = new GridBagConstraints();
		gbc_lblGrt.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblGrt.anchor = GridBagConstraints.WEST;
		gbc_lblGrt.insets = new Insets(5, 5, 5, 5);
		gbc_lblGrt.gridx = 3;
		gbc_lblGrt.gridy = 1;
		otherData.add(lblGrt, gbc_lblGrt);
		
		GRT = new JSpinner();
		GRT.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		GridBagConstraints gbc_spinner_1 = new GridBagConstraints();
		gbc_spinner_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinner_1.anchor = GridBagConstraints.WEST;
		gbc_spinner_1.insets = new Insets(5, 5, 5, 5);
		gbc_spinner_1.gridx = 4;
		gbc_spinner_1.gridy = 1;
		otherData.add(GRT, gbc_spinner_1);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 5;
		gbc_horizontalStrut_1.gridy = 0;
		otherData.add(horizontalStrut_1, gbc_horizontalStrut_1);
		
		JLabel lblNewLabel = new JLabel("Main engine");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.weightx = 30.0;
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(5, 5, 5, 5);
		gbc_lblNewLabel.gridwidth = 3;
		gbc_lblNewLabel.gridx = 6;
		gbc_lblNewLabel.gridy = 0;
		otherData.add(lblNewLabel, gbc_lblNewLabel);
		
		mainEnginePower = new JSpinner();
		mainEnginePower.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(5)));
		GridBagConstraints gbc_spinner_2 = new GridBagConstraints();
		gbc_spinner_2.anchor = GridBagConstraints.WEST;
		gbc_spinner_2.insets = new Insets(5, 5, 5, 5);
		gbc_spinner_2.gridwidth = 2;
		gbc_spinner_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinner_2.weightx = 30.0;
		gbc_spinner_2.gridx = 6;
		gbc_spinner_2.gridy = 1;
		otherData.add(mainEnginePower, gbc_spinner_2);
		
		mainEnginePowerUnit = new JComboBox(PowerUnit.values());
		mainEnginePowerUnit.setRenderer(new PowerUnitRenderer());
		GridBagConstraints gbc_powerUnit = new GridBagConstraints();
		gbc_powerUnit.ipady = 5;
		gbc_powerUnit.weightx = 1.0;
		gbc_powerUnit.anchor = GridBagConstraints.WEST;
		gbc_powerUnit.insets = new Insets(5, 5, 5, 5);
		gbc_powerUnit.fill = GridBagConstraints.HORIZONTAL;
		gbc_powerUnit.gridx = 8;
		gbc_powerUnit.gridy = 1;
		otherData.add(mainEnginePowerUnit, gbc_powerUnit);
		
		GridBagConstraints gbc_selectedVesselAction = new GridBagConstraints();
		gbc_selectedVesselAction.weightx = 100.0;
		gbc_selectedVesselAction.fill = GridBagConstraints.BOTH;
		gbc_selectedVesselAction.gridx = 0;
		gbc_selectedVesselAction.gridy = 1;
		this.add(selectedVesselActions, gbc_selectedVesselAction);
		
		updateVessel = new JNoFocusOutlineButton("Update vessel");
		updateVessel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		updateVessel.setEnabled(false);
		selectedVesselActions.add(updateVessel);
		
		deleteVessel = new JNoFocusOutlineButton("Delete vessel");
		deleteVessel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		deleteVessel.setEnabled(false);
		selectedVesselActions.add(deleteVessel);
		
		revertVessel = new JNoFocusOutlineButton();
		revertVessel.setEnabled(false);
		revertVessel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		revertVessel.setText("Revert vessel");
		selectedVesselActions.add(revertVessel);
		
		identifyVessel = new JNoFocusOutlineButton();
		identifyVessel.setEnabled(false);
		identifyVessel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		identifyVessel.setText("Identify vessel");
		selectedVesselActions.add(identifyVessel);
		
		GridBagConstraints gbc_identifiers = new GridBagConstraints();
		gbc_identifiers.weightx = 100.0;
		gbc_identifiers.weighty = 100.0;
		gbc_identifiers.fill = GridBagConstraints.BOTH;
		gbc_identifiers.gridx = 0;
		gbc_identifiers.gridy = 2;
		selectedVesselData.add(identifiers, gbc_identifiers);
		
		updateInputs(null, false);
	}
	
	public void updateFields(VesselData selected) {
		boolean enabled = selected != null;
		for(JComponent in : new JComponent[] {
			vesselName, IRCS, IMO, MMSI, regNo, regCountry, type, fishingCompany, LOA, LBP, GT, GRT, mainEnginePower, mainEnginePowerUnit
		}) {
			in.setEnabled(enabled);
		}
	}
	
	public void updateButtons(VesselData selected, boolean canBeDeleted) {
		deleteVessel.setEnabled(selected != null && selected.getId() != null && canBeDeleted);
		updateVessel.setEnabled(selected != null);
		revertVessel.setEnabled(selected != null && selected.getId() != null);
	}
	
	public void updateInputs(VesselData selected, boolean canBeDeleted) {
		updateFields(selected);
		updateButtons(selected, canBeDeleted);
	}
	
	public void hideContent() {
		setContentVisibility(false);
	}
	
	public void showContent() {
		setContentVisibility(true);
	}
	
	public void setContentVisibility(boolean visible) {
		selectedVesselData.setVisible(visible);
		selectedVesselActions.setVisible(visible);
	}
}