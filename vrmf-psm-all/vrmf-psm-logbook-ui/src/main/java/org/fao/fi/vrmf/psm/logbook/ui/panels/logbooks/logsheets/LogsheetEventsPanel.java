/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.support.gis.Latitude;
import org.fao.fi.vrmf.psm.logbook.model.support.gis.Longitude;
import org.fao.fi.vrmf.psm.logbook.ui.controllers.support.EditingStatus;
import org.fao.fi.vrmf.psm.logbook.ui.panels.logbooks.logsheets.model.LogsheetEventTableModel;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CodedEntityTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.DateTimeCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.LatitudeTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.LongitudeTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named
public class LogsheetEventsPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8872498289521272459L;
	
	@Getter private JTable logsheetsEventsTable;
	
	public LogsheetEventsPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	public void initialize() {
		GridBagLayout gbl_this = new GridBagLayout();
		gbl_this.columnWidths = new int[0];
		gbl_this.rowHeights = new int[0];
		gbl_this.columnWeights = new double[0];
		gbl_this.rowWeights = new double[0];
		this.setLayout(gbl_this);
		
		JPanel logsheets = new JPanel();
//		logsheets.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Selected logsheet events", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_logsheets = new GridBagConstraints();
		gbc_logsheets.weightx = 100.0;
		gbc_logsheets.weighty = 100.0;
		gbc_logsheets.fill = GridBagConstraints.BOTH;
		gbc_logsheets.insets = new Insets(5, 5, 5, 5);
		gbc_logsheets.anchor = GridBagConstraints.NORTH;
		gbc_logsheets.gridx = 0;
		gbc_logsheets.gridy = 0;
		this.add(logsheets, gbc_logsheets);
		GridBagLayout gbl_logsheets = new GridBagLayout();
		gbl_logsheets.columnWidths = new int[0];
		gbl_logsheets.rowHeights = new int[0];
		gbl_logsheets.columnWeights = new double[0];
		gbl_logsheets.rowWeights = new double[0];
		logsheets.setLayout(gbl_logsheets);

		logsheetsEventsTable = UIUtils.initialize(new JTable(new LogsheetEventTableModel(new ArrayList<Event>())));
		
		logsheetsEventsTable.setDefaultRenderer(EventType.class, new CodedEntityTableCellRenderer());
		logsheetsEventsTable.setDefaultRenderer(Date.class, new DateTimeCellRenderer());
		logsheetsEventsTable.setDefaultRenderer(Latitude.class, new LatitudeTableCellRenderer());
		logsheetsEventsTable.setDefaultRenderer(Longitude.class, new LongitudeTableCellRenderer());
		
		UIUtils.fixColumnWidth(logsheetsEventsTable, 0, 48);
		UIUtils.fixColumnWidth(logsheetsEventsTable, 1, 48);
		UIUtils.fixColumnWidth(logsheetsEventsTable, 4, 80);
		UIUtils.fixColumnWidth(logsheetsEventsTable, 5, 80);
		UIUtils.fixColumnWidth(logsheetsEventsTable, 7, 80);
		UIUtils.fixColumnWidth(logsheetsEventsTable, 8, 80);
		
		GridBagConstraints gbc_logsheetsTable = new GridBagConstraints();
		gbc_logsheetsTable.weighty = 100.0;
		gbc_logsheetsTable.weightx = 100.0;
		gbc_logsheetsTable.anchor = GridBagConstraints.NORTH;
		gbc_logsheetsTable.fill = GridBagConstraints.BOTH;
		gbc_logsheetsTable.gridx = 0;
		gbc_logsheetsTable.gridy = 0;
		logsheets.add(new JScrollPane(logsheetsEventsTable), gbc_logsheetsTable);
	}
	
	public void notifyStatusChange(EditingStatus status) {
		
	}
}
