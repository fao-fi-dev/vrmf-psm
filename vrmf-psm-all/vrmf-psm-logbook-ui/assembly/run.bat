ECHO Launching app, please wait...
@ECHO OFF
SET CURRENT_DIR=%~dp0
start javaw -Dcurrent.dir=%CURRENT_DIR% -jar PSM-UI.jar >> %CURRENT_DIR%/logs/out.log 2>> %CURRENT_DIR%/logs/err.log