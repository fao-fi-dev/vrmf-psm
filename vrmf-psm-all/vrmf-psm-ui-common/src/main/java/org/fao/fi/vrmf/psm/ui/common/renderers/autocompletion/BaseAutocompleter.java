/**
 * (c) 2014 FAO / UN (project: PSM-DTO)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.autocompletion;

import java.util.ArrayList;
import java.util.Collection;

import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Mar 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Mar 2014
 */
abstract public class BaseAutocompleter<DATA> extends ObjectToStringConverter { 
	/* (non-Javadoc)
	 * @see org.jdesktop.swingx.autocomplete.ObjectToStringConverter#getPossibleStringsForItem(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	final public String[] getPossibleStringsForItem(Object item) { 
		if(item == null) 
			return null; 
		
		return BaseAutocompleter.filterNotNulls(extract((DATA)item));
	} 
	
	/* (non-Javadoc)
	 * @see org.jdesktop.swingx.autocomplete.ObjectToStringConverter#getPreferredStringForItem(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	final public String getPreferredStringForItem(Object item) {
		return item == null ? "Please select" : label((DATA)item); 
	}

	abstract protected String label(DATA item);
	
	abstract protected String[] extract(DATA item);
	
	final static private String[] filterNotNulls(String[] toFilter) {
		Collection<String> filtered = new ArrayList<String>();
		
		if(toFilter != null)
			for(String in : toFilter)
				if(in != null)
					filtered.add(in);
		
		return filtered.toArray(new String[filtered.size()]);
	}
}