/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.panels;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import lombok.Getter;
import lombok.Setter;
import javax.swing.JProgressBar;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 7, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 7, 2016
 */
@Named @Singleton
public class GlassPane extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8078089394532699243L;
	
	@Getter @Setter private JLabel text;
	
	public GlassPane() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		JPanel textPanel = new JPanel();
		textPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		GridBagConstraints gbc_textPanel = new GridBagConstraints();
		gbc_textPanel.ipady = 35;
		gbc_textPanel.ipadx = 35;
		gbc_textPanel.gridx = 0;
		gbc_textPanel.gridy = 0;
		add(textPanel, gbc_textPanel);
		GridBagLayout gbl_textPanel = new GridBagLayout();
		gbl_textPanel.columnWidths = new int[0];
		gbl_textPanel.rowHeights = new int[0];
		gbl_textPanel.columnWeights = new double[0];
		gbl_textPanel.rowWeights = new double[0];
		textPanel.setLayout(gbl_textPanel);
		
		JLabel lbPreamble = new JLabel("Please wait:");
		lbPreamble.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lbPreamble = new GridBagConstraints();
		gbc_lbPreamble.insets = new Insets(0, 0, 5, 5);
		gbc_lbPreamble.gridx = 0;
		gbc_lbPreamble.gridy = 0;
		textPanel.add(lbPreamble, gbc_lbPreamble);
		
		text = new JLabel("performing a long and time consuming operation...");
		text.setFont(new Font("Tahoma", Font.PLAIN, 15));
		GridBagConstraints gbc__text = new GridBagConstraints();
		gbc__text.insets = new Insets(0, 5, 5, 0);
		gbc__text.gridx = 1;
		gbc__text.gridy = 0;
		textPanel.add(text, gbc__text);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		GridBagConstraints gbc_progressBar = new GridBagConstraints();
		gbc_progressBar.fill = GridBagConstraints.BOTH;
		gbc_progressBar.gridwidth = 2;
		gbc_progressBar.insets = new Insets(0, 0, 0, 0);
		gbc_progressBar.gridx = 0;
		gbc_progressBar.gridy = 1;
		textPanel.add(progressBar, gbc_progressBar);
	}
}
