/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.table;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
public class DateTableCellRenderer extends AbstractTableCellRenderer<Date> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6119391670996554231L;

	/**
	 * Class constructor
	 */
	public DateTableCellRenderer() {
		super(CENTER);
	}

	/**
	 * Class constructor
	 *
	 * @param alignment
	 */
	public DateTableCellRenderer(int alignment) {
		super(alignment);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.table.AbstractTableCellRenderer#doGetTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	protected void updateComponent(JTable table, Date value, boolean isSelected, boolean hasFocus, int row, int column) {
		setText(value == null ? "----" : new SimpleDateFormat("yyyy/MM/dd").format(value));
	}
}
