/**
 * (c) 2014 FAO / UN (project: PSM-CE-UI)
 */
package org.fao.fi.vrmf.psm.ui.common.support;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.spi.CodedEntity;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.DateTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.DoubleTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.IntegerTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.StringTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.components.JDateTimePicker;
import org.fao.fi.vrmf.psm.ui.common.support.components.JNoFocusOutlineButton;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Mar 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Mar 2014
 */
final public class UIUtils {
	static private Logger LOG = LoggerFactory.getLogger(UIUtils.class);
	
	static Map<Country, byte[]> FLAG_ICONS_MAP = new HashMap<Country, byte[]>();
	
	private UIUtils() { }
	
	static public JTable initialize(JTable table) {
		return initialize(table, null);
	}
	
	static public <C extends CodedEntity> ComboBoxModel modelForCodedEntity(Class<C> type) {
		return modelForCodedEntity(type, true);
	}
	
	static public <C extends CodedEntity> ComboBoxModel modelForCodedEntity(Class<C> type, boolean withNullEntry) {
		C[] values = type.getEnumConstants();
		
		return new DefaultComboBoxModel(withNullEntry ? Utils.withNullEntry(values) : values);
	}
	
	static public JTable initialize(JTable table, TableModel model) {
		if(model != null) table.setModel(model);
		
		table.setAutoCreateRowSorter(true);

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setColumnSelectionAllowed(false);
		
		table.getTableHeader().setReorderingAllowed(false);
		
		table.setDefaultRenderer(String.class, new StringTableCellRenderer());
		table.setDefaultRenderer(Integer.class, new IntegerTableCellRenderer());
		table.setDefaultRenderer(Double.class, new DoubleTableCellRenderer());
		table.setDefaultRenderer(Date.class, new DateTableCellRenderer());
		
//		table.getTableHeader().setDefaultRenderer(new DefaultTableHeaderRenderer(table));
		
		return table;
	}
	
	static public JTable fixColumnWidth(JTable table, int column, int width) {
		return fixColumnWidth(table, column, width, false);
	}
	
	static public JTable fixColumnWidth(JTable table, int column, int width, boolean resizable) {
		TableColumnModel model = table.getColumnModel();
		
		TableColumn fixed = model.getColumn(column);
		fixed.setMaxWidth(width);
		fixed.setMinWidth(fixed.getMaxWidth());
		fixed.setWidth(fixed.getMaxWidth());
		fixed.setResizable(resizable);
		
		return table;
	}
	
	static public void addValueChangeListener(Component parent, final ActionListener listener) {
		for(Component in : allInputs(parent)) {
			final Component $ource = in;
			
			if(in instanceof JTextComponent) {
				LOG.trace("Adding document listener on text field");
				((JTextComponent)in).getDocument().addDocumentListener(new DocumentListener() {
					@Override
					public void changedUpdate(DocumentEvent e) {
						listener.actionPerformed(new ActionEvent($ource, e.hashCode(), "update"));
					}
					
					@Override
					public void insertUpdate(DocumentEvent e) {
						listener.actionPerformed(new ActionEvent($ource, e.hashCode(), "insert"));
					}
					
					@Override
					public void removeUpdate(DocumentEvent e) {
						listener.actionPerformed(new ActionEvent($ource, e.hashCode(), "remove"));
					}
				});
			} else if(in instanceof JComboBox) {
				LOG.trace("Adding action listener on combo box");
				((JComboBox)in).addActionListener(listener);
			} else if(in instanceof JDateTimePicker) {
				LOG.trace("Adding action listener on datetime picker");
				((JDateTimePicker)in).addActionListener(listener);
			} else if(in instanceof JCheckBox) {
				LOG.trace("Adding action listener on checkbox");
				((JCheckBox)in).addActionListener(listener);
			} else if(in instanceof JRadioButton) {
				LOG.trace("Adding action listener on radio button");
				((JRadioButton)in).addActionListener(listener);
			} else if(in instanceof JSpinner) {
				LOG.trace("Adding action listener on spinner");
				((JSpinner)in).addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent e) {
						listener.actionPerformed(new ActionEvent($ource, e.hashCode(), "change"));
					}	
				});
			} else {
				LOG.debug("Skipping input field of type {}", in.getClass().getName());
			}
		}
	}
	
	static public Component[] allInputs(Component parent) {
		if(parent == null) return new Component[0];
		
		Class<?> clazz = parent.getClass();
		
		Set<Component> inputs = new HashSet<Component>();
		Object field;
		while(clazz != null) {
			for(Field in : clazz.getDeclaredFields()) {
				in.setAccessible(true);
				
				try {
					if(((in.getModifiers() & Modifier.STATIC) != Modifier.STATIC) && isInput(field = in.get(parent)))
						inputs.add((Component)field);
				} catch(Throwable t) {
					LOG.error("Unable to inspect field {} ({}) of {}", in.getName(), in.getType(), parent.getClass(), t);
				}
			}
			
			clazz = clazz.getSuperclass();
		}
		
		return inputs.toArray(new Component[inputs.size()]);
	}
	
	static public boolean isInput(Object toTest) {
		if(toTest == null) return false;
		
		return
			toTest instanceof JButton ||
			toTest instanceof JNoFocusOutlineButton ||
			toTest instanceof JTextField ||
			toTest instanceof JTextArea ||
			toTest instanceof JComboBox ||
			toTest instanceof JSpinner ||
			toTest instanceof JDateTimePicker ||
			toTest instanceof JRadioButton ||
			toTest instanceof JCheckBox;
	}
	
	static public File selectFile(JPanel container, String title, String folder, FileFilter filter, String onCancelMessage) {
		return UIUtils.selectFile(container, JFileChooser.FILES_ONLY, title, folder, filter, onCancelMessage);
	}
	
	static public File selectDirectory(JPanel container, String title, String folder, FileFilter filter, String onCancelMessage) {
		return UIUtils.selectFile(container, JFileChooser.DIRECTORIES_ONLY, title, folder, filter, onCancelMessage);
	}
	
	static public File selectFileOrDirectory(JPanel container, String title, String folder, FileFilter filter, String onCancelMessage) {
		return UIUtils.selectFile(container, JFileChooser.FILES_AND_DIRECTORIES, title, folder, filter, onCancelMessage);
	}
	
	static public File selectFile(JPanel container, final int mode, String title, String folder, FileFilter filter, String onCancelMessage) {
		JFileChooser fileChooser = new JFileChooser(folder == null ? "." : folder) { 
			/** Field serialVersionUID */
			final private static long serialVersionUID = -1848467293660284711L;

			@Override
			public void approveSelection() {
				if(mode == JFileChooser.FILES_ONLY && getSelectedFile().isDirectory()) {
					return;
				} else
					super.approveSelection();
			}
		};
		
		fileChooser.setDialogTitle(title);
		fileChooser.setControlButtonsAreShown(true);
		
		if(filter != null)
			fileChooser.setFileFilter(filter);
		
		fileChooser.setFileSelectionMode(mode);
		
		int result = fileChooser.showDialog(container, "Select");
		
		if(result == JFileChooser.APPROVE_OPTION) {
			return fileChooser.getSelectedFile();
		} else {
			UIUtils.dialog(container, JOptionPane.INFORMATION_MESSAGE, "The action has been canceled", onCancelMessage);
			
			return null;
		}
	}

	public static class FolderFilter extends javax.swing.filechooser.FileFilter {
		@Override
		public boolean accept(File file) {
			return file.isDirectory();
		}

		@Override
		public String getDescription() {
			return "Please select a directory";
		}
	}
	
	static public JFrame findFrame(Component component) {
		if(component != null) {
			if(component instanceof JFrame)
				return (JFrame)component;
			
			while((component = component.getParent()) != null)
				if(component instanceof JFrame)
					return (JFrame)component;
		}
		
		return null;
	}
	
	static public boolean confirm(JFrame frame, String title, String message) {
		return JOptionPane.showConfirmDialog(frame, message, title, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION; 
	}
	
	static public boolean confirm(String title, String message) {
		return UIUtils.confirm(null, title, message); 
	}
	
	static public void dialog(final JFrame frame, final int level, final String title, final String message) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JOptionPane.showMessageDialog(frame, message, title, level);
			}
		});
	}

	static public void dialog(Component component, int level, String title, String message) {
		UIUtils.dialog(UIUtils.findFrame(component), level, title, message);
	}

	static public void dialog(Component component, String title, String message) {
		UIUtils.dialog(UIUtils.findFrame(component), JOptionPane.INFORMATION_MESSAGE, title, message);
	}
	
	static public void dialog(Component component, String message) {
		UIUtils.dialog(UIUtils.findFrame(component), JOptionPane.INFORMATION_MESSAGE, "Information", message);
	}
	
	static public void alert(Component component, String message) {
		UIUtils.alert(UIUtils.findFrame(component), message);
	}
	
	static public void alert(Component component, String title, String message) {
		UIUtils.alert(UIUtils.findFrame(component), title, message);
	}
	
	static public void alert(JFrame frame, String message) {
		UIUtils.alert(frame, "Unable to proceed!", message);
	}

	static public void alert(JFrame frame, String title, String message) {
		UIUtils.dialog(frame, JOptionPane.ERROR_MESSAGE, title, message);
	}
	
	static public void error(JFrame frame, String title, String message, Throwable t) {
		String currentMessage = message == null ? "Unexpected error caught: " + t.getMessage() : message;
		
		ErrorInfo info = new ErrorInfo(title, currentMessage, null, null, t, Level.SEVERE, null);
		
		JXErrorPane.showDialog(frame, info);
		
		LOG.error(message, t);
	}
	
	static public JPanel getCurrentCard(JPanel stacked) {
		for (Component component : stacked.getComponents()) {
			if (component instanceof JPanel && component.isVisible()) {
				return (JPanel)component;
			}
		} 
		
		return null;
	}
	
	static public ImageIcon getFlagIcon(Country country) throws IOException {
		if(!FLAG_ICONS_MAP.containsKey(country)) {
			InputStream stream = null;
			
			if(country != null) {
				stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("media/icons/flags/famfamfam/" + country.getIso2Code().toLowerCase() + ".png");
			}
			
			if(stream != null) {
				FLAG_ICONS_MAP.put(country, toByteArray(stream));
				
				stream.close();
			}
		}
		
		byte[] data = FLAG_ICONS_MAP.get(country);
		
		if(data == null) {
			data = toByteArray(Thread.currentThread().getContextClassLoader().getResourceAsStream("media/icons/flags/famfamfam/transparent.png"));
		}
		
		return new ImageIcon(data);
	}
	
	static byte[] toByteArray(InputStream stream) throws IOException {
		if(stream == null) return new byte[0];
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int len = -1;
		byte[] buffer = new byte[8192];
		
		try {
			while((len = stream.read(buffer)) != -1)
				baos.write(buffer, 0, len);
			
			return baos.toByteArray();
		} finally {
			baos.close();
		}
	}
}