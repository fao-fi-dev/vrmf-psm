/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;

import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

import org.fao.fi.vrmf.psm.logbook.model.utils.CoordinatesHelper;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 10, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 10, 2015
 */
public class JCoordinatesInput extends JFormattedTextField {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5003745699513792350L;

	/**
	 * Class constructor
	 *
	 */
	public JCoordinatesInput() {
		super(createFormatter());
		
		addChangeListener();
	}
	
	static private MaskFormatter createFormatter() {
		try {
			MaskFormatter m = new MaskFormatter("##°##''##''''U;###°##''##''''U");
			m.setPlaceholder("00°00'00''N;000°00'00''E");
			return m;
		} catch(ParseException pe) {
			throw new RuntimeException(pe);
		}
	}
	
	private void addChangeListener() {
		final JCoordinatesInput thi$ = this;
		
		thi$.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
			}
			
			@Override
			public void focusLost(FocusEvent e) {
				String value = convertValue((String)thi$.getText());
				
				if(value != null && !CoordinatesHelper.areCorrect(value)) {
					UIUtils.alert(UIUtils.findFrame(thi$), "The value set for these coordinates is not correct (" + value + ")");
				}
			}
		});
	}
	
	public Object getValue() {
		return convertValue(super.getValue());
	}
	
	private String convertValue(Object value) {
		if(value == null || "00°00'00''0;000°00'00''0".equals(value)) return null;
		
		return (String)value;
	}
}