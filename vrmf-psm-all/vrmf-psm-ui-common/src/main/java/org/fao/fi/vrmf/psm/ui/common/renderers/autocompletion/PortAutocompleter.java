/**
 * (c) 2014 FAO / UN (project: PSM-DTO)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.autocompletion;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Mar 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Mar 2014
 */
public class PortAutocompleter extends BaseAutocompleter<Port> { 
	static final public PortAutocompleter INSTANCE = new PortAutocompleter();
	
	private PortAutocompleter() { };

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.autocompletion.BaseAutocompleter#extract(java.lang.Object)
	 */
	@Override
	protected String[] extract(Port item) {
		return new String[] { item.getName() };
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.autocompletion.BaseAutocompleter#label(java.lang.Object)
	 */
	@Override
	protected String label(Port item) {
		return item.getCountry().getIso2Code() + " - " + item.getName() + " (" + item.getCoordinates() + ")";
	}
}