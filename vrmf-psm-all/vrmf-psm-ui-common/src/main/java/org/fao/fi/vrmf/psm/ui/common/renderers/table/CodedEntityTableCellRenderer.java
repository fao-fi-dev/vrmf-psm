/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.table;

import javax.swing.JTable;

import org.fao.fi.vrmf.psm.logbook.model.spi.CodedEntity;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
public class CodedEntityTableCellRenderer extends AbstractTableCellRenderer<CodedEntity> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7956919455025491518L;

	/**
	 * Class constructor
	 */
	public CodedEntityTableCellRenderer() {
		super(LEFT);
	}

	/**
	 * Class constructor
	 *
	 * @param alignment
	 */
	public CodedEntityTableCellRenderer(int alignment) {
		super(alignment);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.table.AbstractTableCellRenderer#doGetTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	final protected void updateComponent(JTable table, CodedEntity value, boolean isSelected, boolean hasFocus, int row, int column) {
		setText(value == null ? "----" : value.getCode() + " - " + value.getName());
	}
}
