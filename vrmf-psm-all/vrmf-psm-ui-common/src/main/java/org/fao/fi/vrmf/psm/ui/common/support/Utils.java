/**
 * (c) 2014 FAO / UN (project: PSM-UI-support)
 */
package org.fao.fi.vrmf.psm.ui.common.support;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.fao.fi.vrmf.psm.logbook.model.spi.CodedEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Mar 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Mar 2014
 */
final public class Utils {
	static private Logger LOG = LoggerFactory.getLogger(Utils.class);
	
	static public boolean IS_LAUNCHING = false;
	
	private Utils() { };
	
	static public String trim(String toTrim) {
		if(toTrim == null)
			return null;
		
		toTrim = toTrim.trim();
		
		return "".equals(toTrim) ? null : toTrim;
	}
	
	static public <C extends CodedEntity> CodedEntity[] withNullEntry(C[] values) {
		List<C> toReturn = new ArrayList<C>();
		
		toReturn.add(null);
		
		if(values != null)
			for(C value : values) {
				toReturn.add(value);
			}
		
		return toReturn.toArray(new CodedEntity[toReturn.size()]);
	}
	
	static public <O> List<O> listFromObjects(O... objects) {
		return objects == null ? new ArrayList<O>() : new ArrayList<O>(Arrays.asList(objects));
	}
	
	static public <O> List<O> immutableListFromObjects(O... objects) {
		return Arrays.asList(objects);
	}
	
	static public boolean follows(Date from, Date to) {
		return from.after(to) || from.equals(to);
	}
	
	static public boolean preceeds(Date from, Date to) {
		return from.before(to);
	}
	
	static String formatDecimal(Number toFormat) {
		return Utils.formatDecimal(toFormat, "###,###.00");
	}
	
	static String formatDecimal(Number toFormat, String pattern) {
		return new DecimalFormat(pattern).format(toFormat == null ? 0D : toFormat.doubleValue());
	}
	
	static public boolean isConnectionAvailable() {
		try {
			LOG.debug("Checking if Internet connection is available...");
			
			URL url = new URL("http://www.fao.org");
			URLConnection urlConn = url.openConnection();
			urlConn.setConnectTimeout(1000);
			urlConn.setReadTimeout(1000);
			urlConn.setAllowUserInteraction(false);
			urlConn.setDoOutput(true);
			
			urlConn.connect();
			
			LOG.debug("Done. Internet connection is available...");
			
			return true;
		} catch(Throwable t) {
			LOG.error("Done. Internet connection is unavailable or slow: {}", Utils.getErrorMessage(t), t);
			
			return false;
		}
	}

	static public String pad(Number value, int maxDigits) {
		String asString = value.toString();

		while (asString.length() < maxDigits) {
			asString = "0" + asString;
		}

		return asString;
	}
	
	static public String pad(String toPad, int maxLength) {
		return pad(toPad, maxLength, true);
	}
	
	static public String pad(String toPad, int maxLength, boolean right) {
		if(toPad == null) return null;
		
		while(toPad.length() < maxLength)
			if(right) {
				toPad += " ";
			} else {
				toPad = " " + toPad;
			}
		
		return toPad;
	}

	static public Object coalesce(Object... values) {
		if (values == null || values.length == 0)
			return null;

		for (Object in : values)
			if (in != null)
				return in;
		

		return null;
	}

	static public String getErrorMessage(Throwable t) {
		String message = t.getMessage();

		return t.getClass().getSimpleName() + (message == null ? "" : " : " + message);
	}

	static public String read(String filePath) throws IOException {
		if (filePath == null)
			throw new IllegalArgumentException("File path can't be null");

		File in = new File(filePath);

		if (in.exists() && in.isFile() && in.canRead()) {
			FileInputStream fis = null;
			ByteArrayOutputStream baos = null;

			byte[] buffer = new byte[8192];
			int len = -1;

			try {
				fis = new FileInputStream(in);
				baos = new ByteArrayOutputStream();

				while ((len = fis.read(buffer)) != -1)
					baos.write(buffer, 0, len);

				fis.close();
				baos.flush();
				baos.close();

				return new String(baos.toByteArray(), "UTF-8");
			} catch (IOException IOe) {
				throw IOe;
			} catch (Throwable t) {
				LOG.error("Unable to read content from {}: {}", filePath, t.getMessage(), t);

				return null;
			}
		}

		throw new IOException("Either file " + filePath + " doesn't exist or it is not a file or it can't be read");
	}

	static public File store(String content, String filePath) throws IOException {
		if (content == null)
			return null;

		if (filePath == null)
			throw new IllegalArgumentException("File path can't be null");

		File out = new File(filePath);

		if (!out.exists() || out.canWrite()) {
			FileOutputStream fos = null;

			try {
				fos = new FileOutputStream(out);

				fos.write(content.getBytes("UTF-8"));

				fos.flush();
				fos.close();

				return out;
			} catch (IOException IOe) {
				throw IOe;
			} catch (Throwable t) {
				LOG.error("Unable to store content in {}: {}", filePath, t.getMessage(), t);

				return null;
			}
		}

		throw new IOException("File " + filePath + " can't be written");
	}
	
	static public Calendar fromDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date.getTime());
		
		return calendar;
	}

	static public void unzipStream(InputStream stream, String outputFolder) throws IOException {
		byte[] buffer = new byte[8192];
	
		// create output directory is not exists
		File folder = new File(outputFolder); 
		
		if (!folder.exists())
			folder.mkdir();

		ZipInputStream zis = new ZipInputStream(stream);
		
		ZipEntry ze = zis.getNextEntry();

		while (ze != null) {

			String fileName = ze.getName();
			File newFile = new File(outputFolder + File.separator + fileName);

			LOG.debug("Unzipping file {}", newFile.getAbsolutePath());

			new File(newFile.getParent()).mkdirs();

			FileOutputStream fos = new FileOutputStream(newFile);

			int len;
			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}

			fos.close();
			ze = zis.getNextEntry();
		}

		zis.closeEntry();
		zis.close();
	}
	
	static public int size(Object[] array) {
		return array == null ? 0 : array.length;
	}
	
	static public int size(Collection<?> collection) {
		return collection == null ? 0 : collection.size();
	}
}
