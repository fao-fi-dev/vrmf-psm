/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.model.utils.CoordinatesHelper;
import org.fao.fi.vrmf.psm.ui.common.spi.CoordinatesChangeListener;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.jxmapviewer.JXMapKit;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.TileFactory;
import org.jxmapviewer.viewer.Waypoint;
import org.jxmapviewer.viewer.WaypointPainter;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 18, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 18, 2015
 */
public class JCoordinatesSelector extends JPanel {
	static private Color TEXT_COLOR = Color.BLUE;
	
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4609716554257996115L;
	
	@Getter private JXMapKit mapPanel;
	@Getter private JTextField latitudeInput;
	@Getter private JTextField longitudeInput;
	
	private TileFactory tileFactory;
	private CustomWaypointPainter painter;
	
	private List<CoordinatesChangeListener> listeners = new ArrayList<CoordinatesChangeListener>();
	
	/**
	 * Class constructor
	 *
	 */
	public JCoordinatesSelector() {
		this(new DefaultTileFactory(new EmbeddedTileFactoryInfo()), null);
	}
	
	/**
	 * Class constructor
	 *
	 * @param tileFactory
	 */
	public JCoordinatesSelector(TileFactory tileFactory) {
		this(tileFactory, null);
	}
	
	/**
	 * Class constructor
	 *
	 * @param coordinates
	 */
	public JCoordinatesSelector(String coordinates) {
		this(new DefaultTileFactory(new EmbeddedTileFactoryInfo()), coordinates);
	}
	
	/**
	 * Class constructor
	 *
	 * @param tileFactory
	 * @param coordinates
	 */
	public JCoordinatesSelector(TileFactory tileFactory, String coordinates) {
		this.tileFactory = tileFactory;
		
		GeoPosition center = coordinates == null ? new GeoPosition(0, 0) : new GeoPosition(CoordinatesHelper.extractLatitude(coordinates), CoordinatesHelper.extractLongitude(coordinates));
		
		painter = new CustomWaypointPainter(coordinates == null ? null : center);

		initialize();

		mapPanel.getMainMap().setCenterPosition(center);
	}
	
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		mapPanel = new JXMapKit();
		mapPanel.setBorder(new TitledBorder(null, "Center the map on the desired coordinates", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		mapPanel.setTileFactory(new DefaultTileFactory(new LazyCachingTileFactoryInfo()));
		
		mapPanel.setMiniMapVisible(false);
		mapPanel.setZoomSliderVisible(false);
		mapPanel.setZoomButtonsVisible(false);
		mapPanel.getMainMap().setRestrictOutsidePanning(false);
		mapPanel.getMainMap().setHorizontalWrapped(false);
		
		GridBagLayout gridBagLayout_1 = (GridBagLayout) mapPanel.getLayout();
		gridBagLayout_1.columnWeights = new double[0];
		GridBagConstraints gbc_mapPanel = new GridBagConstraints();
		gbc_mapPanel.insets = new Insets(0, 5, 0, 5);
		gbc_mapPanel.weighty = 100.0;
		gbc_mapPanel.weightx = 100.0;
		gbc_mapPanel.fill = GridBagConstraints.BOTH;
		gbc_mapPanel.gridx = 0;
		gbc_mapPanel.gridy = 0;
		add(mapPanel, gbc_mapPanel);
		
		JPanel inputPanel = new JPanel();
		GridBagLayout inputPanelGridBagLayout = new GridBagLayout();
		inputPanelGridBagLayout.columnWidths = new int[0];
		inputPanelGridBagLayout.rowHeights = new int[0];
		inputPanelGridBagLayout.columnWeights = new double[0];
		inputPanelGridBagLayout.rowWeights = new double[0];
		inputPanel.setLayout(inputPanelGridBagLayout);

		GridBagConstraints gbc_inputPanel = new GridBagConstraints();
		gbc_inputPanel.gridx = 0;
		gbc_inputPanel.gridy = 1;
		gbc_inputPanel.weightx = 100.0;
		
		add(inputPanel, gbc_inputPanel);
		
		JLabel lblLatitude = new JLabel("Latitude");
		lblLatitude.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		GridBagConstraints gbc_lblLatitude = new GridBagConstraints();
		gbc_lblLatitude.insets = new Insets(5, 5, 5, 5);
		gbc_lblLatitude.anchor = GridBagConstraints.WEST;
		gbc_lblLatitude.gridx = 0;
		gbc_lblLatitude.gridy = 0;
		inputPanel.add(lblLatitude, gbc_lblLatitude);
		
		latitudeInput = new JTextField();
		latitudeInput.setColumns(8);
		latitudeInput.setEditable(false);
		GridBagConstraints gbc_latitudeInput = new GridBagConstraints();
		gbc_latitudeInput.weightx = 100.0;
		gbc_latitudeInput.ipady = 5;
		gbc_latitudeInput.insets = new Insets(5, 5, 5, 5);
		gbc_latitudeInput.anchor = GridBagConstraints.WEST;
		gbc_latitudeInput.fill = GridBagConstraints.HORIZONTAL;
		gbc_latitudeInput.gridx = 1;
		gbc_latitudeInput.gridy = 0;
		inputPanel.add(latitudeInput, gbc_latitudeInput);
		
		JLabel lblLongitude = new JLabel("Longitude");
		lblLongitude.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		GridBagConstraints gbc_lblLongitude = new GridBagConstraints();
		gbc_lblLongitude.insets = new Insets(5, 5, 5, 5);
		gbc_lblLongitude.gridx = 2;
		gbc_lblLongitude.gridy = 0;
		inputPanel.add(lblLongitude, gbc_lblLongitude);
		
		longitudeInput = new JTextField();
		longitudeInput.setColumns(8);
		longitudeInput.setEditable(false);
		GridBagConstraints gbc_longitudeInput = new GridBagConstraints();
		gbc_longitudeInput.weightx = 100.0;
		gbc_longitudeInput.ipady = 5;
		gbc_longitudeInput.insets = new Insets(5, 5, 5, 5);
		gbc_longitudeInput.anchor = GridBagConstraints.WEST;
		gbc_longitudeInput.fill = GridBagConstraints.HORIZONTAL;
		gbc_longitudeInput.gridx = 3;
		gbc_longitudeInput.gridy = 0;
		inputPanel.add(longitudeInput, gbc_longitudeInput);
		
		JButton btnConfirm = new JButton("Confirm");
		GridBagConstraints gbc_btnConfirm = new GridBagConstraints();
		gbc_btnConfirm.insets = new Insets(5, 5, 5, 5);
		gbc_btnConfirm.anchor = GridBagConstraints.EAST;
		gbc_btnConfirm.gridx = 4;
		gbc_btnConfirm.gridy = 0;
		inputPanel.add(btnConfirm, gbc_btnConfirm);
		
		btnConfirm.addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				for(CoordinatesChangeListener in : listeners)
					in.coordinatesSelected(latitudeInput.getText() + ";" + longitudeInput.getText());
			}
		});
		
		JButton btnCancel = new JButton("Cancel");
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.anchor = GridBagConstraints.WEST;
		gbc_btnCancel.insets = new Insets(5, 5, 5, 5);
		gbc_btnCancel.gridx = 5;
		gbc_btnCancel.gridy = 0;
		inputPanel.add(btnCancel, gbc_btnCancel);
		
		btnCancel.addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				for(CoordinatesChangeListener in : listeners)
					in.coordinatesCanceled();
			}
		});
		
		final JXMapViewer mainMap = mapPanel.getMainMap();
		
		mainMap.setZoom(13);
		
		if(tileFactory != null) mainMap.setTileFactory(tileFactory);

		mainMap.setZoom(13);

		mainMap.setOverlayPainter(painter);
		
		updateCoordinates();
	}
	
	public void addCoordinatesChangeListener(CoordinatesChangeListener listener) {
		if(listener != null) listeners.add(listener);
	}
	
	public void center(String coordinates) {
		JXMapViewer mainMap = mapPanel.getMainMap();
		painter.initialPosition = new GeoPosition(CoordinatesHelper.extractLatitude(coordinates), CoordinatesHelper.extractLongitude(coordinates));
		
		mainMap.setCenterPosition(painter.initialPosition);
		
		updateCoordinates();
	}
	
	private void updateCoordinates() {
		JXMapViewer mainMap = mapPanel.getMainMap();
		
		mainMap.setOverlayPainter(painter);
		
		GeoPosition center = mainMap.getCenterPosition();
		
		latitudeInput.setText(CoordinatesHelper.latitudeFromValue(center.getLatitude()));
		longitudeInput.setText(CoordinatesHelper.longitudeFromValue(center.getLongitude()));
	}
	
	static final public void main(String[] args) throws Throwable {
		JFrame frame = new JFrame();
		frame.setSize(new Dimension(640, 480));
		
		GridBagLayout mainLayout = new GridBagLayout();
		mainLayout.rowHeights = new int[0];
		mainLayout.columnWeights = new double[0];
		mainLayout.rowWeights = new double[0];
		
		frame.getContentPane().setLayout(mainLayout);
		
		JCoordinatesSelector s = new JCoordinatesSelector();
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 100.0;
		gbc.weighty = 100.0;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		frame.getContentPane().add(s, gbc);
		
		s.setVisible(true);
		
		frame.setVisible(true);
		frame.toFront();
		frame.repaint();;
	}
	
	private class CustomWaypointPainter extends WaypointPainter<Waypoint> {
		@Setter private GeoPosition initialPosition;
		
		public CustomWaypointPainter(GeoPosition initialPosition) {
			this.initialPosition = initialPosition;
		}
		
		@SuppressWarnings("unused")
		public CustomWaypointPainter(String coordinates) {
			initialPosition = new GeoPosition(CoordinatesHelper.extractLatitude(coordinates), CoordinatesHelper.extractLongitude(coordinates));
		}
		
		protected void doPaint(Graphics2D g, JXMapViewer mainMap, int width, int height) {
			Rectangle rect = mainMap.getViewportBounds();
			g.translate(-rect.x, -rect.y);
			
			Point2D initial = mainMap.getTileFactory().geoToPixel(initialPosition == null ? new GeoPosition(0, 0) : initialPosition, mainMap.getZoom());
			int x = (int)initial.getX();
			int y = (int)initial.getY();
			
			BufferedImage img = null;

			String toDraw = "Old coordinates";

			if(initialPosition != null) {
				g.setFont(new Font("Tahoma", Font.BOLD, 12));
				
				try {
					img = ImageIO.read(Thread.currentThread().getContextClassLoader().getResource("media/icons/maps/redDot.png"));
	
					g.drawImage(img, x - ( img.getWidth() / 2 ), y - ( img.getHeight() / 2 ), null);
					
					g.setColor(TEXT_COLOR);

					g.drawString(toDraw, x - 10, y + img.getHeight());
				} catch(Throwable t) {
					g.drawLine(x-5, y-5, x+5, y+5);
					g.drawLine(x-5, y+5, x+5, y-5);

					g.setColor(TEXT_COLOR);

					g.drawString(toDraw, x - 10, y + 10);
				}
			}
			
			Point2D center = mainMap.getTileFactory().geoToPixel(mainMap.getCenterPosition(), mainMap.getZoom());
			
			if(initialPosition != null) {
				g.setColor(Color.BLUE);
				
				g.drawLine(x, y, x = (int)center.getX(), y = (int)center.getY());
			}
			
			x = (int)center.getX();
			y = (int)center.getY();
			
			toDraw = "New coordinates";
			
			g.setFont(new Font("Tahoma", Font.BOLD, 12));
			
			try {
				img = ImageIO.read(Thread.currentThread().getContextClassLoader().getResource("media/icons/maps/greenDot.png"));

				g.drawImage(img, x - ( img.getWidth() / 2 ), y - ( img.getHeight() / 2 ), null);
				
				g.setColor(TEXT_COLOR);

				g.drawString(toDraw, x - 10, y - img.getHeight());
			} catch(Throwable t) {
				g.drawLine(x-5, y-5, x+5, y+5);
				g.drawLine(x-5, y+5, x+5, y-5);
				
				g.setColor(TEXT_COLOR);

				g.drawString(toDraw, x - 10, y - 10);
			}
			
			updateCoordinates();
		}
	}
}
