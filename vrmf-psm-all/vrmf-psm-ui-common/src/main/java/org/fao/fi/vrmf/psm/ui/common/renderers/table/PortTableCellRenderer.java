/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.table;

import javax.swing.JTable;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
public class PortTableCellRenderer extends AbstractTableCellRenderer<Port> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8392701145128902013L;

	/**
	 * Class constructor
	 */
	public PortTableCellRenderer() {
		super(LEFT);
	}

	/**
	 * Class constructor
	 *
	 * @param alignment
	 */
	public PortTableCellRenderer(int alignment) {
		super(alignment);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.table.AbstractTableCellRenderer#doGetTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	protected void updateComponent(JTable table, Port value, boolean isSelected, boolean hasFocus, int row, int column) {
		setText(value == null ? "----" : value.getName()); 
		
		try {
			setIcon(UIUtils.getFlagIcon(value == null ? null : value.getCountry()));
		} catch(Throwable t) {
			throw new RuntimeException(t);
		}
	}
}
