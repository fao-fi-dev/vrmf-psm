/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.table;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- ----------------------- Date Author Comment ------------- --------------- ----------------------- Dec 1, 2015 Fabio Creation.
 *
 * @version 1.0
 * @since Dec 1, 2015
 */
public class DefaultTableHeaderRenderer implements TableCellRenderer {
	DefaultTableCellRenderer renderer;

	public DefaultTableHeaderRenderer(JTable table) {
		renderer = (DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer();
		renderer.setHorizontalAlignment(JLabel.CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
		return renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
	}
}
