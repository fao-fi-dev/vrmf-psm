/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers;

import java.awt.Component;
import java.io.IOException;

import javax.swing.JList;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultListCellRenderer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class CountryRenderer extends SubstanceDefaultListCellRenderer {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8202115316274411241L;

	/**
	 * Class constructor
	 */
	public CountryRenderer() {
		setOpaque(true);
	}

	/* (non-Javadoc)
	 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
	 */
	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		Country current = (Country) value;

		if(current == null)
			setText("Please select");
		else 
			setText((current.getIso2Code() == null ? "" : current.getIso2Code() + " - ") + current.getName());
		
		try {
			setIcon(UIUtils.getFlagIcon(current));
		} catch(IOException t) {
			throw new RuntimeException(t);
		}
		
		return this;
	}
}
