/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components;

import java.io.File;

import org.jxmapviewer.viewer.TileFactoryInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 16, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 16, 2015
 */
@Slf4j
public class EmbeddedTileFactoryInfo extends TileFactoryInfo {	
	static private String CURRENT_FOLDER = System.getProperty("current.dir");
	static private String MAP_TYPE = "stamen";
	static private String TILES_ARCHIVE_FOLDER = ( CURRENT_FOLDER == null ? "" : CURRENT_FOLDER ) + "maps/offline/" + MAP_TYPE;
	
	/**
	 * Class constructor
	 */
	public EmbeddedTileFactoryInfo() {
		super(
			1, //min level
			19 - 2, //max allowed level
			19, // max level
			256, //tile size
			true, true, // x/y orientation is normal
			TILES_ARCHIVE_FOLDER == null ? null : new File(TILES_ARCHIVE_FOLDER).toURI().toString(), // base url
			"x","y","z" // url args for x, y &amp; z
		);
	}
	
	public String getTileUrl(int x, int y, int zoom) {
		int actualZoom = 19 - zoom;
		
		try {
			String file = actualZoom + File.separator + x + File.separator + y + ".png";
			
			File tile = new File(TILES_ARCHIVE_FOLDER + File.separator + "additional" + File.separator + file);
			
			//The tile is part of the additional data downloaded locally
			if(tile.exists()) {
				return tile.toURI().toURL().toString();
			} else {
				log.debug("Tile for ({}, {}, {}) is not available offline as an additional tile under {}: checking embedded folder for requested tile...", x, y, actualZoom, tile.getAbsolutePath());
			} 			
			
			tile = new File(TILES_ARCHIVE_FOLDER + file);

			if(tile.exists()) {
				log.debug("Tile for ({}, {}, {}) is available as an embedded tile under {}", x, y, actualZoom, tile.getAbsolutePath());

				return tile.toURI().toURL().toString();
			} else {
				//Return the embedded 'missing' tile...
				return Thread.currentThread().getContextClassLoader().getResource("media/maps/offline/missing.png").toString();
			}
		} catch(Throwable t) {
			throw new RuntimeException(t);
		}
	}	
}
