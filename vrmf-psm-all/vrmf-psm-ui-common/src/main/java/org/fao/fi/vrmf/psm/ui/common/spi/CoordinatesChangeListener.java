/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.spi;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 18, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 18, 2015
 */
public interface CoordinatesChangeListener {
	void coordinatesSelected(String coordinates);
	void coordinatesCanceled();
}
