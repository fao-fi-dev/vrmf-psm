/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers;

import java.awt.Component;
import java.io.IOException;

import javax.swing.JList;

import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultListCellRenderer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
public class VesselDataRenderer extends SubstanceDefaultListCellRenderer {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8392701145128902013L;

	/**
	 * Class constructor
	 */
	public VesselDataRenderer() {
		setOpaque(true);
	}

	/* (non-Javadoc)
	 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
	 */
	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		VesselData current = (VesselData)value;

		if(current == null)
			setText("Please select");
		else 
			setText(current.getName() + " [ " + current.getId() + " - " + current.getType().getCode() + " / " + current.getIrcs() + " / " + current.getRegistrationNumber() + " @ " + current.getRegistrationCountry().getIso2Code() + " ]");
		
		try {
			setIcon(UIUtils.getFlagIcon(current == null ? null : current.getRegistrationCountry()));
		} catch(IOException t) {
			throw new RuntimeException(t);
		}	
		
		return this;
	}
}
