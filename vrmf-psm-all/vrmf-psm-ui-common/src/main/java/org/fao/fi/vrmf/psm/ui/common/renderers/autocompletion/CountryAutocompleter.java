/**
 * (c) 2014 FAO / UN (project: PSM-DTO)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.autocompletion;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Mar 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Mar 2014
 */
public class CountryAutocompleter extends BaseAutocompleter<Country> { 
	static final public CountryAutocompleter INSTANCE = new CountryAutocompleter();
	
	private CountryAutocompleter() { };
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.autocompletion.BaseAutocompleter#extract(java.lang.Object)
	 */
	@Override
	protected String[] extract(Country item) {
		return new String[] { item.getName() };
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.autocompletion.BaseAutocompleter#label(java.lang.Object)
	 */
	@Override
	protected String label(Country item) {
		return item.getName();
	}
}