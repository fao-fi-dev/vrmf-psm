/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components;

import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.fao.fi.vrmf.psm.ui.common.spi.CoordinatesChangeListener;
import org.jxmapviewer.viewer.TileFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 18, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 18, 2015
 */
public class JCoordinatesSelectorDialog extends JDialog implements CoordinatesChangeListener {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2420340896763283555L;

	public JCoordinatesSelectorDialog(JFrame parent, TileFactory tileFactory, String coordinates, CoordinatesChangeListener listener) {
		super(parent, "Please select a coordinate", true);
		
		if(parent != null) {
			Dimension parentSize = parent.getSize(); 
			Point p = parent.getLocation(); 
			setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
			
			Dimension size = new Dimension(parentSize.width / 2, parentSize.height / 2);
			setSize(size);
			setPreferredSize(size);
			setMinimumSize(size);
		}
		
		JCoordinatesSelector selector = new JCoordinatesSelector(tileFactory, coordinates);
		selector.addCoordinatesChangeListener(listener);
		selector.addCoordinatesChangeListener(this);
		
		getContentPane().add(selector);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		pack(); 
		
		setVisible(true);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.CoordinatesChangeListener#coordinatesSelected(java.lang.String)
	 */
	@Override
	public void coordinatesSelected(String coordinates) {
		coordinatesCanceled();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.spi.CoordinatesChangeListener#coordinatesCanceled()
	 */
	@Override
	public void coordinatesCanceled() {
		setVisible(false);
		dispose();
	}
}
