/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components.handlers;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 15, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 15, 2015
 */
public abstract class InterceptingActionListener implements ActionListener {
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	final public void actionPerformed(ActionEvent e) {
		try {
			doActionPerformed(e);
		} catch(Throwable t) {
			UIUtils.error(UIUtils.findFrame((Component)e.getSource()), "Unhandled exception caught", t.getMessage(), t);
		}
	}
	
	abstract protected void doActionPerformed(ActionEvent e) throws Throwable;
}
