/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components.binding;

import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.fao.fi.vrmf.psm.ui.common.support.components.JCoordinatesInput;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;

import no.tornado.databinding.converter.ConversionException;
import no.tornado.databinding.uibridge.UIBridge;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 30, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 30, 2015
 */
public class JCoordinatesInputUIBridge implements UIBridge<JCoordinatesInput, ActionListener, String> {
	static public JCoordinatesInputUIBridge INSTANCE = new JCoordinatesInputUIBridge();

	private JCoordinatesInputUIBridge() { }

	public Class<JCoordinatesInput> getUIClass() {
		return JCoordinatesInput.class;
	}

	public Class<? extends String> getUIValueType() {
		return String.class;
	}

	public void setUIValue(JCoordinatesInput component, String value) throws ConversionException {
		component.setValue(value);
	}

	public String getUIValue(JCoordinatesInput component) throws ConversionException {
		return (String)component.getValue();
	}

	//SEE: http://databinding.tornado.no/
	public ActionListener addValueChangeListener(final JCoordinatesInput component, final ChangeListener listener) {
		ActionListener actionListener = new InterceptingActionListener() {
			protected void doActionPerformed(java.awt.event.ActionEvent e) {
				listener.stateChanged(new ChangeEvent(e.getSource()));
			};
		};
		
		return actionListener;
	}

	public void removeValueChangelistener(JCoordinatesInput component, ActionListener listener) {
		component.removeActionListener(listener);
	}
}
