/**
 * (c) 2014 FAO / UN (project: PSM-UI-support)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.utils.CoordinatesHelper;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.jxmapviewer.JXMapKit;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.DefaultWaypoint;
import org.jxmapviewer.viewer.DefaultWaypointRenderer;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.Waypoint;
import org.jxmapviewer.viewer.WaypointPainter;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Mar 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Mar 2014
 */
public class JMapPanel extends JPanel {
	static private Color WAYPOINT_COLOR = Color.BLUE;
	static private Color LINE_COLOR = Color.GREEN;
	static private Color TEXT_COLOR = Color.BLUE;
	
	/** Field serialVersionUID */
	private static final long serialVersionUID = -348496597772532180L;
	
	private Logsheet logsheet;
	private int event = 0;
	
	private JPanel mapWrapper;
	
	@Getter private JXMapKit map;
	@Getter private JPanel controls;
	@Getter private JNoFocusOutlineButton first, last, increase, decrease;
	@Getter private JLabel totalEvents;
	@Getter private JLabel currentEvent;
	
	public JMapPanel() {
		initialize();
	}
	
	final private Comparator<Waypoint> CUSTOM_WAYPOINT_COMPARATOR = new Comparator<Waypoint>() {
		@Override
		public int compare(Waypoint o1, Waypoint o2) {
			CustomWaypoint w1 = (CustomWaypoint)o1;
			CustomWaypoint w2 = (CustomWaypoint)o2;
			
			return w1.compareTo(w2);
		}
	};
	
	private void initialize() {
		GridBagLayout gbl_map = new GridBagLayout();
		gbl_map.rowHeights = new int[] {0, 16};
		gbl_map.rowWeights = new double[0];
		gbl_map.columnWeights = new double[0];

		setLayout(gbl_map);
		
		mapWrapper = new JPanel();
		mapWrapper.setBorder(null);
		
		GridBagConstraints gbc_mapWrapper = new GridBagConstraints();
		gbc_mapWrapper.weighty = 100.0;
		gbc_mapWrapper.weightx = 100.0;
		gbc_mapWrapper.fill = GridBagConstraints.BOTH;
		gbc_mapWrapper.gridx = 0;
		gbc_mapWrapper.gridy = 0;
		add(mapWrapper, gbc_mapWrapper);
		
		GridBagLayout gbl_mapWrapper = new GridBagLayout();
		gbl_mapWrapper.rowWeights = new double[0];
		gbl_mapWrapper.columnWeights = new double[0];
		
		mapWrapper.setLayout(gbl_mapWrapper);
		
		map = new JXMapKit();
		map.setDataProviderCreditShown(true);
		map.setMiniMapVisible(false);
		map.setZoomButtonsVisible(false);
		map.setZoomSliderVisible(false);
		
//		map.setDefaultProvider(DefaultProviders.OpenStreetMaps);
		map.setTileFactory(new DefaultTileFactory(new LazyCachingTileFactoryInfo()));
		
		map.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));

		GridBagConstraints gbc__map = new GridBagConstraints();
		gbc__map.insets = new Insets(5, 5, 5, 5);
		gbc__map.fill = GridBagConstraints.BOTH;
		gbc__map.gridx = 0;
		gbc__map.gridy = 0;
		gbc__map.weighty = 100.0;
		gbc__map.weightx = 100.0;
		
		map.setZoom(16);

		map.getMainMap().setRestrictOutsidePanning(true);
		map.getMainMap().setHorizontalWrapped(false);
		
		map.getMainMap().setOverlayPainter(getNewPainter());
		
		mapWrapper.add(map, gbc__map);
		
		controls = new JPanel();
		GridBagConstraints gbc_controls = new GridBagConstraints();
		gbc_controls.anchor = GridBagConstraints.CENTER;
		gbc_controls.fill = GridBagConstraints.VERTICAL;
		gbc_controls.gridx = 0;
		gbc_controls.gridy = 1;
		gbc_controls.weightx = 100.0;
		
		add(controls, gbc_controls);
		GridBagLayout gbl_controls = new GridBagLayout();
		gbl_controls.columnWeights = new double[0];
		gbl_controls.rowWeights = new double[0];
		controls.setLayout(gbl_controls);
		
		JLabel lblNavigateEvents = new JLabel("Events navigator");
		lblNavigateEvents.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNavigateEvents = new GridBagConstraints();
		gbc_lblNavigateEvents.insets = new Insets(5, 5, 5, 5);
		gbc_lblNavigateEvents.gridx = 0;
		gbc_lblNavigateEvents.gridy = 0;
		controls.add(lblNavigateEvents, gbc_lblNavigateEvents);
						
		first = new JNoFocusOutlineButton("<<");
		first.setToolTipText("Go to first event");
		GridBagConstraints gbc_first = new GridBagConstraints();
		gbc_first.insets = new Insets(5, 5, 5, 5);
		gbc_first.gridx = 1;
		gbc_first.gridy = 0;
		controls.add(first, gbc_first);
		
		decrease = new JNoFocusOutlineButton("<");
		decrease.setToolTipText("Go to previous event");
		GridBagConstraints gbc_decrease = new GridBagConstraints();
		gbc_decrease.insets = new Insets(5, 5, 5, 5);
		gbc_decrease.gridx = 2;
		gbc_decrease.gridy = 0;
		controls.add(decrease, gbc_decrease);
		
		currentEvent = new JLabel("[ NOT SET ]");
		GridBagConstraints gbc_currentEvent = new GridBagConstraints();
		gbc_currentEvent.insets = new Insets(5, 5, 5, 5);
		gbc_currentEvent.gridx = 3;
		gbc_currentEvent.gridy = 0;
		controls.add(currentEvent, gbc_currentEvent);
		
		JLabel lblSeparator = new JLabel("/");
		GridBagConstraints gbc_lblSeparator = new GridBagConstraints();
		gbc_lblSeparator.insets = new Insets(5, 5, 5, 5);
		gbc_lblSeparator.gridx = 4;
		gbc_lblSeparator.gridy = 0;
		controls.add(lblSeparator, gbc_lblSeparator);
		
		totalEvents = new JLabel("[ NOT SET ]");
		GridBagConstraints gbc_totalEvents = new GridBagConstraints();
		gbc_totalEvents.insets = new Insets(5, 5, 5, 5);
		gbc_totalEvents.gridx = 5;
		gbc_totalEvents.gridy = 0;
		controls.add(totalEvents, gbc_totalEvents);
		
		increase = new JNoFocusOutlineButton(">");
		increase.setToolTipText("Go to next event");
		GridBagConstraints gbc_increase = new GridBagConstraints();
		gbc_increase.insets = new Insets(5, 5, 5, 5);
		gbc_increase.gridx = 6;
		gbc_increase.gridy = 0;
		controls.add(increase, gbc_increase);
		
		last = new JNoFocusOutlineButton(">>");
		last.setToolTipText("Go to last event");
		GridBagConstraints gbc_last = new GridBagConstraints();
		gbc_last.insets = new Insets(5, 5, 5, 5);
		gbc_last.gridx = 65;
		gbc_last.gridy = 0;
		controls.add(last, gbc_last);
		
		first.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				firstEvent();
			}
		});
		
		increase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nextEvent();
			}
		});
		
		decrease.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				previousEvent();
			}
		});
		
		last.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lastEvent();
			}
		});
		
		if(!Utils.IS_LAUNCHING) return;
		
		map.setTileFactory(new DefaultTileFactory(new EmbeddedTileFactoryInfo()));
	}
	
	private void firstEvent() {
		event = 0;
			
		updateMap();
	}
	
	private void nextEvent() {
		event++;
			
		updateMap();
	}
	
	private void previousEvent() {
		event--;
		
		updateMap();
	}
	
	private void lastEvent() {
		event = logsheet.getEvents().size() * 2 - 1;
			
		updateMap();
	}
	
	private class CustomWaypoint extends DefaultWaypoint implements Comparable<CustomWaypoint> {
		@Getter private boolean drawText;
		@Getter private int ordinal;
		@Getter private Event event;
		@Getter private boolean start;
		
		@Getter private String customLabel;

		public CustomWaypoint(int ordinal, double latitude, double longitude, String customLabel) {
			super(latitude, longitude);
			
			drawText = true;
			this.ordinal = ordinal;
			this.customLabel = customLabel;
		}
		
		/**
		 * Class constructor
		 *
		 * @param ordinal
		 * @param latitude
		 * @param longitude
		 * @param event
		 * @param start
		 */
		public CustomWaypoint(int ordinal, double latitude, double longitude, Event event, boolean start, boolean drawText) {
			super(latitude, longitude);
			
			this.ordinal = ordinal;
			this.event = event;
			this.customLabel = null;
			this.start = start;
			this.drawText = drawText;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(CustomWaypoint o) {
			return ordinal - o.ordinal;
		}
		
		public String getEventLabel() {
			if(event == null) return ordinal + ": [ NO EVENT SET ]";
			
			return ordinal + ": " + 
				   event.getType().getCode() + " - " + 
				   event.getType().getName() + " [ " +
				  (start ? "START" : "END" ) + " ] " +
				   " @ " + atDate(start ? event.getStartDate() : event.getEndDate() );
		}
	}
	
	private class CustomWaypointRenderer extends DefaultWaypointRenderer {
		@Override
		public void paintWaypoint(Graphics2D g, JXMapViewer map, Waypoint waypoint) {
			g.setColor(WAYPOINT_COLOR);
			g.drawLine(-5, -5, +5, +5);
			g.drawLine(-5, +5, +5, -5);
		};
	}
	
	final static private String atDate(Date date) {
		return date == null ? "[ DATE NOT SET ]" : new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date);
	}
	
	private void updateMap() {
		Set<Waypoint> waypoints = new TreeSet<Waypoint>(CUSTOM_WAYPOINT_COMPARATOR);

		List<Event> events = logsheet == null ? null : logsheet.getEvents();

		if(events == null || events.isEmpty()) {
			first.setEnabled(false);
			decrease.setEnabled(false);
			increase.setEnabled(false);
			last.setEnabled(false);
			
			totalEvents.setText("[ UNAVAILABLE ]");
			currentEvent.setText("[ UNAVAILABLE ]");
		} else {
			first.setEnabled(event > 0);
			decrease.setEnabled(event > 0);
			increase.setEnabled(event < ( events.size() * 2 - 1));
			last.setEnabled(event < ( events.size() * 2 - 1));
			totalEvents.setText(String.valueOf(events.size() * 2));
			currentEvent.setText(String.valueOf(event + 1));
		}
		
		if(logsheet != null) {
			int ordinal = 0;
			
			Port departure = logsheet.getDeparturePort();
			Port arrival = logsheet.getUnloadingPort();
			
			if(departure != null && departure.getCoordinates() != null)
				waypoints.add(new CustomWaypoint(ordinal++, 
												 CoordinatesHelper.extractLatitude(departure.getCoordinates()), 
												 CoordinatesHelper.extractLongitude(departure.getCoordinates()), 
												 "LEAVING " + departure.getName() + " @ " + atDate(logsheet.getDepartureDate())));
			
			if(events != null) {
				int currentEvent = 0;
				for(Event in : events) {
					if(in.getCoordinatesAtStart() != null)
						waypoints.add(new CustomWaypoint(ordinal++, CoordinatesHelper.extractLatitude(in.getCoordinatesAtStart()), CoordinatesHelper.extractLongitude(in.getCoordinatesAtStart()), in, true, event == ( currentEvent * 2 )));
					
					if(in.getCoordinatesAtEnd() != null)
						waypoints.add(new CustomWaypoint(ordinal++, CoordinatesHelper.extractLatitude(in.getCoordinatesAtEnd()), CoordinatesHelper.extractLongitude(in.getCoordinatesAtEnd()), in, false, event == ( currentEvent * 2 + 1 )));
					
					if(event == currentEvent * 2)
						map.setCenterPosition(new GeoPosition( CoordinatesHelper.extractLatitude(in.getCoordinatesAtStart()), CoordinatesHelper.extractLongitude(in.getCoordinatesAtStart())));
					else if(event == (currentEvent * 2 + 1))
						map.setCenterPosition(new GeoPosition( CoordinatesHelper.extractLatitude(in.getCoordinatesAtEnd()), CoordinatesHelper.extractLongitude(in.getCoordinatesAtEnd())));
					
					currentEvent++;
				}
			}
			
			if(arrival != null && arrival.getCoordinates() != null)
				waypoints.add(new CustomWaypoint(ordinal++, 
												 CoordinatesHelper.extractLatitude(arrival.getCoordinates()), 
												 CoordinatesHelper.extractLongitude(arrival.getCoordinates()), 
												 "ENTERING " + arrival.getName() + " @ " + atDate(logsheet.getArrivalDate())));
		}
			
		WaypointPainter<Waypoint> painter = getNewPainter();
		painter.setWaypoints(waypoints);
		
		map.getMainMap().setOverlayPainter(painter);
		map.repaint();
	}
	
	public void updateMap(Logsheet logsheet) {
		this.logsheet = logsheet;
		event = 0;
		
		updateMap();
		
		map.setZoom(16);
	}
	
	private WaypointPainter<Waypoint> getNewPainter() {
		WaypointPainter<Waypoint> painter = new WaypointPainter<Waypoint>() {
			@Override
			protected void doPaint(Graphics2D g, JXMapViewer object, int width, int height) {
				// convert from viewport to world bitmap
				Rectangle rect = object.getViewportBounds();
				g.translate(-rect.x, -rect.y);

				// do the drawing
				int lastX = -1;
				int lastY = -1;

				Point2D pt;

				Set<Waypoint> waypoints = new TreeSet<Waypoint>(CUSTOM_WAYPOINT_COMPARATOR);
				waypoints.addAll(getWaypoints());
				
				CustomWaypoint current;
				for (Waypoint point : waypoints) {
					current = (CustomWaypoint)point;
					
					g.setColor(LINE_COLOR);

					pt = object.getTileFactory().geoToPixel(point.getPosition(), object.getZoom());
					
					if (lastX != -1 && lastY != -1) {
						g.drawLine(lastX, lastY, (int) pt.getX(), (int) pt.getY());
					}
					
					lastX = (int)pt.getX();
					lastY = (int)pt.getY();

					g.setColor(WAYPOINT_COLOR);

					try {
						BufferedImage img = null;
						
						if(current.getEvent() != null) {
							img = ImageIO.read(Thread.currentThread().getContextClassLoader().getResource("media/icons/maps/pin.png"));

							g.drawImage(img, lastX - ( img.getWidth() / 2 ) + 5, lastY - img.getHeight() + 5, null);
							
							if(current.drawText) {
								String toDraw = current.getEventLabel();
								
								g.setFont(new Font("Tahoma", Font.BOLD, 12));
								
//								FontMetrics fm = g.getFontMetrics();
//								Rectangle2D background = fm.getStringBounds(toDraw, g);
//
//								g.setColor(TEXT_BACKGROUND_COLOR);
//								g.fillRect(lastX - 10, lastY - img.getHeight() - fm.getAscent(), (int)background.getWidth(), (int)background.getHeight());
								
								g.setColor(TEXT_COLOR);

								g.drawString(toDraw, lastX - 10, lastY - img.getHeight());
								
								g.setColor(WAYPOINT_COLOR);
								
								g.drawLine(lastX-5, lastY-5, lastX+5, lastY+5);
								g.drawLine(lastX-5, lastY+5, lastX+5, lastY-5);
								
//								if(current.isStart())
//									g.drawString(current.getEventLabel(), lastX - 10, lastY - img.getHeight());
//								else
//									g.drawString(current.getEventLabel(), lastX - 10, lastY + 15);
							}
						} else {
							String toDraw = current.getCustomLabel();

							boolean isEnteringPort = toDraw.startsWith("ENTERING");

							img = ImageIO.read(Thread.currentThread().getContextClassLoader().getResource("media/icons/maps/greenDot.png"));

							g.drawImage(img, lastX - ( img.getWidth() / 2 ), lastY - ( img.getHeight() / 2 ), null);
							
							g.setFont(new Font("Tahoma", Font.BOLD, 12));
							
//							FontMetrics fm = g.getFontMetrics();
//							Rectangle2D background = fm.getStringBounds(toDraw, g);
//
//							g.setColor(TEXT_BACKGROUND_COLOR);
//							g.fillRect(lastX - - ( img.getWidth() / 2 ), lastY + ( img.getHeight() ) - fm.getAscent(), (int)background.getWidth(), (int)background.getHeight());
							
							g.setColor(TEXT_COLOR);
							
							g.drawString(toDraw, lastX - ( img.getWidth() / 2 ), lastY + ( img.getHeight() + ( isEnteringPort ? img.getHeight() / 2 : 0 )));
						}
					} catch(IOException IOe) {
						g.setColor(WAYPOINT_COLOR);
						g.setXORMode(null);
						g.drawLine(lastX-5, lastY-5, lastX+5, lastY+5);
						g.drawLine(lastX-5, lastY+5, lastX+5, lastY-5);
					}
				}
			}
		};
		
		painter.setRenderer(new CustomWaypointRenderer());
		
		return painter;
	}
	
	public void showPanel() {
		map.getMainMap().repaint();
	}
}