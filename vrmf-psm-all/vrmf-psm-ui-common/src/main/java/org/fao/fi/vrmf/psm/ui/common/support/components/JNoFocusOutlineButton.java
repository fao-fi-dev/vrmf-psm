/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 6, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 6, 2015
 */
public class JNoFocusOutlineButton extends JButton {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1907960996900478005L;

	/**
	 * Class constructor
	 *
	 */
	public JNoFocusOutlineButton() {
		super();
		setFocusPainted(false);
	}

	/**
	 * Class constructor
	 *
	 * @param a
	 */
	public JNoFocusOutlineButton(Action a) {
		super(a);
		setFocusPainted(false);
	}

	/**
	 * Class constructor
	 *
	 * @param icon
	 */
	public JNoFocusOutlineButton(Icon icon) {
		super(icon);
		setFocusPainted(false);
	}

	/**
	 * Class constructor
	 *
	 * @param text
	 * @param icon
	 */
	public JNoFocusOutlineButton(String text, Icon icon) {
		super(text, icon);
		setFocusPainted(false);
	}

	/**
	 * Class constructor
	 *
	 * @param text
	 */
	public JNoFocusOutlineButton(String text) {
		super(text);
		setFocusPainted(false);
	}
}