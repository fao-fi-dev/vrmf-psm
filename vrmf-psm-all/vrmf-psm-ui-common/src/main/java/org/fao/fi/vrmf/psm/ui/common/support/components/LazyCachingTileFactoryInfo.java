/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.jxmapviewer.viewer.TileFactoryInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 8, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 8, 2016
 */
@Slf4j
public class LazyCachingTileFactoryInfo extends TileFactoryInfo {
	static private String TILES_ARCHIVE_FOLDER = "maps/offline/stamen";
	
	/**
	 * Default constructor
	 */
	public LazyCachingTileFactoryInfo()
	{
		super("Stamen", 
			   1, 17, 19, 
			   256, true, true,
			   "http://a.tile.stamen.com/toner-lite", //http://otile4.mqcdn.com/tiles/1.0.0/map",
			   "x", "y", "z");
	}
	
	public String getTileUrl(final int x, final int y, final int zoom) {
		final int actualZoom = 19 - zoom;
		
		String url = getLocallyStoredURL(x, y, actualZoom);
		
		//Tile is locally available
		if(url != null) return url;
		
		//Retrieve tile from remote server
		final String $url = this.baseURL + "/" + actualZoom + "/" + x + "/" + y + ".png";
		
		new Thread(new Runnable() {
			public void run() {
				cacheTile(x, y, actualZoom, $url);
			}
		}).start();
		
		return url;
	}
	
	private String getLocallyStoredURL(int x, int y, int zoom) {
		try {
			File folder = new File(TILES_ARCHIVE_FOLDER + File.separator + zoom + File.separator + x + File.separator);
			File local = new File(folder, y + ".png");
			
			if(local.exists()) return local.toURI().toURL().toString();
			
			File additional = new File(TILES_ARCHIVE_FOLDER + File.separator + "additional" + File.separator + File.separator + zoom + File.separator + x, y + ".png");
			
			if(additional.exists()) return additional.toURI().toURL().toString();
			
			return null;
		} catch(Throwable t) {
			log.error("Unable to check for existence of locally stored tile for ({}, {}, {}): {}", x, y, zoom, t.getMessage(), t);
			
			return null;
		}
	}
	
	private void cacheTile(int x, int y, int zoom, String URL) {
		File folder = new File(TILES_ARCHIVE_FOLDER + File.separator + "additional" + File.separator + File.separator + zoom + File.separator + x);
		File cached = new File(folder, y + ".png");
		
		if(!cached.exists()) {
			if(!folder.exists()) folder.mkdirs();

			InputStream is = null;
			OutputStream os = null;
			
			try {
				log.debug("Caching tile from URL {} as file {}", URL, cached.getAbsolutePath());
				
				URL resource = new URL(URL);
				URLConnection connection = resource.openConnection();
				
				connection.setConnectTimeout(15000);
				connection.setReadTimeout(15000);
  
				is = connection.getInputStream();
				os = new FileOutputStream(cached);
				
				int len = -1;
				byte[] buffer = new byte[8192];
				
				while((len = is.read(buffer)) != -1)
					os.write(buffer, 0, len);
				
				os.flush();
			} catch(Throwable t) {
				log.error("Unable to cache tile from {} as {}: {}", URL, cached.getAbsolutePath(), t.getMessage(), t);
			} finally {
				if(os != null) try { os.close(); } catch(Throwable t) { ; };
				if(is != null) try { is.close(); } catch(Throwable t) { ; }
			}
		}
		
	}
}
