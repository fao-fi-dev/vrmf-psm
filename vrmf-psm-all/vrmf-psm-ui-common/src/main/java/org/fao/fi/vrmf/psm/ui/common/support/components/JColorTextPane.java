/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
public class JColorTextPane extends JTextPane {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7075139482352485184L;
	
	private boolean isEditable = false;
	
	/**
	 * Class constructor
	 *
	 */
	public JColorTextPane() {
		super();
		
		setEditable(false);
	}
	
	/**
	 * Class constructor
	 *
	 */
	public JColorTextPane(boolean isEditable) {
		super();
		
		setEditable(isEditable);
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.text.JTextComponent#setEditable(boolean)
	 */
	@Override
	public void setEditable(boolean editable) {
		super.setEditable(editable);
		
		isEditable = editable;
	}

	public void appendNaive(Color color, String toAppend) {
		super.setEditable(true); //Otherwise nothing will be appended...
		
		SimpleAttributeSet attributes = new SimpleAttributeSet();
		StyleConstants.setForeground(attributes, color);

		int length = getText().length();
		
		setCaretPosition(length);
		setCharacterAttributes(attributes, false);
		
		replaceSelection(toAppend);
		
		super.setEditable(isEditable);
	}

	public void append(Color color, String toAppend) {
		super.setEditable(true); //Otherwise nothing will be appended...

		StyleContext styleContext = StyleContext.getDefaultStyleContext();
		AttributeSet attributes = styleContext.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, color);

		int length = getDocument().getLength();
		
		setCaretPosition(length);
		setCharacterAttributes(attributes, false);
		replaceSelection(toAppend);
		
		super.setEditable(isEditable);
	}
}
