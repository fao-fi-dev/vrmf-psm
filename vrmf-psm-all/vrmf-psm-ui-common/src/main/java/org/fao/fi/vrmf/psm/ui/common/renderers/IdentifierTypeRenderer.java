/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.IdentifierType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class IdentifierTypeRenderer extends CodedEntityRenderer<IdentifierType> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -129146832491426599L;
}
