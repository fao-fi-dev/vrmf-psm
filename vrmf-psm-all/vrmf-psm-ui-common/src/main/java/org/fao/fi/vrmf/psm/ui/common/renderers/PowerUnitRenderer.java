/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.PowerUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class PowerUnitRenderer extends CodedEntityRenderer<PowerUnit> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -129146832491426599L;
}
