/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.table;

import javax.swing.JTable;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
public class SpeciesTableCellRenderer extends AbstractTableCellRenderer<Species> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8392701145128902013L;

	/**
	 * Class constructor
	 */
	public SpeciesTableCellRenderer() {
		super(LEFT);
	}

	/**
	 * Class constructor
	 *
	 * @param alignment
	 */
	public SpeciesTableCellRenderer(int alignment) {
		super(alignment);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.table.AbstractTableCellRenderer#doGetTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	protected void updateComponent(JTable table, Species value, boolean isSelected, boolean hasFocus, int row, int column) {
		setText(value == null ? "----" : value.getAlpha3Code() + " - " + value.getLocalName() + " (" + value.getScientificName() + ")"); 
		
	}
}
