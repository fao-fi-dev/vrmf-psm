/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.SplashScreen;
import java.awt.geom.Rectangle2D;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 16, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 16, 2015
 */
public class SplashManager {
	static private SplashScreen SPLASH = SplashScreen.getSplashScreen();
	static private Graphics SPLASH_GRAPHICS;
	static private Rectangle2D SPLASH_TEXT;

	public void initSplash() {
		if(SPLASH != null) {
			Dimension splashSize = SPLASH.getSize();

			int height = splashSize.height;
			int width = splashSize.width;

			// create the Graphics environment for drawing status info
			SPLASH_GRAPHICS = SPLASH.createGraphics();

			SPLASH_GRAPHICS.setFont(new Font("Dialog", Font.PLAIN, 14));

			SPLASH_TEXT = new Rectangle2D.Double(90.0, height * 0.75, width * 0.75, 18.0);
			
			splashText("Starting the application...");
		}
	}
	
	public void disposeSplash() {
		if(SPLASH != null) 
			try {
				SPLASH.close();
			} catch (Throwable t) {
				;
			}
	}
	
	public void splashText(String str) {
		if(SPLASH != null && SPLASH.isVisible()) {
			SPLASH_GRAPHICS.setColor(Color.WHITE);
			SPLASH_GRAPHICS.fillRect((int)90, (int)( SPLASH.getSize().getHeight() * 0.75), (int) SPLASH_TEXT.getWidth(), (int) SPLASH_TEXT.getHeight());
			
			SPLASH_GRAPHICS.setColor(Color.BLACK);
			SPLASH_GRAPHICS.drawString(str, (int)( SPLASH_TEXT.getX() + 12 ), (int)( SPLASH_TEXT.getY() + 15 ));

			SPLASH.update();
		}
    }
}
