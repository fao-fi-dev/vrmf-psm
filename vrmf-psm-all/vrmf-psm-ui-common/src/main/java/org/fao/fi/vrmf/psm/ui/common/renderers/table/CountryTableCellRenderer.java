/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.table;

import java.io.IOException;

import javax.swing.JTable;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
public class CountryTableCellRenderer extends AbstractTableCellRenderer<Country> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8975143247776740960L;

	/**
	 * Class constructor
	 */
	public CountryTableCellRenderer() {
		super(LEFT);
	}

	/**
	 * Class constructor
	 *
	 * @param alignment
	 */
	public CountryTableCellRenderer(int alignment) {
		super(alignment);
	}
	
	@Override
	protected void updateComponent(JTable table, Country value, boolean isSelected, boolean hasFocus, int row, int column) {
		setText(value == null ? "----" : value.getIso2Code() + " - " + value.getName()); 
		
		try {
			setIcon(UIUtils.getFlagIcon(value));
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}
}
