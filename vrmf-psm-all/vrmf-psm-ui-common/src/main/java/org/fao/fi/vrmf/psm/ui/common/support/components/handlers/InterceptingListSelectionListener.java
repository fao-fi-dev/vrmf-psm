/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components.handlers;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 15, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 15, 2015
 */
abstract public class InterceptingListSelectionListener implements ListSelectionListener {
	/* (non-Javadoc)
	 * @see javax.swing.event.InterceptingListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	@Override
	final public void valueChanged(ListSelectionEvent e) {
		try {
			doValueChanged(e);
		} catch(Throwable t) {
			UIUtils.error(null, "Unhandled exception caught", t.getMessage(), t);
		}
	}
	
	abstract protected void doValueChanged(ListSelectionEvent e);
}
