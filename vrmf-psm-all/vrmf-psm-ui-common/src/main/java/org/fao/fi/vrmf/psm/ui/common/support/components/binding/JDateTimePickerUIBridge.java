/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.support.components.binding;

import java.util.Date;

import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;

import org.fao.fi.vrmf.psm.ui.common.support.components.JDateTimePicker;

import no.tornado.databinding.converter.ConversionException;
import no.tornado.databinding.uibridge.DocumentChangeListener;
import no.tornado.databinding.uibridge.UIBridge;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 30, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 30, 2015
 */
public class JDateTimePickerUIBridge implements UIBridge<JDateTimePicker, DocumentListener, Date> {
	static public JDateTimePickerUIBridge INSTANCE = new JDateTimePickerUIBridge();

	private JDateTimePickerUIBridge() { }

	public Class<JDateTimePicker> getUIClass() {
		return JDateTimePicker.class;
	}

	public Class<? extends Date> getUIValueType() {
		return Date.class;
	}

	public void setUIValue(JDateTimePicker component, Date value) throws ConversionException {
		component.setDate(value);
	}

	public Date getUIValue(JDateTimePicker component) throws ConversionException {
		return component.getDate();
	}

	//SEE: http://databinding.tornado.no/
	
	public DocumentListener addValueChangeListener(final JDateTimePicker component, final ChangeListener listener) {
		DocumentListener documentListener = new DocumentChangeListener(listener);
		component.getEditor().getDocument().addDocumentListener(documentListener);
		
		return documentListener;
	}

	public void removeValueChangelistener(JDateTimePicker component, DocumentListener listener) {
		component.getEditor().getDocument().removeDocumentListener(listener);
	}
}
