/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.table;

import java.awt.Component;

import javax.swing.JTable;

import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
abstract public class AbstractTableCellRenderer<ENTITY> extends SubstanceDefaultTableCellRenderer {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5110233941547796467L;
	
	private int alignment = CENTER;
	
	/**
	 * Class constructor
	 *
	 */
	public AbstractTableCellRenderer() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param alignment
	 */
	public AbstractTableCellRenderer(int alignment) {
		super();
		this.alignment = alignment;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@SuppressWarnings("unchecked")
	@Override
	final public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
		updateComponent(table, (ENTITY)value, isSelected, hasFocus, row, column);
		
		setHorizontalAlignment(alignment);
		
		return component;
	}
	
	abstract protected void updateComponent(JTable table, ENTITY value, boolean isSelected, boolean hasFocus, int row, int column);
}
