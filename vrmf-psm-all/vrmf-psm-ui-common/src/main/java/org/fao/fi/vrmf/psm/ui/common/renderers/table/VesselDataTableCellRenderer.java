/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.table;

import java.io.IOException;

import javax.swing.JTable;

import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
public class VesselDataTableCellRenderer extends AbstractTableCellRenderer<VesselData> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8392701145128902013L;

	/**
	 * Class constructor
	 */
	public VesselDataTableCellRenderer() {
		super(LEFT);
	}

	/**
	 * Class constructor
	 *
	 * @param alignment
	 */
	public VesselDataTableCellRenderer(int alignment) {
		super(alignment);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.table.AbstractTableCellRenderer#doGetTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	protected void updateComponent(JTable table, VesselData value, boolean isSelected, boolean hasFocus, int row, int column) {
		setText(value == null ? "----" : value.getName() + " [ " + value.getType().getCode() + " - " + value.getIrcs() + " ]"); 
		
		try {
			setIcon(UIUtils.getFlagIcon(value == null ? null : value.getRegistrationCountry()));
		} catch(IOException e) {
			throw new RuntimeException(e);
		};
	}
}
