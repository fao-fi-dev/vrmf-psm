/**
 * (c) 2014 FAO / UN (project: PSM-DTO)
 */
package org.fao.fi.vrmf.psm.ui.common.renderers.autocompletion;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Mar 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Mar 2014
 */
public class SpeciesAutocompleter extends BaseAutocompleter<Species> { 
	static final public SpeciesAutocompleter INSTANCE = new SpeciesAutocompleter();
	
	private SpeciesAutocompleter() { };
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.autocompletion.BaseAutocompleter#extract(java.lang.Object)
	 */
	@Override
	protected String[] extract(Species item) {
		return new String[] { item.getAlpha3Code(), item.getLocalName(), item.getScientificName() };
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.psm.logbook.ui.renderers.autocompletion.BaseAutocompleter#label(java.lang.Object)
	 */
	@Override
	protected String label(Species item) {
		return item.getAlpha3Code() + " - " + item.getLocalName() + ( item.getScientificName() == null ? "" : ( " (" + item.getScientificName() + ")") );
	}
}