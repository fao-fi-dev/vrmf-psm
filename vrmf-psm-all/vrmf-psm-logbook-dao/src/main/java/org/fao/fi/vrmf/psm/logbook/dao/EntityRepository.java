/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-dao)
 */
package org.fao.fi.vrmf.psm.logbook.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 17, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 17, 2015
 */
@Slf4j
abstract public class EntityRepository<ENTITY, KEY extends Serializable> extends Repository {
	protected Class<? extends ENTITY> repositoryType;

	/**
	 * Class constructor
	 *
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public EntityRepository() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		repositoryType = (Class) pt.getActualTypeArguments()[0];
	}

	public ENTITY add(ENTITY entity) {
		currentSession().save(entity);
		
		return entity;
	}

	public ENTITY saveOrUpdate(ENTITY entity) {
		currentSession().saveOrUpdate(entity);

		return entity;
	}
	
	public ENTITY refresh(ENTITY entity) {
		currentSession().refresh(entity);

		return entity;
	}
	
	public ENTITY merge(ENTITY entity) {
		currentSession().merge(entity);

		return entity;
	}

	public ENTITY update(ENTITY entity) {
		currentSession().saveOrUpdate(entity);

		return entity;
	}

	public ENTITY remove(ENTITY entity) {
		currentSession().delete(entity);

		return entity;
	}

	public ENTITY find(KEY key) {
		long end, start = System.currentTimeMillis();
		
		try {
			return (ENTITY) currentSession().get(repositoryType, key);
		} finally {
			end = System.currentTimeMillis();
			
			log.debug("Returning entity with key {} through {} took {} mSec.", key, this.getClass().getSimpleName(), end - start);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ENTITY> getAll() {
		long end, start = System.currentTimeMillis();
		
		try {
			return currentSession().createCriteria(repositoryType).list();
		} finally {
			end = System.currentTimeMillis();
			
			log.debug("Returning all entities through {} took {} mSec.", this.getClass().getSimpleName(), end - start);
		}
	}
}
