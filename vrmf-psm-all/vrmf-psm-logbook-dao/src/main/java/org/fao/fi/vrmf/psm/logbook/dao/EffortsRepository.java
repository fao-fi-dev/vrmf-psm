/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-dao)
 */
package org.fao.fi.vrmf.psm.logbook.dao;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 16, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 16, 2015
 */
@Named @Singleton
public class EffortsRepository extends EntityRepository<Effort, Integer> {
}
