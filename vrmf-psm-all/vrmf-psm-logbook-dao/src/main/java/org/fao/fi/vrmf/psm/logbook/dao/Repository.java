/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-dao)
 */
package org.fao.fi.vrmf.psm.logbook.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Named("repository") @Singleton
@Transactional(propagation=Propagation.NESTED, isolation=Isolation.READ_UNCOMMITTED)
@Slf4j
public class Repository {
	@Inject @Getter @Setter private SessionFactory sessionFactory;
	
	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public Query createQuery(String query) {
		return currentSession().createQuery(query);
	}
	
	@SuppressWarnings("unchecked")
	public <C> C unique(String query) {
		long end, start = System.currentTimeMillis();
		
		try {
			return (C)createQuery(query).uniqueResult();
		} finally {
			end = System.currentTimeMillis();
			
			log.info("Executing query {} for a unique result took {} mSec", query, end - start);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <C> List<C> list(String query) {
		long end, start = System.currentTimeMillis();
		
		try {
			return (List<C>)createQuery(query).list();
		} finally {
			end = System.currentTimeMillis();
			
			log.info("Executing query {} for a result set took {} mSec", query, end - start);
		}
	}
}
