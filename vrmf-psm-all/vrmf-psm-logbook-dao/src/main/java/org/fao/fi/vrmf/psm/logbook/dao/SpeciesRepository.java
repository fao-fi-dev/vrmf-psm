/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-psm-logbook-dao)
 */
package org.fao.fi.vrmf.psm.logbook.dao;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 19, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 19, 2015
 */
@Named @Singleton
public class SpeciesRepository extends EntityRepository<Species, Integer> {
}