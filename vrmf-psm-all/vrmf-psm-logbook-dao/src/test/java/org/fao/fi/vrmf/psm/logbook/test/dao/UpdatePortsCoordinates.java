/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-dao)
 */
package org.fao.fi.vrmf.psm.logbook.test.dao;

import javax.inject.Inject;

import org.fao.fi.vrmf.psm.logbook.dao.PortsRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 17, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 17, 2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:config/test/spring/psm-logbook-dao-context.xml")
public class UpdatePortsCoordinates {
	@Inject private PortsRepository portRepo;
	
	@Ignore @Test
	public void updatePortCoordinates() {
		for(Port in : portRepo.getAll()) {
			if(in.getCoordinates() != null && !"NULL".equals(in.getCoordinates()) && !"NULL;NULL".equals(in.getCoordinates())) {
				in.setCoordinates(updateCoordinates(in.getCoordinates()));
				
				portRepo.update(in);
			}
		}
	}
	
	private String updateCoordinates(String coordinates) {
		String latitude = coordinates.replaceAll("\\;.+$", "").replaceAll("\\.", "°").replaceAll("(.+°)([0-9]{2})([0-9]+)([a-zA-Z])", "$1$2'$30''$4");
		String longitude = coordinates.replaceAll("^[^\\;]+\\;", "").replaceAll("\\.", "°").replaceAll("(.+°)([0-9]{2})([0-9]+)([a-zA-Z])", "$1$2'$30''$4");
		
		String updated = latitude + ";" + longitude;
		
		System.out.println("Transforming " + coordinates + " to " + updated);
		
		return updated;
	}
}
