/**
 * (c) 2015 FAO / UN (project: vrmf-psm-ce-dao)
 */
package org.fao.fi.vrmf.psm.logbook.test.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.fao.fi.vrmf.psm.logbook.dao.CountriesRepository;
import org.fao.fi.vrmf.psm.logbook.dao.EventsRepository;
import org.fao.fi.vrmf.psm.logbook.dao.LogbooksRepository;
import org.fao.fi.vrmf.psm.logbook.dao.PortsRepository;
import org.fao.fi.vrmf.psm.logbook.dao.SpeciesRepository;
import org.fao.fi.vrmf.psm.logbook.dao.VesselDataRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchAndEffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.CanneryUnloadingData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.VesselUnloadingData;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DiscardReason;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.PowerUnit;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.VesselType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 16, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 16, 2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:config/test/spring/psm-logbook-dao-context.xml")
public class TestPersistence {
	@Inject private CountriesRepository countryRepo;
	@Inject private SpeciesRepository speciesRepo;
	@Inject private PortsRepository portRepo;
	@Inject private LogbooksRepository logbookRepo;
	@Inject private EventsRepository eventRepo;
	@Inject private VesselDataRepository vesselDataRepo;
	
	private String randomCoordinates() {
		return randomCoordinate(true) + ";" + randomCoordinate(false);
	}
	
	private String randomCoordinate(boolean latitude) {
		long degrees = Math.round(Math.random() * ( latitude ? 90 : 180 ));
		long minutes = Math.round(Math.random() * 60);
		long seconds = Math.round(Math.random() * 60);
		int index = (int)Math.round(Math.random() * 10) % 2;
		
		String coordinates = pad(degrees, latitude ? 2 : 3) + "°";
		
		coordinates += pad(minutes, 2) + "'";
		coordinates += pad(seconds, 2) + "''";
		coordinates += (latitude ? new String[] { "N", "S" } : new String[] { "E", "W" })[index];
		
		return coordinates;
	}
	
	private String pad(long value, int length) {
		String padded = String.valueOf(value);
		
		while(padded.length() < length)
			padded = "0" + padded;
		
		return padded;
	}

	private VesselData getVesselDataMock() {
		return vesselDataRepo.saveOrUpdate(
			VesselData.builder().
			name("Foo of the Bars").
			ircs("FOOBAR").
			registrationNumber("FOORNO").
			registrationCountry(countryRepo.find(4)).
			type(VesselType.LINER).
			fishingCompany("FOO Inc.").
			loa(666.0).
			gt(999.0).
			mainEnginePowerUnit(PowerUnit.KW).
			mainEnginePower(123456.0).
			build()
		);
	}
	
	private VesselData getVesselDataMock2() {
		return vesselDataRepo.saveOrUpdate(
			VesselData.builder().
			name("Bar of the Foos").
			ircs("BARFOO").
			registrationNumber("BARNO").
			registrationCountry(countryRepo.find(1)).
			type(VesselType.LINER).
			fishingCompany("BAR Inc.").
			loa(999.0).
			gt(666.0).
			mainEnginePowerUnit(PowerUnit.KW).
			mainEnginePower(654321.0).
			build()
		);
	}
	
	private List<Catches> getCatchesMock(int len) {
		List<Catches> catches = new ArrayList<Catches>();
		
		Catches c = null;
		
		for(;len>=0;len--) {
			c = new Catches();
			c.setQuantity(Math.random() * 666);
			c.setQuantityType(len % 2 == 0 ? QuantityType.NUMBER : QuantityType.METRIC_TONS);
			c.setSpecies(speciesRepo.find(111 + ( len % 2 ) * 222));
			
			catches.add(c);
		}
		
		return catches;
	}

	private Logsheet addLogsheetMock(Logbook logbook, VesselData vessel) {
		Logsheet s = logbook.addLogsheet(vessel);
		
		Calendar start = Calendar.getInstance();
		start.set(Calendar.MONTH, Calendar.JANUARY);
		start.set(Calendar.DAY_OF_MONTH, 1);
		start.set(Calendar.HOUR_OF_DAY, 0);
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);
		
		Calendar end = Calendar.getInstance();
		end.setTimeInMillis(start.getTimeInMillis());
		for(int i=0; i<Math.round(Math.random() * 2) + 1; i++) { end.roll(Calendar.HOUR_OF_DAY, true); };
		
		s.setCaptainName("Nemo");
		s.setCrewSize(15);
		s.setDepartureDate(start.getTime());
		s.setDeparturePort(portRepo.find(1));
		s.setFishOnboardAtStart(Math.random() * 1000);
		s.setFishOnboardQuantityAtStart(QuantityType.METRIC_TONS);
		s.setFishOnboardAfterUnloading(Math.random() * 500);
		s.setFishOnboardQuantityAfterUnloading(QuantityType.KILOGRAMS);
		
		Event e = Event.builder().type(EventType.TRANSIT).coordinatesAtStart(randomCoordinates()).startDate(start.getTime()).coordinatesAtEnd(randomCoordinates()).endDate(end.getTime()).build();
		
		s.addEvent(e);
		
		start = Calendar.getInstance();
		start.setTimeInMillis(end.getTimeInMillis());
		
		end = Calendar.getInstance();
		end.setTimeInMillis(start.getTimeInMillis());
		for(int i=0; i<Math.round(Math.random() * 2) + 1; i++) { end.roll(Calendar.HOUR_OF_DAY, true); };
		
		e = Event.builder().type(EventType.SEARCHING).coordinatesAtStart(randomCoordinates()).startDate(start.getTime()).coordinatesAtEnd(randomCoordinates()).endDate(end.getTime()).build();
		
		s.addEvent(e);
		
		start = Calendar.getInstance();
		start.setTimeInMillis(end.getTimeInMillis());
		
		end = Calendar.getInstance();
		end.setTimeInMillis(start.getTimeInMillis());
		for(int i=0; i<Math.round(Math.random() * 2) + 1; i++) { end.roll(Calendar.HOUR_OF_DAY, true); };
		
		CatchAndEffortEvent e2 = new CatchAndEffortEvent();
		e2.setType(EventType.FISHING);
		e2.setCoordinatesAtStart(randomCoordinates());
		e2.setStartDate(start.getTime());
		e2.setCoordinatesAtEnd(randomCoordinates());
		e2.setEndDate(end.getTime());
		e2.setEfforts(Arrays.asList(new Effort[] {
			Effort.builder().effortType(EffortType.NUMBER_OF_HOOKS).effortValue(Math.random() * 10000).build(),
			Effort.builder().effortType(EffortType.NUMBER_OF_LINES).effortValue(Math.random() * 4000).build()
		}));
				
		e2.setCatches(getCatchesMock(3));
		
		Discards d = new Discards();
		d.setQuantity(Math.random() * 111);
		d.setQuantityType(QuantityType.KILOGRAMS);
		d.setSpecies(speciesRepo.find(333));
		d.setReason(DiscardReason.VESSEL_FULL);
		
		e2.getDiscards().add(d);
		
		s.addEvent(e2);
		
		start = Calendar.getInstance();
		start.setTimeInMillis(end.getTimeInMillis());
		
		end = Calendar.getInstance();
		end.setTimeInMillis(start.getTimeInMillis());
		for(int i=0; i<Math.round(Math.random() * 2) + 1; i++) { end.roll(Calendar.HOUR_OF_DAY, true); };
		
		e = Event.builder().type(EventType.TRANSIT).coordinatesAtStart(randomCoordinates()).startDate(start.getTime()).coordinatesAtEnd(randomCoordinates()).endDate(end.getTime()).build();
		
		s.addEvent(e);

		start = Calendar.getInstance();
		start.setTimeInMillis(end.getTimeInMillis());
		
		s.setArrivalDate(start.getTime());
		s.setUnloadingPort(portRepo.find(2));
		
		s.setComments("Lorem ipsum sit dolor amet");
		s.setCanneryUnloads(new ArrayList<CanneryUnloadingData>());
		s.setOtherVesselUnloads(new ArrayList<VesselUnloadingData>());
		
		CanneryUnloadingData u = new CanneryUnloadingData();
		u.setCatchesUnloaded(getCatchesMock(4));
		u.setCanneryName("Can I can this");
		
		s.getCanneryUnloads().add(u);

		VesselUnloadingData o = new VesselUnloadingData();
		o.setCatchesUnloaded(getCatchesMock(6));
		o.setVesselName("Collector's dream");
		o.setVesselRegistrationCountry(countryRepo.find(1));
		o.setVesselRegistrationNumber("SNAFU");
		o.setDestination("Neverland");
		
		s.getOtherVesselUnloads().add(o);
		
		o = new VesselUnloadingData();
		o.setCatchesUnloaded(getCatchesMock(3));
		o.setVesselName("Collector's nightmare");
		o.setVesselRegistrationCountry(countryRepo.find(2));
		o.setVesselRegistrationNumber("FUD");
		o.setDestination("Neverneverland");
		
		s.getOtherVesselUnloads().add(o);
		
		return s.completeLogsheet();
	}
	
	private Logbook getLogbookMock() {
		Logbook l = new Logbook();
		l.setYear(2015);
		l.setVersion("1.0.0");
		
		VesselData v1 = getVesselDataMock();
		VesselData v2 = getVesselDataMock2();
		
		addLogsheetMock(l, v1);
		addLogsheetMock(l, v2);
		addLogsheetMock(l, v1);
		
		return l.deriveEfforts();
	}
	
	@Test
	public void isInitialized() {
		Assert.assertNotNull(logbookRepo);
	}
	
	@Test
	public void testList() {
		logbookRepo.getAll();
	}

	@Test
	public void persist() throws Exception {
		Logbook persisted = logbookRepo.add(getLogbookMock());
		
		Assert.assertTrue(persisted.getId() > 0);
		
		Logbook retrieved = logbookRepo.find(persisted.getId());
		
		Assert.assertTrue(persisted != retrieved);
		
		Marshaller m = JAXBContext.newInstance(Logbook.class, CatchEvent.class, Event.class, Country.class, Species.class).createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(persisted, System.out);
	}
	
	@Test
	public void update() {
		Logbook persisted = logbookRepo.add(getLogbookMock());
		
		persisted.setVersion("2.0.0");
		
		logbookRepo.update(persisted);
		
		Logbook retrieved = logbookRepo.find(persisted.getId());
		
		Assert.assertEquals(persisted.getVersion(), retrieved.getVersion());
	}
	
	@Test
	public void updateWithoutPersisting() {
		Logbook persisted = logbookRepo.add(getLogbookMock());
		
		persisted.setVersion("3.0.0");
		
		Logbook retrieved = logbookRepo.find(persisted.getId());
		
		Assert.assertNotEquals(persisted.getVersion(), retrieved.getVersion());
	}
	
	@Test
	public void updateWithChildDelete() {
		Logbook persisted = logbookRepo.add(getLogbookMock());

		Event e = persisted.getLogsheets().get(0).getEvents().get(0);
		
		Assert.assertNotNull(e);
		Assert.assertNotNull(e.getId());
		
		System.out.println("Event ID is " + e.getId());
		
		persisted.getLogsheets().get(0).getEvents().clear();
				
		persisted = logbookRepo.update(persisted);
		
		Assert.assertNotNull(logbookRepo.find(persisted.getId()));
		Assert.assertTrue(persisted.getLogsheets().get(0).getEvents().isEmpty());
		
		e = eventRepo.find(e.getId());
		
		Assert.assertNull(e);
	}
	
	@Test
	public void updateChild() {
		Logbook persisted = logbookRepo.add(getLogbookMock());

		Logsheet first = persisted.getLogsheets().get(0);

		VesselData vessel = first.getVesselData();
		
		vessel.setName("SNAFU");
		
		vesselDataRepo.update(vessel);
		
		first = persisted.getLogsheets().get(0);
		
		Assert.assertEquals("SNAFU", first.getVesselData().getName());
	}
	
	@Test
	public void updateWithChildAdded() {
		Logbook persisted = logbookRepo.add(getLogbookMock());

		Event e = persisted.getLogsheets().get(0).getEvents().get(0);
		
		Assert.assertNotNull(e);
		Assert.assertNotNull(e.getId());
		
		System.out.println("Event ID is " + e.getId());
		
		int before = persisted.getLogsheets().get(0).getEvents().size();
		
		persisted.getLogsheets().get(0).getEvents().add(Event.builder().type(EventType.CLEANING).startDate(new Date()).coordinatesAtStart("00.000N;000.000E").build());
				
		persisted = logbookRepo.update(persisted);
		
		int after = persisted.getLogsheets().get(0).getEvents().size();
		
		Assert.assertEquals(before + 1, after);
	}
	
	@Test
	public void testCascade() {
		Logbook persisted = logbookRepo.add(getLogbookMock());

		VesselData v = persisted.getLogsheets().get(0).getVesselData();
		
		Assert.assertNotNull(v);
		
		logbookRepo.remove(persisted);
				
		v = vesselDataRepo.find(v.getId());
		
		Assert.assertNotNull(v);
	}
	
	@Test
	public void testChild() throws Exception {
		Port p = portRepo.find(1);
		
		Assert.assertNotNull(p);
		Assert.assertNotNull(p.getCountry());
		Assert.assertNotNull(p.getCountry().getName());
		
		Marshaller m = JAXBContext.newInstance(Logbook.class, CatchEvent.class, Event.class, Country.class, Species.class).createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(p, System.out);
		
		m.marshal(p.getCountry(), System.out);
		
		System.out.println("ID: " + p.getCountry().getId());
		System.out.println("NAME: " + p.getCountry().getName());
		System.out.println("OFFICIAL NAME: " + p.getCountry().getOfficialName());
		System.out.println("ISO2: " + p.getCountry().getIso2Code());
		System.out.println("ISO3: " + p.getCountry().getIso3Code());

	}
}