/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.plugins.importers.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.fao.fi.vrmf.psm.logbook.report.plugins.importers.FFAImporter;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 29, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 29, 2016
 */
public class TestFFAConverter {
	private FFAImporter importer = new FFAImporter();
	
	private File resourceToFile(String resource) throws Exception {
		InputStream resourceStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
		
		if(resourceStream != null) {
			byte[] buffer = new byte[8192];
			int len = -1;
			
			File targetFile = File.createTempFile("test_temp", ".pdf");
			
			OutputStream targetStream = new FileOutputStream(targetFile);
			
			while((len = resourceStream.read(buffer)) != -1)
				targetStream.write(buffer, 0, len);
			
			targetStream.flush();
			targetStream.close();
			
			resourceStream.close();
			
			return targetFile;
		} 
		
		return null;
	}
	
	@Test
	public void testLL() throws Exception {
		importer.prettyPrint(importer.extractData(resourceToFile("FFA_LL.pdf")), System.out);
	}
	
	@Test
	public void testExtractLL() throws Exception {
		importer.prettyPrint(importer.xmlFromPdf(resourceToFile("FFA_LL.pdf")), System.out);
	}
	
	@Test
	public void testPS() throws Exception {
		importer.prettyPrint(importer.extractData(resourceToFile("FFA_PS.pdf")), System.out);
	}
	
	@Test
	public void testExtractPS() throws Exception {
		importer.prettyPrint(importer.xmlFromPdf(resourceToFile("FFA_PS.pdf")), System.out);
	}
}
