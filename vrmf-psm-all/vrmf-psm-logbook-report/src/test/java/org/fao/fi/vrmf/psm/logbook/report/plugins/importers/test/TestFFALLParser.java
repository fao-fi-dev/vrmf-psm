/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.plugins.importers.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.IdentifierType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.fao.fi.vrmf.psm.logbook.report.plugins.importers.FFAImporter;
import org.fao.fi.vrmf.psm.logbook.report.plugins.importers.FFALLParser;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 29, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 29, 2016
 */
public class TestFFALLParser {
	private FFAImporter importer = new FFAImporter();
	private FFALLParser llParser = new FFALLParser();
	
	private File resourceToFile(String resource) throws Exception {
		InputStream resourceStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
		
		if(resourceStream != null) {
			byte[] buffer = new byte[8192];
			int len = -1;
			
			File targetFile = File.createTempFile("test_temp", ".pdf");
			
			OutputStream targetStream = new FileOutputStream(targetFile);
			
			int copied = 0;
			
			while((len = resourceStream.read(buffer)) != -1) {
				copied += len;
				targetStream.write(buffer, 0, len);
				targetStream.flush();
			}

			targetStream.flush();
			targetStream.close();
			
			resourceStream.close();
			
			System.out.println(copied + " bytes copied from " + resource + " to " + targetFile.getAbsolutePath());
			
			return targetFile;
		} 
		
		return null;
	}
	
	/**
	 * Class constructor
	 */
	public TestFFALLParser() throws Exception {
	}
	
	private Document readLLData() throws Exception { 
		return importer.extractData(resourceToFile("FFA_LL.pdf"));
	}
	
	@Test
	public void testVesselDataExtraction() throws Exception {
		VesselData extracted = llParser.extractVesselData(readLLData());
		
		Assert.assertNotNull(extracted);
		Assert.assertEquals("VESSEL NAME", extracted.getName());
		Assert.assertNotNull(extracted.getRegistrationCountry());
		Assert.assertEquals("BO", extracted.getRegistrationCountry().getIso2Code());
		Assert.assertEquals("COMPANY", extracted.getFishingCompany());
		Assert.assertEquals("aaaaabbbbbccccc", extracted.getIrcs());
		Assert.assertNotNull(extracted.getIdentifiers());
		Assert.assertTrue(extracted.getIdentifiers().size() >= 1);
		Assert.assertEquals(IdentifierType.RFMO_ID, extracted.getIdentifiers().get(0).getType());
		Assert.assertEquals("FFA 12345", extracted.getIdentifiers().get(0).getIdentifier());
	}
	
	@Test
	public void testMainLogsheetDataExtraction() throws Exception {
		Logsheet extracted = llParser.extractMainLogsheetData(readLLData());
		
		Assert.assertNotNull(extracted);
		Assert.assertEquals("CAPTAIN", extracted.getCaptainName());
		Assert.assertNotNull(extracted.getDepartureDate());
		Assert.assertNotNull(extracted.getDeparturePort());
		Assert.assertEquals("KI", extracted.getDeparturePort().getCountry().getIso2Code());
		Assert.assertNotNull(extracted.getArrivalDate());
		Assert.assertNotNull(extracted.getUnloadingPort());
		Assert.assertEquals("KI", extracted.getUnloadingPort().getCountry().getIso2Code());
	}
	
	@Test
	public void testCatchEventsDataExtraction() throws Exception {
		List<Event> extracted = llParser.extractCatchEvents(readLLData());
		
		Assert.assertNotNull(extracted);
		Assert.assertTrue(!extracted.isEmpty());
	}

}
