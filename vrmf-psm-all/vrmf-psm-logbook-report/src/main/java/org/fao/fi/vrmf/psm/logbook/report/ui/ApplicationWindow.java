/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.report.ui.panels.MainPanel;
import org.fao.fi.vrmf.psm.ui.common.panels.GlassPane;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.fao.fi.vrmf.psm.ui.common.support.components.binding.JCoordinatesInputUIBridge;
import org.fao.fi.vrmf.psm.ui.common.support.components.binding.JDateTimePickerUIBridge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import no.tornado.databinding.uibridge.UIBridgeRegistry;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Singleton @Named("app")
public class ApplicationWindow extends WindowAdapter { 
	static final private Logger LOG = LoggerFactory.getLogger(ApplicationWindow.class);
	
	@Getter private JFrame applicationFrame;
	
	@Getter @Inject private GlassPane glassPane;
	@Getter @Inject private MainPanel mainPanel;
	
	/**
	 * Class constructor
	 *
	 */
	public ApplicationWindow() throws Exception {
		if(Utils.IS_LAUNCHING) return;
		
		mainPanel = new MainPanel();
		
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	@PostConstruct
	private void initialize() throws Exception {
		//See: http://databinding.tornado.no/
		UIBridgeRegistry.addBridge(JDateTimePickerUIBridge.INSTANCE);
		UIBridgeRegistry.addBridge(JCoordinatesInputUIBridge.INSTANCE);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		int width = (int)(Math.round(screenSize.getWidth()));
		int height = (int)(Math.round(screenSize.getHeight()) - 32);

		Dimension preferred = new Dimension(width, height);
		Dimension minimum = new Dimension(1366, 768);
		
		width = (int)preferred.getWidth();
		height = (int)preferred.getHeight();
		
		applicationFrame = new JFrame();
		
		BufferedImage logo = ImageIO.read(Thread.currentThread().getContextClassLoader().getResourceAsStream("media/icons/FAO_FI_logo.png"));
		applicationFrame.setIconImage(logo);

		applicationFrame.setResizable(true);
		applicationFrame.setMinimumSize(minimum);
		applicationFrame.setPreferredSize(preferred);
		applicationFrame.setMaximumSize(preferred);
		applicationFrame.setSize(preferred);
		
		applicationFrame.setLocationRelativeTo(null);
		applicationFrame.setLocation((int)(screenSize.getWidth() - width) / 2, 0);

		applicationFrame.addWindowListener(this);
		applicationFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		GlassPane glassPane = getGlassPane();
		
		applicationFrame.setGlassPane(glassPane);
		glassPane.setOpaque(false);
		
		applicationFrame.getContentPane().setBackground(UIManager.getColor("Panel.background"));
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[0];
		gridBagLayout.rowHeights = new int[0];
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		applicationFrame.getContentPane().setLayout(gridBagLayout);
		
		JPanel containerPanel = new JPanel();
		containerPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Data reporting", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_containerPanel = new GridBagConstraints();
		gbc_containerPanel.weighty = 100.0;
		gbc_containerPanel.weightx = 100.0;
		gbc_containerPanel.fill = GridBagConstraints.BOTH;
		gbc_containerPanel.gridx = 0;
		gbc_containerPanel.gridy = 0;
		applicationFrame.getContentPane().add(containerPanel, gbc_containerPanel);
		
		GridBagLayout gbl_containerPanel = new GridBagLayout();
		gbl_containerPanel.columnWidths = new int[0];
		gbl_containerPanel.rowHeights = new int[0];
		gbl_containerPanel.columnWeights = new double[0];
		gbl_containerPanel.rowWeights = new double[0];
		containerPanel.setLayout(gbl_containerPanel);
		
		GridBagConstraints gbc_mainPanel = new GridBagConstraints();
		gbc_mainPanel.gridx = 0;
		gbc_mainPanel.gridy = 0;
		gbc_mainPanel.fill = GridBagConstraints.BOTH;
		gbc_mainPanel.weightx = 100;
		gbc_mainPanel.weighty = 100;
		
		containerPanel.add(mainPanel, gbc_mainPanel);
		
		applicationFrame.setTitle("FAO catch data report manager [ UI version: 1.0.0 ]");
	}
	
	public void showGlassPane(String message) {
		GlassPane glassPane = getGlassPane();
		glassPane.getText().setText(message);
		glassPane.setVisible(true);
	}
	
	public void hideGlassPane() {
		getGlassPane().setVisible(false);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosing(WindowEvent arg0) {
		int dialogButton = JOptionPane.YES_NO_OPTION;
		dialogButton = JOptionPane.showConfirmDialog(this.applicationFrame, "Are you sure you want to close the application?", "Please confirm", dialogButton);

		if(dialogButton == JOptionPane.YES_OPTION) {
			super.windowClosing(arg0);
			
			System.exit(0);
		}
	}

	public Thread getShutdownHook() {
		final ApplicationWindow $this = this;

		return new Thread() {
			@Override
			public void run() {
				try {
					ApplicationWindow.LOG.warn("The application is shutting down...");
				} catch (Throwable t) {
					UIUtils.error($this.applicationFrame, "Unexpected error", "Unable to complete application shutdown: " + Utils.getErrorMessage(t), t);
				}
			}
		};
	};
}
