/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.ui.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.report.model.CatchesByCountry;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
public class CatchReportTableModel extends AbstractTableModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 587756690700916447L;

	@Getter private String[] columnNames = new String[] { 
		"Country", "Species 3AC", "Scientific name", "Name", "Unit", "Quantity"
	};

	@Getter private List<CatchesByCountry> rawData = null;

	/**
	 * Class constructor
	 *
	 * @param rawData
	 */
	public CatchReportTableModel(List<CatchesByCountry> data) {
		super();
		
		rawData = data == null ? new ArrayList<CatchesByCountry>() : data;
	}
	
	public void update() {
		fireTableDataChanged();
	}
	
	public void update(List<CatchesByCountry> data) {
		rawData = data == null ? new ArrayList<CatchesByCountry>() : data;
		
		update();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return rawData.size();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		CatchesByCountry record = rawData == null || rawData.size() < row + 1 ? null : rawData.get(row);
	
		if(record == null) return null;
		
		switch(col) {
			case 0:
				return record.getCountry();
			case 1:
				return record.getSpecies().getAlpha3Code();
			case 2:
				return record.getSpecies().getScientificName();
			case 3:
				return record.getSpecies().getLocalName();
			case 4:
				return record.getQuantityType();
			case 5:
				return record.getQuantity();
			default:
				return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		switch(c) {
			case 0:
				return Country.class;
			case 1:
			case 2:
			case 3:
				return String.class;
			case 4:
				return QuantityType.class;
			case 5:
				return Double.class;
			default:
				return Void.class;
		}
	}
	
	public Object[][] getData() {
		if(rawData == null) return new Object[0][];
		
		Object[][] toReturn = new Object[getRowCount()][getColumnCount()];
		
		for(int row=0; row < getRowCount(); row++) {
			toReturn[row] = new Object[getColumnCount()];
			
			for(int column=0; column < getColumnCount(); column++) {
				toReturn[row][column] = getValueAt(row, column);
			}
		}
		
		return toReturn;
	}
}