/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.plugins.importers;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Feb 1, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Feb 1, 2016
 */
abstract public class AbstractFFAParser {
	protected XPathFactory XPATH_FACTORY = XPathFactory.newInstance();
	protected Map<String, XPathExpression> XPATH_CACHE = new HashMap<String, XPathExpression>();
	
	protected Map<String, Species> SPECIES_MAP = new HashMap<String, Species>();
	protected Map<String, Country> COUNTRY_MAP = new HashMap<String, Country>();
	protected Map<String, Port> PORT_MAP = new HashMap<String, Port>();
	
	public AbstractFFAParser() throws Exception {
		SPECIES_MAP = initializeSpeciesMap();
		PORT_MAP = initializePortMap(COUNTRY_MAP = initializeCountryMap());
	}
	
	protected XPathExpression xpath(String path) throws Exception {
		XPathExpression cached = XPATH_CACHE.get(path); 
		
		if(cached == null) {
			cached = XPATH_FACTORY.newXPath().compile(path);
			
			XPATH_CACHE.put(path, cached);
		}
		
		return cached;
	}
	
	private Map<String, Species> initializeSpeciesMap() throws Exception {
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("data/species.csv");
		Reader dataReader = new InputStreamReader(stream);
		
		CSVReader reader = new CSVReader(dataReader, ',', '"');
		
		String[] data;
		Species species;
		Map<String, Species> map = new HashMap<String, Species>();
		
		while((data = reader.readNext()) != null) {
			species = Species.builder().id(Integer.parseInt(data[0])).alpha3Code(data[1]).localName(data[2]).scientificName(data[3]).build();
			
			map.put(species.getAlpha3Code(), species);
		}
		
		stream.close();
		dataReader.close();
		reader.close();
		
		return map;
	}
	
	private Map<String, Country> initializeCountryMap() throws Exception {
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("data/ffa/FFA_countries.csv");
		Reader dataReader = new InputStreamReader(stream);
		
		CSVReader reader = new CSVReader(dataReader, ',', '"', 1);
		
		String[] data;
		Country country;
		Map<String, Country> map = new HashMap<String, Country>();
		
		while((data = reader.readNext()) != null) {
			country = Country.builder().iso2Code(data[1]).name(data[0]).officialName(data[0]).build();
			
			map.put(country.getIso2Code(), country);
		}
		
		stream.close();
		dataReader.close();
		reader.close();
		
		return map;
	}
	
	private Map<String, Port> initializePortMap(Map<String, Country> countryMap) throws Exception {
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("data/ffa/FFA_ports.csv");
		Reader dataReader = new InputStreamReader(stream);
		
		CSVReader reader = new CSVReader(dataReader, ',', '"', 1);
		
		String[] data;
		Port port;
		Map<String, Port> map = new HashMap<String, Port>();
		
		while((data = reader.readNext()) != null) {
			port = Port.builder().name(data[0]).country(countryMap.get(data[1])).build();
			
			map.put(port.getName(), port);
		}
		
		stream.close();
		dataReader.close();
		reader.close();
		
		return map;
	}
	
	protected Date dateFromString(String day, String time) throws ParseException {
		try {
			return new SimpleDateFormat("dd-MM-yy HH:mm").parse(day + " " + time);
		} catch(ParseException PE) {
			return new SimpleDateFormat("dd-MMM-yy HH:mm").parse(day + " " + time);
		}
	}
}
