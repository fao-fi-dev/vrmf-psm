/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.plugins.importers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPathConstants;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchAndEffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Identifier;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.DiscardReason;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EffortType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.EventType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.IdentifierType;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Feb 1, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Feb 1, 2016
 */
public class FFALLParser extends AbstractFFAParser {
	private final FFAImporter importer = new FFAImporter();
	
	final private String[] LL_SPECIES_1 = new String[] { 
		"Alb",
		"Bet",
		"Pbf",
		"Skj",
		"Yft",
		"Blm",
		"Bum",
		"Mls"
	};
	
	final private String[] LL_SPECIES_2 = new String[] { 
		"Swo",
		"Bsh",
		"Spn",
		"Mak",
		"Ocs",
		"Por",
		"Fal",
		"Thr"
	};
	
	final private String[] LL_CATCH_TYPE = new String[] {
		"No",
		"Kg",
		"Disc"
	};
	
	final private Map<String, String> TYPE_TO_XPATH = new HashMap<String, String>();
	final private Map<String, QuantityType> TYPE_TO_UNIT = new HashMap<String, QuantityType>();
	
	/**
	 * Class constructor
	 */
	public FFALLParser() throws Exception {
		super();
		
		TYPE_TO_XPATH.put("No", "n");
		TYPE_TO_XPATH.put("Kg", "kg");
		TYPE_TO_XPATH.put("Disc", "disc");
		
		TYPE_TO_UNIT.put("No", QuantityType.NUMBER);
		TYPE_TO_UNIT.put("Kg", QuantityType.KILOGRAMS);
		TYPE_TO_UNIT.put("Disc", QuantityType.NUMBER);
	}

	public VesselData extractVesselData(Document data) throws Exception {
		Element TRIP_HEADER = (Element)xpath("//Trip_Header").evaluate(data, XPathConstants.NODE);
		
		String name = (String)xpath("VesselName/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String regCountry = (String)xpath("CountryReg/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String regNo = (String)xpath("RegNo/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String IRCS = (String)xpath("IRCS/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String company = (String)xpath("Company/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String FFAVID = (String)xpath("FFAVID/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		
		VesselData toReturn = new VesselData();
		toReturn.setName(name);
		toReturn.setRegistrationCountry(COUNTRY_MAP.get(regCountry));
		toReturn.setRegistrationNumber(regNo);
		toReturn.setIrcs(IRCS);
		toReturn.setFishingCompany(company);
		toReturn.setIdentifiers(new ArrayList<Identifier>());
		toReturn.getIdentifiers().add(Identifier.builder().type(IdentifierType.RFMO_ID).identifier("FFA " + FFAVID).build());
		
		return toReturn;
	}
	
	public Logsheet extractMainLogsheetData(Document data) throws Exception {
		Element TRIP_HEADER = (Element)xpath("//Trip_Header").evaluate(data, XPathConstants.NODE);
		
		String captain = (String)xpath("Captain/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String portDepart = (String)xpath("PortDepart/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String portUnload = (String)xpath("PortUnload/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		
		String dateDepart = (String)xpath("DateDepart/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String timeDepart = (String)xpath("TimeDepart/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);

		String dateArrival = (String)xpath("DateArrival/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		String timeArrival = (String)xpath("TimeArrival/text()").evaluate(TRIP_HEADER, XPathConstants.STRING);
		
		Logsheet toReturn = new Logsheet();
		
		toReturn.setVesselData(extractVesselData(data));
		toReturn.setCaptainName(captain);

		toReturn.setDeparturePort(PORT_MAP.get(portDepart));
		toReturn.setDepartureDate(dateFromString(dateDepart, timeDepart));

		toReturn.setUnloadingPort(PORT_MAP.get(portUnload));
		toReturn.setArrivalDate(dateFromString(dateArrival, timeArrival));

		return toReturn;
	}
	
	public List<Event> extractCatchEvents(Document data) throws Exception {
		List<Event> catchEvents = new ArrayList<Event>();
		
		NodeList events = eventElements(data);
		Event current;
		Element currentElement;
		
		for(int n=0; n<events.getLength(); n++) {
			currentElement = (Element)events.item(n);
			
			current = extractEvent(currentElement);
			
			if(current != null)
				catchEvents.add(current);
		}
		
		
		return catchEvents;
	}
	
	private NodeList eventElements(Document data) throws Exception {
		return (NodeList)xpath("//WholePage").evaluate(data, XPathConstants.NODESET);
	}
	
	private Event extractEvent(Element eventNode) throws Exception {
		if(isACatchEvent(eventNode))
			return extractCatchEvent(eventNode);
		
		return null;
	}
	
	private boolean isACatchEvent(Element eventNode) throws Exception {
		return "1-A SET".equals((String)xpath("Set/SetDetails/Setfields/Day/ACT/text()").evaluate(eventNode, XPathConstants.STRING));
	}
	
	private Event extractCatchEvent(Element eventNode) throws Exception {
		List<Effort> efforts;
		List<Catches> catches;
		List<Discards> discards;
		
		CatchAndEffortEvent extracted = new CatchAndEffortEvent(EventType.FISHING);
		extracted.setEfforts(efforts = new ArrayList<Effort>());
		extracted.setCatches(catches = new ArrayList<Catches>());
		extracted.setDiscards(discards = new ArrayList<Discards>());

		String stringValue = null;
		Double doubleValue = null;
		
		stringValue = (String)xpath("Set/SetDetails/Setfields/Day/NOHOOKS/text()").evaluate(eventNode, XPathConstants.STRING);
		
		stringValue = stringValue == null ? null : stringValue.trim();
		
		doubleValue = "".equals(stringValue) || stringValue == null ? null : Double.parseDouble(stringValue);
		
		if(doubleValue != null && Double.compare(doubleValue, 0) > 0)
			efforts.add(Effort.builder().effortType(EffortType.NUMBER_OF_HOOKS).effortValue(doubleValue).build());
		
		Element container;
		Discards currentDiscards;
		
		String path;
		
		String date, hour;
		
		date = (String)xpath("Set/SetDetails/Setfields/Day/DateEvent/text()").evaluate(eventNode, XPathConstants.STRING);
		hour = (String)xpath("Set/SetDetails/Setfields/Day/SETSTART/text()").evaluate(eventNode, XPathConstants.STRING);

		extracted.setStartDate(dateFromString(date, hour));
		extracted.setEndDate(dateFromString(date, hour));
		
		for(String type : LL_CATCH_TYPE) {
			path = "Set/CatchDetails/MainSpecies/Species1_" + type;
			
			container = (Element)xpath(path).evaluate(eventNode, XPathConstants.NODE);
			
			for(String species : LL_SPECIES_1) {
				path = species + "_" + TYPE_TO_XPATH.get(type) + "/text()";
				
				stringValue = (String)xpath(path).evaluate(container, XPathConstants.STRING);
			
				stringValue = stringValue == null ? null : stringValue.trim();
				
				doubleValue = "".equals(stringValue) || stringValue == null ? null : Double.parseDouble(stringValue);
				
				if(doubleValue == null || Double.compare(doubleValue, 0) <= 0) continue;
				
				if("Disc".equals(type)) {
					currentDiscards = new Discards();
					currentDiscards.setSpecies(SPECIES_MAP.get(species.toUpperCase()));
					currentDiscards.setQuantityType(TYPE_TO_UNIT.get(type));
					currentDiscards.setQuantity(doubleValue);
					currentDiscards.setReason(DiscardReason.OTHER);
					
					discards.add(currentDiscards);
				} else {
					catches.add(Catches.builder().species(SPECIES_MAP.get(species.toUpperCase())).quantityType(TYPE_TO_UNIT.get(type)).quantity(doubleValue).build());
				}
			}
		}
		
		for(String type : LL_CATCH_TYPE) {
			path = "Set/CatchDetails/MainSpecies/Species2_" + type;
			
			container = (Element)xpath(path).evaluate(eventNode, XPathConstants.NODE);
			
			for(String species : LL_SPECIES_2) {
				path = species + "_" + TYPE_TO_XPATH.get(type) + "/text()";
				
				stringValue = (String)xpath(path).evaluate(container, XPathConstants.STRING);
			
				stringValue = stringValue == null ? null : stringValue.trim();
				
				doubleValue = "".equals(stringValue) || stringValue == null ? null : Double.parseDouble(stringValue);
				
				if(doubleValue == null || Double.compare(doubleValue, 0) <= 0) continue;
				
				if("Disc".equals(type)) {
					currentDiscards = new Discards();
					currentDiscards.setSpecies(SPECIES_MAP.get(species.toUpperCase()));
					currentDiscards.setQuantityType(TYPE_TO_UNIT.get(type));
					currentDiscards.setQuantity(doubleValue);
					currentDiscards.setReason(DiscardReason.OTHER);
					
					discards.add(currentDiscards);
				} else {
					catches.add(Catches.builder().species(SPECIES_MAP.get(species.toUpperCase())).quantityType(TYPE_TO_UNIT.get(type)).quantity(doubleValue).build());
				}
			}
		}
		
		path = "Set/CatchDetails/Other/OtherForm/OtherSpecies";
		
		NodeList allOtherSpecies = (NodeList)xpath(path).evaluate(eventNode, XPathConstants.NODESET);
		Element otherSpecies;
		
		String speciesCode; 
		Species targetSpecies;
		
		for(int n=0; n<allOtherSpecies.getLength(); n++) {
			otherSpecies = (Element)allOtherSpecies.item(n);
			
			speciesCode = (String)xpath("Oth_Species/OthSp/text()").evaluate(otherSpecies, XPathConstants.STRING);
			
			speciesCode = speciesCode == null || "".equals(speciesCode = speciesCode.trim()) ? null : speciesCode;
			
			if(speciesCode == null) continue;
			
			stringValue = (String)xpath("Oth_No/Oth_n/text()").evaluate(otherSpecies, XPathConstants.STRING);
			
			stringValue = stringValue == null ? null : stringValue.trim();
			
			doubleValue = "".equals(stringValue) || stringValue == null ? null : Double.parseDouble(stringValue);
			
			if(doubleValue != null && Double.compare(doubleValue, 0) > 0) {
				targetSpecies = SPECIES_MAP.get(speciesCode.toUpperCase());
				
				if(targetSpecies == null) targetSpecies = Species.builder().alpha3Code("UNK").localName("Uknown / Other").build();
				
				catches.add(Catches.builder().species(targetSpecies).quantityType(QuantityType.NUMBER).quantity(doubleValue).build());
			};
			
			stringValue = (String)xpath("Oth_Kg/Oth_kg/text()").evaluate(otherSpecies, XPathConstants.STRING);
			
			stringValue = stringValue == null ? null : stringValue.trim();
			
			doubleValue = "".equals(stringValue) || stringValue == null ? null : Double.parseDouble(stringValue);
			
			if(doubleValue != null && Double.compare(doubleValue, 0) > 0) {
				targetSpecies = SPECIES_MAP.get(speciesCode.toUpperCase());
				
				if(targetSpecies == null) targetSpecies = Species.builder().alpha3Code("UNK").localName("Uknown / Other").build();
				
				catches.add(Catches.builder().species(targetSpecies).quantityType(QuantityType.KILOGRAMS).quantity(doubleValue).build());
			};
			
			stringValue = (String)xpath("Oth_Disc/Oth_disc/text()").evaluate(otherSpecies, XPathConstants.STRING);
			
			stringValue = stringValue == null ? null : stringValue.trim();
			
			doubleValue = "".equals(stringValue) || stringValue == null ? null : Double.parseDouble(stringValue);
			
			if(doubleValue != null && Double.compare(doubleValue, 0) > 0) {
				targetSpecies = SPECIES_MAP.get(speciesCode.toUpperCase());
				
				if(targetSpecies == null) targetSpecies = Species.builder().alpha3Code("UNK").localName("Uknown / Other").build();
				
				currentDiscards = new Discards();
				currentDiscards.setSpecies(targetSpecies);
				currentDiscards.setQuantityType(QuantityType.NUMBER);
				currentDiscards.setQuantity(doubleValue);
				currentDiscards.setReason(DiscardReason.OTHER);
				
				discards.add(currentDiscards);
			};
		}

		return extracted;
	}
	
	public Logbook extractLogbook(Document data) throws Exception {
		Logbook extracted = new Logbook();
		List<Logsheet> logsheets = new ArrayList<Logsheet>();
		
		extracted.setLogsheets(logsheets);
		
		Logsheet logsheet = extractMainLogsheetData(data);
		logsheet.setVesselData(extractVesselData(data));
		logsheet.setEvents(extractCatchEvents(data));
		
		logsheets.add(logsheet);
		
		return extracted;
	}
	
	public Logbook logbookFromFile(File pdf) throws Exception {
		return extractLogbook(importer.extractData(pdf));
	}
}