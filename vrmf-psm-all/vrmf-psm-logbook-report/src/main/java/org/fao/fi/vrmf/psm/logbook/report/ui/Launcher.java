/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;

import org.fao.fi.vrmf.psm.logbook.report.controllers.ReportController;
import org.fao.fi.vrmf.psm.ui.common.SplashManager;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.pushingpixels.substance.api.skin.SubstanceMarinerLookAndFeel;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 25, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 25, 2015
 */
@Slf4j
public class Launcher {
	static private SplashManager SPLASH_MANAGER = new SplashManager();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Utils.IS_LAUNCHING = true;
			
			JFrame.setDefaultLookAndFeelDecorated(true);
			
			UIManager.setLookAndFeel(new SubstanceMarinerLookAndFeel());
			
			SPLASH_MANAGER.initSplash();
			
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					ClassPathXmlApplicationContext ctx = null;
					
					try {
						SPLASH_MANAGER.splashText("Initializing the report application...");
						
						ctx = new ClassPathXmlApplicationContext("config/spring/psm-logbook-report-ui-context.xml");
						
						final ApplicationWindow window = ctx.getBean(ApplicationWindow.class);
						final JFrame frame = window.getApplicationFrame();
						
						Runtime.getRuntime().addShutdownHook(new Thread(window.getShutdownHook()));
						
						SPLASH_MANAGER.splashText("Preparing the UI...");
						
						frame.setVisible(true);
						frame.toFront();
						frame.repaint();
						
						SPLASH_MANAGER.splashText("Ready!");
						
						SPLASH_MANAGER.disposeSplash();
						
						final ReportController controller = ctx.getBean(ReportController.class);
						controller.showAllData();
					} catch(Throwable t) {
						log.error("Unexpected problem when starting the application: {}", t.getMessage(), t);
						
						if(ctx != null) ctx.close();
					}
				}
			});
		} catch (Throwable t) { 
			log.error("Cannot start the application: {}", t.getMessage(), t);
			
			SPLASH_MANAGER.disposeSplash();
		} 
	}
}
