/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.ui.panels;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.inject.Named;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import lombok.Getter;
import java.awt.Component;
import javax.swing.Box;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 23, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 23, 2016
 */
@Named
public class HeaderPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8365928533298333796L;
	
	@Getter private JTextField totalLogbooks;
	@Getter private JTextField totalLogsheets;
	@Getter private JButton btnReload;
	@Getter private JButton btnImportLogbook;
	@Getter private JButton btnImportFFA_LL;
	private Component _horizontalStrut;
	

	/**
	 * Create the panel.
	 */
	public HeaderPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[0];
		gridBagLayout.rowWeights = new double[0];
		setLayout(gridBagLayout);
		
		JLabel lblTotalLogbooks = new JLabel("Total logbooks:");
		lblTotalLogbooks.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblTotalLogbooks = new GridBagConstraints();
		gbc_lblTotalLogbooks.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblTotalLogbooks.anchor = GridBagConstraints.WEST;
		gbc_lblTotalLogbooks.insets = new Insets(5, 5, 5, 5);
		gbc_lblTotalLogbooks.gridx = 0;
		gbc_lblTotalLogbooks.gridy = 0;
		add(lblTotalLogbooks, gbc_lblTotalLogbooks);
		
		totalLogbooks = new JTextField();
		totalLogbooks.setEnabled(false);
		totalLogbooks.setEditable(false);
		GridBagConstraints gbc_totalLogbooks = new GridBagConstraints();
		gbc_totalLogbooks.weightx = 20.0;
		gbc_totalLogbooks.anchor = GridBagConstraints.WEST;
		gbc_totalLogbooks.insets = new Insets(5, 5, 5, 5);
		gbc_totalLogbooks.fill = GridBagConstraints.HORIZONTAL;
		gbc_totalLogbooks.gridx = 1;
		gbc_totalLogbooks.gridy = 0;
		add(totalLogbooks, gbc_totalLogbooks);
		totalLogbooks.setColumns(10);
		
		JLabel lblTotalLogsheets = new JLabel("Total logsheets:");
		lblTotalLogsheets.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblTotalLogsheets = new GridBagConstraints();
		gbc_lblTotalLogsheets.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblTotalLogsheets.anchor = GridBagConstraints.WEST;
		gbc_lblTotalLogsheets.insets = new Insets(5, 5, 5, 5);
		gbc_lblTotalLogsheets.gridx = 2;
		gbc_lblTotalLogsheets.gridy = 0;
		add(lblTotalLogsheets, gbc_lblTotalLogsheets);
		
		totalLogsheets = new JTextField();
		totalLogsheets.setEnabled(false);
		totalLogsheets.setEditable(false);
		GridBagConstraints gbc_totalLogsheets = new GridBagConstraints();
		gbc_totalLogsheets.weightx = 20.0;
		gbc_totalLogsheets.insets = new Insets(5, 5, 5, 5);
		gbc_totalLogsheets.fill = GridBagConstraints.HORIZONTAL;
		gbc_totalLogsheets.gridx = 3;
		gbc_totalLogsheets.gridy = 0;
		add(totalLogsheets, gbc_totalLogsheets);
		totalLogsheets.setColumns(10);
		
		btnReload = new JButton("Refresh");
		GridBagConstraints gbc_btnReload = new GridBagConstraints();
		gbc_btnReload.insets = new Insets(5, 5, 5, 5);
		gbc_btnReload.anchor = GridBagConstraints.EAST;
		gbc_btnReload.gridx = 4;
		gbc_btnReload.gridy = 0;
		add(btnReload, gbc_btnReload);
		
		JLabel lblImport = new JLabel("Import");
		lblImport.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(5, 5, 5, 5);
		gbc_label.gridx = 5;
		gbc_label.gridy = 0;
		add(lblImport, gbc_label);

		btnImportLogbook = new JButton("Logbook");
		GridBagConstraints gbc_btnImportLogbook = new GridBagConstraints();
		gbc_btnImportLogbook.insets = new Insets(5, 5, 5, 5);
		gbc_btnImportLogbook.anchor = GridBagConstraints.EAST;
		gbc_btnImportLogbook.gridx = 6;
		gbc_btnImportLogbook.gridy = 0;
		add(btnImportLogbook, gbc_btnImportLogbook);
		
		btnImportFFA_LL = new JButton("FFA LL report");
		GridBagConstraints gbc_btnImportFFA_LL = new GridBagConstraints();
		gbc_btnImportFFA_LL.insets = new Insets(5, 5, 5, 0);
		gbc_btnImportFFA_LL.anchor = GridBagConstraints.EAST;
		gbc_btnImportFFA_LL.gridx = 7;
		gbc_btnImportFFA_LL.gridy = 0;
		add(btnImportFFA_LL, gbc_btnImportFFA_LL);
		
		_horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut.weightx = 100.0;
		gbc_horizontalStrut.insets = new Insets(0, 0, 0, 5);
		gbc_horizontalStrut.gridx = 8;
		gbc_horizontalStrut.gridy = 0;
		add(_horizontalStrut, gbc_horizontalStrut);
	}
}
