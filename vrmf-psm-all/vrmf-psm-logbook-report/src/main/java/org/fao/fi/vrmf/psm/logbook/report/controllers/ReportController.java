/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.controllers;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.report.model.CatchesByCountry;
import org.fao.fi.vrmf.psm.logbook.report.services.DataServices;
import org.fao.fi.vrmf.psm.logbook.report.ui.ApplicationWindow;
import org.fao.fi.vrmf.psm.logbook.report.ui.model.CatchReportTableModel;
import org.fao.fi.vrmf.psm.logbook.report.ui.panels.DataReportPanel;
import org.fao.fi.vrmf.psm.logbook.report.ui.panels.HeaderPanel;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.components.handlers.InterceptingActionListener;
import org.jdesktop.swingx.JXDatePicker;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 23, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 23, 2016
 */
@Named @Singleton
public class ReportController {
	@Inject private ApplicationWindow application;
	@Inject private DataServices dataServices;
		
	private DataReportPanel byFlagCountry, byPortCountry;
	private HeaderPanel header;
	
	private List<Logsheet> logsheets;
	
	@PostConstruct
	private void initialize() throws Exception {
		List<String> logbookFiles = dataServices.logbookFiles(null);
		logsheets = dataServices.readLogsheets(null);
		
		header = application.getMainPanel().getHeader();
		
		byFlagCountry = application.getMainPanel().getByFlagCountry();
		byPortCountry = application.getMainPanel().getByPortCountry();
		
		byFlagCountry.setTitle("FLAG countries");
		byPortCountry.setTitle("PORT countries");
		
		header.getTotalLogbooks().setText(String.valueOf(logbookFiles.size()));
		header.getTotalLogsheets().setText(String.valueOf(logsheets.size()));
		
		List<Country> flagCountries = dataServices.flagCountries(logsheets);
		flagCountries.add(0, null);
		
		List<Species> flagCountrySpecies = dataServices.flagCountrySpecies(logsheets);
		flagCountrySpecies.add(0, null);
		
		byFlagCountry.getCountry().setModel(new DefaultComboBoxModel(flagCountries.toArray(new Country[flagCountries.size()])));
		byFlagCountry.getSpecies().setModel(new DefaultComboBoxModel(flagCountrySpecies.toArray(new Species[flagCountrySpecies.size()])));

		byFlagCountry.getBtnRefresh().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				refreshByFlagCountry();
			}
		});
		
		byFlagCountry.getBtnExport().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				exportByFlagCountry();
			}
		});
		
		for(JComboBox in : new JComboBox[] {
			byFlagCountry.getCountry(),
			byFlagCountry.getSpecies()
		}) in.addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				refreshByFlagCountry();
			};
		});
		
		for(JToggleButton in : new JToggleButton[] {
			byFlagCountry.getUnitNo(),
			byFlagCountry.getUnitKg(),
			byFlagCountry.getUnitMt()
		}) in.addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				refreshByFlagCountry();
			};
		});
		
		for(JXDatePicker in : new JXDatePicker[] {
			byFlagCountry.getDateFrom(),
			byFlagCountry.getDateTo()
		}) {
			in.addActionListener(new InterceptingActionListener() {
				protected void doActionPerformed(ActionEvent e) throws Throwable {
					if(JXDatePicker.COMMIT_KEY.equals(e.getActionCommand())) {
						refreshByFlagCountry();
					}
				}
			});
			
			in.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent e) {
					if("date".equals(e.getPropertyName())) {
						refreshByFlagCountry();
					}
				}
			});
		}
		
		UIUtils.initialize(byPortCountry.getDataTable());

		List<Country> portCountries = dataServices.portCountries(logsheets);
		portCountries.add(0, null);
		
		List<Species> portCountrySpecies = dataServices.portCountrySpecies(logsheets);
		portCountrySpecies.add(0, null);

		byPortCountry.getCountry().setModel(new DefaultComboBoxModel(portCountries.toArray(new Country[portCountries.size()])));
		byPortCountry.getSpecies().setModel(new DefaultComboBoxModel(portCountrySpecies.toArray(new Species[portCountrySpecies.size()])));
		
		byPortCountry.getBtnRefresh().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				refreshByPortCountry();
			}
		});
		
		byPortCountry.getBtnExport().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				exportByPortCountry();
			}
		});
		
		header.getBtnReload().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				try {
					application.showGlassPane("Reloading logbooks data...");
					
					reload();
				} finally {
					application.hideGlassPane();
				}
			}
		});
		
		header.getBtnImportLogbook().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				try {
					application.showGlassPane("Importing logbook data...");
					
					File selected = UIUtils.selectFile(application.getMainPanel(), "Import an existing logbook file", null, new FileNameExtensionFilter("PSM logbook files", "logbook"), "The import has been canceled");
					
					if(selected != null) {
						if(!dataServices.alreadyExists(selected.getName()) || UIUtils.confirm("The selected logbook file was already imported", "If you proceed, its previous data will be overwritten.\n\nAre you sure you want to continue?")) {
							dataServices.importLogbook(selected);
							
							reload();
						}
					}
				} finally {
					application.hideGlassPane();
				}
			}
		});
		
		header.getBtnImportFFA_LL().addActionListener(new InterceptingActionListener() {
			@Override
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				try {
					application.showGlassPane("Importing FFA LL report data...");
					
					File selected = UIUtils.selectFile(application.getMainPanel(), "Import an existing FFA LL report file", null, new FileNameExtensionFilter("FFA LL report files", "pdf"), "The import has been canceled");
					
					if(selected != null) {
						if(!dataServices.alreadyExists(selected.getName().replace(".pdf", ".logbook")) || UIUtils.confirm("The selected file was already imported", "If you proceed, its previous data will be overwritten.\n\nAre you sure you want to continue?")) {
							dataServices.importFFALL(selected);
							
							reload();
						}
					}
				} finally {
					application.hideGlassPane();
				}
			}
		});
		
		for(JComboBox in : new JComboBox[] {
			byPortCountry.getCountry(),
			byPortCountry.getSpecies()
		}) in.addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				refreshByPortCountry();
			};
		});
		
		for(JToggleButton in : new JToggleButton[] {
			byPortCountry.getUnitNo(),
			byPortCountry.getUnitKg(),
			byPortCountry.getUnitMt()
		}) in.addActionListener(new InterceptingActionListener() {
			protected void doActionPerformed(ActionEvent e) throws Throwable {
				refreshByPortCountry();
			};
		});
		
		for(JXDatePicker in : new JXDatePicker[] {
			byPortCountry.getDateFrom(),
			byPortCountry.getDateTo()
		}) {
			in.addActionListener(new InterceptingActionListener() {
				protected void doActionPerformed(ActionEvent e) throws Throwable {
					if(JXDatePicker.COMMIT_KEY.equals(e.getActionCommand())) {
						refreshByPortCountry();
					}
				}
			});
			
			in.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent e) {
					if("date".equals(e.getPropertyName())) {
						refreshByPortCountry();
					}
				}
			});
		}
	}
	
	private void reload() throws Exception {
		List<String> logbookFiles = dataServices.logbookFiles(null);
		logsheets = dataServices.readLogsheets(null);
		
		header.getTotalLogbooks().setText(String.valueOf(logbookFiles.size()));
		header.getTotalLogsheets().setText(String.valueOf(logsheets.size()));
		
		if(logbookFiles.isEmpty()) {
			UIUtils.alert(application.getApplicationFrame(), "No logbook file is currently available in local repository");
		} else {
			if(logsheets.isEmpty()) {
				UIUtils.alert(application.getApplicationFrame(), "No logsheet is currently available among logbooks found in local repository");
			} else {
				List<Country> flagCountries = dataServices.flagCountries(logsheets);
				flagCountries.add(0, null);
				
				List<Species> flagCountrySpecies = dataServices.flagCountrySpecies(logsheets);
				flagCountrySpecies.add(0, null);

				byFlagCountry.getCountry().setModel(new DefaultComboBoxModel(flagCountries.toArray(new Country[flagCountries.size()])));
				byFlagCountry.getSpecies().setModel(new DefaultComboBoxModel(flagCountrySpecies.toArray(new Species[flagCountrySpecies.size()])));

				List<Country> portCountries = dataServices.portCountries(logsheets);
				portCountries.add(0, null);
				
				List<Species> portCountrySpecies = dataServices.portCountrySpecies(logsheets);
				portCountrySpecies.add(0, null);

				byPortCountry.getCountry().setModel(new DefaultComboBoxModel(portCountries.toArray(new Country[portCountries.size()])));
				byPortCountry.getSpecies().setModel(new DefaultComboBoxModel(portCountrySpecies.toArray(new Species[portCountrySpecies.size()])));
				
				showAllData();
			}
		}
	}
	
	public void showAllData() throws Exception {
		byFlagCountry.getCountry().setSelectedItem(null);
		byFlagCountry.getSpecies().setSelectedItem(null);
		byFlagCountry.getDateFrom().setDate(null);
		byFlagCountry.getDateTo().setDate(null);
		byFlagCountry.getUnitNo().setSelected(true);
		byFlagCountry.getUnitKg().setSelected(true);
		byFlagCountry.getUnitMt().setSelected(true);
		
		byPortCountry.getCountry().setSelectedItem(null);
		byPortCountry.getSpecies().setSelectedItem(null);
		byPortCountry.getDateFrom().setDate(null);
		byPortCountry.getDateTo().setDate(null);
		byPortCountry.getUnitNo().setSelected(true);
		byPortCountry.getUnitKg().setSelected(true);
		byPortCountry.getUnitMt().setSelected(true);
		
		try {
			application.showGlassPane("Updating data by flag country...");
			
			refreshByFlagCountry();
			
			application.hideGlassPane();
			
			application.showGlassPane("Updating data by port country...");
			
			refreshByPortCountry();
			
			application.hideGlassPane();
		} finally {
			application.hideGlassPane();
		}
	}
	
	private void refreshByFlagCountry() {
		Country country = (Country)byFlagCountry.getCountry().getSelectedItem();
		Species species = (Species)byFlagCountry.getSpecies().getSelectedItem();
		Date from = byFlagCountry.getDateFrom().getDate();
		Date to = byFlagCountry.getDateTo().getDate();
		
		boolean selectNo = byFlagCountry.getUnitNo().isSelected();
		boolean selectKg = byFlagCountry.getUnitKg().isSelected();
		boolean selectMt = byFlagCountry.getUnitMt().isSelected();
		
		List<QuantityType> selected = new ArrayList<QuantityType>();
		if(selectNo) selected.add(QuantityType.NUMBER);
		if(selectKg) selected.add(QuantityType.KILOGRAMS);
		if(selectMt) selected.add(QuantityType.METRIC_TONS);
		
		List<CatchesByCountry> catches = 
			dataServices.filterByUnit(
				dataServices.filterBySpecies(
					dataServices.filterByCountry(
						dataServices.byCountry(
							dataServices.groupUniqueCatchesByDatesByFlagCountry(logsheets, from, to)
						), country
					), species
				), selected.toArray(new QuantityType[0])
			);

		((CatchReportTableModel)byFlagCountry.getDataTable().getModel()).update(catches);
		
		byFlagCountry.getBtnExport().setEnabled(!catches.isEmpty());
	}
	
	private void exportByFlagCountry() throws IOException {
		Country country = (Country)byFlagCountry.getCountry().getSelectedItem();
		Species species = (Species)byFlagCountry.getSpecies().getSelectedItem();
		Date from = byFlagCountry.getDateFrom().getDate();
		Date to = byFlagCountry.getDateTo().getDate();
		
		boolean selectNo = byFlagCountry.getUnitNo().isSelected();
		boolean selectKg = byFlagCountry.getUnitKg().isSelected();
		boolean selectMt = byFlagCountry.getUnitMt().isSelected();
		
		String filename = "catchesReportByFlagState";
		
		if(country != null) filename += "_" + country.getIso2Code();
		if(species != null) filename += "_" + species.getAlpha3Code();
		if(from != null) filename += "_from_" + new SimpleDateFormat("yyyy_MM_dd").format(from);
		if(to != null) filename += "_to_" + new SimpleDateFormat("yyyy_MM_dd").format(to);
		
		if(selectNo) filename += "_no";
		if(selectKg) filename += "_kg";
		if(selectMt) filename += "_mt";
		
		File folder = UIUtils.selectDirectory(byFlagCountry, "Select a destination folder", "./export", null, "Operation canceled");
		
		if(folder != null) {
			File target = dataServices.storeReport(folder, filename, ((CatchReportTableModel)byFlagCountry.getDataTable().getModel()).getRawData());
			
			UIUtils.dialog(application.getApplicationFrame(), "Operation completed", "Data by flag country have been exported to CSV file '" + target.getAbsolutePath() + "'");
		}
	}

	private void refreshByPortCountry() {
		Country country = (Country)byPortCountry.getCountry().getSelectedItem();
		Species species = (Species)byPortCountry.getSpecies().getSelectedItem();
		Date from = byPortCountry.getDateFrom().getDate();
		Date to = byPortCountry.getDateTo().getDate();
		
		boolean selectNo = byPortCountry.getUnitNo().isSelected();
		boolean selectKg = byPortCountry.getUnitKg().isSelected();
		boolean selectMt = byPortCountry.getUnitMt().isSelected();
		
		List<QuantityType> selected = new ArrayList<QuantityType>();
		if(selectNo) selected.add(QuantityType.NUMBER);
		if(selectKg) selected.add(QuantityType.KILOGRAMS);
		if(selectMt) selected.add(QuantityType.METRIC_TONS);
		
		List<CatchesByCountry> catches = 
			dataServices.filterByUnit(
				dataServices.filterBySpecies(
					dataServices.filterByCountry(
						dataServices.byCountry(
							dataServices.groupUniqueCatchesByDatesByPortCountry(logsheets, from, to)
						), country
					), species
				), selected.toArray(new QuantityType[0])
			);

		((CatchReportTableModel)byPortCountry.getDataTable().getModel()).update(catches);
		
		byPortCountry.getBtnExport().setEnabled(!catches.isEmpty());
	}
	
	private void exportByPortCountry() throws IOException {
		Country country = (Country)byPortCountry.getCountry().getSelectedItem();
		Species species = (Species)byPortCountry.getSpecies().getSelectedItem();
		Date from = byPortCountry.getDateFrom().getDate();
		Date to = byPortCountry.getDateTo().getDate();
		
		boolean selectNo = byPortCountry.getUnitNo().isSelected();
		boolean selectKg = byPortCountry.getUnitKg().isSelected();
		boolean selectMt = byPortCountry.getUnitMt().isSelected();
		
		String filename = "catchesReportByPortState";
		
		if(country != null) filename += "_" + country.getIso2Code();
		if(species != null) filename += "_" + species.getAlpha3Code();
		if(from != null) filename += "_from_" + new SimpleDateFormat("yyyy_MM_dd").format(from);
		if(to != null) filename += "_to_" + new SimpleDateFormat("yyyy_MM_dd").format(to);
		
		if(selectNo) filename += "_no";
		if(selectKg) filename += "_kg";
		if(selectMt) filename += "_mt";
		
		File folder = UIUtils.selectDirectory(byPortCountry, "Select a destination folder", "./export", null, "Operation canceled");
		
		if(folder != null) {
			File target = dataServices.storeReport(folder, filename, ((CatchReportTableModel)byPortCountry.getDataTable().getModel()).getRawData());
			
			UIUtils.dialog(application.getApplicationFrame(), "Operation completed", "Data by port country have been exported to CSV file '" + target.getAbsolutePath() + "'");
		}
	}
}