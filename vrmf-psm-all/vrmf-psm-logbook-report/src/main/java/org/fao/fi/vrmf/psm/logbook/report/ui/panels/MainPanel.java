/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.ui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.JPanel;

import org.fao.fi.vrmf.psm.ui.common.support.Utils;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 24, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 24, 2016
 */
@Named @Singleton
public class MainPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -288186356087758819L;

	@Inject private @Getter HeaderPanel header;
	@Inject private @Getter DataReportPanel byFlagCountry;
	@Inject private @Getter DataReportPanel byPortCountry;
	
	/**
	 * Class constructor
	 *
	 */
	public MainPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		header = new HeaderPanel();
		byFlagCountry = new DataReportPanel();
		byPortCountry = new DataReportPanel();
		
		initialize();
	}
	
	/**
	 * Create the panel.
	 */
	@PostConstruct
	private void initialize() {
		GridBagLayout mainLayout = new GridBagLayout();
		mainLayout.rowHeights = new int[0];
		mainLayout.columnWeights = new double[0];
		mainLayout.rowWeights = new double[0];
		
		setLayout(mainLayout);
		
		GridBagConstraints gbc_header = new GridBagConstraints();
		gbc_header.fill = GridBagConstraints.HORIZONTAL;
		gbc_header.anchor = GridBagConstraints.WEST;
		gbc_header.insets = new Insets(0, 0, 5, 0);
		gbc_header.gridwidth = 2;
		gbc_header.weightx = 100.0;
		gbc_header.gridx = 0;
		gbc_header.gridy = 0;
		add(header, gbc_header);
		
		GridBagConstraints gbc_byCountryFlag = new GridBagConstraints();
		gbc_byCountryFlag.weightx = 50.0;
		gbc_byCountryFlag.weighty = 100.0;
		gbc_byCountryFlag.insets = new Insets(0, 0, 0, 5);
		gbc_byCountryFlag.fill = GridBagConstraints.BOTH;
		gbc_byCountryFlag.gridx = 0;
		gbc_byCountryFlag.gridy = 1;
		add(byFlagCountry, gbc_byCountryFlag);
		
		GridBagConstraints gbc_byPortFlag = new GridBagConstraints();
		gbc_byPortFlag.weighty = 100.0;
		gbc_byPortFlag.weightx = 50.0;
		gbc_byPortFlag.fill = GridBagConstraints.BOTH;
		gbc_byPortFlag.gridx = 1;
		gbc_byPortFlag.gridy = 1;
		add(byPortCountry, gbc_byPortFlag);
	}
}
