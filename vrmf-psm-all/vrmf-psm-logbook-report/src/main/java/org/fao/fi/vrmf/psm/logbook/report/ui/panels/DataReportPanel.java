/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.ui.panels;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.border.TitledBorder;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.report.model.CatchesByCountry;
import org.fao.fi.vrmf.psm.logbook.report.ui.model.CatchReportTableModel;
import org.fao.fi.vrmf.psm.ui.common.renderers.CountryRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.SpeciesRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CodedEntityTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.renderers.table.CountryTableCellRenderer;
import org.fao.fi.vrmf.psm.ui.common.support.UIUtils;
import org.fao.fi.vrmf.psm.ui.common.support.Utils;
import org.jdesktop.swingx.JXDatePicker;

import lombok.Getter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 23, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 23, 2016
 */
@Named
public class DataReportPanel extends JPanel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -984991147556429643L;
	
	@Getter private JTable dataTable;
	@Getter private JComboBox country;
	@Getter private JXDatePicker dateFrom;
	@Getter private JXDatePicker dateTo;
	@Getter private JComboBox species;
	@Getter private JToggleButton unitNo;
	@Getter private JToggleButton unitKg;
	@Getter private JToggleButton unitMt;
	@Getter private JButton btnRefresh;
	@Getter private JButton btnExport;

	/**
	 * Create the panel.
	 */
	public DataReportPanel() {
		if(Utils.IS_LAUNCHING) return;
		
		initialize();
	}
	
	@PostConstruct
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{1.0};
		setLayout(gridBagLayout);
		
		JPanel controls = new JPanel();
		GridBagConstraints gbc_controls = new GridBagConstraints();
		gbc_controls.weightx = 20.0;
		gbc_controls.insets = new Insets(0, 0, 5, 0);
		gbc_controls.fill = GridBagConstraints.BOTH;
		gbc_controls.gridx = 0;
		gbc_controls.gridy = 0;
		add(controls, gbc_controls);
		GridBagLayout gbl_controls = new GridBagLayout();
		gbl_controls.columnWidths = new int[0];
		gbl_controls.rowHeights = new int[0];
		gbl_controls.columnWeights = new double[0];
		gbl_controls.rowWeights = new double[0];
		controls.setLayout(gbl_controls);
		
		JLabel lblCountry = new JLabel("Country");
		GridBagConstraints gbc_lblCountry = new GridBagConstraints();
		gbc_lblCountry.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCountry.anchor = GridBagConstraints.WEST;
		gbc_lblCountry.insets = new Insets(5, 5, 5, 0);
		gbc_lblCountry.gridx = 0;
		gbc_lblCountry.gridy = 0;
		controls.add(lblCountry, gbc_lblCountry);
		
		country = new JComboBox();
		GridBagConstraints gbc__country = new GridBagConstraints();
		gbc__country.insets = new Insets(0, 5, 5, 5);
		gbc__country.fill = GridBagConstraints.HORIZONTAL;
		gbc__country.gridx = 0;
		gbc__country.gridy = 1;
		controls.add(country, gbc__country);
		
		country.setRenderer(new CountryRenderer());
		
		JLabel lblFrom = new JLabel("From");
		GridBagConstraints gbc_lblFrom = new GridBagConstraints();
		gbc_lblFrom.insets = new Insets(5, 5, 5, 0);
		gbc_lblFrom.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblFrom.anchor = GridBagConstraints.WEST;
		gbc_lblFrom.gridx = 0;
		gbc_lblFrom.gridy = 2;
		controls.add(lblFrom, gbc_lblFrom);
		
		dateFrom = new JXDatePicker();
		GridBagConstraints gbc__dateFrom = new GridBagConstraints();
		gbc__dateFrom.insets = new Insets(0, 5, 5, 5);
		gbc__dateFrom.fill = GridBagConstraints.HORIZONTAL;
		gbc__dateFrom.gridx = 0;
		gbc__dateFrom.gridy = 3;
		controls.add(dateFrom, gbc__dateFrom);
		
		JLabel lblTo = new JLabel("To");
		GridBagConstraints gbc_lblTo = new GridBagConstraints();
		gbc_lblTo.insets = new Insets(5, 5, 5, 0);
		gbc_lblTo.anchor = GridBagConstraints.WEST;
		gbc_lblTo.gridx = 0;
		gbc_lblTo.gridy = 4;
		controls.add(lblTo, gbc_lblTo);
		
		dateTo = new JXDatePicker();
		GridBagConstraints gbc__dateTo = new GridBagConstraints();
		gbc__dateTo.insets = new Insets(0, 5, 5, 5);
		gbc__dateTo.fill = GridBagConstraints.HORIZONTAL;
		gbc__dateTo.anchor = GridBagConstraints.WEST;
		gbc__dateTo.gridx = 0;
		gbc__dateTo.gridy = 5;
		controls.add(dateTo, gbc__dateTo);
		
		JLabel lblSpecies = new JLabel("Species");
		GridBagConstraints gbc_lblSpecies = new GridBagConstraints();
		gbc_lblSpecies.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblSpecies.anchor = GridBagConstraints.WEST;
		gbc_lblSpecies.insets = new Insets(5, 5, 5, 0);
		gbc_lblSpecies.gridx = 0;
		gbc_lblSpecies.gridy = 6;
		controls.add(lblSpecies, gbc_lblSpecies);
		
		species = new JComboBox();
		GridBagConstraints gbc__species = new GridBagConstraints();
		gbc__species.insets = new Insets(0, 5, 5, 5);
		gbc__species.fill = GridBagConstraints.HORIZONTAL;
		gbc__species.gridx = 0;
		gbc__species.gridy = 7;
		controls.add(species, gbc__species);
		
		species.setRenderer(new SpeciesRenderer());
		
		JPanel units = new JPanel();
		GridBagConstraints gbc_units = new GridBagConstraints();
		gbc_units.insets = new Insets(0, 0, 5, 0);
		gbc_units.fill = GridBagConstraints.HORIZONTAL;
		gbc_units.gridx = 0;
		gbc_units.gridy = 8;
		controls.add(units, gbc_units);
		GridBagLayout gbl_units = new GridBagLayout();
		gbl_units.columnWeights = new double[0];
		gbl_units.rowWeights = new double[0];
		units.setLayout(gbl_units);
		
		unitNo = new JToggleButton("No.");
		unitNo.setSelected(true);
		GridBagConstraints gbc__unitNo = new GridBagConstraints();
		gbc__unitNo.insets = new Insets(5, 5, 5, 5);
		gbc__unitNo.gridx = 0;
		gbc__unitNo.gridy = 0;
		units.add(unitNo, gbc__unitNo);
		
		unitKg = new JToggleButton("Kg");
		unitKg.setSelected(true);
		GridBagConstraints gbc__unitKg = new GridBagConstraints();
		gbc__unitKg.insets = new Insets(5, 5, 5, 5);
		gbc__unitKg.gridx = 1;
		gbc__unitKg.gridy = 0;
		units.add(unitKg, gbc__unitKg);
		
		unitMt = new JToggleButton("Mt");
		unitMt.setSelected(true);
		GridBagConstraints gbc__unitMt = new GridBagConstraints();
		gbc__unitMt.insets = new Insets(5, 5, 5, 5);
		gbc__unitMt.gridx = 2;
		gbc__unitMt.gridy = 0;
		units.add(unitMt, gbc__unitMt);
		
		JPanel actions = new JPanel();
		GridBagConstraints gbc_actions = new GridBagConstraints();
		gbc_actions.insets = new Insets(0, 0, 5, 0);
		gbc_actions.fill = GridBagConstraints.BOTH;
		gbc_actions.gridx = 0;
		gbc_actions.gridy = 9;
		controls.add(actions, gbc_actions);
		GridBagLayout gbl_actions = new GridBagLayout();
		gbl_actions.columnWeights = new double[0];
		gbl_actions.rowWeights = new double[0];
		actions.setLayout(gbl_actions);
		
		btnRefresh = new JButton("Refresh");
		GridBagConstraints gbc__btnRefresh = new GridBagConstraints();
		gbc__btnRefresh.insets = new Insets(5, 5, 5, 5);
		gbc__btnRefresh.gridx = 0;
		gbc__btnRefresh.gridy = 0;
		actions.add(btnRefresh, gbc__btnRefresh);
		
		btnExport = new JButton("Export");
		btnExport.setEnabled(false);
		GridBagConstraints gbc__btnExport = new GridBagConstraints();
		gbc__btnExport.insets = new Insets(5, 5, 5, 5);
		gbc__btnExport.gridx = 1;
		gbc__btnExport.gridy = 0;
		actions.add(btnExport, gbc__btnExport);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.weighty = 100.0;
		gbc_verticalStrut.gridx = 0;
		gbc_verticalStrut.gridy = 10;
		controls.add(verticalStrut, gbc_verticalStrut);
		
		JPanel data = new JPanel();
		data.setBorder(new TitledBorder(null, "Data", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_data = new GridBagConstraints();
		gbc_data.weightx = 100.0;
		gbc_data.fill = GridBagConstraints.BOTH;
		gbc_data.gridx = 1;
		gbc_data.gridy = 0;
		add(data, gbc_data);
		GridBagLayout gbl_data = new GridBagLayout();
		gbl_data.columnWeights = new double[]{1.0};
		gbl_data.rowWeights = new double[]{1.0};
		data.setLayout(gbl_data);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		data.add(scrollPane, gbc_scrollPane);
		
		dataTable = new JTable();
		scrollPane.setViewportView(dataTable);

		UIUtils.initialize(dataTable);
		
		dataTable.setModel(new CatchReportTableModel(new ArrayList<CatchesByCountry>()));
		dataTable.setDefaultRenderer(Country.class, new CountryTableCellRenderer());
		dataTable.setDefaultRenderer(QuantityType.class, new CodedEntityTableCellRenderer());
	}
	
	public void setTitle(String title) {
		setBorder(new TitledBorder(null, "Data report panel for " + title, TitledBorder.LEADING, TitledBorder.TOP, null, null));
	}
}
