/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.model;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 23, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 23, 2016
 */
@NoArgsConstructor @AllArgsConstructor
public class CatchesByCountry extends Catches {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 706354454725239410L;
	
	@Getter @Setter private Country country;
	
	public CatchesByCountry(Country country, Catches catches) {
		super(null, catches.getSpecies(), catches.getQuantity(), catches.getQuantityType());
		
		this.country = country;
	}
}
