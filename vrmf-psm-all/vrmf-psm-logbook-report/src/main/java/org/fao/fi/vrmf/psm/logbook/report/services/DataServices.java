/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchAndEffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.EffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches.CatchesKey;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.codes.QuantityType;
import org.fao.fi.vrmf.psm.logbook.report.model.CatchesByCountry;
import org.fao.fi.vrmf.psm.logbook.report.plugins.importers.FFALLParser;

import au.com.bytecode.opencsv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 22, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 22, 2016
 */
@Named @Singleton
@Slf4j
public class DataServices {
	final static private String REPOSITORY_FOLDER = "repo";
	
	/**
	 * Class constructor
	 *
	 */
	public DataServices() throws Exception {
	}
	
	private FFALLParser ffaLLParser = new FFALLParser();
	
	public boolean alreadyExists(String filename) {
		return new File(REPOSITORY_FOLDER, filename).exists();
	}
	
	public void importLogbook(File toImport) throws IOException {
		File target = new File(REPOSITORY_FOLDER, toImport.getName());
		
		byte[] buffer = new byte[8192];
		
		int len = -1;
		
		FileInputStream in = new FileInputStream(toImport);
		FileOutputStream out = new FileOutputStream(target);
		
		while((len = in.read(buffer)) != -1) {
			out.write(buffer, 0, len);
		}
		
		in.close();
		out.flush();
		out.close();
	}
	
	public void importFFALL(File toImport) throws Exception {
		File target = new File(REPOSITORY_FOLDER, toImport.getName().replace(".pdf", ".logbook"));
		
		FileOutputStream stream = null; 
		GZIPOutputStream zipped = null;
		
		boolean imported = true;
		
		try {
			zipped = new GZIPOutputStream(stream = new FileOutputStream(target));

			JAXBContext.newInstance(Logbook.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller().marshal(ffaLLParser.logbookFromFile(toImport), zipped);
		} catch(Exception t) {
			imported = false;
			
			throw t;
		} finally {
			if(zipped != null) {
				zipped.flush();
				zipped.close();
			}
			
			if(stream != null) {
				stream.flush();
				stream.close();
			}
			
			if(!imported) {
				System.out.println("DELETED: " + target.delete());
			}
		}
	}
	
	public List<String> logbookFiles(String folder) throws Exception {
		String[] files = new File(folder == null ? REPOSITORY_FOLDER : folder).list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".logbook");
			}
		});
		
		List<String> paths = new ArrayList<String>();
		
		if(files != null)
			for(String file : files)
				paths.add(new File(folder == null ? REPOSITORY_FOLDER : folder, file).getAbsolutePath());
		
		return paths;
	}
	
	public List<Logsheet> readLogsheets(String folder) throws Exception {
		List<Logsheet> imported = new ArrayList<Logsheet>();
		
		List<String> toImport = logbookFiles(folder);
		
		for(String file : toImport) {
			try {
				List<Logsheet> forFile = fromFile(file);
				
				if(forFile != null && !forFile.isEmpty())
					log.info("{} logsheets found in file {}", forFile.size(), file);
				else
					log.warn("No logsheets found in file {}", forFile.size(), file);
				
				imported.addAll(forFile);
			} catch(Throwable t) {
				log.error("Unable to import logbook from file {} in repo {}", file, REPOSITORY_FOLDER, t);
			}
		}
		
		return imported;
	}
	
	public List<Country> flagCountries(List<Logsheet> logsheets) {
		List<Country> filtered = new ArrayList<Country>();
		
		Country current = null;
		
		for(Logsheet in : logsheets) {
			if(!filtered.contains(current = in.getVesselData().getRegistrationCountry()))
				filtered.add(current);
		}
		
		Collections.sort(filtered, new Comparator<Country>() {
			@Override
			public int compare(Country o1, Country o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		return filtered;
	}

	public List<Country> portCountries(List<Logsheet> logsheets) {
		List<Country> filtered = new ArrayList<Country>();
		
		Country current = null;
		
		for(Logsheet in : logsheets) {
			if(!filtered.contains(current = in.getUnloadingPort().getCountry()))
				filtered.add(current);
		}
		
		Collections.sort(filtered, new Comparator<Country>() {
			@Override
			public int compare(Country o1, Country o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		return filtered;
	}
	
	public List<Species> flagCountrySpecies(List<Logsheet> logsheets) {
		List<Species> filtered = new ArrayList<Species>();
		List<Catches> catches =  uniqueCatchesByFlagCountryAndDates(logsheets, null, null, null);
		
		Species current = null;
		
		for(Catches in : catches) {
			if(!filtered.contains(current = in.getSpecies()))
				filtered.add(current);
		}
		
		Collections.sort(filtered, new Comparator<Species>() {
			public int compare(Species o1, Species o2) {
				return o1.getAlpha3Code().compareTo(o2.getAlpha3Code());
			}
		});
		
		return filtered;
	}
	
	public List<Species> portCountrySpecies(List<Logsheet> logsheets) {
		List<Species> filtered = new ArrayList<Species>();
		List<Catches> catches =  uniqueCatchesByPortCountryAndDates(logsheets, null, null, null);
		
		Species current = null;
		
		for(Catches in : catches) {
			if(!filtered.contains(current = in.getSpecies()))
				filtered.add(current);
		}
		
		Collections.sort(filtered, new Comparator<Species>() {
			public int compare(Species o1, Species o2) {
				return o1.getAlpha3Code().compareTo(o2.getAlpha3Code());
			}
		});
		
		return filtered;
	}
	
	public List<Logsheet> filterLogsheetsByFlagCountry(List<Logsheet> logsheets, Country flagCountry) {
		List<Logsheet> filtered = new ArrayList<Logsheet>();
		
		for(Logsheet in : logsheets) {
			if(flagCountry == null || in.getVesselData().getRegistrationCountry().equals(flagCountry))
				filtered.add(in);
		}
		
		return filtered;
	}
	
	public List<Logsheet> filterLogsheetsByPortCountry(List<Logsheet> logsheets, Country portCountry) {
		List<Logsheet> filtered = new ArrayList<Logsheet>();
		
		for(Logsheet in : logsheets) {
			if(portCountry == null || in.getUnloadingPort().getCountry().equals(portCountry))
				filtered.add(in);
		}
		
		return filtered;
	}
	
	public List<Catches> uniqueCatchesByFlagCountryAndDates(List<Logsheet> logsheets, Country flagCountry, Date from, Date to) {
		return uniqueCatchesForEvents(catchEvents(filterLogsheetsByFlagCountry(logsheets, flagCountry), from, to));
	}
	
	public List<Catches> uniqueCatchesByPortCountryAndDates(List<Logsheet> logsheets, Country portCountry, Date from, Date to) {
		return uniqueCatchesForEvents(catchEvents(filterLogsheetsByPortCountry(logsheets, portCountry), from, to));
	}
	
	public Map<Country, List<Catches>> groupUniqueCatchesByDatesByFlagCountry(List<Logsheet> logsheets, Date from, Date to) {
		List<Country> flagCountries = flagCountries(logsheets);
		Map<Country, List<Catches>> map = new HashMap<Country, List<Catches>>();
		
		for(Country in : flagCountries) {
			map.put(in, uniqueCatchesByFlagCountryAndDates(logsheets, in, from, to));
			
			if(map.get(in).isEmpty()) map.remove(in);
		}
		
		return map;
	}
	
	public Map<Country, List<Catches>> groupUniqueCatchesByDatesByPortCountry(List<Logsheet> logsheets, Date from, Date to) {
		List<Country> portCountries = portCountries(logsheets);
		Map<Country, List<Catches>> map = new HashMap<Country, List<Catches>>();
		
		for(Country in : portCountries) {
			map.put(in, uniqueCatchesByPortCountryAndDates(logsheets, in, from, to));
			
			if(map.get(in).isEmpty()) map.remove(in);
		}
		
		return map;
	}

	public List<CatchesByCountry> filterByCountry(List<CatchesByCountry> catches, Country country) {
		List<CatchesByCountry> filtered = new ArrayList<CatchesByCountry>();

		for(CatchesByCountry in : catches) 
			if(country == null || country.equals(in.getCountry()))
				filtered.add(in);
		
		return filtered;
	}
	
	public List<CatchesByCountry> filterBySpecies(List<CatchesByCountry> catches, Species species) {
		List<CatchesByCountry> filtered = new ArrayList<CatchesByCountry>();

		for(CatchesByCountry in : catches) 
			if(species == null || species.equals(in.getSpecies()))
				filtered.add(in);
		
		return filtered;
	}
	
	public List<CatchesByCountry> byCountry(Map<Country, List<Catches>> mapByCountry) {
		List<Country> keys = new ArrayList<Country>(mapByCountry.keySet());
		
		Collections.sort(keys, new Comparator<Country>() {
			@Override
			public int compare(Country o1, Country o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		List<CatchesByCountry> toReturn = new ArrayList<CatchesByCountry>();
		
		for(Country in : keys) {
			for(Catches of : mapByCountry.get(in)) {
				toReturn.add(new CatchesByCountry(in, of));
			}
		}

		Collections.sort(toReturn, new Comparator<CatchesByCountry>() {
			@Override
			public int compare(CatchesByCountry o1, CatchesByCountry o2) {
				Country c1 = o1.getCountry(), c2 = o2.getCountry();
				Species s1 = o1.getSpecies(), s2 = o2.getSpecies();
				QuantityType q1 = o1.getQuantityType(), q2 = o2.getQuantityType();
				
				int c = c1.getName().compareTo(c2.getName());
				
				if(c != 0) return c;
				
				int s = s1.getAlpha3Code().compareTo(s2.getAlpha3Code());
				
				if(s != 0) return s;
				
				int q = q1.getCode().compareTo(q2.getCode());
				
				if(q != 0) return q;
				
				return Double.compare(o1.getQuantity(), o2.getQuantity());
			}
		});
		
		return toReturn;
	}
	
	public List<CatchesByCountry> filterByUnit(List<CatchesByCountry> catches, QuantityType... units) {
		Set<QuantityType> toKeep = new HashSet<QuantityType>();
		for(QuantityType in : units)
			toKeep.add(in);
		
		List<CatchesByCountry> filtered = new ArrayList<CatchesByCountry>();
		
		for(CatchesByCountry in : catches) 
			if(toKeep.contains(in.getQuantityType()))
				filtered.add(in);
		
		return filtered;
	}
	
	public List<Catches> defaultSort(List<Catches> toSort) {
		List<Catches> sorted = new ArrayList<Catches>(toSort);
		
		Collections.sort(sorted, new Comparator<Catches>() {
			@Override
			public int compare(Catches o1, Catches o2) {
				Species s1 = o1.getSpecies(), s2 = o2.getSpecies();
				QuantityType q1 = o1.getQuantityType(), q2 = o2.getQuantityType();
				
				int s = s1.getAlpha3Code().compareTo(s2.getAlpha3Code());
				
				if(s != 0) return s;
				
				int q = q1.getCode().compareTo(q2.getCode());
				
				if(q != 0) return q;
				
				return Double.compare(o1.getQuantity(), o2.getQuantity());
			}
		});
		
		return sorted;
	}
	
	public List<CatchesRecord> catchEvents(List<Logsheet> logsheets, Date from, Date to) {
		List<CatchesRecord> catchEvents = new ArrayList<CatchesRecord>();
		
		for(Logsheet in : logsheets) {
			if(in.getEvents() != null) {
				for(Event of : in.getEvents()) {
					if(of.isCatchEvent()) {
						if(includeCatchEventByDate((CatchEvent)of, from, to))
							catchEvents.add((CatchEvent)of);
					}
				}
			}
		}
		
		return catchEvents;
	}
	
	public List<Catches> uniqueCatchesForLogsheets(List<Logsheet> logsheets) {
		return uniqueCatchesForEvents(catchEvents(logsheets, null, null));
	}
	
	public List<Catches> uniqueCatchesForEvents(List<CatchesRecord> catchEvents) {
		List<Catches> unique = new ArrayList<Catches>();
		
		Map<CatchesKey, Double> bySpeciesAndUnit = new HashMap<CatchesKey, Double>();
		
		for(CatchesRecord event : catchEvents) {
			if(event.getCatches() != null && !event.getCatches().isEmpty()) {
				for(Catches in : event.getCatches()) {
					if(!bySpeciesAndUnit.containsKey(in.key())) {
						bySpeciesAndUnit.put(in.key(), 0D);
					}
					
					bySpeciesAndUnit.put(in.key(), bySpeciesAndUnit.get(in.key()) + in.getQuantity());
				}
			}
		}

		for(CatchesKey in : bySpeciesAndUnit.keySet()) {
			unique.add(Catches.builder().species(in.getSpecies()).quantityType(in.getQuantityType()).quantity(bySpeciesAndUnit.get(in)).build());
		}
		
		return unique;
	}
	
	public File storeReport(File folder, String filename, List<CatchesByCountry> catches) throws IOException {
		File target = new File(folder, filename + ".csv");
		
		Writer fileWriter = new FileWriter(target);
		
		CSVWriter writer = new CSVWriter(fileWriter, ',', '"');
		
		try {
			writer.writeNext("Country code", "Country", "Species code", "Species scientific name", "Species name", "Unit", "Quantity");
			
			for(CatchesByCountry in : catches) {
				writer.writeNext(
					in.getCountry().getIso2Code(),
					in.getCountry().getName(),
					in.getSpecies().getAlpha3Code(),
					in.getSpecies().getScientificName(),
					in.getSpecies().getLocalName(),
					in.getQuantityType().getCode(),
					String.valueOf(in.getQuantity())
				);
			}
		} finally {
			writer.flush();
			writer.close();
		}
		
		return target;
	}
	
	private List<Logsheet> fromFile(String file) throws IOException, JAXBException {
		FileInputStream stream = null; 
		GZIPInputStream zipped = null;
		
		try {
			zipped = new GZIPInputStream(stream = new FileInputStream(file));

			return ((Logbook)JAXBContext.newInstance(Logbook.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createUnmarshaller().unmarshal(zipped)).getLogsheets();
		} catch(IOException IOe) {
			if("Not in GZIP format".equals(IOe.getMessage())) {
				throw new IOException("The selected file is in an invalid format");
			} else
				throw IOe;
		} finally {
			if(stream != null)
				stream.close();
			
			if(zipped != null)
				zipped.close();
		}	
	}
	
	private boolean includeCatchEventByDate(CatchEvent event, Date from, Date to) {
		if(event != null && from == null && to == null) return true;
		
		long eventEnd = event.getEndDate().getTime();
		
		if(from != null) {
			if(eventEnd < from.getTime()) return false;
		}
		
		if(to != null) {
			if(eventEnd > to.getTime()) return false;
		}
		
		return true;
	}
}
