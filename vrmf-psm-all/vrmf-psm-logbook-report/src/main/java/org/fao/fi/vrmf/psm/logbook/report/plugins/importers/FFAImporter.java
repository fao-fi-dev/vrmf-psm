/**
 * (c) 2016 FAO / UN (project: vrmf-psm-logbook-report-ui)
 */
package org.fao.fi.vrmf.psm.logbook.report.plugins.importers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 29, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 29, 2016
 */
public class FFAImporter {
	static private String LL_XSL = "xsl/FFA_LL.xsl";
	static private String PS_XSL = "xsl/FFA_PS.xsl";
	
	public Document xmlFromPdf(File file) throws IOException, SAXException, ParserConfigurationException {
		FileInputStream fileInputStream;

		fileInputStream = new FileInputStream(file);
		PDDocument document = PDDocument.load(fileInputStream);
		document.setAllSecurityToBeRemoved(true);

		PDAcroForm form = document.getDocumentCatalog().getAcroForm();

		if(form == null) throw new IllegalArgumentException("The provided file is not in the expected format");
		
		Document documentXML = form.getXFA().getDocument();

		fileInputStream.close();
		
		Document xmlDocument = documentXML.getDocumentElement().getOwnerDocument();
		xmlDocument.normalize();

		return xmlDocument;
	}
	
	public boolean isForLL(Document toCheck) {
		return toCheck.getElementsByTagName("LL_LOGSHEET").getLength() == 1;
	}
	
	public boolean isForPS(Document toCheck) {
		return toCheck.getElementsByTagName("LOGSHEET").getLength() == 1;
	}

	public Document extractData(File file) throws IOException, SAXException, SAXParseException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
		return extractData(file, null);
	}

	public Document extractData(File file, String stylesheet) throws IOException, SAXException, SAXParseException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
		Document pdf = xmlFromPdf(file);
		
		if(stylesheet == null)
			stylesheet = isForLL(pdf) ? LL_XSL : PS_XSL;
		
		TransformerFactory factory = TransformerFactory.newInstance();
		StreamSource xslStream = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream(stylesheet));
		Transformer transformer = factory.newTransformer(xslStream);
		Source in = new DOMSource(pdf);
		Result out = new DOMResult();
		
		transformer.transform(in, out);
		
		return (Document)((DOMResult)out).getNode();
	}
	
	public void prettyPrint(Document toPrint, PrintStream sink) throws TransformerConfigurationException, TransformerException {
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		
		StreamResult result = new StreamResult(new StringWriter());
		DOMSource source = new DOMSource(toPrint);
		
		transformer.transform(source, result);
		
		String xmlString = result.getWriter().toString();

		sink.println(xmlString);
	}
}
