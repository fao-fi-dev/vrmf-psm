<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:xdp="http://ns.adobe.com/xdp/"
				xmlns:xfa="http://www.xfa.org/schema/xfa-data/1.0/"
				exclude-result-prefixes="xfa">
	
	<xsl:output method="xml" indent="yes" encoding="utf-8"/>
	
	<xsl:strip-space elements="*"/>
	<xsl:template match="text()"/>
	
	<xsl:template match="//LOGSHEET">
		<xsl:copy-of select="."/>
	</xsl:template>
</xsl:stylesheet>