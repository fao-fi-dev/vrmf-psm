/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.fao.fi.vrmf.psm.logbook.dao.LogbooksRepository;
import org.fao.fi.vrmf.psm.logbook.dao.LogsheetsRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchAndEffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.EffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.TransshipmentEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Port;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

import lombok.extern.slf4j.Slf4j;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
@Named @Singleton
@Slf4j
public class LogsheetsServices extends BasicServices {
	@Inject private LogbooksRepository logbooks;
	@Inject private LogsheetsRepository logsheets;
	
	public List<VesselData> allVessels() {
		long end, start = System.currentTimeMillis();
		
		try {
			return genericRepository.list("from VesselData order by name asc, ircs asc, registrationCountry.name asc");
		} finally {
			end = System.currentTimeMillis();
			
			log.trace("Loading all vessels took {} mSec", end - start);
		}
	}
	
	public List<Port> portsForCountry(Country country) {
		if(country == null) return new ArrayList<Port>();
		
		long end, start = System.currentTimeMillis();
		
		try {
			return genericRepository.list("from Port where country_id = " + country.getId() + " and coordinates is not null order by name asc");
		} finally {
			end = System.currentTimeMillis();
			
			log.trace("Loading ports for {} took {} mSec", country.getName(), end - start);
		}
	}
	
	public Logsheet reload(Logsheet logsheet) {
		return logsheets.refresh(logsheet);
	}
	
	public Logsheet delete(Logbook parentLogbook, Logsheet logsheet) {
		if(parentLogbook != null && logsheet != null) {
			if(logsheet.getId() == null) {
				logsheets.remove(logsheet);
			} else {
				parentLogbook.getLogsheets().remove(logsheet);
				
				logbooks.update(parentLogbook);
			}
		}
		
		return logsheet;
	}
	
	public Logsheet update(Logbook parentLogbook, Logsheet logsheet) {
		if(parentLogbook != null && logsheet != null) { 
			if(logsheet.getId() == null) {
				int number = 0;
				
				for(Logsheet in : parentLogbook.getLogsheets()) {
					number = Math.max(number, in.getNumber() == null ? 0 : in.getNumber());
				}
				
				logsheet.setNumber(number + 1);
				
				logbooks.update(parentLogbook);
			} else {
				logsheets.update(logsheet);	
			}
		}
		
		return logsheet;
	}
	
	private String getExportedLogsheetFilename(Logbook logbook, Logsheet logsheet) {
		return logbook.getYear() + "_" + 
			   new SimpleDateFormat("yyyy_MM_dd").format(logbook.getCreationDate()) + "_" + 
			   logbook.getId() + "_" + 
			   logbook.getVersion().replaceAll("\\.", "_") + "_" + 
			   logsheet.getId() + "_" +
			   logbook.getCreationDate().getTime() +
			   ".logsheet";
	}
	
	public boolean exportedLogsheetExists(File folder, Logbook owner, Logsheet toExport, String type) {
		return new File(folder, getExportedLogsheetFilename(owner, toExport).replace(".logsheet", "." + ( type == null ? "logsheet" : type ))).exists();
	}
	
	public File export(File folder, Logbook owner, Logsheet toExport, String type) throws Exception {
		toExport.setDerivedEfforts(toExport.deriveEfforts());
		toExport.setDerivedEfforts(toExport.uniqueDerivedEfforts());
		
		toExport.setUniqueEfforts(toExport.uniqueEfforts());
		toExport.setUniqueCatches(toExport.uniqueCatches());
		toExport.setUniqueDiscards(toExport.uniqueDiscards());

		Iterator<Logsheet> logsheets = owner.getLogsheets().iterator();
			
		while(logsheets.hasNext()) {
			if(!logsheets.next().equals(toExport))
				logsheets.remove();
		}
			
		File target = new File(folder, getExportedLogsheetFilename(owner, toExport).replace(".logsheet", "." + ( type == null ? "logsheet" : type )));
		
		if("xml".equals(type)) exportLogsheetAsXML(owner, toExport, target);
		else if("html".equals(type)) exportLogsheetAsHTML(owner, toExport, target);
		else if("pdf".equals(type)) exportLogsheetAsPDF(owner, toExport, target);
		else exportLogsheet(owner, toExport, target);
		
		toExport.setDerivedEfforts(null);
		
		toExport.setDerivedEfforts(null);
		toExport.setDerivedEfforts(null);
		
		toExport.setUniqueEfforts(null);
		toExport.setUniqueCatches(null);
		toExport.setUniqueDiscards(null);
			
		reload(toExport);
			
		return target;
	}
	
	private void exportLogsheet(Logbook logbook, Logsheet toExport, File target) throws Exception {
		FileOutputStream stream = null; 
		GZIPOutputStream zipped = null;

		try {
			zipped = new GZIPOutputStream(stream = new FileOutputStream(target));
			
			Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(logbook, zipped);
			
			zipped.flush();
			stream.flush();
		} finally {
			if(zipped != null)
				zipped.close();
			
			if(stream != null)
				stream.close();
		}
	}
	
	private void exportLogsheetAsXML(Logbook logbook, Logsheet toExport, File target) throws Exception {
		FileOutputStream stream = null; 

		try {
			stream = new FileOutputStream(target);
			
			Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(logbook, stream);
			
			stream.flush();
		} finally {
			if(stream != null)
				stream.close();
		}
	}
	
	private void exportLogsheetAsHTML(Logbook logbook, Logsheet toExport, File target) throws Exception {
		convertToHTML(logbook, toExport, target);
	}

	private void exportLogsheetAsPDF(Logbook logbook, Logsheet toExport, File target) throws Exception {
		convertToPDF(logbook, toExport, target);
	}
	
	private void convertToHTML(Logbook logbook, Logsheet toExport, File target) throws Exception {
		ByteArrayOutputStream xmlStream = null;
		
		try {
			xmlStream = new ByteArrayOutputStream();
			
			Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(logbook, xmlStream);
			
			xmlStream.flush();
		} finally {
			if(xmlStream != null)
				xmlStream.close();
		}
		
		ByteArrayInputStream input = new ByteArrayInputStream(xmlStream.toByteArray());
		
		FileOutputStream output = null;
		
		try {
			output = new FileOutputStream(target);
			
			TransformerFactory factory = TransformerFactory.newInstance();
			StreamSource xslStream = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("xsl/logsheet2xhtml.xsl"));
			Transformer transformer = factory.newTransformer(xslStream);
			StreamSource in = new StreamSource(input);
			StreamResult out = new StreamResult(output);
			transformer.transform(in, out);
			
			output.flush();
		} finally {
			if(input != null)
				input.close();
			
			if(output != null)
				output.close();
		}
	}
	
	private void convertToPDF(Logbook logbook, Logsheet toExport, File target) throws Exception {
		ByteArrayOutputStream xmlStream = null;
		
		try {
			xmlStream = new ByteArrayOutputStream();
			
			Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(logbook, xmlStream);
			
			xmlStream.flush();
		} finally {
			if(xmlStream != null)
				xmlStream.close();
		}
		
		ByteArrayInputStream htmlIn, input = new ByteArrayInputStream(xmlStream.toByteArray());
		ByteArrayOutputStream htmlOut = new ByteArrayOutputStream();
		
		FileOutputStream output = null;
		
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			StreamSource xslStream = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("xsl/logsheet2xhtml.xsl"));
			Transformer transformer = factory.newTransformer(xslStream);
			StreamSource in = new StreamSource(input);
			StreamResult out = new StreamResult(htmlOut);
			transformer.transform(in, out);
			
			htmlOut.flush();
			
			htmlIn = new ByteArrayInputStream(htmlOut.toByteArray());
			
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			
			domFactory.setNamespaceAware(false);
			domFactory.setValidating(false);
			domFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			domFactory.setFeature("http://xml.org/sax/features/validation", false);
			domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(htmlIn);

			output = new FileOutputStream(target);
			
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(doc, null);

			renderer.layout();
			renderer.createPDF(output);
			
			output.flush();
		} finally {
			if(input != null)
				input.close();
			
			if(htmlOut != null)
				htmlOut.close();
			
			if(output != null)
				output.close();
		}
	}
	
	public File toPdf(File exported) throws Exception {
		InputStream logsheetStream = null, htmlStream = null; 
		GZIPInputStream zippedLogsheetStream = null;
		
		File targetPdf = new File(exported.getAbsolutePath().replace(".logsheet", ".pdf"));
		
		ByteArrayOutputStream htmlOutput = null;
		FileOutputStream pdfOutput = null;
		
		try {
			zippedLogsheetStream = new GZIPInputStream(logsheetStream = new FileInputStream(exported));
			htmlOutput = new ByteArrayOutputStream();
			
			TransformerFactory factory = TransformerFactory.newInstance();
			StreamSource xslStream = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("xsl/logsheet2xhtml.xsl"));
			Transformer transformer = factory.newTransformer(xslStream);
			StreamSource in = new StreamSource(zippedLogsheetStream);
			StreamResult out = new StreamResult(htmlOutput);
			transformer.transform(in, out);
			
			htmlOutput.flush();
			
			htmlStream = new ByteArrayInputStream(htmlOutput.toByteArray());
			
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			
			domFactory.setNamespaceAware(false);
			domFactory.setValidating(false);
			domFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			domFactory.setFeature("http://xml.org/sax/features/validation", false);
			domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(htmlStream);

			pdfOutput = new FileOutputStream(targetPdf);
			
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(doc, null);

			renderer.layout();
			renderer.createPDF(pdfOutput);
			
			pdfOutput.flush();
			
			return targetPdf;
		} finally {
			if(pdfOutput != null)
				pdfOutput.close();
			
			if(htmlOutput != null)
				htmlOutput.close();
			
			if(zippedLogsheetStream != null)
				zippedLogsheetStream.close();
			
			if(logsheetStream != null)
				logsheetStream.close();
			
			if(htmlStream != null)
				htmlStream.close();
		}
	}
}