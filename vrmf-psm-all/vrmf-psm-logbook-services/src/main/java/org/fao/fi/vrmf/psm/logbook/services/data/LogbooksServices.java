/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.fao.fi.vrmf.psm.logbook.dao.LogbooksRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchAndEffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.CatchEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.EffortEvent;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.TransshipmentEvent;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
@Named @Singleton
public class LogbooksServices extends BasicServices  {
	@Inject private LogbooksRepository logbooks;
	
	public List<Logbook> allLogbooks() {
		return genericRepository.list("from Logbook order by creationDate asc");
	}
	
	public Logbook update(Logbook logbook) {
		return logbooks.update(logbook);
	}
	
	public Logbook delete(Logbook logbook) {
		return logbooks.remove(logbook);
	}
	
	public Logbook reload(Logbook logbook) {
		return logbooks.refresh(logbook);
	}
	
	private String getExportedLogbookFilename(Logbook logbook) {
		return logbook.getYear() + "_" + 
			   new SimpleDateFormat("yyyy_MM_dd").format(logbook.getCreationDate()) + "_" + 
			   logbook.getId() + "_" + 
			   logbook.getVersion().replaceAll("\\.", "_") + "_" +
			   logbook.getCreationDate().getTime() +
			   ".logbook";
	}
	
	public boolean exportedLogbookExists(File folder, Logbook toExport, String type) {
		return new File(folder, getExportedLogbookFilename(toExport).replace(".logbook", "." + ( type == null ? "logbook" : type ))).exists();
	}
	
	public File export(File folder, Logbook logbook, String type) throws Exception {
		if(logbook.getLogsheets() != null) {
			for(Logsheet in : logbook.getLogsheets()) {
				in.setDerivedEfforts(in.deriveEfforts());
				in.setDerivedEfforts(in.uniqueDerivedEfforts());
				
				in.setUniqueEfforts(in.uniqueEfforts());
				in.setUniqueCatches(in.uniqueCatches());
				in.setUniqueDiscards(in.uniqueDiscards());
			}
		}
		
		logbook.setDerivedEfforts(logbook.derivedEfforts());
		logbook.setDerivedEfforts(logbook.uniqueDerivedEfforts());
		
		logbook.setUniqueEfforts(logbook.uniqueEfforts());
		logbook.setUniqueCatches(logbook.uniqueCatches());
		logbook.setUniqueDiscards(logbook.uniqueDiscards());
		
		File target = new File(folder, getExportedLogbookFilename(logbook).replace(".logbook", "." + ( type == null ? "logbook" : type )));
		
		if("xml".equals(type)) exportLogbookAsXML(logbook, target);
		else if("html".equals(type)) exportLogbookAsHTML(logbook, target);
		else if("pdf".equals(type)) exportLogbookAsPDF(logbook, target);
		else exportLogbook(logbook, target);
		
		if(logbook.getLogsheets() != null) {
			for(Logsheet in : logbook.getLogsheets()) {
				in.setDerivedEfforts(null);
				in.setDerivedEfforts(null);
				
				in.setUniqueEfforts(null);
				in.setUniqueCatches(null);
				in.setUniqueDiscards(null);
			}
		}
		
		logbook.setDerivedEfforts(null);
		logbook.setDerivedEfforts(null);
		
		logbook.setUniqueEfforts(null);
		logbook.setUniqueCatches(null);
		logbook.setUniqueDiscards(null);
		
		reload(logbook);
		
		return target;
	}
	
	private void exportLogbook(Logbook logbook, File target) throws Exception {
		FileOutputStream stream = null; 
		GZIPOutputStream zipped = null;

		try {
			zipped = new GZIPOutputStream(stream = new FileOutputStream(target));
			
			Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(logbook, zipped);
			
			zipped.flush();
			stream.flush();
		} finally {
			if(zipped != null)
				zipped.close();
			
			if(stream != null)
				stream.close();
		}
	}
	
	private void exportLogbookAsXML(Logbook logbook, File target) throws Exception {
		FileOutputStream stream = null; 

		try {
			stream = new FileOutputStream(target);
			
			Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(logbook, stream);
			
			stream.flush();
		} finally {
			if(stream != null)
				stream.close();
		}
	}
	
	private void exportLogbookAsHTML(Logbook logbook, File target) throws Exception {
		convertToHTML(logbook, target);
	}

	private void exportLogbookAsPDF(Logbook logbook, File target) throws Exception {
		convertToPDF(logbook, target);
	}

	public Logbook importLogbook(File file) throws IOException, JAXBException {
		FileInputStream stream = null; 
		GZIPInputStream zipped = null;
		
		try {
			zipped = new GZIPInputStream(stream = new FileInputStream(file));

			return (Logbook)JAXBContext.newInstance(Logbook.class, EffortEvent.class, CatchAndEffortEvent.class, CatchEvent.class, Event.class).createUnmarshaller().unmarshal(zipped);
		} catch(IOException IOe) {
			if("Not in GZIP format".equals(IOe.getMessage())) {
				throw new IOException("The selected file is in an invalid format");
			} else
				throw IOe;
		} finally {
			if(stream != null)
				stream.close();
			
			if(zipped != null)
				zipped.close();
		}
	}
	
	private void convertToHTML(Logbook logbook, File target) throws Exception {
		ByteArrayOutputStream xmlStream = null;
		
		try {
			xmlStream = new ByteArrayOutputStream();
			
			Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(logbook, xmlStream);
			
			xmlStream.flush();
		} finally {
			if(xmlStream != null)
				xmlStream.close();
		}
		
		ByteArrayInputStream input = new ByteArrayInputStream(xmlStream.toByteArray());
		
		FileOutputStream output = null;
		
		try {
			output = new FileOutputStream(target);
			
			TransformerFactory factory = TransformerFactory.newInstance();
			StreamSource xslStream = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("xsl/logbook2xhtml.xsl"));
			Transformer transformer = factory.newTransformer(xslStream);
			StreamSource in = new StreamSource(input);
			StreamResult out = new StreamResult(output);
			transformer.transform(in, out);
			
			output.flush();
		} finally {
			if(input != null)
				input.close();
			
			if(output != null)
				output.close();
		}
	}
	
	private void convertToPDF(Logbook logbook, File target) throws Exception {
		ByteArrayOutputStream xmlStream = null;
		
		try {
			xmlStream = new ByteArrayOutputStream();
			
			Marshaller m = JAXBContext.newInstance(Logbook.class, TransshipmentEvent.class, CatchAndEffortEvent.class, EffortEvent.class, CatchEvent.class, Event.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(logbook, xmlStream);
			
			xmlStream.flush();
		} finally {
			if(xmlStream != null)
				xmlStream.close();
		}
		
		ByteArrayInputStream htmlIn, input = new ByteArrayInputStream(xmlStream.toByteArray());
		ByteArrayOutputStream htmlOut = new ByteArrayOutputStream();
		
		FileOutputStream output = null;
		
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			StreamSource xslStream = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("xsl/logbook2xhtml.xsl"));
			Transformer transformer = factory.newTransformer(xslStream);
			StreamSource in = new StreamSource(input);
			StreamResult out = new StreamResult(htmlOut);
			transformer.transform(in, out);
			
			htmlOut.flush();
			
			htmlIn = new ByteArrayInputStream(htmlOut.toByteArray());
			
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			
			domFactory.setNamespaceAware(false);
			domFactory.setValidating(false);
			domFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			domFactory.setFeature("http://xml.org/sax/features/validation", false);
			domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(htmlIn);

			output = new FileOutputStream(target);
			
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(doc, null);

			renderer.layout();
			renderer.createPDF(output);
			
			output.flush();
		} finally {
			if(input != null)
				input.close();
			
			if(htmlOut != null)
				htmlOut.close();
			
			if(output != null)
				output.close();
		}
	}
	
	public File toPdf(File exported) throws Exception {
		InputStream logbookStream = null, htmlStream = null; 
		GZIPInputStream zippedLogsheetStream = null;
		
		File targetPdf = new File(exported.getAbsolutePath().replace(".logbook", ".pdf"));
		
		ByteArrayOutputStream htmlOutput = null;
		FileOutputStream pdfOutput = null;
		
		try {
			zippedLogsheetStream = new GZIPInputStream(logbookStream = new FileInputStream(exported));
			htmlOutput = new ByteArrayOutputStream();
			
			TransformerFactory factory = TransformerFactory.newInstance();
			StreamSource xslStream = new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("xsl/logbook2xhtml.xsl"));
			Transformer transformer = factory.newTransformer(xslStream);
			StreamSource in = new StreamSource(zippedLogsheetStream);
			StreamResult out = new StreamResult(htmlOutput);
			transformer.transform(in, out);
			
			htmlOutput.flush();
			
			htmlStream = new ByteArrayInputStream(htmlOutput.toByteArray());
			
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			
			domFactory.setNamespaceAware(false);
			domFactory.setValidating(false);
			domFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			domFactory.setFeature("http://xml.org/sax/features/validation", false);
			domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(htmlStream);

			pdfOutput = new FileOutputStream(targetPdf);
			
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(doc, null);

			renderer.layout();
			renderer.createPDF(pdfOutput);
			
			pdfOutput.flush();
			
			return targetPdf;
		} finally {
			if(pdfOutput != null)
				pdfOutput.close();
			
			if(htmlOutput != null)
				htmlOutput.close();
			
			if(zippedLogsheetStream != null)
				zippedLogsheetStream.close();
			
			if(logbookStream != null)
				logbookStream.close();
			
			if(htmlStream != null)
				htmlStream.close();
		}
	}
}