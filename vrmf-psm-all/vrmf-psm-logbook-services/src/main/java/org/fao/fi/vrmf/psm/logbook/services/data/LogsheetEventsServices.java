/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.dao.EventsRepository;
import org.fao.fi.vrmf.psm.logbook.dao.LogsheetsRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
@Named @Singleton
public class LogsheetEventsServices extends BasicServices {
	@Inject private LogsheetsRepository logsheets;
	@Inject private EventsRepository events;
		
	public Event reload(Event logsheet) {
		return events.refresh(logsheet);
	}
	
	public Event delete(Logsheet parentLogsheet, Event event) {
		if(parentLogsheet != null && event != null) {
			if(event.getId() == null) {
				events.remove(event);
			} else {
				parentLogsheet.getEvents().remove(event);
				
				logsheets.update(parentLogsheet);
			}
		}
		
		return event;
	}
	
	public Event update(Logsheet parentLogsheet, Event event) {
		if(parentLogsheet != null && event != null) { 
			if(event.getId() == null) {
				logsheets.update(parentLogsheet);
			} else {
				events.update(event);
			}
		}
		
		return event;
	}
}