/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.dao.EffortsRepository;
import org.fao.fi.vrmf.psm.logbook.dao.EventsRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.EffortsRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Effort;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
@Named @Singleton
public class LogsheetEventEffortsServices extends BasicServices {
	@Inject private EventsRepository events;
	@Inject private EffortsRepository efforts;
		
	public Effort reload(Effort effort) {
		return efforts.refresh(effort);
	}
	
	public Effort delete(Event parentEvent, Effort effort) {
		if(parentEvent != null && effort != null) {
			if(effort.getId() == null) {
				efforts.remove(effort);
			} else {
				((EffortsRecord)parentEvent).getEfforts().remove(effort);
				
				events.update(parentEvent);
			}
		}
		
		return effort;
	}
	
	public Effort update(Event parentEvent, Effort effort) {
		if(parentEvent != null && effort != null) { 
			if(effort.getId() == null) {
				events.update(parentEvent);
			} else {
				efforts.update(effort);
			}
		}
		
		return effort;
	}
}