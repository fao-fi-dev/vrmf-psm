/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.dao.VesselDataRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.Logbook;
import org.fao.fi.vrmf.psm.logbook.model.base.Logsheet;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Identifier;
import org.fao.fi.vrmf.psm.logbook.model.base.vessels.VesselData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 26, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 26, 2015
 */
@Named @Singleton
public class VesselsServices extends BasicServices {
	@Inject private VesselDataRepository vessels;
	
	public List<VesselData> allVessels() {
		return vessels.getAll();
	}
	
	public VesselData update(VesselData vessel) {
		return vessels.update(vessel);
	}
	
	public VesselData delete(VesselData vessel) {
		return vessels.remove(vessel);
	}
	
	public VesselData reload(VesselData vessel) {
		if(vessel == null || vessel.getId() == null) return vessel;
		
		if(vessel.getIdentifiers() != null) {
			Iterator<Identifier> identifiers = vessel.getIdentifiers().iterator();
			
			Identifier current;
			while(identifiers.hasNext()) {
				current = identifiers.next();
				
				if(current == null || current.getId() == null)
					identifiers.remove();
			}
		}
		
		return vessels.refresh(vessel);
	}
	
	public boolean canBeDeleted(VesselData vessel) {
		if(vessel == null || vessel.getId() == null) return true;
		
		return referencingLogbooks(vessel).isEmpty();
	}
	
	public List<Logsheet> referencingLogsheets(VesselData vessel) {
		List<Logsheet> referencing = new ArrayList<Logsheet>();
		
		if(vessel != null && vessel.getId() != null) {
			referencing = genericRepository.list("from Logsheet where vesseldata_id = " + vessel.getId());
		}
		
		return referencing;
	}
	
	public List<Logbook> referencingLogbooks(VesselData vessel) {
		List<Logbook> referencing = new ArrayList<Logbook>();
		
		if(vessel != null && vessel.getId() != null) {
			referencing = genericRepository.list("select lb from Logbook as lb inner join lb.logsheets as sh inner join sh.vesselData as ve where ve.id = " + vessel.getId());
		}
		
		return referencing;
	}
	
	public List<Logsheet> referencingCompletedLogsheets(VesselData vessel) {
		List<Logsheet> referencing = new ArrayList<Logsheet>();
		
		if(vessel != null && vessel.getId() != null) {
			referencing = genericRepository.list("from Logsheet where completed = true and vesseldata_id = " + vessel.getId());
		}
		
		return referencing;
	}
	
	public List<Logbook> referencingCompletedLogbooks(VesselData vessel) {
		List<Logbook> referencing = new ArrayList<Logbook>();
		
		if(vessel != null && vessel.getId() != null) {
			referencing = genericRepository.list("select lb from Logbook as lb inner join lb.logsheets as sh inner join sh.vesselData as ve where lb.completed = true and ve.id = " + vessel.getId());
		}
		
		return referencing;
	}
}