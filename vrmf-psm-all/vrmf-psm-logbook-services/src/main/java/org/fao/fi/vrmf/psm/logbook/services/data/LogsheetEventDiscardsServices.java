/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.dao.DiscardsRepository;
import org.fao.fi.vrmf.psm.logbook.dao.EventsRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Discards;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
@Named @Singleton
public class LogsheetEventDiscardsServices extends BasicServices {
	@Inject private EventsRepository events;
	@Inject private DiscardsRepository discardz;
		
	public Discards reload(Discards discards) {
		return discardz.refresh(discards);
	}
	
	public Discards delete(Event parentEvent, Discards discards) {
		if(parentEvent != null && discards != null) {
			if(discards.getId() == null) {
				discardz.remove(discards);
			} else {
				((CatchesRecord)parentEvent).getDiscards().remove(discards);
				
				events.update(parentEvent);
			}
		}
		
		return discards;
	}
	
	public Discards update(Event parentEvent, Discards discards) {
		if(parentEvent != null && discards != null) { 
			if(discards.getId() == null) {
				events.update(parentEvent);
			} else {
				discardz.update(discards);
			}
		}
		
		return discards;
	}
}