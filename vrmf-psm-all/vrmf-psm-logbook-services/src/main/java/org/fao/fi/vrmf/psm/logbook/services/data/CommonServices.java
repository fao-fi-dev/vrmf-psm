/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.model.base.reference.Country;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Species;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 15, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 15, 2015
 */
@Named @Singleton
public class CommonServices extends BasicServices {
	public List<Species> allSpecies() {
		return genericRepository.list("from Species order by localName asc");
	}

	public List<Country> allCountries() {
		return genericRepository.list("from Country where iso2code is not null AND iso2code <> 'NULL' order by name asc");
	}
	
	public List<Country> allCountriesWithPorts() {
		return genericRepository.list("select distinct co from Port as po inner join po.country as co order by co.name asc");
//		return dao.list("select co from Country as co inner join Port as po on co.id = po.countryId is where co.iso2code <> 'NULL' order by co.name asc");
	}
	
	public List<Country> allCountriesWithValidPorts() {
		return genericRepository.list("select distinct co from Port as po inner join po.country as co where po.coordinates is not null order by co.name asc");
//		return dao.list("select co from Country as co inner join Port as po on co.id = po.countryId is where co.iso2code <> 'NULL' order by co.name asc");
	}
}
