/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.dao.CatchesRepository;
import org.fao.fi.vrmf.psm.logbook.dao.EventsRepository;
import org.fao.fi.vrmf.psm.logbook.model.base.events.Event;
import org.fao.fi.vrmf.psm.logbook.model.base.events.spi.CatchesRecord;
import org.fao.fi.vrmf.psm.logbook.model.base.reference.Catches;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 27, 2015
 */
@Named @Singleton
@Transactional(propagation=Propagation.NESTED, isolation=Isolation.READ_UNCOMMITTED)
public class LogsheetEventCatchesServices extends BasicServices {
	@Inject private EventsRepository events;
	@Inject private CatchesRepository catchez;
		
	public Catches reload(Catches catches) {
		return catchez.refresh(catches);
	}
	
	public Catches delete(Event parentEvent, Catches catches) {
		if(parentEvent != null && catches != null) {
			if(catches.getId() == null) {
				catchez.remove(catches);
			} else {
				((CatchesRecord)parentEvent).getCatches().remove(catches);
				
				events.update(parentEvent);
			}
		}
		
		return catches;
	}
	
	public Catches update(Event parentEvent, Catches catches) {
		if(parentEvent != null && catches != null) { 
			if(catches.getId() == null) {
				events.update(parentEvent);
			} else {
				catchez.update(catches);
			}
		}
		
		return catches;
	}
}