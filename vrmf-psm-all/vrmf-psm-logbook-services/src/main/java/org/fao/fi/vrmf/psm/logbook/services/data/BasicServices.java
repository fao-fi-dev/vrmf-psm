/**
 * (c) 2015 FAO / UN (project: vrmf-psm-logbook-services)
 */
package org.fao.fi.vrmf.psm.logbook.services.data;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.psm.logbook.dao.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Dec 15, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Dec 15, 2015
 */
@Named @Singleton
@Transactional(propagation=Propagation.NESTED, isolation=Isolation.READ_UNCOMMITTED)
public class BasicServices {
	@Inject protected @Named("repository") Repository genericRepository;
}
