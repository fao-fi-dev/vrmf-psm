<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:ns2="http://vessels.model.ce.psm.vrmf.fi.fao.org" 
				xmlns:ns3="http://reference.model.ce.psm.vrmf.fi.fao.org" 
				xmlns:ns4="http://logbook.model.ce.psm.vrmf.fi.fao.org" >
				
	<xsl:output method="xml" 
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" 
				doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" indent="yes"
				media-type="text/html"/>
				
	<xsl:template match="/ns4:logbook">
		<html>
			<head>
				<title>Logsheet details for logsheet #<xsl:value-of select="//ns4:logsheet/@id"/></title>
				<style type="text/css">
					@page { 
						margin: 1in; 
					}
				
					body {
						background-color: white;
						font-family: sans-serif;
						font-size: 70%;
					}
					
					h1, h2, h3, h4 {
						width: 100%;
						border-bottom: 1px solid;
					}
					
					h4 {
						margin-bottom: .2em;
					}
					
					header, footer {
						color: silver;
					}
					
					header {
						margin-bottom: 4em;
					}
					
					footer {
						margin-top: 4em;
					}
					
					table {
						width: 100%;
						margin-top: .2em;
						border-spacing: 10px;
						border-collapse: collapse;
						border: 1px solid;
					}
					
					table thead,
					table tfoot {
						background-color: silver;
					}
					
					table thead {
						border-bottom: 1px solid;
					}
					
					table tbody tr {
						border-bottom: 1px dotted;
					}
					
					table tfoot {
						border-top: 1px solid;
					}
					
					table thead tr th,
					table tfoot tr th {
						text-align: left;
					}
					
					th, td {
						padding: 2px 5px;
					}
					
					.footer {
						border-top: 1px dotted;
						margin-top: 2em;
						padding-top: 3em;
					}
					
					.label {
						display: inline-block;
						margin-bottom: 1em;
					}
					
					.notes {
						border: 1px solid;
						height: 128px;
					}
					
					.code {
						font-family: monospace;
						font-size: 120%;
					}
					
					.coordinates {
						font-family: monospace;
						color: gray;
					}
					
					.datetime {
						font-family: monospace;
					}
					
					.name {
						font-variant:small-caps;
					}
					
					.nested {
						margin-left: 1em;
						margin-right: 1em;
					}
					
					.nested table {
						font-size: 80%;
					}
					
					ul {
						border-bottom: 1px dotted;
					}
					
					ul, li {
						padding-left: 0 !important;
						margin-left: 0 !important;
						list-style: none;
						list-style-position: outside;
					}

					li.event {
						border-right: 1px solid;
						border-left: 1px solid;
						border-bottom: 1px solid;
						
						padding-bottom: .5em;
						margin-bottom: .5em;
					}
					
					li.event div {
						padding-left: .5em;
					}
					
					li.event div.type {
						background-color: silver;
						border-top: 1px solid;
						border-bottom: 1px solid;
					}
					
					li.effort, li.catch, li.discard {
						font-size: 80%;
						list-style: decimal-leading-zero;
						margin-bottom: .5em;
						margin-left: 2em !important;
					}
				</style>
			</head>
			<body>
				<header>
					<h2>
						[ INSERT HEADER HERE ] 
					</h2>
				</header>
				<h1>Logsheet details</h1>
				<div class="logbookData">
					<h2>Parent logbook:</h2>
					<div class="nested">
						<div><b>Version:</b> <span class="code"><xsl:value-of select="@version"/></span></div>
						<div><b>Year:</b> <span><xsl:value-of select="@year"/></span></div>
						<div><b>Creation date:</b><span class="datetime"><xsl:value-of select="@creationDate"/></span></div>
					</div>
				</div>
				
				<xsl:apply-templates select="//ns4:logsheet"></xsl:apply-templates>
				
				<div class="footer">
					<div>
						<b class="label">Date:</b> <span>___________________&#160;&#160;&#160;&#160;</span>
						<b class="label">Signature:</b> <span>_________________________________________________________</span>
					</div>
					<div><b class="label">Notes:</b></div>
					<div class="notes"></div>
				</div>
				
				<footer>
					<h2>
						[ INSERT FOOTER HERE ] 
					</h2>
				</footer>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="ns4:logsheet">
		<xsl:apply-templates select="ns4:vesselData"></xsl:apply-templates>
		<h2>Trip details:</h2>
		<div class="trip">
			<div class="nested">
				<div><b>Captain:</b> <span class="name"><xsl:value-of select="ns4:captainName"/></span></div>
				<div><b>Crew size:</b> <span class="quantity"><xsl:value-of select="ns4:crewSize"/></span></div>
			</div>
			<h3>Departure:</h3>
			<div class="nested">
				<div><b>Date:</b> <span class="datetime"><xsl:value-of select="ns4:departureDate"/></span></div>
				<div><b>Port:</b> 
					<span class="name"><xsl:value-of select="ns4:departurePort/ns3:name"/></span>
					<span>(
						<span class="code"><xsl:value-of select="ns4:departurePort/ns3:country/ns3:iso2Code"/></span> - 
						<span class="code"><xsl:value-of select="ns4:departurePort/ns3:country/ns3:name"/></span>
					)</span>
					<span>[ <span class="coordinates"><xsl:value-of disable-output-escaping="yes" select="ns4:departurePort/ns3:coordinates"/></span> ]</span>
				</div>			
				<div><b>Fish onboard:</b> <span class="quantity"><xsl:value-of select="ns4:fishOnboardAtStart"/> (<span class="code"><xsl:value-of select="ns4:fishOnboardQuantityAtStart"/></span>)</span></div>
			</div>
			<h3>Arrival:</h3>
			<div class="nested">
				<div><b>Date:</b> <span class="datetime"><xsl:value-of select="ns4:arrivalDate"/></span></div>
				<div><b>Port:</b> 
					<span class="name"><xsl:value-of select="ns4:unloadingPort/ns3:name"/></span>
					<span>(
						<span class="code"><xsl:value-of select="ns4:unloadingPort/ns3:country/ns3:iso2Code"/></span> - 
						<span class="code"><xsl:value-of select="ns4:unloadingPort/ns3:country/ns3:name"/></span>
					)</span>
					<span>[ <span class="coordinates"><xsl:value-of disable-output-escaping="yes" select="ns4:unloadingPort/ns3:coordinates"/></span> ]</span>
				</div>			
				<div><b>Fish onboard:</b> <span class="quantity"><xsl:value-of select="ns4:fishOnboardAfterUnloading"/> (<span class="code"><xsl:value-of select="ns4:fishOnboardQuantityAfterUnloading"/></span>)</span></div>
			</div>
			
			<xsl:if test="ns4:events/* != ''">
			<h3 style="page-break-before:always">Events: [ <xsl:value-of select="count(ns4:events/ns4:event)"/> ]</h3>
				<xsl:apply-templates select="ns4:events"></xsl:apply-templates>
			</xsl:if>
			<xsl:if test="ns4:events/* != ''">
			<h3 style="page-break-before:always">Overall indicators:</h3>
			</xsl:if>
			<xsl:if test="ns4:uniqueCatches/* != ''">
				<xsl:apply-templates select="ns4:uniqueCatches"></xsl:apply-templates>
			</xsl:if>
			<xsl:if test="ns4:uniqueDiscards/* != ''">
				<xsl:apply-templates select="ns4:uniqueDiscards"></xsl:apply-templates>
			</xsl:if>
			<xsl:if test="ns4:uniqueEfforts/* != ''">
				<xsl:apply-templates select="ns4:uniqueEfforts"></xsl:apply-templates>
			</xsl:if>
			<xsl:if test="ns4:derivedEfforts/* != ''">
				<xsl:apply-templates select="ns4:derivedEfforts"></xsl:apply-templates>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template match="ns4:vesselData">
		<div class="vesselData">
			<h2>Vessel data:</h2>
				<div class="nested">
				<div><b>Name:</b> <span class="name"><xsl:value-of select="ns2:name"/></span></div>
				<div><b>Type:</b> <span class="code"><xsl:value-of select="@type"/></span></div>
				<div><b>Reg. No.:</b> <span class="code"><xsl:value-of select="ns2:registrationNumber"/></span> (<span class="code"><xsl:value-of select="ns2:registrationCountry/ns3:iso2Code"/></span> - <span class="code"><xsl:value-of select="ns2:registrationCountry/ns3:name"/></span>)</div>
				<div><b>IRCS:</b> <span class="code"><xsl:value-of select="ns2:ircs"/></span></div>
				<xsl:if test="ns2:imo != ''">
				<div><b>IMO:</b> <span class="code"><xsl:value-of select="ns2:imo"/></span></div>
				</xsl:if>
				<xsl:if test="ns2:mmsi != ''">
				<div><b>MMSI:</b> <span class="code"><xsl:value-of select="ns2:mmsi"/></span></div>
				</xsl:if>
				<xsl:if test="ns2:licenseNumber != ''">
				<div><b>Fishing license:</b> <span class="code"><xsl:value-of select="ns2:licenseNumber"/></span></div>
				</xsl:if>
				<div><b>Fishing company:</b> <span class="name"><xsl:value-of select="ns2:fishingCompany"/></span></div>
				<h4>Dimensions:</h4>
				<div><b>LOA:</b> <span class="quantity"><xsl:value-of select="ns2:loa"/></span> meters</div>
				<xsl:if test="ns2:lbp != '0.0'">
				<div><b>LBP:</b> <span class="quantity"><xsl:value-of select="ns2:lbp"/></span> meters</div>
				</xsl:if>
				<div><b>GT:</b> <span class="quantity"><xsl:value-of select="ns2:gt"/></span> metric tons</div>
				<xsl:if test="ns2:grt != '0.0'">
				<div><b>GRT:</b> <span class="quantity"><xsl:value-of select="ns2:grt"/></span> metric tons</div>
				</xsl:if>
				<div><b>Main engine:</b> <span class="quantity"><xsl:value-of select="ns2:mainEnginePower"/></span> <span class="code"><xsl:value-of select="ns2:mainEnginePowerUnit"/></span></div>
				<xsl:if test="count(ns2:identifiers/ns2:identifier) >0">
				<h4>Identifiers:</h4>
				<xsl:apply-templates select="ns2:identifiers"></xsl:apply-templates>
				</xsl:if>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="ns2:identifiers">
		<ul>
		<xsl:for-each select="ns2:identifier">
			<li>
				<b><span class="code"><xsl:value-of select="@type"/></span>:</b>
				<span class="code"><xsl:value-of select="@identifier"/></span>
			</li>
		</xsl:for-each>
		</ul>
	</xsl:template>
	<xsl:template match="ns4:events">
		<div class="nested">
			<ul class="events">
			<xsl:for-each select="ns4:event">
				<li class="event">
					<div class="type"><b><span class="code"><xsl:value-of select="@type"/></span></b></div>
					<div><b>From:</b> <span class="datetime"><xsl:value-of select="startDate"/></span> [ <span class="coordinates"><xsl:value-of select="coordinatesAtStart"/></span> ]</div>
					<div><b>To:</b> <span class="datetime"><xsl:value-of select="endDate"/></span> [ <span class="coordinates"><xsl:value-of select="coordinatesAtEnd"/></span> ]</div>
					<xsl:if test="comments != ''">
					<div><span><b>Comments:</b> <i><xsl:value-of select="comments"/></i></span></div>
					</xsl:if>
					<xsl:if test="@type='TRANSSHIPPING'">
						<div class="nested transshipments">
							<h4>Transhipment details:</h4>
							<div><b>Receiving vessel name:</b> <span class="name"><xsl:value-of select="vesselName"/></span></div>
							<div><b>Receiving vessel reg. no.:</b> <span class="code"><xsl:value-of select="vesselRegistrationNumber"/></span> (<span class="code"><xsl:value-of select="vesselRegistrationCountry/ns3:iso2Code"/></span> - <span class="code"><xsl:value-of select="vesselRegistrationCountry/ns3:name"/></span>)</div>
							<div><b>Destination:</b> <span><xsl:value-of select="destination"/></span></div>
							<xsl:if test="catches/catch/* != ''">
							<h4>Transhipped catches: [ <xsl:value-of select="count(catches/catch)"/> ]</h4>
								<table>
									<thead>
										<tr>
											<th>3-alpha-code</th>
											<th>Name</th>
											<th>Scientific name</th>
											<th>Quantity</th>
											<th>Unit</th>
										</tr>
									</thead>
									<tbody>
										<xsl:for-each select="catches/catch">
											<tr>
												<td><span class="code"><xsl:value-of select="ns3:species/@alpha3Code"/></span></td>
												<td><span class="name"><xsl:value-of select="ns3:species/ns3:localName"/></span></td>
												<td><span class="name"><i><xsl:value-of select="ns3:species/ns3:scientificName"/></i></span></td>
												<td><span class="quantity"><xsl:value-of select="ns3:quantity"/></span></td>
												<td><span class="code"><xsl:value-of select="ns3:quantityType"/></span></td>
											</tr>
										</xsl:for-each>
									</tbody>
								</table>
							</xsl:if>
						</div>
					</xsl:if>
					<xsl:if test="@type != 'TRANSSHIPPING'">
						<xsl:if test="efforts/effort/* != ''">
						<div class="nested efforts">
							<h4>Recorded efforts: [ <xsl:value-of select="count(efforts/effort)"/> ]</h4>
							<table>
								<thead>
									<tr>
										<th>Type</th>
										<th>Value</th>
									</tr>
								</thead>
								<tbody>
									<xsl:for-each select="efforts/effort">
										<tr>
											<td><span class="code"><xsl:value-of select="ns3:effortType"/></span></td>
											<td><span class="quantity"><xsl:value-of select="ns3:effortValue"/></span></td>
										</tr>
									</xsl:for-each>
								</tbody>
							</table>
						</div>
						</xsl:if>
						<xsl:if test="catches/catch/* != ''">
						<div class="nested catches">
							<h4>Recorded catches: [ <xsl:value-of select="count(catches/catch)"/> ]</h4>
							<table>
								<thead>
									<tr>
										<th>3-alpha-code</th>
										<th>Name</th>
										<th>Scientific name</th>
										<th>Quantity</th>
										<th>Unit</th>
									</tr>
								</thead>
								<tbody>
									<xsl:for-each select="catches/catch">
										<tr>
											<td><span class="code"><xsl:value-of select="ns3:species/@alpha3Code"/></span></td>
											<td><span class="name"><xsl:value-of select="ns3:species/ns3:localName"/></span></td>
											<td><span class="name"><i><xsl:value-of select="ns3:species/ns3:scientificName"/></i></span></td>
											<td><span class="quantity"><xsl:value-of select="ns3:quantity"/></span></td>
											<td><span class="code"><xsl:value-of select="ns3:quantityType"/></span></td>
										</tr>
									</xsl:for-each>
								</tbody>
							</table>
						</div>
						</xsl:if>
						<xsl:if test="discards/discard/* != ''">
						<div class="nested discards">
							<h4>Recorded discards: [ <xsl:value-of select="count(discards/discard)"/> ]</h4>
							<table>
								<thead>
									<tr>
										<th>3-alpha-code</th>
										<th>Name</th>
										<th>Scientific name</th>
										<th>Quantity</th>
										<th>Unit</th>
										<th>Reason</th>
									</tr>
								</thead>
								<tbody>
									<xsl:for-each select="discards/discard">
									<tr>
										<td><span class="code"><xsl:value-of select="ns3:species/@alpha3Code"/></span></td>
										<td><span class="name"><xsl:value-of select="ns3:species/ns3:localName"/></span></td>
										<td><span class="name"><i><xsl:value-of select="ns3:species/ns3:scientificName"/></i></span></td>
										<td><span class="quantity"><xsl:value-of select="ns3:quantity"/></span></td>
										<td><span class="code"><xsl:value-of select="ns3:quantityType"/></span></td>
										<td><span class="code"><xsl:value-of select="ns3:reason"/></span></td>
									</tr>
									</xsl:for-each>
								</tbody>
							</table>
						</div>
						</xsl:if>
					</xsl:if>
				</li>
			</xsl:for-each>
			</ul>
		</div>
	</xsl:template>
	<xsl:template match="ns4:uniqueCatches">
		<div class="nested unique catches">
			<h4>Catches: [ <xsl:value-of select="count(ns4:catches)"/> ]</h4>
			<table>
				<thead>
					<tr>
						<th>3-alpha-code</th>
						<th>Name</th>
						<th>Scientific name</th>
						<th>Quantity</th>
						<th>Unit</th>
					</tr>
				</thead>
				<tbody>
					<xsl:for-each select="ns4:catches">
						<tr>
							<td><span class="code"><xsl:value-of select="ns3:species/@alpha3Code"/></span></td>
							<td><span class="name"><xsl:value-of select="ns3:species/ns3:localName"/></span></td>
							<td><span class="name"><i><xsl:value-of select="ns3:species/ns3:scientificName"/></i></span></td>
							<td><span class="quantity"><xsl:value-of select="ns3:quantity"/></span></td>
							<td><span class="code"><xsl:value-of select="ns3:quantityType"/></span></td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</div>
	</xsl:template>
	<xsl:template match="ns4:uniqueDiscards">
		<div class="nested unique discards">
			<h4>Discards: [ <xsl:value-of select="count(ns4:discard)"/> ]</h4>
			<table>
				<thead>
					<tr>
						<th>3-alpha-code</th>
						<th>Name</th>
						<th>Scientific name</th>
						<th>Quantity</th>
						<th>Unit</th>
					</tr>
				</thead>
				<tbody>
					<xsl:for-each select="ns4:discard">
						<tr>
							<td><span class="code"><xsl:value-of select="ns3:species/@alpha3Code"/></span></td>
							<td><span class="name"><xsl:value-of select="ns3:species/ns3:localName"/></span></td>
							<td><span class="name"><i><xsl:value-of select="ns3:species/ns3:scientificName"/></i></span></td>
							<td><span class="quantity"><xsl:value-of select="ns3:quantity"/></span></td>
							<td><span class="code"><xsl:value-of select="ns3:quantityType"/></span></td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</div>
	</xsl:template>
	<xsl:template match="ns4:uniqueEfforts">
		<div class="nested unique efforts">
			<h4>Efforts: [ <xsl:value-of select="count(ns4:effort)"/> ]</h4>
			<table>
				<thead>
					<tr>
						<th>Type</th>
						<th>Value</th>
					</tr>
				</thead>
				<tbody>
					<xsl:for-each select="ns4:effort">
						<tr>
							<td><span class="code"><xsl:value-of select="ns3:effortType"/></span></td>
							<td><span class="quantity"><xsl:value-of select="ns3:effortValue"/></span></td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</div>
	</xsl:template>
	<xsl:template match="ns4:derivedEfforts">
		<div class="nested unique derived efforts">
			<h4>Derived efforts: [ <xsl:value-of select="count(ns4:derivedEffort)"/> ]</h4>
			<table>
				<thead>
					<tr>
						<th>Type</th>
						<th>Value</th>
					</tr>
				</thead>
				<tbody>
					<xsl:for-each select="ns4:derivedEffort">
						<tr>
							<td><span class="code"><xsl:value-of select="ns3:effortType"/></span></td>
							<td><span class="quantity"><xsl:value-of select="ns3:effortValue"/></span></td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>